<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', function () {
    return Redirect::to('login');
});

Auth::routes();

/*busquedas*/
Route::get('/home', 'inicioController@index')->name('home');
Route::get('/anuncios/mostrar/dos', 'anuncioController@resultadoBusquedas')->name('anuncio.buscar');
Route::get('/home/show/{producto}', 'inicioController@show')->name('home.show');
/**/

/*inicio informativo */
route::get('/quienes-somos', function () {
    return view('inicio-thitonix.quienes-somos');
})->name('quienes-somos');

route::get('/publicar-anuncio', function () {
    return view('inicio-thitonix.publicar-anuncio');
})->name('publicar-anuncio');

route::get('/mis-misiones', function () {
    return view('inicio-thitonix.publicar-mision');
})->name('mis-misiones');

route::get('/medios-pago', function () {
    return view('inicio-thitonix.medios-de-pago');
})->name('medios-pago');

route::get('/blog', function () {
    return Redirect::to('login');
})->name('blog');

route::get('/servicio-cliente', function () {
    return view('inicio-thitonix.servicio-al-cliente');
})->name('servicio-cliente');

route::get('/politica-privacidad', function () {
    return view('inicio-thitonix.legal.politica-privacidad');
})->name('politica-privacidad');


route::get('/terminos', function () {
    return view('inicio-thitonix.legal.terminos');
})->name('terminos');


route::get('/legal-thitonix', function () {
    return view('inicio-thitonix.legal.legal-thitonix');
})->name('legal-thitonix');
/**/


//notificaciones
Route::get('notificaciones', 'NotificacionController@verificar')->name('notificaciones');

Route::post('users/create/pago', 'ProcesarPagosController@actualizarUsuariorPago');

Route::get('pago/pruebas', 'Soap\ConectSoapController@statusPayment');
route::get('/respuesta', function () {

    return redirect('login')->with('message', ['success', 'Se envio la activacion de cuenta a su correo. ']);

});



/*Processo automatico por cron*/
Route::get('process/pagos', 'ProcesarPagosController@procesarPagosCoing');



/*registrar cliente*/
Route::get('activacion/{code}', 'Auth\UserController@activate');
Route::post('complete/{id}', 'Auth\UserController@complete');
/*--registrar cliente-- */


/*password*/
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');

/*--password--*/


/*ver factura*/
Route::get('descargar/pago/factura/{id_pago}', 'PanelUsuarioController@descargarFactura')->name('descargar.factura');
/*--ver factura--*/


/* metronic dashboard admin*/
Route::get('/admin', function () {
    return view('admin.auth.login');
});
//acceso como administrador
Route::post('/login-adm', 'Admin\Auth\LoginController@acceder');

/**** metronic dashboard admin****/
/* Persmisos para misionero*/
Route::get('sistema/redes-sociales', 'Admin\Configuracion\Sistema\ConfSistemaController@redSociales')->name('admin.conf-sistena.social');

Route::group(['middleware' => ['role:misionero']],function () {

    Route::get('/perfil/{usuario}/index', 'HomeController@perfil')->name('perfil.index');
    Route::get('/perfil/{usuario}/editar', 'HomeController@perfilEditar')->name('perfil.editar');
    Route::post('/perfil/actualizar', 'HomeController@perfilActualizar')->name('perfil.actualizar');
    Route::get('/cambio', 'HomeController@autorizarCambioPw')->name('perfil.cambiopw');
    Route::post('/cambio/proceso', 'HomeController@realizarCambioPw')->name('perfil.cambiopwcambio');
    Route::resource('roles/create', 'RoleController@subirfile');
    Route::get('/medio-pago', 'HomeController@verMedioPago')->name('medio.pago');

        //propuesta

    Route::post('/home/proponer', 'propuestaController@realizarPropuesta')->name('realizar.propuesta');

    Route::get('propuestas/{product}', 'propuestaController@verPropuestas')->name('propuestas.show');

    Route::get('propuestas/ver/{product}', 'propuestaController@verPropuesta');
    Route::post('enviar/mensaje', 'propuestaController@mensajes');

    //Products

    Route::get('products/all/buscar', 'inicioController@getProductos')->name('home.search');


    //anuncios
    Route::get('anuncios', 'anuncioController@index')->name('anuncio.index');

    Route::get('/anuncios/create', 'anuncioController@listaCategoria')->name('anuncio.anuncio');

    Route::post('/anuncios/create', 'anuncioController@cargarAnuncio')->name('anuncio.crearAnuncio');


    //misiones
    Route::get('/misiones', 'misionesController@index')->name('misiones.index');
    Route::get('/misiones/pago', 'misionesController@indexMision')->name('misiones.indexMision');
    Route::get('/misiones/ssp/index', 'misionesController@sspIndex')->name('misiones.indexssp');
    Route::get('/misiones/envia/{id}', 'misionesController@envia')->name('misiones.envia');
    Route::get('/misiones/recibe/{id}', 'misionesController@recibe')->name('misiones.recibe');
    Route::get('/misiones/view/{id}', 'misionesController@view')->name('misiones.view');

    //panel menu

    Route::post('products/store', 'ProductController@store')->name('products.store');

    Route::get('/homeadmin', 'ProductController@indexadmin')->name('homeadmin');

    Route::get('anuncios/edit/{productoid}', 'ProductController@Edit');

    Route::put('products/{product}', 'ProductController@update')->name('products.update');

    Route::delete('products/{product}', 'ProductController@destroy')->name('products.destroy');

    Route::get('products/{product}/edit', 'ProductController@edit')->name('products.edit');

    Route::get('consulta/mensajes/{product}/{user}/{status}', 'ProductController@historialMensajes')->name('historialMensajes');
    Route::post('/anuncios/eliminar/img', 'ProductController@eliminarPreImagenes')->name('anuncio.eliminar.img');
    Route::get('/anuncios/depurar/img', 'ProductController@depurarPreImagenes')->name('anuncio.depurar.img');

    Route::get('/anuncios/mostrar/img', 'ProductController@verPreImagenes')->name('anuncio.mostrar.img');

    Route::get('/propuesta/mostrar/img', 'ProductController@verPreImagenesPropuesta')->name('propuesta.mostrar.img');

    Route::post('/anuncios/cargar/img', 'ProductController@cargarPreImagenes')->name('anuncio.cargar.img');


    //proceso de pago

    //Route::post('pago/enviar/', 'ProductController@getConnectSoap')->name('actualizar.propuesta');



    Route::get('pago/procesar', 'ProcesarPagosController@procesarPago')->name('pago.procesar');
    Route::get('medios/pago', 'ProcesarPagosController@medios')->name('medios.pago');
    Route::post('pago/add', 'ProcesarPagosController@addPagoParameter')->name('pagos.add');
    //Route::post('pago/depositar', 'ProcesarPagosController@depositarPago')->name('pagos.depositar')->middleware('permission:api.factura');
    //Route::get('pago/respuesta', 'ProductController@respuestaPago')->name('pago.respuesta');

    //actualizar usaurio para pago
    Route::post('/pago/statusInterno/{id_producto}/{id_proveedor}', 'ProcesarPagosController@estadoPagoInterno');
    //ver estado
    Route::get('api.status/pago/usu', 'PanelUsuarioController@mostrarTablaUsuPago')->name('api.panelusuPago');


    Route::post('pago/depositar-coing', 'ProcesarPagosController@depositarPagoCoing')->name('pagos.depositar.coing');


     //rutas de panel usuario

    Route::get('panelusu', 'PanelUsuarioController@tabla')->name('products.tablausuario');
    Route::get('api.panelusu', 'PanelUsuarioController@mostrarTablausu')->name('api.panelusu');
    Route::get('editusu/{id}/', ['as' => 'panelusu', 'uses' => 'PanelUsuarioController@edit'])->name('panelusu.edit');
    Route::get('deleteusu/{id}/', ['as' => 'panelusu', 'uses' => 'PanelUsuarioController@delete'])->name('panelusu.delete');
    Route::put('panelusu/{id}', 'PanelUsuarioController@update')->name('panelusu.update');
    Route::get('api.propuestas', 'PanelUsuarioController@tablaUsuPropuesta')->name('api.propuestas');

    Route::get('editar/propuesta/{id_producto}/{id_usuario}', 'propuestaController@verEditarPropuesta');
    Route::post('editar/propuesta/', 'propuestaController@actualizarPropuesta')->name('actualizar.propuesta');

    //rutas de estado entrega

    Route::post('cancelar/mision', 'EstadoEntregaController@cancelar')->name('cancelar.mision');

    Route::post('enviar/mensaje/cancelar', 'EstadoEntregaController@mensajes');
    Route::get('status/finalizado', 'EstadoEntregaController@estadoFinalizado');

    //finalizar mision
    Route::post('finalizar/mision', 'EstadoEntregaController@finalizar')->name('finalizar.mision');

    //calificaciones
    Route::get('status/calificacion/{id_usuario}', 'CalificacionUsuarioController@estadoCalificacion')->name('status.calificacion');

     //panel entrega
    Route::get('entregas/usu', 'PanelUsuarioController@entregasUsu')->name('entregas.panelusu.usu');


});

    /* Persmisos para administrador*/
Route::group(['middleware' => ['role:operador|develop|administrador']],function () {
        //Roles
    Route::post('roles/store', 'RoleController@store')->name('roles.store')
        ->middleware('permission:roles.create');

    Route::get('roles', 'RoleController@index')->name('roles.index')
        ->middleware('permission:roles.index');

    Route::get('roles/create', 'RoleController@create')->name('roles.create')
        ->middleware('permission:roles.create');

    Route::put('roles/{role}', 'RoleController@update')->name('roles.update')
        ->middleware('permission:roles.edit');

    Route::get('roles/{role}', 'RoleController@show')->name('roles.show')
        ->middleware('permission:roles.show');

    Route::delete('roles/{role}', 'RoleController@destroy')->name('roles.destroy')
        ->middleware('permission:roles.destroy');

    Route::get('roles/{role}/edit', 'RoleController@edit')->name('roles.edit')
        ->middleware('permission:roles.edit');
    //Users
    Route::get('users', 'UserController@index')->name('users.index')
        ->middleware('permission:users.index');

    Route::put('users/{user}', 'UserController@update')->name('users.update')
        ->middleware('permission:users.edit');

    Route::get('users/{user}', 'UserController@show')->name('users.show')
        ->middleware('permission:users.show');

    Route::delete('users/{user}', 'UserController@destroy')->name('users.destroy')
        ->middleware('permission:users.destroy');

    Route::get('users/{user}/edit', 'UserController@edit')->name('users.edit')
        ->middleware('permission:users.edit');


    //rutas de panel admin

    Route::get('paneladm', 'paneladmincontroller@tabla')->name('products.tabla');

    Route::get('api.paneladm', 'paneladmincontroller@mostrarTabla')->name('api.paneladm');

    Route::get('aprovar/{id}/{id_usuario}', ['as' => 'aprovar', 'uses' => 'paneladmincontroller@aprovacion'])->name('products.aprovacion');

    Route::get('rechazar/{id}/{id_usuario}', 'paneladmincontroller@rechazar')->name('products.rechazar');

    Route::get('paneladm/verificar/{id}/{id_usuario}/ver', 'paneladmincontroller@posVerificar')->name('products.posVerificar');

    Route::get('paneladmmodal/{id}', 'paneladmincontroller@mostrarModal')->name('products.mostrarModal');

    Route::post('verificar', 'paneladmincontroller@verificar')->name('products.verificar');

    Route::get('api.status/pago/adm', 'paneladmincontroller@mostrarTablaAdmPago')->name('api.paneladmPago');
    Route::post('consultar/medio/pago', 'paneladmincontroller@mostrarMedioPago')->name('api.mostrarMedioPago');

    Route::get('api.cancelados', 'paneladmincontroller@mostrarTablaCancelados')->name('api.cancelados');

    Route::get('cancelado/final/{id_producto}/{status}', 'paneladmincontroller@cancelarFinal')->name('cancelado.final');

    //panel entrega
    Route::get('entregas/admin', 'paneladmincontroller@entregasAdm')->name('entregas.panelusu.admin');


    Route::get('pago/aprovado/{id_pago}', 'ProcesarPagosController@aprovarPago')->name('pago.aprovar');

    /* metronic dashboard admin*/


    Route::get('/admin/home', 'Admin\HomeController@index')->name('admin.home.index');

    Route::get('/admin/home/dashboard', 'Admin\HomeController@dashboard')->name('admin.home.dashboard');

    Route::get('/admin/notificacion-status', 'Admin\HomeController@notificationStatus')->name('admin.home.notificacion-status');

    Route::get('/admin/template', function () {
        return view('admin.index');
    })->name('dashboard.template');

    //productos y misiones

    Route::get('/admin/general/productos-misiones', 'Admin\Administrador\General\ProductosMisionesController@index')->name('admin.productos.misiones');


    Route::get('table-productos-misiones', 'Admin\Administrador\General\ProductosMisionesController@tableView')->name('table.productos.misiones');

    Route::get('table-productos-misiones/{id}', 'Admin\Administrador\General\ProductosMisionesController@view')->name('table.productos.misiones.id');

    Route::get('table-productos-misiones/aprovar/{id}', 'Admin\Administrador\General\ProductosMisionesController@aprovar')->name('table.productos.misiones.aprovar');

    Route::get('table-productos-misiones/rechazar/{id}/{id_usuario}', 'Admin\Administrador\General\ProductosMisionesController@rechazar')->name('table.productos.misiones.rechazar');

    Route::post('table-productos-misiones/editar', 'Admin\Administrador\General\ProductosMisionesController@editar')->name('table.productos.misiones.editar');


    // pagos por pasarela
    Route::get('/admin/general/pagos', 'Admin\Administrador\General\PagosController@pagos')->name('admin.pagos');

    // pagos internos

    Route::get('table-pagos', 'Admin\Administrador\General\PagosController@tableView')->name('table.pagos');


    Route::get('table-pagos/view/{id}', 'Admin\Administrador\General\PagosController@View')->name('table.pagos.view');

    Route::get('table-pagos/deposito/{id}/{status}', 'Admin\Administrador\General\PagosController@depositoProceso')->name('deposito.proceso');

    Route::get('/admin/general/descargar/factura/{id}', 'Admin\Administrador\General\PagosController@descargarFactura')->name('descargar.factura');
    //entregas admin
    Route::get('/admin/general/entregas', 'Admin\Administrador\General\EntregasController@index')->name('admin.entregas');

    Route::get('table-entregas', 'Admin\Administrador\General\EntregasController@tableView')->name('table.entregas');

    Route::get('table-entregas/view/{id}', 'Admin\Administrador\General\EntregasController@View')->name('table.entregas.view');

    Route::get('table-entregas/final/{id_producto}/{status}', 'Admin\Administrador\General\EntregasController@cancelarFinal')->name('table.entregas.final');

    //lista de admin

    Route::get('/admin/configuracion/usuarios/admin', 'Admin\Configuracion\Usuarios\ListaAdminController@index')->name('admin.conf.admin');


    Route::get('table-admin', 'Admin\Configuracion\Usuarios\ListaAdminController@tableView')->name('admin.table.admin');

    Route::get('table-admin/view/{id}', 'Admin\Configuracion\Usuarios\ListaAdminController@view')->name('admin.table.view');

    Route::get('table-admin/bloquear/{id}', 'Admin\Configuracion\Usuarios\ListaAdminController@bloquear')->name('admin.table.bloquear');

    Route::get('table-admin/activar/{id}', 'Admin\Configuracion\Usuarios\ListaAdminController@activar')->name('admin.table.activar');

    Route::post('table-admin/crear', 'Admin\Configuracion\Usuarios\ListaAdminController@crear')->name('admin.table.activar');

    Route::get('table-admin/editar/{id}', 'Admin\Configuracion\Usuarios\ListaAdminController@editar')->name('admin.table.editar');

    Route::post('table-admin/actualizar', 'Admin\Configuracion\Usuarios\ListaAdminController@actualizar')->name('admin.table.actualizar');


    Route::get('/admin/configuracion/usuarios/clientes', 'Admin\Configuracion\Usuarios\ListaClienteController@index')->name('admin.conf.clientes');


    Route::get('table-clientes', 'Admin\Configuracion\Usuarios\ListaClienteController@tableView')->name('admin.table.cliente');

    Route::get('table-clientes/view/{id}', 'Admin\Configuracion\Usuarios\ListaClienteController@view')->name('admin.table.view');

    Route::get('table-clientes/bloquear/{id}', 'Admin\Configuracion\Usuarios\ListaClienteController@bloquear')->name('admin.table.bloquear');

    Route::get('table-clientes/activar/{id}', 'Admin\Configuracion\Usuarios\ListaClienteController@activar')->name('admin.table.activar');

    //configuracion del sistema

    Route::get('configuracion-sistema', 'Admin\Configuracion\Sistema\ConfSistemaController@index')->name('admin.conf-sistena.index');

    Route::get('configuracion-sistema/list', 'Admin\Configuracion\Sistema\ConfSistemaController@list')->name('admin.conf-sistena.list');

    Route::post('configuracion-sistema/update', 'Admin\Configuracion\Sistema\ConfSistemaController@update')->name('admin.conf-sistena.update');

    Route::get('configuracion-sistema/test-mail', 'Admin\Configuracion\Sistema\ConfSistemaController@testMail')->name('admin.conf-sistena.testMail');



    // Perfil administrador
    Route::get('/admin/mi-perfil', 'Admin\MiPerfil\IndexController@index')->name('admin.mi-perfil.index');

    Route::get('/admin/mi-perfil/list', 'Admin\MiPerfil\IndexController@list')->name('admin.mi-perfil.list');

    Route::post('/admin/mi-perfil/update', 'Admin\MiPerfil\IndexController@update')->name('admin.mi-perfil.update');


    /*-- metronic dashboard admin--*/
});

