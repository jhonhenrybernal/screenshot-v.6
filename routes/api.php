<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/

Route::post('/pago/status/{idusuario}', 'ProcesarPagosController@estadoPago');
Route::post('/pago/pendiente', 'ProcesarPagosController@verificarEstadoPendiente');
Route::post('/pago/pendienteAll', 'ProcesarPagosController@verificarEstadoPendienteTodos');
Route::post('pago/depositar', 'ProcesarPagosController@depositarPago')->name('pagos.depositar');



/*rutas para pagos*/
//pagos test
Route::get('pago/cambio-estado/{id_producto}', 'TestPagosController@cambioEstado')->name('cambio-estado');
Route::get('pago/estado/{id_producto}/{estado}', 'TestPagosController@estado');
Route::get('pago/restaurar/{id_producto}', 'TestPagosController@restaurar');
Route::get('create/all/product', 'TestPagosController@cargarAllAnuncios');

//cancelaciones
Route::get('cancelar/admin', 'TestPagosController@aprobarCancelarInit');
Route::get('cancelar/cambio/{id_producto}/{status}', 'TestPagosController@adminCancelar');

//calificaciones
Route::post('enviar/calificacion', 'CalificacionUsuarioController@calificacion')->name('enviar.calificacion');
// test coinbase
Route::get('coinbase', 'TestPagosController@coinbaseCommerce');

//procesar coinbase
Route::get('coin/list', 'CoinbaseController@list')->name('coin.list');
Route::get('coin/view/{id_pago}', 'CoinbaseController@view')->name('coin.view');
Route::post('coin/cancel', 'CoinbaseController@cancel')->name('coin.cancel');
Route::post('coin/update', 'CoinbaseController@update')->name('coin.update');
Route::get('coin/webhook', 'CoinbaseController@webhook')->name('coin.webhook');
Route::get('coin/webhook/response', 'CoinbaseController@webhookResponse')->name('coin.webhook');