<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class alertas extends Model
{
    protected $table = 'alertas';
    public $timestamps = false;

    protected $fillable = [ 'id', 'id_stados_anuncios', 'date','id_producto','id_usuario'];

    public function usuario()
    {
    	 return  $this->hasOne(User::class,'id_producto','id','name');
    }
}
