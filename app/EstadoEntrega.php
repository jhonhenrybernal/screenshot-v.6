<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EstadoEntrega extends Model
{
    protected $table = 'estado_entrega';
    public $timestamps = false;

    protected $fillable = ['id', 'id_producto', 'id_usuario_entregas', 'id_usuario','fecha','estado','comentarios' ];

}
