<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\productos;

class PreImg extends Model
{
    protected $table = 'pre_imagenes';
    public $timestamps = false;

    protected $fillable = [
        'id_usuario', 'rutaImagen', 'nombreImagen'	, 
    ];
}
