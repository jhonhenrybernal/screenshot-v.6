<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pagos extends Model
{
    protected $table = 'pago_prd';
    public $timestamps = false;
    protected $fillable = [
        'id',
        'id_proveedor',
        'meta_value',
        'id_producto',
        'status',
        'transactionId',
        'estado_msg',
        'metodo_pago',
        'cliente_ganancia',
        'comision_screen_total',
        'iva',
        'id_propuesta',
        'id_usuario',
        'comision_pasarela',
        'fecha_pago'
    ];
}
