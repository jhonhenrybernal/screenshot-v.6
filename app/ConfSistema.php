<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ConfSistema extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'id',
        'key_coingbase',
        'email_admin',
        'redssocial_facebook',
        'redssocial_twitter',
        'redssocial_instagram'
    ];

}
