<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Facturas extends Model
{
    protected $table = 'facturas';
    public $timestamps = false;

    protected $fillable = [
       'id','id_producto','id_usuario','numero_factura','fecha_generada','estado','nombre_file'
    ];
}
