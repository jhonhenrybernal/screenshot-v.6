<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notificaciones extends Model
{
    protected $table = 'notificaciones';
    public $timestamps = false;

    protected $fillable = [ 'id', 'id_usuario', 'id_producto','id_estado_anuncio','mensaje','visto','visto_adm'];
}
