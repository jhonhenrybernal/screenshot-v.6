<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class categoria extends Model
{
    protected $table = 'categorias';
    public $timestamps = false;

    protected $fillable = [
        'id', 'tipo',  
    ];



public function estado(){

        return  $this->belongsTo(estados::class,'id_estados_anuncios', 'id','estado');
    }

 public function usuario(){

        return  $this->belongsTo(User::class,'id_usuario', 'id','name','apellido');
    }

  public function productos(){

        return  $this->belongsTo(User::class,'id_categoria', 'mision', 'id_imagen'	, 'id_oferta'	, 'id_usuario'	, 'misionDetalladamente');
  } 


   }       
