<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class log extends Model
{
    protected $table = 'log';
    public $timestamps = false;

    protected $fillable = [
        'id_usuario', 'meta_value', 'accion','id_producto','id_propuesta', 
    ];

  }
