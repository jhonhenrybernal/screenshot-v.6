<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\EntidadPago;

class MediosPago extends Model
{
    protected $table = 'medios_pago';
    public $timestamps = false;

    protected $fillable = [
      'id', 'entidad_bancaria_id', 
      'id_usuario',
      'numero_doc',
      'numero_cel',
      'numero_cuenta',
      'Nombre_titular_cuenta_bancaria',
      'tipo_cuenta_bancaria',
      'id_entidad_medio_pago',
      'fecha_actualizado',
      'imagen_qr',
      'codigo_qr',
    ];


    public function entidad_bancaria(){

      return $this->hasOne(EntidadPago::class,'id','entidad_bancaria_id','imagen','entidad');
    }

}
