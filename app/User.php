<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use App\Notifications\ResetPasswordNotification;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\productos;
use App\Roles;
use App\ciudad;
use App\tipoDocumento;
use App\Departamentos;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Contracts\Auth\MustVerifyEmail;

class User extends Authenticatable implements MustVerifyEmail
{



    use Notifiable;
    use HasRoles;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */


    protected $fillable = [
        'name',
        'apellido',
        'tipo_documento',
        'email',
        'password',
        'code',
        'active',
        'celular',
        'avatar',
        'lastname',
        'username',
        'cedula',
        'tipo_role',
        'telefono_local',
        'barrio',
        'pais',
        'id_ciudad',
        'tipo_documento',
        'id_departamento'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];




    public function productos(){

        return  $this->belongsTo(productos::class, 'id_categoria', 'mision', 'id_imagen' , 'id_oferta'   , 'id_usuario'  , 'misionDetalladamente');
    }

     public function categoria(){

        return  $this->belongsTo(categoria::class,'id_categoria', 'id','tipo');
    }

     public function estado(){

        return  $this->belongsTo(estados::class,'id_estados_anuncios', 'id','estado');
    }

     public function mensaje(){

        return  $this->belongsTo(mensajes::class, 'id', 'mensaje ', 'id_usuario', 'id_producto');
    }

     public function ciudades(){

        return  $this->hasOne(ciudad::class,'id','id_ciudad');
    }

     public function departamento(){

        return  $this->hasOne(Departamentos::class,'id','id_departamento');
    }

    public function tipoDocumento(){
        return  $this->hasOne(tipoDocumento::class,'id','tipo_documento');
    }



}
