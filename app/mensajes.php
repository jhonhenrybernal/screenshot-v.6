<?php

namespace App;
use App\categoria;
use App\estados;
use App\productos;
use App\User;
use App\img;

use Illuminate\Database\Eloquent\Model;

class mensajes extends Model
{
    protected $table = 'mensajes';
    public $timestamps = false;

    protected $fillable = [ 'id', 'id_usuario', 'id_producto','mensaje'];


    public function categoria(){
     $oData =    $this->hasOne(productos::class,'id_producto','id');
      
        return  $this->hasOne(categoria::class,$oData->id_categoria, 'id','tipo');
    }

     public function estado(){

        return  $this->belongsTo(estados::class,'id_estado_anuncio', 'id','estado');
    }

     public function usuario(){

        return  $this->belongsTo(User::class,'id_usuario', 'id','name','apellido');
    }


      public function anuncio_misiones(){

        return  $this->belongsTo(anuncio_misiones::class, 'id', 'tipo', 'mensaje');
    }

     public function productos(){

        return  $this->belongsTo(productos::class,'id_producto','id','mision', 'misionDetalladamente','fecha', 'id_estados_anuncios');
    }

     public function imagenendeVarios(){

        return  $this->hasMany(img::class,'id_producto','id','nombreImagen');
    }
}
