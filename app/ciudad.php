<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Departamentos;

class ciudad extends Model
{
    protected $table = 'ciudad';
    public $timestamps = false;

    protected $fillable = [
      'id',  'ciudad','departamento'
    ];

     public function departamento(){

         return  $this->hasOne(Departamentos::class,'id','departamento');
    }
}
