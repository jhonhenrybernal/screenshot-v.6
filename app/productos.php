<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\categoria;
use App\estados;
use App\mensajes;
use App\User;
use App\img;;
use App\EstadoEntrega;
use App\ciudad;
use App\propuestas;
use App\alertas;

class productos extends Model
{


    protected $table = 'productos';
  
    protected $fillable = [
      'id',  'id_categoria', 'mision', 'id_imagen'	, 'id_oferta'	, 'id_usuario'	, 'misionDetalladamente','id_ciudad' ,'fecha','id_estados_anuncios','id_tipo','id_producto_propuesta_final'];

    public function categoria(){

        return  $this->belongsTo(categoria::class,'id_categoria', 'id','tipo');
    }

     public function estado(){

        return  $this->belongsTo(estados::class,'id_estados_anuncios', 'id','estado');
    }

     public function usuario(){

        return  $this->belongsTo(User::class,'id_usuario', 'id','name','apellido');
    }

    public function unUsuario(){

        return  $this->hasOne(User::class,'id', 'id_usuario','name','apellido');
    }

     public function mensajes(){

         return  $this->hasOne(mensajes::class,'id','id_producto','mensaje');
    }

      public function anuncio_misiones(){

        return  $this->belongsTo(anuncio_misiones::class, 'id', 'tipo', 'mensaje');
    }

     public function imagenendeuno(){

        return  $this->hasOne(img::class,'id_producto','id','nombreImagen');
    }

    public function imagenendeVarios(){

        return  $this->hasMany(img::class,'id_producto','id','nombreImagen');
    }

    public function ciudades(){
         return  $this->belongsTo(ciudad::class,'id_ciudad','id','ciudad');
    }

     public function scopeProductos($query , $name)
    {
         return $query->where('misionDetalladamente','LIKE',"%$name%"); 
    }

    public function propuestas()
    {
        return  $this->hasOne(propuestas::class,'id_producto_propuesta','id');
    }

    public function alertas()
    {
        return  $this->hasMany(alertas::class,'id_producto','id');
    }

    public function entregas()
    {
        return  $this->hasOne(EstadoEntrega::class, 'id_producto', 'id');
    }
}
