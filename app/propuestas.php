<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class propuestas extends Model
{
    protected $table = 'propuestas_aproducto';
    public $timestamps = false;

    protected $fillable = [ 'id', 'presio','precio_envio','precio_final','id_usuario_propuesta','id_usuario_producto','id_producto','id_producto_propuesta','estado','descripcion','fecha_propuesta'];

    public function usuario()
    {
    	 return  $this->hasOne(User::class,'id_producto','id','name');
    }

}
