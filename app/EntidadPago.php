<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EntidadPago extends Model
{
    public $table = 'entidad_medio_pago';
    public $timestamps = false;

    protected $fillable = ['id', 'entidad', 'imagen','tipo'];
}
