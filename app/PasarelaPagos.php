<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PasarelaPagos extends Model
{
    protected $table = 'pasarela_pago';
    public $timestamps = false;

    protected $fillable = [ 'id', 'id_proveedor	', 'id_pasarela','id_propuesta','status','transaction_id', 'network','code'];
}
