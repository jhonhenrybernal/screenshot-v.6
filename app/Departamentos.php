<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Departamentos extends Model
{
    protected $table = 'departamentos';
    public $timestamps = false;

    protected $fillable = [
      'id',  'nombre'
    ];
}
