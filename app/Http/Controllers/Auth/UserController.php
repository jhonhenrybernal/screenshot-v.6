<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Role;
use Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Carbon\Carbon;

class UserController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::paginate();

        return view('users.index', compact('users'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);

        return view('users.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        $roles = Role::get();

        return view('users.edit', compact('user', 'roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);
        $user->update($request->all());

        $user->roles()->sync($request->get('roles'));

        return redirect()->route('users.edit', $user->id)
            ->with('info', 'Usuario guardado con éxito');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id)->delete();

        return back()->with('info', 'Eliminado correctamente');
    }

    public function activate($code){
        $user = User::where('code',$code)->first();

        if (empty($user['password'])) {
            return view('auth.date_complete',['id'=>$user['id']]);
        }

    	if ($user['active']) {
    		return response()->view('errors.activate-user', [], 500);
    	}else {
            return redirect('login');
        }

    	return response()->view('errors.500', [], 500);
    }

    public function complete(Request $request,$id){

    	if ($request['password'] == $request['password-confirm']) {

	    	$user = User::find($id);
	    	$user->password = Hash::make($request['password']);
	    	$user->remember_token  = Str::random(60);
            $user->email_verified_at = Carbon::now();
	    	$user->active = 1;
	    	$user->save();

    		return redirect()->route('login');
    	}

    	return back()->with('message', ['erro', 'Contraseñas no coinciden.']);
    }
}
