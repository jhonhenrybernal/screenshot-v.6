<?php

namespace App\Http\Controllers;

use App\ciudad;
use App\Departamentos;
use App\img;
use App\mensajes;
use App\Pagos;
use App\productos;
use App\propuestas;
use App\tipoDocumento;
use App\User;
use Auth;
use DB;
use Illuminate\Http\Request;

class propuestaController extends Controller
{

    //metodo para iniciar propuesta desde lista de productos en inicio
    public function realizarPropuesta(Request $request)
    {

        if (empty($request['presio'])) {
            return \Redirect::back()->withInput($request->all())->with('message', ['danger', 'Ingresar precio']);
        }
        $oValidacion = $this->postProductos()->validatorMessage($request['descripcion']);

        if ($oValidacion == 1) {
            return \Redirect::back()->withInput($request->all())->with('message', ['danger', 'EL mensaje no cumple con las buenas practicas de comuncacion en la plataforma ']);
        }

        if (Auth::user()->id_ciudad == null) {
            return \Redirect::back()->withInput($request->all())->with('message', ['danger', 'Por favor completar su perfil']);
        }


        $productos                       = new productos;
        $productos->timestamps = false;
        $productos->id_categoria         = $request->id_categoria;
        $productos->mision               = 'Nueva propuesta: '.$request->mision;
        $productos->id_estados_anuncios  = 6;
        $productos->id_mensaje           = 1;
        $productos->id_usuario           = Auth::user()->id;
        $productos->misionDetalladamente = $request->descripcion;
        $productos->id_ciudad            = Auth::user()->id_ciudad;
        $productos->fecha                = date("Y-m-d");
        $productos->fecha_actualizada    = date("Y-m-d");
        $productos->created_at    = date("Y-m-d H:i:s");
        $productos->id_tipo              = 2;
        $productos->save();

        $id_producto = $productos->id;
        $id_producto_usuario =$request['usuario'];

        $aMetaValue[] = [
            'titulo'  => 'Nueva propuesta: ' . $request->mision,
            'msm'     => $request->descripcion,
            'presio'  => $request->presio,
            'date'    => date('Y-m-d h:i'),
            'article' => 'panel panel-success',
            'icon'    => 'glyphicon-plus',
            'user'    => 'Usuario: ' . Auth::user()->name . ' ' . Auth::user()->apellido,
            'avatar'  => Auth::user()->avatar,
        ];

        $mensaje                    = new mensajes();
        $mensaje->id_usuario        = Auth::user()->id;
        $mensaje->id_producto       = $id_producto;
        $mensaje->id_estado_anuncio = 6;
        $mensaje->mensaje           = 'Propuesta por aprovacion';
        $mensaje->mensajes_value    = json_encode($aMetaValue);
        $mensaje->save();

        $calculos = new ProductController;
        $total_calculo = $calculos->calculoPropuestas($request->presio);

        $oPropuesta                        = new propuestas();
        $oPropuesta->timestamps = false;
        $oPropuesta->presio                = $request->presio;
        $oPropuesta->precio_final          = $total_calculo['total'];
        $oPropuesta->id_usuario_propuesta  = Auth::user()->id;
        $oPropuesta->id_usuario_producto   = $id_producto_usuario;
        $oPropuesta->id_producto           = $request->id;
        $oPropuesta->id_producto_propuesta = $id_producto;
        $oPropuesta->estado                = 6;
        $oPropuesta->descripcion           = $request->descripcion;
        $oPropuesta->fecha_propuesta       = date("Y-m-d H:i:s");
        $oPropuesta->save();

        $users = [
            'nombre_Usuario' => str_replace('.com','',$_SERVER["HTTP_HOST"]),
            'email'          =>  $this->confSystem()->getEmail(),
            'from'           => 'no-repply@'.$_SERVER["HTTP_HOST"],
            'subject'        => str_replace('.com','',$_SERVER["HTTP_HOST"]).'- Nueva propuesta',
        ];
        $mensage = [
            'mision'      => $request->mision,
            'descripcion' => $request->descripcion,
        ];
        $this->postProductos()->mailAdmin($users, $mensage, 'emails.propuesta');
        $this->postProductos()->procesarAllPreImagenes($id_producto);
        $this->postProductos()->log(Auth::user()->id, json_encode($aMetaValue), 'nueva propuesta');

        //notificar al administrador por app
        $this->enviarNotificacion()->notificar(Auth::user()->id, $id_producto, 1, 'Nueva propuesta','adm');

        return redirect()->back()->with('message', ['success', 'Propuesta enviada, pronto se estaran comunicando contigo. ']);
    }

    public function verEditarPropuesta($id_producto, $id_usuario)
    {
        $idPropuesta = propuestas::where('id_producto_propuesta', $id_producto)->first();
        $oImg        = img::where('id_producto', $id_producto)->get();

        $product  = productos::find($idPropuesta['id_producto']);
        $img      = $product->imagenendeVarios;
        $usuarios = $product->usuario;
        $ciudad   = $product->ciudades;
        return view('propuestas.cambiar-propuesta-anuncio', ['product' => $product, 'imagenes' => $img, 'usuario' => $usuarios, 'ciudad' => $ciudad, 'propuesta' => $idPropuesta]);
    }

    public function actualizarPropuesta(Request $request)
    {
        $propuesta = propuestas::where('id_producto_propuesta', $request['id_producto_propuesta'])->where('id_usuario_propuesta', Auth::user()->id)->first();
        $oImg      = img::where('id_producto', $request['id_producto_propuesta'])->get();

        $id_mensaje = mensajes::where('id_usuario', Auth::user()->id)->where('id_producto', $request['id_producto_propuesta'])->first();

        $oValidacion = $this->postProductos()->validatorMessage($request['descripcion']);

        if ($oValidacion == 1) {
            return \Redirect::back()->withInput($request->all())->with('message', ['danger', 'EL mensaje con cumple con las buenas practicas de comuncacion en la plataforma ']);
        }

        $aMetaValue[] = [
            'titulo'  => 'Nueva propuesta: ' . $request->mision,
            'msm'     => $request['descripcion'],
            'presio'  => $request['presio'],
            'date'    => date('Y-m-d h:i'),
            'article' => 'panel-primary',
            'icon'    => 'glyphicon-plus',
            'user'    => 'Usuario: ' . Auth::user()->name . ' ' . Auth::user()->apellido,
            'avatar'  => Auth::user()->avatar,
        ];

        $mensaje                    = mensajes::find($id_mensaje['id']);
        $mensaje->id_estado_anuncio = 6;
        $mensaje->mensajes_value    = json_encode($aMetaValue);
        $mensaje->save();

        $productos                       = productos::find($request['id_producto_propuesta']);
        $productos->id_estados_anuncios  = 6;
        $productos->fecha_actualizada    = date("Y-m-d");
        $productos->misionDetalladamente = $request['descripcion'];
        $productos->save();

        $id_producto = $productos->id;

        $oPropuesta              = propuestas::find($propuesta['id']);
        $oPropuesta->presio      = $request['presio'];
        $oPropuesta->descripcion = $request['descripcion'];
        $oPropuesta->fecha_propuesta       = date("Y-m-d");
        $oPropuesta->save();

        $users = [
            'nombre_Usuario' => str_replace('.com','',$_SERVER["HTTP_HOST"]),
            'email'          =>  $this->confSystem()->getEmail(),
            'from'           => 'no-repply@'.$_SERVER["HTTP_HOST"],
            'subject'        => str_replace('.com','',$_SERVER["HTTP_HOST"]).'-Nueva propuesta',
        ];
        $mensage = [
            'mision'      => 'Propuesta actualizada',
            'descripcion' => $request['descripcion'],
        ];

        $this->postProductos()->mailAdmin($users, $mensage, 'emails.propuesta');

        if ($request->hasFile('img')) {
            $this->postProductos()->cargarImagen($request, $request['id_producto_propuesta']);

        }
        $this->postProductos()->log(Auth::user()->id, json_encode($aMetaValue), 'nuevapropuesta');
        //notificar al administrador por app
        $this->enviarNotificacion()->notificar(Auth::user()->id, $id_producto, 1, 'Nueva propuesta', 'adm');


        return redirect()->back()->with('message', ['success', 'Propuesta enviada, pronto se estaran comunicando contigo . ']);

    }

    public function verPropuestas($id)
    {

        $this->postProductos()->alertas($data = null, $id);
        $product          = productos::find($id);
        $img              = $product['imagenendeVarios'];
        $oConsulPropuesta = DB::table('propuestas_aproducto')
            ->join('users', 'propuestas_aproducto.id_usuario_propuesta', 'users.id')
            ->join('productos', 'propuestas_aproducto.id_producto_propuesta', 'productos.id')
            ->select('users.name', 'propuestas_aproducto.*', 'productos.id_estados_anuncios as estado', 'users.avatar')
            ->where('id_producto', '=', $id)
            ->get();

        return view('propuestas.propuestas', ['product' => $product, 'imagenes' => $img, 'propuesta' => $oConsulPropuesta]);
    }

    public function verPropuesta($id_producto)
    {

        $oPropuesta       = productos::find($id_producto);
        $oPropuestas      = propuestas::where('id_producto_propuesta', $id_producto)->first();
        $oPagos           = Pagos::where('id_producto', $id_producto)->first();
        $imagenendeVarios = img::where('id_producto', $id_producto)->get();
        $aTipoDocumento   = tipoDocumento::all();
        $aUsers           = User::where('id', $oPropuesta['id_usuario'])->first();
        $userAut          = User::find(Auth::user()->id);
        $ciudad           = ciudad::all();
        $departamentos    = Departamentos::all();

        $contMisiones = productos::whereIn('id_estados_anuncios', [26])->where('id_usuario', Auth::user()->id)->get()->count();
        $contCanceladas = productos::whereIn('id_estados_anuncios', [24])->where('id_usuario', Auth::user()->id)->get()->count();

        $productsAuth = productos::find($oPropuesta->id_producto_propuesta_final);

        if ($oPropuesta['id_estados_anuncios'] == 19) {
            return redirect()->action('anuncioController@index');
        }
        return view('propuestas.propuesta', ['departamentos' => $departamentos, 'usuario_auth' => $userAut, 'productos' => $oPropuesta, 'imagenes' => $imagenendeVarios, 'aTipoDocumento' => $aTipoDocumento, 'usuarios' => $aUsers, 'ciudad' => $ciudad, 'pagos' => $oPagos, 'oPropuestas' => $oPropuestas,'contMisiones' => $contMisiones,
            'contCanceladas' => $contCanceladas,'productsAuth' =>  $productsAuth]);
    }

    public function mensajes(Request $Mail)
    {

        $oValidacion = $this->postProductos()->validatorMessage($Mail->mensaje);

        if ($oValidacion == 1) {
            return response()->json(false);
        }

        $mensaje = mensajes::where('id_producto', $Mail->id_producto)->first();
        $last    = last(json_decode($mensaje['mensajes_value']));

        if ($last->article == 'panel-primary' or $Mail->id_usuario == Auth::user()->id) {
            $article = 'panel-success';
        } else {
            $article = 'panel-primary';
        }

        $aMeta[] = [
            'titulo'  => 'Nuevo mensaje',
            'msm'     => $Mail->mensaje,
            'date'    => date('Y-m-d h:i'),
            'article' => $article,
            'icon'    => 'glyphicon-plus',
            'avatar'  => Auth::user()->avatar,
            'user'    => 'Usuario: ' . Auth::user()->name . ' ' . Auth::user()->apellido,
        ];

        $msm = ($mensaje['mensaje'] == 'Nuevo mensaje') ? 'Hay alguien que esta interesado en tu anuncio' : 'Nuevo mensaje';

        $aMetaValue                 = array_merge(json_decode($mensaje['mensajes_value'], true), $aMeta);
        $mensaje->id_estado_anuncio = 11;
        $mensaje->mensaje           = $msm;
        $mensaje->id_usuario        = Auth::user()->id;
        $mensaje->mensajes_value    = json_encode($aMetaValue);
        $mensaje->save();

        $product                      = productos::find($Mail->id_producto);
        $product->id_estados_anuncios = 11;
        $product->save();

        $alertas = [
            'id_stados_anuncios' => 14,
            'id_producto'        => $Mail->id_producto,
            'id_usuario'         => $Mail->id_usuario,
            'date'               => date('Y-m-d h:i'),
        ];

        $this->postProductos()->alertas($alertas, $Mail->id_producto);
        $this->postProductos()->log($Mail->id_usuario, json_encode($aMetaValue), $msm);

        $productoAlert = productos::where('id',$Mail->id_producto)->first();
        $idUsuarioProducto =  $productoAlert['id_usuario'];
        $idProductoProducto  = $productoAlert['id'];
        //enviar alerta a propuesta persona
        $this->enviarNotificacion()->notificar($idUsuarioProducto,$idProductoProducto,11,'Nuevo mensaje propuesta de interes');

        return response()->json(true);

    }
}
