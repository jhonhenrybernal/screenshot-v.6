<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\productos;
use App\Pagos;
use App\MediosPago;
use App\EntidadPago;
use App\img;
use Auth;
use Yajra\DataTables\Datatables;

class misionesController extends Controller
{

     public function index()
    {
        $products = productos::whereIn('id_estados_anuncios', [19,23,24,28,30])->where('id_tipo', 2)->where('id_usuario', Auth::user()->id)->paginate(16);

        $pagos = Pagos::join('users', 'pago_prd.id_usuario', 'users.id')
            ->join('productos', 'pago_prd.id_producto', 'productos.id')
            ->join('estados_anuncios', 'pago_prd.status', 'estados_anuncios.id')
            ->select('productos.mision as mision', 'pago_prd.fecha_pago as fecha', 'pago_prd.id as idFactura', 'pago_prd.status as id_estado', 'estados_anuncios.estado as estado','pago_prd.iva as iva','pago_prd.comision_pasarela as comisionPasarela','pago_prd.comision_screen_total  as comisionScreenTotal','pago_prd.cliente_ganancia as clienteGanancia','pago_prd.comision_pasarela as comisionPasarela')->where('users.id', Auth::user()->id)->whereIn('pago_prd.status', [19,23,28,30])->get();

        $MedioPago = MediosPago::where('id_usuario',Auth::user()->id )->first();
        $entidad = EntidadPago::orderBy('id', 'DESC')->get();
        $entidadMedioPago = array();

        if (!empty($MedioPago->id_entidad_medio_pago)) {
            $entidadMedioPago = json_decode($MedioPago->id_entidad_medio_pago);
        }


        return view('misiones.index', ['products' => $products,'pagos' => $pagos,'mediosPago' => $MedioPago,'entidad' => $entidad,'entidadMedioPago' => $entidadMedioPago]);
    }

    public function indexMision()
    {
        $products = productos::whereIn('id_estados_anuncios',[19,23,24,28,30])->where('id_usuario', Auth::user()->id)->paginate(16);

        $pagos = Pagos::join('users', 'pago_prd.id_usuario', 'users.id')
            ->join('productos', 'pago_prd.id_producto', 'productos.id')
            ->join('estados_anuncios', 'pago_prd.status', 'estados_anuncios.id')
            ->select('productos.mision as mision', 'pago_prd.fecha_pago as fecha', 'pago_prd.id as idFactura', 'pago_prd.status as id_estado', 'estados_anuncios.estado as estado','pago_prd.iva as iva','pago_prd.comision_pasarela as comisionPasarela','pago_prd.comision_screen_total  as comisionScreenTotal','pago_prd.cliente_ganancia as clienteGanancia','pago_prd.comision_pasarela as comisionPasarela')->where('users.id', Auth::user()->id)->whereIn('pago_prd.status',[19,23,28,30])->get();

        $MedioPago = MediosPago::where('id_usuario',Auth::user()->id )->first();
        $entidad = EntidadPago::orderBy('entidad', 'DESC')->get();
        $entidadMedioPago = array();

        if (!empty($MedioPago->id_entidad_medio_pago)) {
            $entidadMedioPago = json_decode($MedioPago->id_entidad_medio_pago);
        }


        return view('misiones.index', ['products' => $products,'pagos' => $pagos,'mediosPago' => $MedioPago,'abrirModal' => 1,'entidad' => $entidad,'entidadMedioPago' => $entidadMedioPago]);
    }

    public function sspIndex()
    {
        $pagos = Pagos::join('users', 'pago_prd.id_usuario', 'users.id')
        ->join('productos', 'pago_prd.id_propuesta', 'productos.id')
        ->join('estados_anuncios', 'pago_prd.status', 'estados_anuncios.id')
        ->select('productos.mision as mision','pago_prd.cliente_ganancia as cliente_ganancia', 'pago_prd.fecha_pago as fecha', 'pago_prd.id as idFactura', 'pago_prd.status as id_estado', 'estados_anuncios.estado as estado','pago_prd.iva as iva','pago_prd.comision_pasarela as comisionPasarela','pago_prd.comision_screen_total  as comisionScreenTotal','pago_prd.cliente_ganancia as clienteGanancia','pago_prd.comision_pasarela as comisionPasarela')->where('pago_prd.id_usuario', Auth::user()->id)->whereIn('pago_prd.status', [27,28,29,30,35])->get();

        return Datatables::of($pagos)->addColumn('action', function ($pagos) {
            if ($pagos->id_estado == 29 || $pagos->id_estado == 28) {
                return '<div class="form-check">'.'<input type="checkbox" class="form-check-input" name="deposito" value="'.$pagos->idFactura.'" checked disabled>'.'</div>';
            }else {
                return '<div class="form-check">'.'<input type="checkbox" class="form-check-input" name="deposito" value="'.$pagos->idFactura.'" >'.'</div>';
            }


        })->make(true);
    }

    public function envia($id)
    {
        $products = productos::find($id);
        $productsEnvio = productos::find($products->id_producto_propuesta_final);

        $contMisiones = productos::whereIn('id_estados_anuncios', [24])->where('id_usuario', Auth::user()->id)->get()->count();
        $contCanceladas = productos::whereIn('id_estados_anuncios', [22])->where('id_usuario', Auth::user()->id)->get()->count();


        $img      = img::where('id_producto', $productsEnvio->id)->get();

        return view('misiones.envia', [
            'productsEnvio' => $productsEnvio,
            'imagenes' => $img,
            'productoEnviado' =>  $productsEnvio,
            'products' =>  $productsEnvio,
            'contMisiones' => $contMisiones,
            'contCanceladas' => $contCanceladas]);
    }

    public function recibe($id)
    {

        $products = productos::find($id);
        $productoPropuesta = productos::find($products->id_producto_propuesta_final);

        $img = img::where('id_producto', $productoPropuesta->id)->get();

        $contMisiones = productos::whereIn('id_estados_anuncios', [28])->where('id_usuario', Auth::user()->id)->get()->count();
        $contCanceladas = productos::whereIn('id_estados_anuncios', [22])->where('id_usuario', Auth::user()->id)->get()->count();

        $usuarioAuth = Auth::user()->id;
        $usuarioPropuesta = $productoPropuesta->id_usuario;


        return view('misiones.recibe', ['products' => $productoPropuesta,'imagenes' => $img,'productsAuth' =>  $products,'contMisiones' => $contMisiones,'contCanceladas' => $contCanceladas,'usuarioAuth' => $usuarioAuth,'usuarioPropuesta' => $usuarioPropuesta]);
    }
}
