<?php

namespace App\Http\Controllers\Soap;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use SoapFault;
use SoapClient;
use App\Exceptions\Handler;

class ConectSoapController extends Controller
{
    
	//permite conectar url iniciando soap 
	public function __construct(){
		$this->urlPay = 'https://dev.pagosinteligentes.com/webservices/mainwebservices.asmx?WSDL';
		$this->urlPayToken = 'https://ecommerce.pagosinteligentes.com/checkout/gateway.aspx?&Token=';
		$this->urlStatus = 'https://gateway.pagosinteligentes.com/WebServices/MainWebServicesV2.asmx?wsdl';
		//url de pruebas
		//$this->urlPayToken = 'https://dev.pagosinteligentes.com/Gateway/GatewayV2.aspx?&Token=';
	}

	public function processPayment($data){
		if ($this->testConnect()) {
			$data = [
				'PaymentInfo' => [

					'AcID' => 1,//26,
					'Reference1' => $data['Reference1'],  //aca va se inicial de tipo,id producto,#consecutivo
					'Amount' => $data['Amount'],
					'Description' => $data['Description'],
					'Tax' => 16,
					'BDev' => 13,
					'MerchantOption1' => 1001,
					'FirstName' => $data['FirstName'].' '. $data['LastName'],
					'LastName' => $data['LastName'],
					'Mobile' => $data['Mobile'],
					'Phone' => $data['Mobile'],
					'Email' => $data['Email'],
					'Region' => $data['Region'],
					'City' =>  $data['City'],
					'Address' => $data['Address'],
					'Country ' => 'CO',
					'ReturnURL' => $data['ReturnURL'],
					'IdNumber' =>$data['IdNumber'],
					'IdType' => $data['IdType'],
					'EnableCard' => 'true',
					'EnablePSE' => 'true',
					'EnableCash' => 'true',
					'SourceId' =>  $data['SourceId'],
					'AccountPassword' =>'1234',
					'Language' => 'es',
					'Reference2' => $data['Reference2'],
					'Reference3' => $data['Reference3'],
					'AccountId' =>  26,
				],
				'IsTest' => 'true'
			];

			$opts = array(
			        'http' => array(
			            'user_agent' => 'PHPSoapClient'
			        )
			    );	
			$context = stream_context_create($opts);
			$params = array ('encoding' => 'UTF-8', 'verifypeer' => false, 'verifyhost' => false, 'soap_version' => SOAP_1_2, 'trace' => 1, 'exceptions' => 1, "connection_timeout" => 180, 'stream_context' => stream_context_create($opts) , array('cache_wsdl' => WSDL_CACHE_NONE));	
			$client = new SoapClient($this->urlStatus, $params);

			$token = $client->GenerateToken($data);
			$proccess = $this->urlPayToken.$token->GenerateTokenResult;
			return $proccess;
		}
		else{
			return  false;	
		}
	}


	public function testConnect(){

        try {

        	//script para comprobar conexion de url
		   $ch = curl_init($this->urlPay);
			curl_setopt($ch, CURLOPT_TIMEOUT, 5);
			curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			$data = curl_exec($ch);
			$httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
			curl_close($ch);
			if($httpcode>=200 && $httpcode<300){
				$opts = array(
			        'http' => array(
			            'user_agent' => 'PHPSoapClient'
			        )
			    );
			    $context = stream_context_create($opts);
			    $soapClientOptions = array(
			        'stream_context' => $context,
			        'cache_wsdl' => WSDL_CACHE_NONE,
			        'exceptions' => 0
			    );
				$this->client = new SoapClient($this->urlPay, $soapClientOptions);
				//retorna si url response
			  	return !is_soap_fault($this->client);
			} else {
			  	return false;
			}
		}
		catch(Exception $e) {
		    echo $e->getMessage();
		}
	}

	public function statusPayment(){
		
		/*if ($this->testConnect()) {
			dd('bien');
		}else{
			dd('mal');
		}
 		dd('fin');*/
		$params2 = [
				'PaymentInfo' => [

					'AcID' => 1,
					'Reference1' => '123',  //aca va se inicial de tipo,id producto,#consecutivo
					'Amount' => 50000,
					'Description' => '123dd',
					'Tax' => 16,
					'BDev' => 19,
					'MerchantOption1' => '1001',
					'FirstName' => 'prueba mas de bernal',
					'LastName' => 'apellido',
					'Mobile' => 23232232,
					'Phone' => 23232323,
					'Email' => 'mail@mail.com',
					'Region' => 'Cali',
					'City' =>  'cali',
					'Address' => '1213',
					'Country ' => 'CO',
					'ReturnURL' => 'urlprueba',
					'IdNumber' => 12345345,
					'IdType' => 'CC',
					'EnableCard' => 'true',
					'EnablePSE' => 'true',
					'EnableCash' => 'true',
					'SourceId' =>  'screenshot1',
					//'AccountId' =>  26,
					'AccountPassword' =>'1234',
					//'Language' => 'es',
					
				],
				'IsTest' => 'true'
			];

		$params3 = [
					'TransactionId' => 4147074,  //aca va se inicial de tipo,id producto,#consecutivo
					'AccountId' =>  '26',
					'AccountPassword' =>'1234',
				
			];
		$opts = array(
			        'http' => array(
			            'user_agent' => 'PHPSoapClient'
			        )
			    );	
		$context = stream_context_create($opts);
		$params = array ('encoding' => 'UTF-8', 'verifypeer' => false, 'verifyhost' => false, 'soap_version' => SOAP_1_2, 'trace' => 1, 'exceptions' => 1, "connection_timeout" => 180, 'stream_context' => stream_context_create($opts) );	

		$client2 = new SoapClient($this->urlQa, $params);
		$tk = $client2->generateToken($params2); 
		dd($params2);
		//dd($client2->GetTransactionByTransactionId($params3));
		//dd($client2->GetTransactionByTransactionId ($params3));
	}
    
}


