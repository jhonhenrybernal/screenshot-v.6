<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Pagos;
use App\EstadoEntrega;
use App\productos;
use App\propuestas;
use App\mensajes;
use App\User;
use Auth;

class EstadoEntregaController extends Controller
{

    public function iniciarEntrega($id_producto,$id_usuario,$id_usuario_entregas,$estado,$msm){

        $entrega = new EstadoEntrega();
        $entrega->id_producto = $id_producto;
        $entrega->id_usuario =  $id_usuario;
        $entrega->id_usuario_entregas =  $id_usuario_entregas;
        $entrega->fecha =  date("Y-m-d h:i:s");
        $entrega->estado = $estado;
        $entrega->comentarios =  $msm;
        $entrega->save();

        return true;
    }


    public function cancelar(Request $request)
    {
        $preCancelarMision = 31;
        $preCancelaInforma = 34;


        //precancela producto principal
        $productos = productos::find($request['id_producto']);
        $productos->id_estados_anuncios = $preCancelarMision;
        $productos->save();


        //precancela producto contrario
        $idPropuesta = $productos->id_producto_propuesta_final;
        $productosContrario = productos::find($idPropuesta);
        $productosContrario->id_estados_anuncios = $preCancelaInforma;
        $productosContrario->save();



        //enviar alerta a producto persona principal
        $idUsuarioProductoPrincipal =  $productos->id_usuario;
        $idProductoProductoPrincipal  = $productos->id;
        $this->enviarNotificacion()->notificar($idUsuarioProductoPrincipal,$idProductoProductoPrincipal,$preCancelarMision,'Mision cancelaca por la mision:'. $productos->mision.'. En espera de aprovacion por el admin'  );

        $entregaPrincipal = EstadoEntrega::
        where('id_producto',$idProductoProductoPrincipal)->
        where('id_usuario_entregas',$idUsuarioProductoPrincipal)->
        first();

        $entregasPri = EstadoEntrega::find($entregaPrincipal['id']);
        $entregasPri->estado = $preCancelarMision;
        $entregasPri->fecha = date("Y-m-d h:i");
        $entregasPri->comentarios = $request['mensaje'];
        $entregasPri->save();

        //enviar alerta a producto persona contrario
        $idUsuarioProductoContrario =  $productosContrario->id_usuario;
        $idProductoProductoContrario  = $productosContrario->id;
        $this->enviarNotificacion()->notificar($idUsuarioProductoContrario,$idProductoProductoContrario, $preCancelaInforma,'Mision cancelaca por la mision:'. $productos->mision.'. En espera de aprovacion por el admin' );


        $entregaContrario = EstadoEntrega::
        where('id_producto',$idProductoProductoContrario)->
        where('id_usuario_entregas',$idUsuarioProductoContrario)->
        first();

        $entregasCom = EstadoEntrega::find($entregaContrario['id']);
        $entregasCom->estado = $preCancelaInforma;
        $entregasCom->fecha = date("Y-m-d h:i");
        $entregasCom->comentarios = '';
        $entregasCom->save();


        try {

            if ( $productos['id_tipo'] == 1) {
                $idMensaje = $productos->id_producto_propuesta_final;
                $titulo = 'Algo ocurrio con la entrega';
                $msm = "El producto con el titulo de mision ".$productos->mision." y con el detalle " .$productos->misionDetalladamente.", con la propuesta ".$productosContrario->misionDetalladamente.".";
            }elseif ( $productos['id_tipo'] == 2) {
                $idMensaje = $productos->id;
                $titulo = 'Algo ocurrio con la envio';
                $msm = "El producto con el titulo de mision ".$productosContrario->mision." y con el detalle " .$productosContrario->misionDetalladamente.", con la propuesta ".$productos->misionDetalladamente.".";
            }

            $mensaje = mensajes::where('id_producto', $idMensaje)->first();

            if ($mensaje != null) {

                $last    = last(json_decode($mensaje['mensajes_value']));

                $aMeta[] = [
                    'titulo'  => $titulo,
                    'msm'     => $msm,
                    'date'    => date('Y-m-d h:i'),
                    'article' => 'panel-danger',
                    'icon'    => 'glyphicon-plus',
                    'user'    => 'Usuario quien lo cancela: ' . Auth::user()->name . ' ' . Auth::user()->apellido,
                ];

                $aMetaValue                 = array_merge(json_decode($mensaje['mensajes_value'], true), $aMeta);
                $mensaje->id_estado_anuncio = $preCancelarMision;
                $mensaje->mensajes_value    = json_encode($aMetaValue);
                $mensaje->mensaje    = 'Cancelaste la mision';
                $mensaje->save();
            }
            return response()->json(true);
        } catch (Exception $e) {
             return response()->json(false);
        }



    }


    public function mensajes(Request $Mail)
    {
        $oValidacion = $this->postProductos()->validatorMessage($Mail->mensaje);

        if ($oValidacion == 1) {
            return response()->json(false);
        }

        $mensaje = mensajes::where('id_producto', $Mail->id_producto)->first();
        $last    = last(json_decode($mensaje['mensajes_value']));
        if ($last->article == 'panel-primary' or $Mail->id_usuario == Auth::user()->id) {
            $article = 'panel-success';
        } else {
            $article = 'panel-primary';
        }

        $aMeta[] = [
            'titulo'  => 'Nuevo mensaje',
            'msm'     => $Mail->mensaje,
            'date'    => date('Y-m-d h:i'),
            'article' => $article,
            'icon'    => 'glyphicon-plus',
            'user'    => 'Usuario: ' . Auth::user()->name . ' ' . Auth::user()->apellido,
            'avatar' => 'sys'
        ];

        $aMetaValue                 = array_merge(json_decode($mensaje['mensajes_value'], true), $aMeta);
        $mensaje->id_estado_anuncio = 22;
        $h->mensaje           = 'La entrega no fue realizado';
        $mensaje->id_usuario        = Auth::user()->id;
        $mensaje->mensajes_value    = json_encode($aMetaValue);
        $mensaje->save();

        $product                      = productos::find($Mail->id_producto);
        $product->id_estados_anuncios = 22;
        $product->save();

        $alertas = [
            'id_stados_anuncios' => 22,
            'id_producto'        => $Mail->id_producto,
            'id_usuario'         => $Mail->id_usuario,
            'date'               => date('Y-m-d h:i'),
        ];

        $this->postProductos()->alertas($alertas, $Mail->id_producto);
        //enviar alerta a producto persona
        $this->enviarNotificacion()->notificar($idUsuarioProducto,$idProductoProducto,$id_stados_anuncios,'Mision cancelar');

        return response()->json(true);

    }

    public function finalizar(Request $request)
    {
        //recibido
        if($request['final'] == 2){

            $estadoRecibe = 35;
            $estadoEnvia = 24;
            try {
                 //producto recibido
                $productos = productos::find($request['idProducto']);
                $productos->id_estados_anuncios = $estadoRecibe;
                $productos->update();

                mensajes::where('id_producto',$request['idProducto'])->update(['id_estado_anuncio' => $estadoRecibe,'mensaje' => 'Finalizo entrega']);

                EstadoEntrega::where('id_producto',$request['idProducto'])
                ->update([
                    'fecha' =>  date("Y-m-d h:i"),
                    'estado' => $estadoRecibe,
                    'comentarios' => 'Finalizo entrega']);

                 //enviar alerta a producto persona
                $this->enviarNotificacion()->notificar($productos->id_usuario,$productos->id,$estadoRecibe,'Finalizo entrega');





                //propuesta entregado

                $productoPropuesta = productos::where('id',$productos->id_producto_propuesta_final)->first();
                $productoPropuesta->update([
                    'id_estados_anuncios' =>  $estadoEnvia]);


                $user = User::find($productos->id_usuario);

                $propuesta        = propuestas::where('id_producto',$productos->id)->first();
                $propuesta->estado = $estadoEnvia;
                $propuesta->update();
                $resultPago = $this->postProductos()->calculoPropuestas($propuesta['presio']);


                $mensajesPropuesta = mensajes::where('id_producto', $productos->id_producto_propuesta_final)->first();
                if ($mensajesPropuesta != null) {
                    $last    = last(json_decode($mensajesPropuesta['mensajes_value']));
                    if ($last->article == 'panel-primary') {
                        $article = 'panel-success';
                    } else {
                        $article = 'panel-primary';
                    }

                    $aMeta[] = [
                        'titulo'  => 'Nuevo mensaje',
                        'msm'     => 'Finalizo entrega.',
                        'date'    => date('Y-m-d h:i'),
                        'article' => $article,
                        'icon'    => 'glyphicon-plus',
                        'user'    => 'Usuario: ' .  $user->name . ' ' . $user->apellido,
                    ];

                    $aMetaValue                 = array_merge(json_decode($mensajesPropuesta['mensajes_value'], true), $aMeta);
                    $mensajesPropuesta->update([
                        'id_estado_anuncio' => $estadoEnvia,
                        'mensajes_value'    => json_encode($aMetaValue),
                        'mensaje' => 'Finalizo entrega',
                    ]);
                }




                EstadoEntrega::where('id_producto',$productos->id_producto_propuesta_final)->update([
                    'fecha' =>  date("Y-m-d h:i"),
                    'estado' => $estadoEnvia,
                    'comentarios' =>  'Finalizo entrega']);

                //propuesta y producto
                Pagos::where('id_producto', $productos->id)->where('id_propuesta', $productos->id_producto_propuesta_final)->update(['status' => $estadoRecibe, 'estado_msg' => 'Entregado, consignar a misionero']);


               //enviar alerta a producto persona
                $this->enviarNotificacion()->notificar($productoPropuesta['id_usuario'],$productoPropuesta['id'],$estadoEnvia,'Finalizo entrega');
                return response()->json(true);

            } catch (Exception $e) {
                return response()->json(false);
            }

         //enviado
        }elseif($request['final'] == 1){
            $estado = 23;

            try {

                $productoPropuesta = productos::where('id_producto_propuesta_final',$request['idProducto'])->first();
                if ($productoPropuesta->id_estados_anuncios == 19) {
                   return response()->json('no-finalizado');
                }

                $productoPropuesta->update([
                    'id_estados_anuncios' =>  $estado]);


                $mensajesPropuesta = mensajes::where('id_producto',$productoPropuesta->id)->first();

                $user = User::find($productos->id_usuario);

                $last    = last(json_decode($mensajesPropuesta['mensajes_value']));
                if ($last->article == 'panel-primary') {
                    $article = 'panel-success';
                } else {
                    $article = 'panel-primary';
                }

                $aMeta[] = [
                    'titulo'  => 'Nuevo mensaje',
                    'msm'     => 'Producto enviado.',
                    'date'    => date('Y-m-d h:i'),
                    'article' => $article,
                    'icon'    => 'glyphicon-plus',
                    'user'    => 'Usuario: ' . $user->name . ' ' . $user->apellido,
                ];
                $aMetaValue                 = array_merge(json_decode($mensajesPropuesta['mensajes_value'], true), $aMeta);
                $mensajesPropuesta->update([
                    'id_estado_anuncio' => $estado,
                    'mensajes_value'    => json_encode($aMetaValue),
                    'mensaje' => 'Producto enviado.',
                ]);


                $entregasPropuesta = EstadoEntrega::where('id_producto',$request['idProducto'])->update([
                    'fecha' => date("Y-m-d h:i"),
                    'estado' => $estado,
                    'comentarios' =>  'Producto enviado.']);
                $propuesta        = propuestas::where('id_producto_propuesta',$productos->id)->first();
                $resultPago = $this->postProductos()->calculoPropuestas($propuesta['presio']);
                 $pago = Pagos::create([
                    'id_proveedor'  => 1,
                    'fecha_pago'    => date('Y-m-d H:i:s'),
                    'id_producto'   => $request['idProducto'],
                    'id_propuesta'  => $productoPropuesta->id,
                    'id_usuario'    => $productoPropuesta->id_usuario,
                    'status'        => 27,
                    'estado_msg'    => 'Usuario solicita que sea depositado el dinero',
                    'TransactionId' => 123,
                    'metodo_pago'   => 2,
                    'iva' => $resultPago['iva'],
                    'comision_pasarela'   => $resultPago['pasarela'],
                    'comision_screen_total' => $resultPago['conmision_total'],
                    'cliente_ganancia' => $resultPago['comision_cliente'],
                ]);

                $this->enviarNotificacion()->notificar($productoPropuesta['id_usuario'],$productoPropuesta['id'],$estado,'Producto enviado.');
                return response()->json(true);

            } catch (Exception $e) {
                return response()->json(false);
            }

        }
    }

    //funcion cron para dar finalizado automaticamente

    public function estadoFinalizado()
    {
        $aProductFinal = productos::where('id_estados_anuncios', 24)->orderBy('fecha', 'Desc')->get();

        $estado = 25;
        foreach ($aProductFinal as $product) {
            $fechaProducto = $product->fecha_actualizada;
            $nuevafecha = strtotime ( '+10 day' , strtotime ( $fechaProducto ) ) ;
            $fechaActual = date('Y-m-d');
            $pago = Pagos::where('id_producto',$product->id)->first();

            if($nuevafecha == $fechaActual){

                //recibido
                $productos = productos::find($aProductFinal->id);
                $productos->id_estados_anuncios = $estado;
                $productos->save();

                $mensajesProducto = mensajes::find($aProductFinal->id);
                $mensajesProducto->id_estado_anuncio = $estado;
                $mensajesProducto->save();


                //enviado
                $productoPropuesta = productos::find($productos->id_producto_propuesta_final);
                $productoPropuesta->id_estados_anuncios = $estado;
                $productoPropuesta->save();
                $mensajesPropuesta = mensajes::find($productos->id_producto_propuesta_final);
                $mensajesPropuesta->id_estado_anuncio = $estado;
                $mensajesPropuesta->save();



                $entrega = EstadoEntrega::where('id_producto',$request['idProducto']);
                $entrega->fecha =  date("Y-m-d h:i");
                $entrega->estado = $estado;
                $entrega->comentarios =  'Finalizado';
                $entrega->save();

            }
        }
    }
}
