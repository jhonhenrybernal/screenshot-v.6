<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\productos;
use App\propuestas;
use App\mensajes;
use App\img;
use Auth;

class inicioController extends Controller
{
      public function index()
    {
            $aProductsdos = [];
            $aProductsuno = [];
            $aPrdCouDos = productos::where('id_tipo', 1)->orderBy('fecha', 'Asc')->count();

            if ($aPrdCouDos  >= 8 && $aPrdCouDos  <= 16) {
                $cant = $aPrdCouDos - 5;
            }else if ($aPrdCouDos  > 16) {
                $cant = 5;
            }else if ($aPrdCouDos  < 7) {
                $cant = 0;
            }
        if(Auth::check()){
            $aProductsuno = productos::where('id_estados_anuncios', 2)->where('id_usuario', '<>',Auth::user()->id)->orderBy('id', 'Desc')->paginate(5);
            if ($aPrdCouDos > 8) {
                # code...
                $aProductsdos = productos::where('id_estados_anuncios', 2)->where('id_usuario', '<>',Auth::user()->id)->orderBy('id', 'Asc')->paginate($cant);
            }
        }else{
            $aProductsdos = productos::where('id_estados_anuncios', 2)->orderBy('id', 'Asc')->paginate(5);
            if ($aPrdCouDos > 8) {
                $aProductsuno = productos::where('id_estados_anuncios', 2)->orderBy('id', 'Desc')->paginate($cant);
            }

        }

        return  response()->view('inicio.index',['products' => $aProductsuno,'aProductsdos' => $aProductsdos],404);
    }

    public function getProductos(Request $request)
    {
        $term = trim($request->q);
        $productos = productos::productos($term)->orderBy('id','DESC')->get();
        return response()->json($productos);
    }


     public function show($id)
    {
        if (!Auth::check()) {
            return redirect()->route('login');
        }
        $product = productos::find($id);
        $img = $product->imagenendeVarios;
        $usuarios = $product->usuario;
        $idUsuario = $product->id_usuario;
        $ciudad = $product->ciudades;
        return view('inicio.ver-anuncio', ['product' => $product, 'imagenes' => $img, 'usuario' => $usuarios, 'ciudad' => $ciudad,'id_usuario' => $idUsuario ]);
    }


}
