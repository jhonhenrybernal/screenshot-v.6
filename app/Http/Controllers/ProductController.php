<?php

namespace App\Http\Controllers;

use App\alertas;
use App\img;
use App\PreImg;
use App\log;
use App\mensajes;
use App\productos;
use App\propuestas;
use Auth;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Intervention\Image\ImageManagerStatic as Image;

class ProductController extends Controller
{
    public function generarCodigo()
    {
        $key     = '';
        $pattern = '1234567890';
        $max     = strlen($pattern) - 1;
        for ($i = 0; $i < 3; $i++) {
            $key .= $pattern{mt_rand(0, $max)};
        }

        return $key;
    }

    public function indexadmin()
    {
        $products = productos::paginate();

        return view('homeadmin', compact('products'));
    }

    public function store(Request $request)
    {
        $product = Product::create($request->all());

        return redirect()->route('products.edit', $product->id)
            ->with('info', 'Rol guardado con éxito');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $producto = productos::find($id);

        return view('products.edit', ['producto' => $producto]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $product = Product::update($request->all());

        return redirect()->route('products.edit', $product)
            ->with('info', 'Rol guardado con éxito');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::find($id)->delete();

        return back()->with('info', 'Eliminado correctamente');
    }

    public function getProductosBuscar($oData)
    {
        $term      = trim($oData->q);
        $productos = productos::productos($term)->orderBy('id', 'DESC')->get();
        return response()->json($productos);
    }
     //jbernal 12-09-2019 cambio de modo carga de imagenes
    /*public function cargarImagen($oImg, $id_producto)
    {
        dd($oImg);
        $image_array = $oImg->file('img');

        $array_len = count($image_array);

        for ($i = 0; $i < $array_len; $i++) {
            $image_size = $image_array[$i]->getClientSize();
            $image_ext  = $image_array[$i]->getClientOriginalExtension();

            $new_image_name = rand(123456, 999999) . "." . $image_ext;

            $destination_path = public_path('/imagenesproductos');

            $image_array[$i]->move($destination_path, $new_image_name);

            $img               = new img;
            $img->rutaImagen   = $image_size;
            $img->id_producto  = $id_producto;
            $img->nombreImagen = $new_image_name;
            $img->save();

        }
    }*/

    public function mail($datos, $vistaMensage, $blade)
    {
        Mail::send($blade, ['msm' => $vistaMensage], function ($message) use ($datos) {
            $message->subject($datos);
            $message->to($datos['email'], $datos['nombre_Usuario']);
            $message->from($datos['from']);
            $message->subject($datos['subject']);
        });
    }

    public function mailAdmin($datos, $vistaMensage, $blade)
    {
        Mail::send($blade, ['msm' => $vistaMensage], function ($message) use ($datos) {
            $message->subject($datos);
            $message->to($datos['email'], $datos['nombre_Usuario']);
            $message->from($datos['from']);
            $message->subject($datos['subject']);
        });
    }

    public function validatorMessage($oData)
    {
        $oValidacionData = DB::table('validaciones')->get();
        foreach ($oValidacionData as $value) {
            $lista = json_decode($value->parametro);
            foreach ($lista as $val) {
                foreach ($val as $listas) {
                    foreach ($listas as $key => $value) {
                        $contains = str_contains($oData, $value);

                        $result = 1;
                        if ($contains) {
                            return $result;
                            break;
                        }
                    }
                }
            }
        }
    }

    public function historialMensajes($id_producto, $id_usuario, $status)
    {
        $aHistorial = mensajes::where('id_producto', $id_producto)->whereIn('id_estado_anuncio', [8,11, 12,22,29,31])->first();

        $producto   = productos::where('id', $aHistorial->id_producto)->first();
        if ($aHistorial['id_estado_anuncio'] == 11 and $aHistorial['id_usuario'] != Auth::user()->id) {
            $id_estado = 12;
            $aHistorial->id_estado_anuncio =  $id_estado;
            $aHistorial->save();

            $producto->id_estados_anuncios =  $id_estado;
            $producto->save();

            $id_stados_anuncios = $id_estado;
            $propuesta        = propuestas::where('id_producto_propuesta', $id_producto)->first();
            $idUsuarioPropuesta = $propuesta['id_usuario_propuesta'];
            $idProductoPropuesta  = $propuesta['id_producto'];

            $propuesta->estado = $id_estado;
            $propuesta->save();

            //enviar alerta a propuesta persona
            $this->enviarNotificacion()->notificar($idUsuarioPropuesta,$idProductoPropuesta,$id_stados_anuncios,'Su propuesta fue aprovado');

            return response()->json(json_decode($aHistorial['mensajes_value']));
        } else {
            return response()->json(json_decode($aHistorial['mensajes_value']));
        }
    }

    public function log($id_usuario,$data, $action,$id_producto = null,$id_propuesta = null )
    {
        $savelog             = new log();
        $savelog->id_usuario = $id_usuario;
        $savelog->id_producto = $id_producto;
        $savelog->id_propuesta = $id_propuesta;
        $savelog->meta_value = $data;
        $savelog->accion     = $action;
        $savelog->save();
        return $savelog;
    }

    public function alertas($data, $id_producto)
    {

        if ($id_producto) {
            $alerta = alertas::where('id_producto', $id_producto)->delete();

            return $alerta;

        } else {

            $alerta                     = new alertas;
            $alerta->id_stados_anuncios = $data['id_stados_anuncios'];
            $alerta->id_producto        = $data['id_producto'];
            $alerta->id_usuario         = $data['id_usuario'];
            $alerta->date               = $data['date'];
            $alerta->save();

            return $alerta;
        }
    }


        /* carga de imagenes*/

    public function verPreImagenes(){

        $img = PreImg::select('pre_imagenes.nombreImagen as imagen','pre_imagenes.id as id')->where('pre_imagenes.id_usuario', Auth::user()->id)->get();
        $output = '';
        foreach($img as $row)
         {
          $url =   url('/imagenesPreproductos/'.$row['imagen']);
           $output .= '
           <div class="cargar"><img src="'. $url.'"  class="load-img" /><button onclick="eliminarPreImagenes('.$row['id'].')" class="borrar">Borrar</button>
           </div>
          ';
         }

         return response()->json($output);
    }

     public function eliminarPreImagenes(Request $request){

        $imgList = PreImg::where('id',$request['id'])->where('id_usuario',Auth::user()->id)->first();

        if ($imgList) {
            $destination_path = public_path('/imagenesPreproductos/');
            unlink($destination_path. $imgList['nombreImagen']);
             $img = PreImg::where('id',$request['id'])->delete();
            $eliminado = true;
        }else{
            $eliminado = false;
        }

         return response()->json($eliminado);
    }

    public function cargarPreImagenes(Request $request){

        $imgList = PreImg::where('id_usuario',Auth::user()->id)->get();

        if ( count($imgList) >= 4) {
            return response()->json('No se permite mas imagenes');
        }
        $image = $request->image;
        /*$image = str_replace('data:image/png;base64,', '', $image);
        $image = str_replace(' ', '+', $image);
        $imageName = str_random(10) . '.png';

        $data = base64_decode($image);

        $imageName = time() . '.png';
        $destination_path = public_path('/imagenesPreproductos/');

        file_put_contents($destination_path.$imageName, $data);*/


        $imageName = str_random(10) . '.png';
        $image = Image::make($image);
        $image->resize(500, 500);
        $image->save(public_path('/imagenesPreproductos/' .$imageName));

        $img               = new PreImg;
        $img->id_usuario  = Auth::user()->id;
        $img->nombreImagen = $imageName;
        $img->save();

        return response()->json('Imagen ' .$imageName .' cargado');
    }

    public function procesarAllPreImagenes($id_producto){

        $imgList = PreImg::where('id_usuario',Auth::user()->id)->get();

        if ($imgList) {
            foreach ($imgList as $img) {

                $origen = public_path('/imagenesPreproductos/').$img['nombreImagen'];
                $destino = public_path('/imagenesproductos/');
                $resultCopy = (copy($origen, $destino . $img['nombreImagen']))?true:false;
                $nameImg = $img['nombreImagen'];

                $img               = new img;
                $img->rutaImagen   = 'imagenesproductos';
                $img->id_producto  = $id_producto;
                $img->nombreImagen = $nameImg;
                $img->save();


            }

            $this->eliminarAllPreImagenes();
        }

    }

     public function eliminarAllPreImagenes(){
        $imgList = PreImg::where('id_usuario',Auth::user()->id)->get();

        foreach ($imgList as $img) {
            $destination_path = public_path('/imagenesPreproductos/');
            unlink($destination_path. $img['nombreImagen']);
            PreImg::where('id_usuario',Auth::user()->id)->delete();
        }

     }



     public function verPreImagenesPropuesta(){

        $img = PreImg::select('pre_imagenes.nombreImagen as imagen','pre_imagenes.id as id')->where('pre_imagenes.id_usuario', Auth::user()->id)->get();
        $output = '<div class="row">';
        foreach($img as $row)
         {
          $url =   url('/imagenesPreproductos/'.$row['imagen']);
          $output .= '
            <div>
                <ul>
                    <li>
                    <img src="'. $url.'" width="60" height="60"/>
                    <a  onclick="eliminarPreImagenes('.$row['id'].')" ><i class="fa fa-times " aria-hidden="true" ></i></a>
                    </li>
                </ul>
            </div>';

          /* $output .= '
          <div class="col-md-2" style="margin-bottom:16px;">
           <img src="'. $url.'" class="img-upload" />
           <a onclick="eliminarPreImagenes('.$row['id'].')" ><i class="fa fa-times " aria-hidden="true" ></i></a>
          </div>
          ';*/
         }

         return response()->json($output);
    }

    public function calculoPropuestas($presio){
          //condiciones de pago
        /*if ($factura['presio'] >= 100000) {
            $comision = $factura['presio'] * 7 / 100;
        } elseif ($factura['presio'] <= 100000) {
            $comision = $factura['presio'] * 10 / 100;
        }*/
        $comision = (($presio * 5) / 100) +  500;

        $iva            = $comision * 19 / 100;
        $pasarela       = ($presio * 3 / 100) + 1000;
        $comision_final = $comision - $iva;
        $cliente        = $presio - ($comision + $pasarela);
        $total          = $cliente + $comision_final + $pasarela + $iva + $comision;

        $result = [
            'iva' => $iva,
            'pasarela' => $pasarela,
            'conmision_total' => $comision_final,
            'comision_cliente' => $cliente,
            'total' => $total,
            'comision_propuesta' => $comision
        ];
        return $result;
    }

    public function depurarPreImagenes()
    {
        PreImg::where('id_usuario', Auth::user()->id)->delete();
    }

}
