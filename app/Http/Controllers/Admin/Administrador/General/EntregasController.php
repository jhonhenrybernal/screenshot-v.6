<?php

namespace App\Http\Controllers\Admin\Administrador\General;

use App\EstadoEntrega;
use App\productos;
use App\propuestas;
use App\mensajes;
use App\Pagos;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


class EntregasController extends Controller
{

    public function index()
    {
        $this->homeAdmin()->endViewNotify('entrega');
        return view('admin.general.entregas');
    }

    private function entregasAll(){
        return EstadoEntrega::join('estados_anuncios', 'estado_entrega.estado', 'estados_anuncios.id')
            ->join('users as userEntrega', 'estado_entrega.id_usuario_entregas', 'userEntrega.id')
            ->join('users as userRecibe', 'estado_entrega.id_usuario', 'userRecibe.id')
            ->join('productos', 'estado_entrega.id_producto', 'productos.id')
            ->whereIn('estado_entrega.estado', [19, 22, 24, 32, 31])
            ->select('productos.id as id', 'estado_entrega.estado as std', 'estados_anuncios.estado as estado', 'userEntrega.name as nombreEntrega', 'userRecibe.name as nombreRecibe', 'userRecibe.apellido as apellidoRecibe', 'userEntrega.apellido as apellidoEntrega', 'estado_entrega.fecha as fecha', 'productos.mision as mision')->get();

    }

    public function tableView(Request $request)
    {
        return $this->entregasAll();
    }


    public function view($id)
    {
        $producto = productos::with('usuario', 'categoria', 'estado', 'imagenendeVarios', 'mensajes', 'propuestas','entregas')->select('productos.*')->where('productos.id', $id)->orderBy('id', 'desc')->first();
        $productoFinal = productos::with('usuario', 'categoria', 'estado', 'imagenendeVarios', 'mensajes', 'propuestas','entregas')->select('productos.*')->where('productos.id', $producto['id_producto_propuesta_final'])->orderBy('id', 'desc')->first();

        return response()->json(['productoUno' => $producto,'productoDos' => $productoFinal]);



        $entrega = EstadoEntrega::join('proveedores_pago', 'pago_prd.id_proveedor', 'proveedores_pago.id')
        ->join('productos as producto', 'pago_prd.id_producto', 'producto.id')
        ->join('productos as propuesta', 'pago_prd.id_propuesta', 'propuesta.id')
        ->join('users', 'pago_prd.id_usuario', 'users.id')
        ->join('estados_anuncios', 'pago_prd.status', 'estados_anuncios.id')
        ->select(
            'pago_prd.id',
            'proveedores_pago.nombre as prov',
            'producto.mision as misionProducto',
            'propuesta.mision as misionPropuesta',
            'users.name as nombre',
            'users.apellido as apellido',
            'pago_prd.metodo_pago as mpago',
            'pago_prd.comision_screen_total as tScreen',
            'pago_prd.cliente_ganancia as tClient',
            'estados_anuncios.estado as estado',
            'pago_prd.status as idStatus',
            'pago_prd.transactionId as transactionId'
        )
        ->where('pago_prd.id', $id)->first();

        $recibe = EstadoEntrega::join('proveedores_pago', 'pago_prd.id_proveedor', 'proveedores_pago.id')
        ->join('productos as producto', 'pago_prd.id_producto', 'producto.id')
        ->join('productos as propuesta', 'pago_prd.id_propuesta', 'propuesta.id')
        ->join('users', 'pago_prd.id_usuario', 'users.id')
        ->join('estados_anuncios', 'pago_prd.status', 'estados_anuncios.id')
        ->select(
            'pago_prd.id',
            'proveedores_pago.nombre as prov',
            'producto.mision as misionProducto',
            'propuesta.mision as misionPropuesta',
            'users.name as nombre',
            'users.apellido as apellido',
            'pago_prd.metodo_pago as mpago',
            'pago_prd.comision_screen_total as tScreen',
            'pago_prd.cliente_ganancia as tClient',
            'estados_anuncios.estado as estado',
            'pago_prd.status as idStatus',
            'pago_prd.transactionId as transactionId'
        )
        ->where('pago_prd.id', $id)->first();
    }

    public function cancelarFinal($id_producto, $status)
    {
        $productos = productos::where('id', $id_producto)->first();
        $productoContrario = productos::where('id', $productos->id_producto_propuesta_final)->first();
        $entrega = EstadoEntrega::where('id_producto', $productos->id)->whereIn('estado',[31,34])->first();
        $entregaContrario = EstadoEntrega::where('id_producto', $productos->id_producto_propuesta_final)->whereIn('estado', [31, 34])->first();

        //verifica si es ununcio o mision
        switch ($productos['id_tipo']) {
            case 1:
                $propuestas = propuestas::where('id_producto', $productos->id)->where('id_producto_propuesta', $productos->id_producto_propuesta_final)->first();

                if ($status == 1) {
                    $canceladofinal = 2;
                    $canceladofinalContrario = 22;
                }elseif ($status == 2) {
                    $canceladofinal = 32;
                    $canceladofinalContrario = 19;
                }

                $productos->id_estados_anuncios = $canceladofinal;
                $productos->fecha_actualizada = date('Y-m-d h:i');

                $productoContrario->id_estados_anuncios = $canceladofinalContrario;
                $productoContrario->fecha_actualizada = date('Y-m-d h:i');

                $propuestas->estado = $canceladofinal;

                $entrega->estado = $canceladofinal;

                $entregaContrario->estado = $canceladofinalContrario;


                $mensaje = mensajes::where('id_producto', $productos->id)->where('id_usuario', $productos->usuario->id)->where('id_estado_anuncio', '19')->first();

                if (!empty($mensaje)) {
                    $last    = last(json_decode($mensaje['mensajes_value']));
                    $aMeta[] = [
                        'titulo'  => 'Nuevo mensaje',
                        'msm'     => 'El producto fue cancelado por el administrador.',
                        'date'    => date('Y-m-d h:i'),
                        'article' => 'panel-danger',
                        'icon'    => 'glyphicon-plus',
                        'user'    => 'Administrador: ' . Auth::user()->name . ' ' . Auth::user()->apellido,
                    ];


                    $aMetaValue                 = array_merge(json_decode($mensaje['mensajes_value'], true), $aMeta);
                    $mensaje->mensaje           = 'Mensaje de administrador';
                    $mensaje->id_usuario        = Auth::user()->id;
                    $mensaje->mensajes_value    = json_encode($aMetaValue);
                    $mensaje->save();

                }
                if ($productos->id_tipo == 1) {
                    $estadopago    = Pagos::where('id_producto', $productos->id)->where('id_propuesta', $productos->id_producto_propuesta_final)->first();
                } else if ($productos->id_tipo == 2) {
                    $estadopago    = Pagos::where('id_producto', $productos->id_producto_propuesta_final)->where('id_propuesta', $productos->id)->first();
                }

                $estadopago->status = $canceladofinal;
                $estadopago->estado_msg = 'El producto fue cancelado por el administrador.';


                $productos->save();
                $productoContrario->save();
                $propuestas->save();
                $entrega->save();
                $entregaContrario->save();
                $estadopago->save();
                //iniciar entregas
                $msm = array(
                    ['usuario_id' => $productos['id_usuario'], 'prd' => $productos['id'], 'estado' => $canceladofinal, 'msm' => 'Producto cancelado'],
                    ['usuario_id' => $productoContrario['id_usuario'], 'prd' => $productoContrario['id'], 'estado' => $canceladofinalContrario, 'msm' => 'Propuesta cancelado.']
                );

                foreach ($msm as $m) {
                    $this->enviarNotificacion()->notificar($m['usuario_id'], $m['prd'], $m['estado'], $m['msm']);
                }

                return response()->json(['status' => true, 'msm' => '', 'prd' => productos::join('users', 'productos.id_usuario', 'users.id')
                ->join('estados_anuncios', 'productos.id_estados_anuncios', 'estados_anuncios.id')
                ->select('productos.id_tipo as tipo', 'productos.id_estados_anuncios', 'estados_anuncios.estado as estado', 'users.id as id_usuario', 'users.name as name', 'users.apellido as apellido', 'productos.id as id', 'productos.mision as mision', 'productos.created_at as created_at')->orderBy('created_at', 'desc')->get()]);

                case 2:

                    $propuestas = propuestas::where('id_producto_propuesta', $productos->id)->where('id_producto', $productos->id_producto_propuesta_final)->first();

                    if ($status == 1) {
                        $canceladofinal = 22;
                        $canceladofinalContrario = 2;
                    }elseif ($status == 2) {
                        $canceladofinal = 32;
                        $canceladofinalContrario = 19;
                }

                $productos->id_estados_anuncios = $canceladofinal;
                $productos->fecha_actualizada = date('Y-m-d h:i');

                $productoContrario->id_estados_anuncios = $canceladofinalContrario;
                $productoContrario->fecha_actualizada = date('Y-m-d h:i');

                $propuestas->estado = $canceladofinal;

                $entrega->estado = $canceladofinal;

                $entregaContrario->estado = $canceladofinalContrario;

                $mensaje = mensajes::where('id_producto', $productos->id)->where('id_usuario',$productos->usuario->id)->where('id_estado_anuncio','19')->first();

                if (!empty($mensaje)) {
                    $last    = last(json_decode($mensaje['mensajes_value']));
                    $aMeta[] = [
                        'titulo'  => 'Nuevo mensaje',
                        'msm'     => 'El producto fue cancelado por el administrador.',
                        'date'    => date('Y-m-d h:i'),
                        'article' => 'panel-danger',
                        'icon'    => 'glyphicon-plus',
                        'user'    => 'Administrador: ' . Auth::user()->name . ' ' . Auth::user()->apellido,
                    ];


                    $aMetaValue                 = array_merge(json_decode($mensaje['mensajes_value'], true), $aMeta);
                    $mensaje->mensaje           = 'Mensaje de administrador';
                    $mensaje->id_usuario        = Auth::user()->id;
                    $mensaje->mensajes_value    = json_encode($aMetaValue);
                    $mensaje->save();
                }

                if ($productos->id_tipo == 1) {
                    $estadopago    = Pagos::where('id_producto', $productos->id)->where('id_propuesta',$productos->id_producto_propuesta_final)->first();
                } else if ($productos->id_tipo == 2) {
                    $estadopago    = Pagos::where('id_producto', $productos->id_producto_propuesta_final)->where('id_propuesta', $productos->id)->first();
                }

                $estadopago->status = $canceladofinal;
                $estadopago->estado_msg = 'El producto fue cancelado por el administrador.';

                $productos->save();
                $productoContrario->save();
                $propuestas->save();
                $entrega->save();
                $estadopago->save();


                $msm = array(
                    ['usuario_id' => $productos['id_usuario'], 'prd' => $productos['id'], 'estado' => $canceladofinal, 'msm' => 'Propuesta cancelado'],
                    ['usuario_id' => $productoContrario['id_usuario'], 'prd' => $productoContrario['id'], 'estado' => $canceladofinalContrario, 'msm' => 'Anuncio cancelado, producto pasa a estar disponible.']
                );

                foreach ($msm as $m) {
                    $this->enviarNotificacion()->notificar($m['usuario_id'], $m['prd'], $m['estado'], $m['msm']);
                }
                return response()->json(['status' =>true , 'msm' => '', 'prd' => $this->entregasAll()]);
        }
        try {
        } catch (\Throwable $th) {

            return response()->json(['status' => false, 'msm' => $th->getMessage(), 'prd' => $this->entregasAll()]);
        }
    }

}
