<?php

namespace App\Http\Controllers\Admin\Administrador\General;

use App\Pagos;
use App\productos;
use App\propuestas;
use App\Proveedores;
use App\tipoDocumento;
use App\User;
use App\img;
use App\Http\Controllers\Controller;
use App\MediosPago;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Mail;
use Validator;
use App\EntidadPago;
use App\Facturas;

class PagosController extends Controller
{


    public function pagos(){
        $this->homeAdmin()->endViewNotify('pago');
        return view('admin.general.pagos');
    }

    private function pagosAll(){
        return Pagos::join('proveedores_pago', 'pago_prd.id_proveedor', 'proveedores_pago.id')
            ->join('estados_anuncios', 'pago_prd.status', 'estados_anuncios.id')
            ->join('users', 'pago_prd.id_usuario', 'users.id')
            ->select('proveedores_pago.nombre as prov', 'pago_prd.id', 'users.name as nombre', 'users.apellido as apellido', 'estados_anuncios.estado as std', 'pago_prd.metodo_pago as mpago', 'pago_prd.comision_screen_total as tScreen', 'pago_prd.cliente_ganancia as tClient', 'pago_prd.status as idStatus', 'pago_prd.id_propuesta as idPropuesta')->get();

    }

    public function tableView(Request $request)
    {
        return $this->pagosAll();
    }

    public function view($id){
        return Pagos::join('proveedores_pago', 'pago_prd.id_proveedor', 'proveedores_pago.id')
            ->join('productos as producto', 'pago_prd.id_producto', 'producto.id')
            ->join('productos as propuesta', 'pago_prd.id_propuesta', 'propuesta.id')
            ->join('users', 'pago_prd.id_usuario', 'users.id')
            ->join('estados_anuncios', 'pago_prd.status', 'estados_anuncios.id')
            ->select(
                'pago_prd.id',
                'proveedores_pago.nombre as prov',
                'producto.mision as misionProducto',
                'propuesta.mision as misionPropuesta',
                'users.name as nombre',
                'users.apellido as apellido',
                'pago_prd.metodo_pago as mpago',
                'pago_prd.comision_screen_total as tScreen',
                'pago_prd.cliente_ganancia as tClient',
                'estados_anuncios.estado as estado',
                'pago_prd.status as idStatus',
                'pago_prd.transactionId as transactionId')
            ->where('pago_prd.id',$id)->first();

    }





    /*--De aca abajo procesos antiguos --*/

    public function procesarPago($request)
    {

        //debe entrar id producto y id propuesta
        /* $producto  = productos::find($request['id_producto']);
        $user      = User::find(Auth::user()->id);
        $propuesta = propuestas::where('id_producto_propuesta', $producto['id'])->first();
        $img = img::where('id_producto',$request['id_producto'])->first();
        $tipo      = substr($producto->categoria->tipo, -0, -3);

        $protocol = isset($_SERVER["HTTPS"]) ? 'https' : 'http';
        */
        //test pago//

        $result = [
            'procesarPago' => 'http://localhost:8000/api/pago/cambio-estado/' . $request['id_producto'],
            'result'       => true,
            'mensaje'      => 'Datos fueron actualizados',
            'proveedor'    => 'Test pago'
        ];
        return response()->json($result);
        //----//




        /*procesar pago socn pago sinteligentes*/
        /*$params = [
            'Reference1'      => $this->postProductos()->generarCodigo() . $producto['id_producto'],
            'Amount'          => $propuesta['presio'],
            'Description'     => $producto['misionDetalladamente'],
            'FirstName'       => Auth::user()->name,
            'LastName'        => Auth::user()->apellido,
            'Mobile'          => Auth::user()->telefono_local,
            'Email'           => Auth::user()->email,
            'City'            => Auth::user()->ciudades['ciudad'],
            'Region'          => Auth::user()->departamento['nombre'],
            'Address'         => Auth::user()->direccion,
            'ReturnURL'       => $protocol . '://' . $_SERVER['HTTP_HOST'] . '/api/pago/status/' . Crypt::encrypt(Auth::user()->id),
            'IdNumber'        => Auth::user()->numero_documento,
            'IdType'          => tipoDocumento::where('id', Auth::user()->tipo_documento)->value('sigla'),
            'SourceId'        => 'screenshot',
            'AccountPassword' => '1234',
            'Language'        => 'es',
            'Reference2'      => $id_producto,
            'Reference3'      => 1,
        ];

        $return $this->soap()->processPayment($params);*/


        /*procesar con coingbase*/
        $checkoutData = [
            'name' => $producto['mision'],
            'description' => $producto['misionDetalladamente'],
            "logo_url" => $protocol . '://' . $_SERVER['HTTP_HOST'] . "/imagenesproductos/" . $img['nombreImagen'],
            'pricing_type' => 'fixed_price',
            'local_price' => [
                'amount' => $propuesta['presio'],
                'currency' => $request['moneda']
            ],
            'requested_info' => []
        ];

        $coinResultPay = $this->coinbase()->sendPayment($request, $checkoutData);

        return $coinResultPay;
    }

    public function actualizarUsuariorPago(Request $request)
    {
        foreach (request()->all() as $validate) {

            if (!empty($validate)) {
                $status = true;
            }
        }

        if ($status) {
            $usert                   = User::find($request['id_user']);
            $usert->name             = $request['nombre'];
            $usert->lastname         = $request['apellido'];
            $usert->tipo_documento   = $request['tipo_documento'];
            $usert->numero_documento = $request['numero_documento'];
            $usert->email            = $request['email'];
            $usert->id_ciudad        = $request['ciudad'];
            $usert->id_departamento  = $request['departamento'];
            $usert->celular          = $request['numero_telefonico'];
            $usert->direccion        = $request['direccion'];
            $usert->barrio           = $request['barrio'];
            $usert->save();

            $result = $this->procesarPago($request);
        } else {
            $result = [
                'result'  => false,
                'mensaje' => 'Debe estar completo el fomulario',
            ];
        }

        return response()->json($result);
    }

    public function estadoPago(Request $request, $token)
    {
        //respuestas pago inteligente

        /*• Iniciada: 0
        o La transacción inicialmente esta en este estado cuando el usuario inicia el proceso de
        pago. Si el usuario cancela la operación sin finalizar el proceso, queda en estado 0.

        • Pendiente: 1
        o La transacción queda en pendiente hasta recibir respuesta de las redes financieras.
        • Aprobada: 2
        o Cuando una transacción es aprobada por las redes financieras queda en este estado.
        • Rechazada: 3
        o La transacción se rechaza por parte de las redes financieras.
        */

        //ref2=propuestas , ref3=provedores
        $Prov       = Proveedores::find($request['Ref3']);
        $prodMision = productos::find($request['Ref2']);

        $propuestaCliente = propuestas::where('id_producto_propuesta', $prodMision['id'])->first();
        $prodCliente      = productos::find($propuestaCliente['id_producto']);
        $prodCliente->fecha_actualizada    = date("Y-m-d");
        $prodClientePropuesta = productos::find($prodMision['id']);
        $prodClientePropuesta->fecha_actualizada    = date("Y-m-d");
        $userCliente      = User::find($prodCliente['id_usuario']);

        $user = User::where('id', $prodMision['id_usuario'])->first();
        //condiciones de pago
        if ($propuestaCliente['presio'] >= 100000) {
            $comision = $propuestaCliente['presio'] * 7 / 100;
        } elseif ($propuestaCliente['presio'] <= 100000) {
            $comision = $propuestaCliente['presio'] * 10 / 100;
        }
        $iva            = $comision * 19 / 100;
        $pasarela       = ($propuestaCliente['presio'] * 3 / 100) + 1000;
        $comision_screen_total = $comision - $iva;
        $cliente_ganancia = $propuestaCliente['presio'] - ($comision + $pasarela);

        $respuesta = [
            'nombre_Usuario_misionero' => $user['name'] . ' ' . $user['apellido'],
            'nombre_Usuario_cliente'   => $userCliente['name'] . ' ' . $userCliente['apellido'],
            'producto'                 => $prodMision['mision'],
            'destalles_mision:'        => $prodMision['misionDetalladamente'],
            'estado'                   => $request['Status'],
            'Msg'                      => $request['Msg'],
            'tipo_pago'                => $request['MethodPayment'],
            'Aut_pse'                  => $request['Aut'],
            'valor_de_pago'            => $request['Total'],
            'num_transaccion'          => $request['TransactionId'],
            'avatar'                   => $user['avatar'],
            'url'                      => 'propuestas/ver/' . $request['Ref2'],
        ];

        $iniciarRecibido = 32;
        $pagoRealizado = 26;
        $pagoRechazado = 21;
        $pagoIniciado = 20;
        $productoEntregado = 23;
        $iniciarEntrega = 19;

        //parametrizacion notificaciones
        $propuesta        = propuestas::where('id_producto_propuesta', $request['Ref2'])->first();
        $idUsuarioPropuesta = $propuesta['id_usuario_propuesta'];
        $idProductoPropuesta  = $propuesta['id_producto_propuesta'];

        //entregas recibe
        $productoAlert = productos::where('id', $propuesta['id_producto'])->first();
        $idUsuarioProducto =  $productoAlert['id_usuario'];
        $idProductoProducto  = $productoAlert['id'];


        if ($request['Status'] == 0) { //2) { • Aprobada: 2


            $pago = Pagos::create([
                'id_proveedor'  => $request['Ref3'],
                'fecha_pago'    => date('Y-m-d H:i:s'),
                'id_producto'   => $propuestaCliente['id_producto'],
                'id_propuesta'  => $request['Ref2'],
                'id_usuario'    => $idUsuarioPropuesta,
                'status'        => $pagoRealizado,
                'estado_msg'    => $request['Msg'],
                'TransactionId' => $request['TransactionId'],
                'metodo_pago'   => $request['MethodPayment'],
                'iva' => $iva,
                'comision_pasarela'   => $pasarela,
                'comision_screen_total' => $comision_screen_total,
                'cliente_ganancia' => $cliente_ganancia,
            ]);


            //se confirma la propuesta con el producto
            $prodCliente->id_estados_anuncios = $iniciarRecibido;
            $prodCliente->id_producto_propuesta_final = $prodMision['id'];
            $prodCliente->save();

            //se confirma la propuesta
            $prodClientePropuesta->id_estados_anuncios = $iniciarEntrega;
            $prodClientePropuesta->id_producto_propuesta_final = $propuestaCliente['id_producto'];
            $prodClientePropuesta->save();
            $id_stados_anuncios = $iniciarRecibido;

            $propuestaCliente->estado = $iniciarEntrega;
            $propuestaCliente->save();

            //enviar alerta a propuesta persona
            $this->enviarNotificacion()->notificar($idUsuarioPropuesta, $idProductoPropuesta, $id_stados_anuncios, 'Inicio de envio');

            //enviar alerta a producto persona
            $this->enviarNotificacion()->notificar($idUsuarioProducto, $idProductoProducto, $id_stados_anuncios, 'Inicio de entrega');



            //Enviando por pago realizado
            $this->entregas()->iniciarEntrega($idProductoPropuesta, $propuesta['id_usuario_producto'], $idUsuarioPropuesta, $iniciarEntrega, 'Enviando');


            //Recibiendo por pago realizado
            $this->entregas()->iniciarEntrega($idProductoProducto, $idUsuarioPropuesta, $idUsuarioProducto, $iniciarRecibido, 'Recibiendo');
        } elseif ($request['Status'] == 1) { //• Pendiente: 1
            $prodCliente->id_estados_anuncios = $pagoRechazado;
            $prodCliente->save();
            $prodClientePropuesta->id_estados_anuncios = $pagoRechazado;
            $prodClientePropuesta->save();
            $id_stados_anuncios = $pagoRechazado;
            //enviar alerta a propuesta persona
            $this->enviarNotificacion()->notificar($idUsuarioPropuesta, $idProductoPropuesta, $id_stados_anuncios, 'Su propuesta fue aprovado');
        } elseif ($request['Status'] == 21) {
            $prodCliente->id_estados_anuncios = $pagoRealizado;
            $prodCliente->save();
            $prodClientePropuesta->id_estados_anuncios = $iniciarEntrega;
            $prodClientePropuesta->save();

            $id_stados_anuncios = $pagoRealizado;
            //enviar alerta a propuesta persona
            $this->enviarNotificacion()->notificar($idUsuarioPropuesta, $idProductoPropuesta, $id_stados_anuncios, 'Producto rechazado');

            //enviar alerta a producto persona
            $this->enviarNotificacion()->notificar($idUsuarioProducto, $idProductoProducto, $id_stados_anuncios, 'Producto rechazado');
        } elseif (/*$request['Status'] == 0 temporal*/false) { //Iniciado: 20
            $prodCliente->id_estados_anuncios = $pagoIniciado;
            $prodCliente->save();
            $prodClientePropuesta->id_estados_anuncios = $pagoIniciado;
            $prodClientePropuesta->save();

            $id_stados_anuncios = $pagoIniciado;

            //enviar alerta a propuesta persona
            $this->enviarNotificacion()->notificar($idUsuarioPropuesta, $idProductoPropuesta, $id_stados_anuncios, 'Pago iniciado');

            //enviar alerta a producto persona
            $this->enviarNotificacion()->notificar($idUsuarioProducto, $idProductoProducto, $id_stados_anuncios, 'Pago iniciado');
        }




        $datos = [
            'email'          => $userCliente['email'],
            'nombre_Usuario' => $userCliente['name'] . ' ' . $userCliente['apellido'],
            'from'           => 'no-repply@mail.com.co',
            'subject'        => 'SCREENSHOT-Pago fue ' . $request['Msg'],
        ];

        $vistaMensage = [
            'nombre_Usuario_cliente' => $userCliente['name'] . ' ' . $userCliente['apellido'],
            'estado'                 => $request['Msg'],
            'TransactionId'          => $request['TransactionId'],
            'tipo_pago'              => $request['MethodPayment'],
            'Aut_pse'                => $request['Aut'],
            'producto'               => $prodMision['mision'],
            'destalles_mision:'      => $prodMision['misionDetalladamente'],
            'url'                    => 'propuestas/ver/' . Crypt::encrypt($prodMision['id']),
        ];

        $this->mail($datos, $vistaMensage, 'emails.respuesta_pago');
        return view('pago.respuesta', ['datos_respuesta' => $respuesta]);
    }

    public function estadoPagoInterno($id_producto, $id_proveedor)
    {

        $idProd = Crypt::descrypt($id_producto);
        $idProv = Crypt::descrypt($id_proveedor);
        $Prov   = Proveedores::where('id', $idProv)->first();
        $prod   = productos::where('id', $idProd)->first();
        $user   = User::where('id', $prod['id_usuario'])->first();

        $respuesta = [
            'nombre_Usuario'    => $user['name'] . ' ' . $user['apellido'],
            'producto'          => $prod['mision'],
            'destalles_mision:' => $prod['misionDetalladamente'],
            'estado'            => $request['Msg'],
            'tipo_pago'         => $request['MethodPayment'],
            'Aut_pse'           => $request['Aut'],
            'url'               => '/api/pago/statusInterno/' . Crypt::encrypt($prod['id']),
        ];

        return view('pago.respuesta_interno', ['datos_respuesa' => $respuesta]);
    }

    public function mail($datos, $vistaMensage, $blade)
    {
        Mail::send($blade, ['msm' => $vistaMensage], function ($message) use ($datos) {
            $message->subject($datos);
            $message->to($datos['email']);
            $message->from($datos['from'], $datos['nombre_Usuario']);
            $message->subject($datos['subject']);
        });
    }

    public function verificarEstadoPendiente(Request $request)
    {
        $status        = $this->soap()->clientStatusPayment($request->action);

        $Estadopago    = Pagos::where('status', 1)->where('id_producto', $request['Ref2'])->where('id_usuario', Auth::user()->id)->first();
        foreach ($status as $key => $value) {
            if ($value->Status = 1) {
                return response()->json(false);
            } elseif ($value->Status = 2) {

                $pago         = Pagos::find($Estadopago['id']);
                $pago->estado = $request['Status'];
                $pago->save();

                return response()->json(true);
            }
        }
    }

    public function medios()
    {
        $bancaria = 1;
        $giros = 2;
        $app = 3;

        $entidadBancaria = EntidadPago::where('tipo', $bancaria)->orderBy('entidad', 'DESC')->get();
        $entidadGiros = EntidadPago::where('tipo', $giros)->orderBy('entidad', 'DESC')->get();
        $entidadApp = EntidadPago::where('tipo', $app)->orderBy('entidad', 'DESC')->get();
        $pagoMedio = MediosPago::where('id_usuario', Auth::user()->id)->first();

        $entidadMedioPago = array();

        if (!empty($pagoMedio->id_entidad_medio_pago)) {
            $entidadMedioPago = json_decode($pagoMedio->id_entidad_medio_pago);
        }

        return view('pago.medios', ['bancaria' => $entidadBancaria, 'giros' => $entidadGiros, 'app' => $entidadApp, 'pagoMedio' => $pagoMedio, 'entidadMedioPago' => $entidadMedioPago]);
    }

    public function addPagoParameter(Request $request)
    {
        $pagoMedio = MediosPago::where('id_usuario', Auth::user()->id)->first();
        if (empty($pagoMedio->id)) {
            $arequest = $request->all();

            //agrupa todo los id key
            $MediosEntPago = array();
            foreach ($arequest as $key => $value) {
                if (is_numeric($key)) {
                    $MediosEntPago[] = $key;
                }
            }

            $pagoParams = MediosPago::create([
                'id_usuario'  => Auth::user()->id,
                'numero_doc'    => $request['numeroDocumento'],
                'numero_cel'   => $request['numeroCel'],
                'numero_cuenta'  => $request['numeroCuenta'],
                'entidad_bancaria_id'   => $request['entidadBancariaId'],
                'Nombre_titular_cuenta_bancaria'    => $request['nombreTitular'],
                'tipo_cuenta_bancaria'        => $request['tipoCuenta'],
                'id_entidad_medio_pago'  =>  json_encode($MediosEntPago),
                'fecha_actualizado' => date('Y-m-d H:i:s')
            ]);
        } else {
            $arequest = $request->all();

            //agrupa todo los id key
            $MediosEntPago = array();
            foreach ($arequest as $key => $value) {
                if (is_numeric($key)) {
                    $MediosEntPago[] = $key;
                }
            }

            $pagoMedio->numero_doc = $request['numeroDocumento'];
            $pagoMedio->numero_cel = $request['numeroCel'];
            $pagoMedio->numero_cuenta = $request['numeroCuenta'];
            $pagoMedio->entidad_bancaria_id = $request['entidadBancariaId'];
            $pagoMedio->Nombre_titular_cuenta_bancaria = $request['nombreTitular'];
            $pagoMedio->tipo_cuenta_bancaria = $request['tipoCuenta'];
            $pagoMedio->id_entidad_medio_pago = json_encode($MediosEntPago);

            $pagoMedio->fecha_actualizado = date('Y-m-d H:i:s');
            $pagoMedio->save();
        }

        return redirect()->action('misionesController@indexMision');
    }

    public function depositarPago(Request $request)
    {

        if (empty($request['deposito'])) {

            $result = array('status' => false, 'msm' => 'Seleccione un deposito por favor.');
            return response()->json($result);
        }
        if (empty($request['metodo'])) {

            $result = array('status' => false, 'msm' => 'Seleccione el metodo de pago por favor.');
            return response()->json($result);
        }

        $depositos = array_filter(explode(',', $request['deposito']), 'strlen');
        foreach ($depositos as  $deposito) {
            $solicitarPago = Pagos::find($deposito);
            $solicitarPago->metodo_pago = $request['metodo'];
            $solicitarPago->status = 29;
            $solicitarPago->save();
        }
        $result = array('status' => true, 'msm' => 'Solicitud de deposito en proceso.');
        return response()->json($result);
    }

    public function depositoProceso($id,$status){

        try {
            //code...
            if ($status == 'si') {
                $estadoPago = 28;//realizado
            }else{
                $estadoPago = 27;//no realizado
            }
            // pagos
            $pagos = Pagos::where('id_propuesta',$id)->first();
            $pagos->status = $estadoPago;
            $pagos->update();

            //productos a su propuesta
            $productos = productos::find($id);
            $productos->id_estados_anuncios = $estadoPago;
            $productos->update();

            //propuestas
            $propuestas = propuestas::where('id_producto_propuesta', $id)->where('id_producto', $productos['id_producto_propuesta_final'])->first();
            $propuestas->estado = $estadoPago;
            $propuestas->update();

            if ($status == 'si') {

                //generar factura
                $numProducto = $productos->id;
                $sumProductTotal = $productos->id + 1;
                $factura = new Facturas();
                $factura->id_producto    = $productos->id;
                $factura->id_usuario     = $productos->id_usuario;
                $factura->fecha_generada = date('Y-m-d');
                $factura->estado         = $estadoPago;
                $factura->numero_factura = date('m') . $productos->id_tipo . $numProducto . '_' . $sumProductTotal;
                $factura->save();


                //notificaiones plataforma
                $idUsuarioProducto =  $productos['id_usuario'];
                $idProductoProducto  = $productos['id'];
                $this->enviarNotificacion()->notificar($idUsuarioProducto, $idProductoProducto, $estadoPago, 'Pago fue aprovado y depositado');

                //notificacion via mail
                $userCliente   = User::find($productos['id_usuario']);
                $datos = [
                    'email'          => $userCliente['email'],
                    'nombre_Usuario' => $userCliente['name'] . ' ' . $userCliente['apellido'],
                    'from'           => 'no-repply@mail.com.co',
                    'subject'        => 'SCREENSHOT-Deposito realizado ',
                ];

                $vistaMensage = [
                    'nombre_Usuario_cliente' => $userCliente['name'] . ' ' . $userCliente['apellido'],
                    'estado'                 => 'Pago fue aprovado y depositado',
                    'producto'               => $productos['mision'],
                ];
                $this->mail($datos, $vistaMensage, 'emails.deposito_realizado');

            }
            return response()->json(['status' => true, 'msm' =>'', 'prd' =>  $this->pagosAll()]);
        } catch (\Throwable $th) {
            return response()->json(['status' => false,'msm' => $th->getMessage(), 'prd' => $this->pagosAll()]);
        }
    }

    public function descargarFactura($id)
    {
        $factura = Facturas::join('productos', 'facturas.id_producto', 'productos.id')
        ->join('pago_prd', 'facturas.id_producto', 'pago_prd.id_propuesta')
        ->join('propuestas_aproducto', 'facturas.id_producto', 'propuestas_aproducto.id_producto_propuesta')
        ->select('propuestas_aproducto.presio as presio', 'productos.mision as mision', 'facturas.numero_factura as numero_factura', 'pago_prd.metodo_pago as metodoPago', 'propuestas_aproducto.presio as presioProducto')->where('facturas.id_producto', $id)->first();
        $propuestas = propuestas::where('id_producto_propuesta', $id)->first();
        //condiciones de pago

        //$total_calculo =  $this->postProductos()->calculoPropuestas($factura['presio']);
        $pagos = Pagos::where('id_propuesta', $id)->first();
        //$iva            = $total_calculo['iva'];
        //$pasarela       = $total_calculo['pasarela'];
        $cliente        = $pagos['cliente_ganancia'];
        $comision_screen = $pagos['comision_screen_total'];
        $total          =  $cliente + $comision_screen;
        //$comision       = 1;

        //$entidad = EntidadPago::find($factura['metodoPago']);
        $view       = view('pdf.factura-pago')->with(compact('factura', 'cliente', 'total', 'comision_screen', 'propuestas'));
        $pdf        = \App::make('dompdf.wrapper');
        $paper_size = array(0, 0, 700, 600); //ajuste de hoja
        $pdf->loadHTML($view)->setPaper($paper_size);

        return $pdf->stream();
    }
}
