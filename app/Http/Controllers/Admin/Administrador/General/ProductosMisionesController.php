<?php

namespace App\Http\Controllers\Admin\Administrador\General;

use App\categoria;
use App\Facturas;
use App\Http\Controllers\Controller;
use App\mensajes;
use App\productos;
use App\propuestas;
use App\User;
use App\EstadoEntrega;
use App\EntidadPago;
use App \MediosPago;
use App\Pagos;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Datatables;

class ProductosMisionesController extends Controller
{

	public function index(){
        $this->homeAdmin()->endViewNotify('prdPropu');
		return view('admin.general.productos-misiones');
	}


    public function tableView(Request $request)
    {
        return productos::join('users', 'productos.id_usuario', 'users.id')
        ->join('estados_anuncios', 'productos.id_estados_anuncios', 'estados_anuncios.id')
        ->select('productos.id_tipo as tipo', 'productos.id_estados_anuncios','estados_anuncios.estado as estado', 'users.id as id_usuario', 'users.name as name','users.apellido as apellido','productos.id as id','productos.mision as mision','productos.created_at as created_at')->orderBy('created_at', 'desc')->get();

    }


    public function view($id){
        $prd = productos::find($id)->first();
        return productos::with('usuario', 'categoria', 'estado', 'imagenendeVarios', 'mensajes', 'propuestas')->select('productos.*')->where('productos.id', $id)->orderBy('id','desc')->first();
    }

    public function aprovar($id_producto){
        $productos = productos::findOrFail($id_producto);
        $id_usuario = $productos['id_usuario'];
        $user      = User::findOrFail($id_usuario);
        $mensaje   = mensajes::where('id_producto',$id_producto)->first();
        $propuestaAprovada =  8;
        $productoCurso = 2;
        $nuevoMensajePropuesta = 11;
        //propuestas actualizada
        if ($productos['id_estados_anuncios'] == 6 or $productos['id_estados_anuncios'] == 8) {
            $mensaje->id_usuario        = $id_usuario;
            $mensaje->id_producto       = $id_producto;
            $mensaje->id_estado_anuncio = $propuestaAprovada;
            $mensaje->mensaje           = 'Su propuesta fue aprovado';
            $mensaje->save();

            $idMsm = $mensaje->id;

            $productos->id_estados_anuncios = $propuestaAprovada;
            $productos->id_mensaje          = $idMsm;
            $productos->save();

            $id_estado = $productos['id_estados_anuncios'];

            $users = [
                'nombre_Usuario' => $user['name'],
                'email'          => $user['email'],
                'from'           => 'no-repply@mail.com.co',
                'subject'        => 'SCREENSHOT-Su propuesta fue aprovado ',
            ];
            $mensage = [
                'mision'      => 'propuesta Aprovada',
                'descripcion' => $productos['misionDetalladamente'],
            ];

            $this->postProductos()->mail($users, $mensage, 'emails.propuesta_aprovada');

            $alertas = [
                'id_stados_anuncios' => 14,
                'id_producto'        => propuestas::where('id_producto_propuesta', $id_producto)->value('id_producto'),
                'id_usuario'         => propuestas::where('id_producto_propuesta', $id_producto)->value('id_usuario_propuesta'),
                'date'               => date('Y-m-d h:i'),
            ];
            //notificar cantidad propuestas
            $this->postProductos()->alertas($alertas, $id = null);


            $id_stados_anuncios = $id_estado;
            $propuesta        = propuestas::where('id_producto_propuesta', $id_producto)->first();
            $idUsuarioPropuesta = $propuesta['id_usuario_propuesta'];
            $idProductoPropuesta  = $propuesta['id_producto_propuesta'];

            $propuesta->estado = $propuestaAprovada;
            $propuesta->save();

            $productoAlert = productos::find($propuesta->id_producto);
            $idUsuarioProducto =  $productoAlert['id_usuario'];
            $idProductoProducto  = $productoAlert['id'];


            //enviar alerta a propuesta persona
            $this->enviarNotificacion()->notificar($idUsuarioPropuesta,$idProductoPropuesta,$id_stados_anuncios,'Su propuesta fue aprovado');

            //enviar alerta a producto persona
            $this->enviarNotificacion()->notificar($idUsuarioProducto,$idProductoProducto,$id_stados_anuncios,'Nueva propuesta');

            $titulo = 'Su propuesta fue aprovado';
            $msm    = 'Propuesta fue aprovado con la mision: ' . $productos['mision'] . ' y detalles: ' . $productos['misionDetalladamente'] . '';
            $aMeta[] = [
                'titulo'  => $titulo,
                'msm'     => $msm,
                'date'    => date('Y-m-d'),
                'article' => 'panel panel-success',
            ];

            $this->postProductos()->log(Auth::user()->id, json_encode($aMeta), $titulo);
            return response()->json(['status' => true, 'prd' => productos::join('users', 'productos.id_usuario', 'users.id')
                ->join('estados_anuncios', 'productos.id_estados_anuncios', 'estados_anuncios.id')
                ->select('productos.id_tipo as tipo', 'productos.id_estados_anuncios', 'estados_anuncios.estado as estado', 'users.id as id_usuario', 'users.name as name', 'users.apellido as apellido', 'productos.id as id', 'productos.mision as mision', 'productos.created_at as created_at')->orderBy('created_at', 'desc')->get()]);



            //nueva y Actualizado, en espera de aprovacion anuncio
        } elseif ($productos['id_estados_anuncios'] == 1 or $productos['id_estados_anuncios'] == 5) {

            $mensaje->id_usuario        = $id_usuario;
            $mensaje->id_producto       = $id_producto;
            $mensaje->id_estado_anuncio = $productoCurso;
            $mensaje->mensaje           = 'Su Anuncio fue aprovado';
            $mensaje->save();

            $idMsm = $mensaje->id;

            $productos->id_estados_anuncios = $productoCurso;
            $productos->id_mensaje          = $idMsm;
            $productos->save();

            $id_estado = $productos['id_estados_anuncios'];


            $users = [
                'nombre_Usuario' => $user['name'],
                'email'          => $user['email'],
                'from'           => 'no-repply@mail.com.co',
                'subject'        => 'SCREENSHOT-Su Anuncio fue aprovado',
            ];
            $mensage = [
                'mision'      => 'Su Anuncio fue aprovado',
                'descripcion' => $productos['misionDetalladamente'],
            ];

            $this->postProductos()->mail($users, $mensage, 'emails.aprovar');

            $id_stados_anuncios = $id_estado;

            $productoAlert = productos::where('id',$id_producto)->first();
            $idUsuarioProducto =  $productoAlert['id_usuario'];
            $idProductoProducto  = $productoAlert['id'];

                  //enviar alerta a propuesta persona
            $this->enviarNotificacion()->notificar($idUsuarioProducto,$idProductoProducto,$id_stados_anuncios,'Su anuncio fue aprovado');

            $titulo = 'Su anuncio fue aprovado';
            $msm    = 'Anuncio fue aprovado con la mision: ' . $productos['mision'] . ' y detalles: ' . $productos['misionDetalladamente'] . '';
            $aMeta[] = [
                'titulo'  => $titulo,
                'msm'     => $msm,
                'date'    => date('Y-m-d'),
                'article' => 'panel panel-success',
            ];

            $this->postProductos()->log(Auth::user()->id, json_encode($aMeta), $titulo);
            return response()->json(['status' => true,'prd'=> productos::join('users', 'productos.id_usuario', 'users.id')
                ->join('estados_anuncios', 'productos.id_estados_anuncios', 'estados_anuncios.id')
                ->select('productos.id_tipo as tipo', 'productos.id_estados_anuncios', 'estados_anuncios.estado as estado', 'users.id as id_usuario', 'users.name as name', 'users.apellido as apellido', 'productos.id as id', 'productos.mision as mision', 'productos.created_at as created_at')->orderBy('created_at', 'desc')->get()]);


          //Nuevo mensaje propuesta sin aprovar
        } elseif ($productos['id_estados_anuncios'] == 10) {
            $mensaje->id_estado_anuncio =  $nuevoMensajePropuesta;
            $mensaje->save();

            $idMsm = $mensaje->id;

            $productos->id_estados_anuncios =  $nuevoMensajePropuesta;
            $productos->id_mensaje          = $idMsm;
            $productos->save();

            $id_estado = $productos['id_estados_anuncios'];

            $users = [
                'nombre_Usuario' => $user['name'],
                'email'          => $user['email'],
                'from'           => 'no-repply@mail.com.co',
                'subject'        => 'SCREENSHOT-Su propuesta fue aprovado',
            ];
            $mensage = [
                'mision'      => 'Su Anuncio fue aprovado',
                'descripcion' => $productos['misionDetalladamente'],
            ];

            $this->postProductos()->mail($users, $mensage, 'emails.propuesta_aprovada');

            $id_stados_anuncios = $id_estado;
            $propuesta        = propuestas::where('id_producto_propuesta', $id_producto)->first();
            $idUsuarioPropuesta = $propuesta['id_usuario_propuesta'];
            $idProductoPropuesta  = $propuesta['id_producto'];

            $propuesta->estado =  $nuevoMensajePropuesta;
            $propuesta->save();

            $productoAlert = productos::where('id',$id_producto)->first();
            $idUsuarioProducto =  $productoAlert['id_usuario'];
            $idProductoProducto  = $productoAlert['id'];


            //enviar alerta a propuesta persona
            $this->enviarNotificacion()->notificar($idUsuarioPropuesta,$idProductoPropuesta,$id_stados_anuncios,'Su propuesta no fue aprovado');

            //enviar alerta a producto persona
            $this->enviarNotificacion()->notificar($idUsuarioProducto,$idProductoProducto,$id_stados_anuncios,'Su propuesta no fue aprovado');

            $titulo  = 'Nuevo mensaje de reuesta propuesta';
            $msm     = 'La persona que creo el anuncio: ' . $productos['mision'] . 'id de propucto ' . $productos->id . ' y detalles: ' . $productos['misionDetalladamente'] . 'esta ineteresado en una propuesta';
            $aMeta[] = [
                'titulo'  => $titulo,
                'msm'     => $msm,
                'date'    => date('Y-m-d'),
                'article' => 'panel panel-success',
            ];

            $this->postProductos()->log(Auth::user()->id, json_encode($aMeta), $titulo);
            return response()->json(['status' => true, 'prd' => productos::join('users', 'productos.id_usuario', 'users.id')
                ->join('estados_anuncios', 'productos.id_estados_anuncios', 'estados_anuncios.id')
                ->select('productos.id_tipo as tipo', 'productos.id_estados_anuncios', 'estados_anuncios.estado as estado', 'users.id as id_usuario', 'users.name as name', 'users.apellido as apellido', 'productos.id as id', 'productos.mision as mision', 'productos.created_at as created_at')->orderBy('created_at', 'desc')->get()]);


        }

        $aMeta[] = [
            'titulo'  => 'ya fue aprovado',
            'msm'     => 'click en aprovar ya fue aprobado',
            'date'    => date('Y-m-d'),
            'article' => 'panel panel-danger',
        ];

        $this->postProductos()->log(Auth::user()->id, json_encode($aMeta), 'Yafue aprovado');
        return response()->json(['status' => false, 'prd' => productos::join('users', 'productos.id_usuario', 'users.id')
            ->join('estados_anuncios', 'productos.id_estados_anuncios', 'estados_anuncios.id')
            ->select('productos.id_estados_anuncios', 'estados_anuncios.estado as estado', 'users.id as id_usuario', 'users.name as name', 'users.apellido as apellido', 'productos.id as id', 'productos.mision as mision', 'productos.created_at as created_at')->orderBy('created_at', 'desc')->get()]);



    }

    public function rechazar($id, $id_usuario)
    {

        $user      = User::findOrFail($id_usuario);
        $productos = productos::findOrFail($id);
        $mensaje   = mensajes::find($productos->id);
        $rechazado = 3;

        $mensaje->id_usuario        = $id_usuario;
        $mensaje->id_producto       = $id;
        $mensaje->id_estado_anuncio =  $rechazado;
        $mensaje->mensaje           = 'Su anuncio fue rechazado por no cumplir los acuerdos de creacion de anuncio, por favor lea las condiciones ____aqui_____  ';
        $mensaje->save();

        $idMsm = $mensaje->id;

        $productos->id_estados_anuncios =  $rechazado;
        $productos->id_mensaje          = $idMsm;
        $productos->save();

        $id_estado = $productos['id_estados_anuncios'];

        if ($productos['id_estados_anuncios'] == 6) {
            $titulo = 'Su propuesta fue aprovado';
            $msm    = 'Propuesta fue aprovado con la mision: ' . $productos['mision'] . ' y detalles: ' . $productos['misionDetalladamente'] . '';
        } elseif ($productos['id_estados_anuncios'] == 1) {
            $titulo = 'Su anuncio fue aprovado';
            $msm    = 'Anuncio fue aprovado con la mision: ' . $productos['mision'] . ' y detalles: ' . $productos['misionDetalladamente'] . '';
        } elseif ($productos['id_estados_anuncios'] == 10) {
            $titulo  = 'Nuevo mensaje de reuesta propuesta';
            $msm     = 'La persona que creo el anuncio: ' . $productos['mision'] . 'id de propucto ' . $productos->id . ' y detalles: ' . $productos['misionDetalladamente'] . 'esta ineteresado en una propuesta';
            $aMeta[] = [
                'titulo'  => $titulo,
                'msm'     => $msm,
                'date'    => date('Y-m-d'),
                'article' => 'panel panel-success',
            ];

            $this->postProductos()->log(Auth::user()->id, json_encode($aMeta), 'Su anuncio fue rechazado');
        }

        $users = [
            'nombre_Usuario' => $user['name'],
            'email'          => $user['email'],
            'from'           => 'no-repply@mail.com.co',
            'subject'        => 'SCREENSHOT-Propuesta rechazada ',
        ];
        $mensage = [
            'mision'      => 'propuesta a rectificar',
            'descripcion' => $productos['misionDetalladamente'],
        ];

        $this->postProductos()->mailAdmin($users, $mensage, 'emails.rechazada-propuesta');

        $id_stados_anuncios = $id_estado;
        $productoAlert = productos::where('id', $id)->first();
        $idUsuarioProducto =  $productoAlert['id_usuario'];
        $idProductoProducto  = $productoAlert['id'];

        //enviar alerta a producto persona
        $this->enviarNotificacion()->notificar($idUsuarioProducto, $idProductoProducto, $id_stados_anuncios, 'Su propuesta no fue aprovado');


        return response()->json(['status' => true, 'prd' => productos::join('users', 'productos.id_usuario', 'users.id')
        ->join('estados_anuncios', 'productos.id_estados_anuncios', 'estados_anuncios.id')
        ->select('productos.id_tipo as tipo', 'productos.id_estados_anuncios', 'estados_anuncios.estado as estado', 'users.id as id_usuario', 'users.name as name', 'users.apellido as apellido', 'productos.id as id', 'productos.mision as mision', 'productos.created_at as created_at')->orderBy('created_at', 'desc')->get()]);

    }

    public function editar(Request $request)
    {
        $user      = User::findOrFail($request['id_usuario']);
        $productos = productos::findOrFail($request['id_producto']);
        $mensaje   = mensajes::find($productos->id);
        $verificar = 4;
        $reEnviarRespuesta = 13;
        $leido = 12;


        if ($productos['id_estados_anuncios'] == 6) {
            $status = $reEnviarRespuesta;
            $titulo = 'propuesta';
            $template = 'emails.verificar-propuesta';
        } elseif ($productos['id_estados_anuncios'] == 1) {
            $status =  $verificar;
            $titulo = 'anuncio';
            $template = 'emails.verificar';
        } elseif ($productos['id_estados_anuncios'] == 10) {
            $msm_value[] = last(json_decode($mensaje['mensajes_value']));
            $status      =  $leido;
            $titulo      = 'mensaje';
            $template = 'emails.verificar-propuesta';

            foreach ($msm_value as $value) {
                $aMeta[] = [
                    'titulo'  => $titulo,
                    'msm'     => 'Su ' . $titulo . ' necesita realizar cambios para el/la ' . $titulo . ' ' . $request['mensaje'] . ($productos['id_estados_anuncios'] == 10) ? 'actualmente esta con la ' . $titulo . ': ' . $value->titulo . ' y detalles: ' . $value->msm . '.' : '',
                    'date'    => date('Y-m-d'),
                    'article' => 'panel-info panel-outline',
                    'icon'    => 'panel panel-success',
                ];
            }
            $this->postProductos()->log(Auth::user()->id, json_encode($aMeta), 'Su anuncio necesita cambios');
        } elseif ($productos['id_estados_anuncios'] == 5) {
            $status =  $verificar;
            $titulo = 'anuncio';
            $template = 'emails.verificar';
        }

        $mensaje->id_usuario        = $request['id_usuario'];
        $mensaje->id_producto       = $request['id_producto'];
        $mensaje->id_estado_anuncio = $status;
        $mensaje->mensaje           = 'Su ' . $titulo . ' necesita realizar cambios por favor: ' . $request['mensaje'] . ' actualmente esta con la mision: ' . $productos['mision'] . ' y detalles: ' . $productos['misionDetalladamente'] . '.';
        $mensaje->save();

        $idMsm = $mensaje->id;

        $productos->id_estados_anuncios = $status;
        $productos->id_mensaje          = $idMsm;
        $productos->save();

        $id_estado = $productos['id_estados_anuncios'];

        $users = [
            'nombre_Usuario' => $user['name'],
            'email'          => $user['email'],
            'from'           => 'no-repply@mail.com.co',
            'subject'        => 'SCREENSHOT-verificar '. $titulo,
        ];
        $mensage = [
            'mision'      => 'propuesta a rectificar',
            'descripcion' => $productos['misionDetalladamente'],
        ];

        $this->postProductos()->mailAdmin($users, $mensage, $template);

        $id_stados_anuncios = $id_estado;
        $productoAlert = productos::where('id', $request['id_producto'])->first();
        $idUsuarioProducto =  $productoAlert['id_usuario'];
        $idProductoProducto  = $productoAlert['id'];
        //enviar alerta a producto persona
        $this->enviarNotificacion()->notificar($idUsuarioProducto, $idProductoProducto, $id_stados_anuncios, 'Su propuesta no fue aprovado');

        if ($productos['id_estados_anuncios'] == 6 or $productos['id_estados_anuncios'] == 10) {
            $propuesta        = propuestas::where('id_producto_propuesta', $request['id_producto'])->first();
            $propuesta->estado = $status;
            $propuesta->save();
        }


        return response()->json(['status' => true, 'prd' => productos::join('users', 'productos.id_usuario', 'users.id')
        ->join('estados_anuncios', 'productos.id_estados_anuncios', 'estados_anuncios.id')
        ->select('productos.id_tipo as tipo', 'productos.id_estados_anuncios', 'estados_anuncios.estado as estado', 'users.id as id_usuario', 'users.name as name', 'users.apellido as apellido', 'productos.id as id', 'productos.mision as mision', 'productos.created_at as created_at')->orderBy('created_at', 'desc')->get()]);

    }
}
