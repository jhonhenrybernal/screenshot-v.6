<?php

namespace App\Http\Controllers\Admin\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\UserRequest;
use Illuminate\Support\Facades\Auth;
use App\User;

class LoginController extends Controller
{
    public function acceder(UserRequest $request){
		$credentials = $request->only('email', 'password');
		$user = User::where('email',$request['email'])->first();

		if($user['tipo_role'] === 'misionero' || $user['active'] === 0){
			return response()->json(['msm' => 'Acceso restringido', 'status' => false]);
        }

    	if (Auth::attempt($credentials)) {
    		return response()->json(['msm' => '', 'status' => true]);
    	}else{
    		return response()->json(['msm' => 'Datos incorrectos', 'status' => false]);
    	}

    }
}
