<?php

namespace App\Http\Controllers\Admin\MiPerfil;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\User;
use Illuminate\Support\Facades\Hash;

class IndexController extends Controller
{
    private function entity()
    {
        return User::find(Auth::user()->id);
    }


    public function index()
    {
        return view('admin.miPerfil.index');
    }

    public function list()
    {
        return $this->entity();
    }

    public function update(Request $request){
        $user = $this->entity();

        if (Hash::check($request['data']['pwold'], $user['password'])) { // The passwords match...
            $user->password = Hash::make($request['data']['pwNow']);
            $user->update();
            return response()->json(['status' => true, 'type' => 'success', 'msm' => 'Actualizado', 'user' =>  $user]);
        }
        return response()->json(['status' => false, 'type' => 'danger', 'msm' => 'Error, Password anterior no coincide', 'user' =>  $user]);

    }

}
