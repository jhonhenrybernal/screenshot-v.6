<?php

namespace App\Http\Controllers\Admin\Configuracion\Sistema;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\ConfSistema;
use Illuminate\Support\Facades\Mail;

class ConfSistemaController extends Controller
{
    private function entity(){
       return ConfSistema::find(1);
    }

    public function getEmail(){
         return ConfSistema::find(1)->value('email_admin');
    }


    public function index()
    {
        return view('admin.sistema.index');
    }

    public function list()
    {
        return $this->entity();
    }


    public function update(Request $request){


        $conf = $this->entity();
        if ($conf == null) { // crear por primera vez
            $confNew = new ConfSistema();
            $confNew->key_coingbase = $request['data']['tokenCoing'];
            $confNew->email_admin = $request['data']['emailAdmin'];
            $confNew->redssocial_facebook = $request['data']['redssocial_facebook'];
            $confNew->redssocial_twitter = $request['data']['redssocial_twitter'];
            $confNew->redssocial_instagram = $request['data']['redssocial_instagram'];
            $confNew->save();

            return response()->json(['status' => true, 'type' => 'success', 'msm' => 'Actualizado', 'conf' => $this->entity()]);
        }
        $conf->email_admin = $request['data']['emailAdmin'];
        $conf->key_coingbase = $request['data']['tokenCoing'];
        $conf->redssocial_facebook = $request['data']['redsocial_facebook'];
        $conf->redssocial_twitter = $request['data']['redsocial_twitter'];
        $conf->redssocial_instagram = $request['data']['redsocial_instagram'];
        $conf->update();
        return response()->json(['status' => true, 'type' => 'success', 'msm' => 'Actualizado', 'conf' => $this->entity()]);

    }


    public function testMail(){
         $conf = $this->entity();

          $datos = [
                'nombre_Usuario' => str_replace('.com','',$_SERVER["HTTP_HOST"]),
                'email'          => $conf->email_admin,
                'from'           => 'no-repply@'.$_SERVER["HTTP_HOST"],
                'subject'        => str_replace('.com','',$_SERVER["HTTP_HOST"]).'- Test email',
            ];
         Mail::send('emails.test', ['msm' => ''], function ($message) use ($datos) {
            $message->subject($datos);
            $message->to($datos['email'], $datos['nombre_Usuario']);
            $message->from($datos['from']);
            $message->subject($datos['subject']);
        });
         return response()->json(['status' => true, 'type' => 'success', 'msm' => 'Email enviado, confirme...']);

    }

    public function redSociales(){
        $entity = $this->entity();
        return response()->json(['facebook' => $entity->redssocial_facebook, 'twitter' => $entity->redssocial_twitter, 'instagram' => $entity->redssocial_instagram]);
    }
}
