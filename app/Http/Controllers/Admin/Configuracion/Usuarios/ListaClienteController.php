<?php

namespace App\Http\Controllers\Admin\Configuracion\Usuarios;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Hash;


class ListaClienteController extends Controller
{
    private function users()
    {
        return User::where('tipo_role', 'misionero')->orderBy('created_at', 'desc')->get();
    }

	public function index(){
		return view('admin.usuario.lista-clientes');
	}

	 public function tableView(Request $request)
    {
        return $this->users();
    }


    public function view($id)
    {
        return User::where('id', $id)->first();
    }


    public function bloquear($id){
        $user = User::where('id', $id)->first();

        if ($user['email'] == 'jhonhenrybernal@gmail.com' || $user['email'] == 'dpena424@gmail.com' || $user['email'] == 'jbernal@gmail.com') {
            return response()->json(['status' => false,'msm' => 'admin','user' => $this->users()]);
        }
        $user->active = 0;
        $user->update();
        return response()->json(['status' => true, 'msm' => 'admin', 'user' => $this->users()]); return response()->json(['status' => true, 'msm' => 'admin', 'user' => $this->users()]);
    }

    public function activar($id)
    {
        $user = User::where('id', $id)->first();

        if ($user['email'] == 'jhonhenrybernal@gmail.com' || $user['email'] == 'dpena424@gmail.com' || $user['email'] == 'jbernal@gmail.com') {
            return response()->json(['status' => false, 'msm' => 'admin', 'user' => $this->users()]);
        }
        $user->active = 1;
        $user->update();
        return response()->json(['status' => true, 'msm' => 'admin', 'user' => $this->users()]);
    }

}
