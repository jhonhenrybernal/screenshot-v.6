<?php

namespace App\Http\Controllers\Admin\Configuracion\Usuarios;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;


class ListaAdminController extends Controller
{
    private function users()
    {
        return User::where('tipo_role', 'administrador')->orWhere('tipo_role', 'operador')->orderBy('created_at', 'desc')->get();
    }

	public function index(){
		return view('admin.usuario.lista-admin');
	}

	 public function tableView(Request $request)
    {
        return $this->users();
    }


    public function view($id)
    {
        return User::where('id', $id)->first();
    }


    public function bloquear($id){
        $user = User::where('id', $id)->first();

        if ($user['email'] == 'jhonhenrybernal@gmail.com' || $user['email'] == 'dpena424@gmail.com' || $user['email'] == 'jbernal@gmail.com') {
            return response()->json(['status' => false,'msm' => 'admin','user' => $this->users()]);
        }
        $user->active = 0;
        $user->update();
        return response()->json(['status' => true, 'msm' => 'admin', 'user' => $this->users()]); return response()->json(['status' => true, 'msm' => 'admin', 'user' => $this->users()]);
    }

    public function activar($id)
    {
        $user = User::where('id', $id)->first();

        if ($user['email'] == 'jhonhenrybernal@gmail.com' || $user['email'] == 'dpena424@gmail.com' || $user['email'] == 'jbernal@gmail.com') {
            return response()->json(['status' => false, 'msm' => 'admin', 'user' => $this->users()]);
        }
        $user->active = 1;
        $user->update();
        return response()->json(['status' => true, 'msm' => 'admin', 'user' => $this->users()]);
    }

    public function crear(Request $request){
        $userEmail = User::where('email', $request['email'])->first();
        if (!empty($userEmail)) {
            return response()->json(['status' => false, 'type' => 'danger', 'msm' => 'Error,  email ya existe', 'user' => $this->users()]);
        }
        $userName = User::where('username', $request['username'])->first();
        if (!empty($userName)) {
            return response()->json(['status' => false, 'type' => 'danger', 'msm' => 'Error,  username ya existe', 'user' => $this->users()]);
        }
        try {
            $user = new User();
            $user->name = $request['name'];
            $user->apellido = $request['apellido'];
            $user->tipo_documento = $request['tipo_documento'];
            $user->email = $request['email'];
            $password = $this->generarCodigo();
            $pw = Hash::make($password);
            $user->password = $pw;
            $user->remember_token = Str::random(60);
            $user->active = 1;
            $user->celular = $request['celular'];
            $user->username = $request['username'];
            $user->cedula = $request['cedula'];
            $user->tipo_role = strtolower($request['permisos']);
            $user->telefono_local = $request['celular'];
            $user->save();

            $user->assignRole($request['permisos']);

            $dates = array('name' => $request['name'], 'code' => $password,'user' => $request['email']);

            $this->Email($dates, $request['email'], 'emails.acceso_admin');
            return response()->json(['status'=> true,'type' =>'success','msm'=>'Creado, se envio el password al correo','user' => $this->users()]);
        } catch (\Throwable $th) {
            return response()->json(['status' => false, 'type' => 'danger', 'msm' => 'Error,'. $th->getMessage(), 'user' => $this->users()]);
        }
    }

    private function Email($dates, $email,$template)
    {
        Mail::send($template, $dates, function ($message) use ($email) {
            $message->subject('Bienvenido a la plataforma');
            $message->to($email);
            $message->from('no-repply@mail.com.ve', 'Bienvenido a thitonix');
        });
    }

    private function generarCodigo($longitud = 9)
    {
        $key = '';
        $pattern = '1234567890abcdefghijklmnopqrstuvwxyz';
        $max = strlen($pattern) - 1;
        for ($i = 0; $i < $longitud; $i++) $key .= $pattern{mt_rand(0, $max)};
        return $key;
    }

    public function editar($id){
        return  response()->json(['user' => User::where('id', $id)->first()]);
    }

    public function actualizar(Request $request)
    {
        try {
            $user = User::find($request['id']);
            if ($user['tipo_role'] != $request['permisos']) {
                $user->tipo_role = strtolower($request['permisos']);
            }

            if ($request['pw'] == 1) {
                $password = $this->generarCodigo();
                $pw = Hash::make($password);
                $user->password = $pw;

                $dates = array('name' => $user['name'], 'code' => $password, 'user' => $user['email']);

                $this->Email($dates, $user['email'], 'emails.reset_pw_admin');
            }
            $user->save();

            return response()->json(['status' => true, 'type' => 'success', 'msm' => 'Actualizado', 'user' => $this->users()]);
        } catch (\Throwable $th) {
            return response()->json(['status' => false, 'type' => 'danger', 'msm' => 'Error,' . $th->getMessage(), 'user' => $this->users()]);
        }
    }



}
