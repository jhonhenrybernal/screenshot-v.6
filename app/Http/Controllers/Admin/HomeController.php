<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Notificaciones;
use App\User;
use App\productos;
use Carbon\Carbon;


class HomeController extends Controller
{
    public function index(){

        $countUsers = User::where('tipo_role','misionero')->count();


        //Chart productos y misiones
        $prdPropu[] = ['Mes', 'Productos','Propuestas'];
        for ($ms=1; $ms < 12; $ms++) {

            $prd = productos::where('id_tipo',1)->whereRaw('MONTH(created_at) = '. $ms)->count();
            $mis = productos::where('id_tipo',2)->whereRaw('MONTH(created_at) = ' . $ms)->count();

            if ($prd >= 1 or $mis >= 1) {
                $meses = array("Enero"=> 1, "Febrero"=> 2, "Marzo" =>3, "Abril"=>4, "Mayo"=>5, "Junio"=>6, "Julio"=> 7, "Agosto"=>8, "Septiembre"=>9, "Octubre"=>10, "Noviembre"=>11, "Diciembre"=>12);

                $prdPropu[] = [ array_search($ms,$meses), $prd, $mis];
            }
        }

        return view('admin.dashboard.index',['countUsers' => $countUsers, 'prdPropu' => json_encode($prdPropu)]);
    }



    public function endViewNotify($tipo){

        switch ($tipo) {
            case 'prdPropu':
                $getPropuPrd = Notificaciones::whereIn('id_estado_anuncio', [1,5, 6])->get();
                foreach ($getPropuPrd as $row) {
                    $row->visto_adm = 0;
                    $row->save();
                }
                break;
            case 'entrega':
                $getEntrega = Notificaciones::where('id_estado_anuncio', 19)->get();
                foreach ($getEntrega as $row) {
                    $row->visto_adm = 0;
                    $row->save();
                }
                break;
            case 'pago':
                $getpago = Notificaciones::whereIn('id_estado_anuncio', [20, 29, 34])->get();
                foreach ($getpago as $row) {
                    $row->visto_adm = 0;
                    $row->save();
                }
                break;
        }
    }

    public function notificationStatus()
    {
        $countAlert = Notificaciones::where('visto_adm', 1)->whereIn('id_estado_anuncio', [1, 5, 6, 19, 21, 29, 34])->count();

        //Activar alerta front
        $alerta = '';
        if ($countAlert >= 1) {
            $alerta = 'kt-pulse';
            $viewNull = false;
        } else {
            $viewNull = true;
            $viewCantCss = 50;
        }

        $viewPrd = false;
        $countPrd = Notificaciones::where('visto_adm', 1)->where('id_estado_anuncio', 1)->count();

        //ver productos
        if ($countPrd >= 1) {
            $viewPrd = true;
            $viewCantCss = 70;
        }

        $viewPropu = false;
        $countPropu = Notificaciones::where('visto_adm', 1)->whereIn('id_estado_anuncio', [5, 6])->count();
        //ver propuestas
        if ($countPropu >= 1) {
            $viewPropu = true;
            $viewCantCss = 70;
        }

        $viewEntrega = false;
        $countEntrega = Notificaciones::where('visto_adm', 1)->where('id_estado_anuncio', 19)->count();
        //ver propuestas
        if ($countEntrega >= 1) {
            $viewEntrega = true;
            $viewCantCss = 70;
        }

        $viewPagos = false;
        $countpago = Notificaciones::where('visto_adm', 1)->whereIn('id_estado_anuncio', [20, 29])->count();
        //ver propuestas
        if ($countpago >= 1) {
            $viewPagos = true;
            $viewCantCss = 70;
        }
        return response()->json(['alerta' => $alerta, 'countAlert' => $countAlert, 'viewPrd' => $viewPrd, 'viewPropu' => $viewPropu, 'viewEntrega' => $viewEntrega, 'viewPagos' => $viewPagos, 'countPrd' => $countPrd, 'countPropu' => $countPropu, 'countEntrega' => $countEntrega, 'countpago' => $countpago, 'viewNull' => $viewNull, 'viewCantCss' => $viewCantCss]);
    }
}
