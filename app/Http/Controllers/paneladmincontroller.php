<?php

namespace App\Http\Controllers;

use App\categoria;
use App\Facturas;
use App\Http\Controllers\Controller;
use App\mensajes;
use App\productos;
use App\propuestas;
use App\User;
use App\EstadoEntrega;
use App\EntidadPago;
use App \MediosPago;
use App\Pagos;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Datatables;

class paneladmincontroller extends Controller
{

    public function tabla()
    {
        return view('tablas.paneladmin');

    }

    public function mostrarTabla()
    {
        $productos = productos::with('usuario', 'categoria', 'estado', 'imagenendeVarios', 'mensajes', 'propuestas')->select('productos.*')->whereNotIn('productos.id_estados_anuncios',[29,31])->orderBy('id','desc');

        return Datatables::of($productos)->addColumn('action', function ($productos) {

            if ($productos->id_estados_anuncios == 2 or $productos->id_estados_anuncios == 3 or $productos->id_estados_anuncios == 4 or $productos->id_estados_anuncios == 8 or $productos->id_estados_anuncios == 11 or $productos->id_estados_anuncios == 13 or $productos->id_estados_anuncios == 12 or $productos->id_estados_anuncios == 24 or $productos->id_estados_anuncios == 23 or $productos->id_estados_anuncios == 26) {
                return '<a data-producto-mision="' . $productos->mision . '" data-usuario-name="' . $productos->usuario['name'] . '" data-usuario-lastname="' . $productos->usuario['apellido'] . '"  data-producto-categoria="' . $productos->categoria['tipo'] . '" data-producto-imagenes=' . $productos['imagenendevarios'] . ' data-producto-destalles="' . $productos->misionDetalladamente . '"  class="btn btn-warning detallesAlert" title="Detalles"><i class="fa fa-info" aria-hidden="true"></i></a>';
            } else {
                return '
                <a data-producto-mision="' . $productos->mision . '"  data-usuario-lastname="' . $productos->usuario['apellido'] . '" data-producto-categoria="' . $productos->categoria['tipo'] . '"  data-producto-imagenes=' . $productos['imagenendevarios'] . '  data-usuario-name="' . $productos->usuario['name'] . '" data-producto-destalles="' . $productos->misionDetalladamente . '" class="btn btn-warning detallesAlert" title="Detalles"><i class="fa fa-info" aria-hidden="true"></i></a>'
                 . '<a type="button" class="btn btn-info" href="/aprovar/' . $productos->id . '/' . $productos->id_usuario . '" title="Aprovar"><i class="fa fa-check-square-o" aria-hidden="true"></i></a>'
                  .'<a type="button" class="btn btn-danger" href="/rechazar/' . $productos->id . '/' . $productos->id_usuario . '" title="Rechazar"><i class="fa fa-times" aria-hidden="true"></i></a>' . '<a onclick="rechazarAlert(' . $productos->id . ',' . $productos->id_usuario . ')"  class="btn btn-warning" title="Verificar"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>';
            }

        })->make(true);
    }

    public function aprovacion($id_producto, $id_usuario)
    {

        $user      = User::findOrFail($id_usuario);
        $productos = productos::findOrFail($id_producto);
        $mensaje   = mensajes::where('id_producto',$id_producto)->first();
        $propuestaAprovada =  8;
        $productoCurso = 2;
        $nuevoMensajePropuesta = 11;
        //propuestas actualizada 
        if ($productos['id_estados_anuncios'] == 6 or $productos['id_estados_anuncios'] == 8) {
            $mensaje->id_usuario        = $id_usuario;
            $mensaje->id_producto       = $id_producto;
            $mensaje->id_estado_anuncio = $propuestaAprovada;
            $mensaje->mensaje           = 'Su propuesta fue aprovado';
            $mensaje->save();

            $idMsm = $mensaje->id;

            $productos->id_estados_anuncios = $propuestaAprovada;
            $productos->id_mensaje          = $idMsm;
            $productos->save();

            $id_estado = $productos['id_estados_anuncios'];

            $users = [
                'nombre_Usuario' => $user['name'],
                'email'          => $user['email'],
                'from'           => 'no-repply@mail.com.co',
                'subject'        => 'SCREENSHOT-Su propuesta no fue aprovado ',
            ];
            $mensage = [
                'mision'      => 'propuesta Aprovada',
                'descripcion' => $productos['misionDetalladamente'],
            ];

            $this->postProductos()->mail($users, $mensage, 'emails.propuesta_aprovada');

            $alertas = [
                'id_stados_anuncios' => 14,
                'id_producto'        => propuestas::where('id_producto_propuesta', $id_producto)->value('id_producto'),
                'id_usuario'         => propuestas::where('id_producto_propuesta', $id_producto)->value('id_usuario_propuesta'),
                'date'               => date('Y-m-d h:i'),
            ];
            //notificar cantidad propuestas
            $this->postProductos()->alertas($alertas, $id = null);

       
            $id_stados_anuncios = $id_estado;
            $propuesta        = propuestas::where('id_producto_propuesta', $id_producto)->first();
            $idUsuarioPropuesta = $propuesta['id_usuario_propuesta'];
            $idProductoPropuesta  = $propuesta['id_producto_propuesta'];

            $propuesta->estado = $propuestaAprovada;
            $propuesta->save();

            $productoAlert = productos::find($propuesta->id_producto);
            $idUsuarioProducto =  $productoAlert['id_usuario'];
            $idProductoProducto  = $productoAlert['id'];   
 
            
            //enviar alerta a propuesta persona
            $this->enviarNotificacion()->notificar($idUsuarioPropuesta,$idProductoPropuesta,$id_stados_anuncios,'Su propuesta fue aprovado');

            //enviar alerta a producto persona
            $this->enviarNotificacion()->notificar($idUsuarioProducto,$idProductoProducto,$id_stados_anuncios,'Nueva propuesta');

            $titulo = 'Su propuesta fue aprovado';
            $msm    = 'Propuesta fue aprovado con la mision: ' . $productos['mision'] . ' y detalles: ' . $productos['misionDetalladamente'] . '';
            
            //nueva y Actualizado, en espera de aprovacion anuncio
        } elseif ($productos['id_estados_anuncios'] == 1 or $productos['id_estados_anuncios'] == 5) {

            $mensaje->id_usuario        = $id_usuario;
            $mensaje->id_producto       = $id_producto;
            $mensaje->id_estado_anuncio = $productoCurso;
            $mensaje->mensaje           = 'Su Anuncio fue aprovado';
            $mensaje->save();

            $idMsm = $mensaje->id;

            $productos->id_estados_anuncios = $productoCurso;
            $productos->id_mensaje          = $idMsm;
            $productos->save();

            $id_estado = $productos['id_estados_anuncios'];
            

            $users = [
                'nombre_Usuario' => $user['name'],
                'email'          => $user['email'],
                'from'           => 'no-repply@mail.com.co',
                'subject'        => 'SCREENSHOT-Su Anuncio fue aprovado',
            ];
            $mensage = [
                'mision'      => 'Su Anuncio fue aprovado',
                'descripcion' => $productos['misionDetalladamente'],
            ];

            $this->postProductos()->mail($users, $mensage, 'emails.aprovar');

            $id_stados_anuncios = $id_estado;
            
            $productoAlert = productos::where('id',$id_producto)->first();
            $idUsuarioProducto =  $productoAlert['id_usuario'];
            $idProductoProducto  = $productoAlert['id'];   

                  //enviar alerta a propuesta persona
            $this->enviarNotificacion()->notificar($idUsuarioProducto,$idProductoProducto,$id_stados_anuncios,'Su anuncio fue aprovado');

            $titulo = 'Su anuncio fue aprovado';
            $msm    = 'Anuncio fue aprovado con la mision: ' . $productos['mision'] . ' y detalles: ' . $productos['misionDetalladamente'] . '';
                        
          //Nuevo mensaje propuesta sin aprovar 
        } elseif ($productos['id_estados_anuncios'] == 10) {
            $mensaje->id_estado_anuncio =  $nuevoMensajePropuesta;
            $mensaje->save();

            $idMsm = $mensaje->id;

            $productos->id_estados_anuncios =  $nuevoMensajePropuesta;
            $productos->id_mensaje          = $idMsm;
            $productos->save();

            $id_estado = $productos['id_estados_anuncios'];

            $users = [
                'nombre_Usuario' => $user['name'],
                'email'          => $user['email'],
                'from'           => 'no-repply@mail.com.co',
                'subject'        => 'SCREENSHOT-Su propuesta fue aprovado',
            ];
            $mensage = [
                'mision'      => 'Su Anuncio fue aprovado',
                'descripcion' => $productos['misionDetalladamente'],
            ];

            $this->postProductos()->mail($users, $mensage, 'emails.propuesta_aprovada');

            $id_stados_anuncios = $id_estado;
            $propuesta        = propuestas::where('id_producto_propuesta', $id_producto)->first();
            $idUsuarioPropuesta = $propuesta['id_usuario_propuesta'];
            $idProductoPropuesta  = $propuesta['id_producto'];

            $propuesta->estado =  $nuevoMensajePropuesta;
            $propuesta->save();

            $productoAlert = productos::where('id',$id_producto)->first();
            $idUsuarioProducto =  $productoAlert['id_usuario'];
            $idProductoProducto  = $productoAlert['id'];   
 
            
            //enviar alerta a propuesta persona
            $this->enviarNotificacion()->notificar($idUsuarioPropuesta,$idProductoPropuesta,$id_stados_anuncios,'Su propuesta no fue aprovado');

            //enviar alerta a producto persona
            $this->enviarNotificacion()->notificar($idUsuarioProducto,$idProductoProducto,$id_stados_anuncios,'Su propuesta no fue aprovado');

            $titulo  = 'Nuevo mensaje de reuesta propuesta';
            $msm     = 'La persona que creo el anuncio: ' . $productos['mision'] . 'id de propucto ' . $productos->id . ' y detalles: ' . $productos['misionDetalladamente'] . 'esta ineteresado en una propuesta'; 
          
        }

        $aMeta[] = [
            'titulo'  => $titulo,
            'msm'     => $msm,
            'date'    => date('Y-m-d'),
            'article' => 'panel panel-success',
        ];

        $this->postProductos()->log(Auth::user()->id, json_encode($aMeta), $titulo);

        
        return redirect()->route('products.tabla')->with('message', ['success', 'Se envio un correo al usuario para ser informado.']);

    }

    public function rechazar($id, $id_usuario)
    {

        $user      = User::findOrFail($id_usuario);
        $productos = productos::findOrFail($id);
        $mensaje   = mensajes::find($productos->id);
        $rechazado = 3;

        $mensaje->id_usuario        = $id_usuario;
        $mensaje->id_producto       = $id;
        $mensaje->id_estado_anuncio =  $rechazado;
        $mensaje->mensaje           = 'Su anuncio fue rechazado por no cumplir los acuerdos de creacion de anuncio, por favor lea las condiciones ____aqui_____  ';
        $mensaje->save();

        $idMsm = $mensaje->id;

        $productos->id_estados_anuncios =  $rechazado;
        $productos->id_mensaje          = $idMsm;
        $productos->save();   

        $id_estado = $productos['id_estados_anuncios'];

        if ($productos['id_estados_anuncios'] == 6) {
            $titulo = 'Su propuesta fue aprovado';
            $msm    = 'Propuesta fue aprovado con la mision: ' . $productos['mision'] . ' y detalles: ' . $productos['misionDetalladamente'] . '';
        } elseif ($productos['id_estados_anuncios'] == 1) {
            $titulo = 'Su anuncio fue aprovado';
            $msm    = 'Anuncio fue aprovado con la mision: ' . $productos['mision'] . ' y detalles: ' . $productos['misionDetalladamente'] . '';
        } elseif ($productos['id_estados_anuncios'] == 10) {
            $titulo  = 'NUevo mensaje de reuesta propuesta';
            $msm     = 'LA persona que creo el anuncio: ' . $productos['mision'] . 'id de propucto ' . $productos->id . ' y detalles: ' . $productos['misionDetalladamente'] . 'esta ineteresado en una propuesta';
            $aMeta[] = [
                'titulo'  => $titulo,
                'msm'     => $msm,
                'date'    => date('Y-m-d'),
                'article' => 'panel panel-success',
            ];

            $this->postProductos()->log(Auth::user()->id, json_encode($aMeta), 'Su anuncio fue rechazado');
        }

        $users = [
            'nombre_Usuario' => $user['name'],
            'email'          => $user['email'],
            'from'           => 'no-repply@mail.com.co',
            'subject'        => 'SCREENSHOT-Propuesta rechazada ',
        ];
        $mensage = [
            'mision'      => 'propuesta a rectificar',
            'descripcion' => $productos['misionDetalladamente'],
        ];

        $this->postProductos()->mailAdmin($users, $mensage, 'emails.rechazada-propuesta');

        $id_stados_anuncios = $id_estado;
        $productoAlert = productos::where('id',$id)->first();
        $idUsuarioProducto =  $productoAlert['id_usuario'];
        $idProductoProducto  = $productoAlert['id'];   
               
          //enviar alerta a producto persona
        $this->enviarNotificacion()->notificar($idUsuarioProducto,$idProductoProducto,$id_stados_anuncios,'Su propuesta no fue aprovado');

    
        return redirect()->route('products.tabla')->with('message', ['success', 'Se envio un correo al usuario para ser informado.']);

    }

    public function posVerificar($id, $id_usuario)
    {
        return productos::with('usuario', 'categoria', 'estado', 'mensajes')->select('productos.*')->find($id);
    }

    public function verificar(Request $request)
    {
        $user      = User::findOrFail($request['id_usuario']);
        $productos = productos::findOrFail($request['id_producto']);
        $mensaje   = mensajes::find($productos->id);
        $verificar = 4;
        $reEnviarRespuesta = 13;
        $leido = 12;


        if ($productos['id_estados_anuncios'] == 6) {
            $status = $reEnviarRespuesta;
            $titulo = 'propuesta';
        } elseif ($productos['id_estados_anuncios'] == 1) {
            $status =  $verificar;
            $titulo = 'anuncio';

        } elseif ($productos['id_estados_anuncios'] == 10) {
            $msm_value[] = last(json_decode($mensaje['mensajes_value']));
            $status      =  $leido;
            $titulo      = 'mensaje';

            foreach ($msm_value as $value) {
                $aMeta[] = [
                    'titulo'  => $titulo,
                    'msm'     => 'Su ' . $titulo . ' necesita realizar cambios para el/la ' . $titulo . ' ' . $request['mensaje'] . ($productos['id_estados_anuncios'] == 10) ? 'actualmente esta con la ' . $titulo . ': ' . $value->titulo . ' y detalles: ' . $value->msm . '.' : '',
                    'date'    => date('Y-m-d'),
                    'article' => 'panel-info panel-outline',
                    'icon'    => 'panel panel-success',
                ];
            }
            $this->postProductos()->log(Auth::user()->id, json_encode($aMeta), 'Su anuncio necesita cambios');
        } elseif ($productos['id_estados_anuncios'] == 5) {
            $status =  $verificar;
            $titulo = 'anuncio';

        }
        
        $mensaje->id_usuario        = $request['id_usuario'];
        $mensaje->id_producto       = $request['id_producto'];
        $mensaje->id_estado_anuncio = $status;
        $mensaje->mensaje           = 'Su ' . $titulo . ' necesita realizar cambios por favor: ' . $request['mensaje'] . ' actualmente esta con la mision: ' . $productos['mision'] . ' y detalles: ' . $productos['misionDetalladamente'] . '.';
        $mensaje->save();

        $idMsm = $mensaje->id;

        $productos->id_estados_anuncios = $status;
        $productos->id_mensaje          = $idMsm;
        $productos->save();

        $id_estado = $productos['id_estados_anuncios'];
        
        $users = [
            'nombre_Usuario' => $user['name'],
            'email'          => $user['email'],
            'from'           => 'no-repply@mail.com.co',
            'subject'        => 'SCREENSHOT-verificar propuesta',
        ];
        $mensage = [
            'mision'      => 'propuesta a rectificar',
            'descripcion' => $productos['misionDetalladamente'],
        ];

        $this->postProductos()->mailAdmin($users, $mensage, 'emails.verificar-propuesta');
        
        $id_stados_anuncios = $id_estado;
        $productoAlert = productos::where('id',$request['id_producto'])->first();
        $idUsuarioProducto =  $productoAlert['id_usuario'];
        $idProductoProducto  = $productoAlert['id'];  
          //enviar alerta a producto persona
        $this->enviarNotificacion()->notificar($idUsuarioProducto,$idProductoProducto,$id_stados_anuncios,'Su propuesta no fue aprovado');

        if ($productos['id_estados_anuncios'] == 6 or $productos['id_estados_anuncios'] == 10) {
            $propuesta        = propuestas::where('id_producto_propuesta', $request['id_producto'])->first();
            $propuesta->estado = $status;
            $propuesta->save();
        }
        

        return response()->json(true);
    }

    public function entregasAdm()
    {
        $entrega = EstadoEntrega::join('users', 'estado_entrega.id_usuario', 'users.id')        
        ->join('productos', 'estado_entrega.id_producto', 'productos.id')
        ->join('estados_anuncios', 'estado_entrega.estado', 'estados_anuncios.id')
        ->select('productos.mision as mision','users.name as name','estados_anuncios.estado as estado','estado_entrega.fecha as fechaPago','estado_entrega.estado as id_estado')->get();
        
        return Datatables::of($entrega)->addColumn('action', function ($entrega) {
            if ($entrega['id_estado'] == 1 or $entrega['id_estado'] == 3) {
                return '<a type="button" class="btn btn-default btn-xs btn-warning"  target="_blank">Pendiente por aprobar</a>';
            } elseif ($entrega['id_estado'] == 2  or $entrega['id_estado'] == 26) {
                return '<a type="button" class="btn btn-default btn-xs btn-warning" href="descargar/pago/factura/' . $entrega['idFactura'] . '" target="_blank">Descargar factura</a>';
            }

        })->make(true);
    }

    public function mostrarTablaAdmPago()
    {

        $pagos = Pagos::join('users', 'pagos.id_usuario', 'users.id')
        ->join('productos', 'pagos.id_producto', 'productos.id')
        ->join('estados_anuncios', 'pagos.status', 'estados_anuncios.id')
        ->select('pagos.id','users.name','users.id as idUser','productos.mision as mision', 'pagos.fecha_pago as fecha', 'pagos.id as idFactura', 'pagos.status as id_estado', 'estados_anuncios.estado as estado','pagos.iva as iva','pagos.comision_pasarela as comisionPasarela','pagos.comision_screen_total  as comisionScreenTotal','pagos.cliente_ganancia as clienteGanancia','pagos.comision_pasarela as comisionPasarela','pagos.metodo_pago')->whereIn('pagos.status', [26,27,28,29])->get();

        return Datatables::of($pagos)->addColumn('action', function ($pagos) {
            if ($pagos->id_estado == 27 or $pagos->id_estado == 26 or $pagos->id_estado == 29) {
                return '
                <a type="button" class="btn btn-info"  href="pago/aprovado/'. $pagos->id.'" title="Finalizar pago"><i class="fa fa-check-square"></i></a>
                <a type="button" class="btn btn-warning modal-medio-consignar" data-pago-metodo='.$pagos->metodo_pago.' data-usuario='.$pagos->idUser.' href="#" title="Medio a consignar"><i class="fa fa-address-card-o"></i></a>';
            }
             if ($pagos->id_estado == 28) {
                return '<a type="button" class="btn btn-default btn-xs btn-warning" href="descargar/pago/factura/' . $pagos['idFactura'] . '" target="_blank">Descargar factura</a>';  
            } 
                              

        })->make(true);
    }

    public function mostrarMedioPago(Request $request){
       
        $EntidadMedioPago = EntidadPago::find($request['metodoPago']);
        $datosPago  = MediosPago::where('id_usuario',$request['idUsuario'])->first();

         if($EntidadMedioPago->tipo == 3){
            $data = [
                'tipo' => $EntidadMedioPago->tipo,
                'imagen' => $EntidadMedioPago->imagen,
                'numeroTel' => $datosPago['numero_cel'],
            ];  
          }elseif($EntidadMedioPago->tipo == 2){          
              $data = [
                'tipo' => $EntidadMedioPago->tipo,
                'imagen' => $EntidadMedioPago->imagen,
                'numeroDoc' => $datosPago['numero_doc'],
            ]; 
          }elseif($EntidadMedioPago->tipo== 1){
              $data = [
                'tipo' => $EntidadMedioPago->tipo,
                'imagen' => $EntidadMedioPago->imagen,
                'numeroDoc' => $datosPago['numero_doc'],
                'nombreTitular' => $datosPago['Nombre_titular_cuenta_bancaria'],
                'numeroCuenta' => $datosPago['numero_cuenta'],
                'tipoCuenta' => $datosPago['tipo_cuenta_bancaria'],
            ];  
          }

       return $data;
    }

    public function mostrarTablaAdmCancela(){

         $productos = productos::with('usuario', 'categoria', 'estado', 'imagenendeVarios', 'mensajes', 'propuestas')->select('productos.*')->whereIn('pagos.status', [26,27,28])->orderBy('id','desc');

        return Datatables::of($productos)->addColumn('action', function ($productos) {

            if ($productos->id_estados_anuncios == 2 or $productos->id_estados_anuncios == 3 or $productos->id_estados_anuncios == 4 or $productos->id_estados_anuncios == 8 or $productos->id_estados_anuncios == 11 or $productos->id_estados_anuncios == 13 or $productos->id_estados_anuncios == 12 or $productos->id_estados_anuncios == 24 or $productos->id_estados_anuncios == 23 or $productos->id_estados_anuncios == 26) {
                return '<a data-producto-mision="' . $productos->mision . '" data-usuario-name="' . $productos->usuario['name'] . '" data-usuario-lastname="' . $productos->usuario['apellido'] . '"  data-producto-categoria="' . $productos->categoria['tipo'] . '" data-producto-imagenes=' . $productos['imagenendevarios'] . ' data-producto-destalles="' . $productos->misionDetalladamente . '"  class="btn btn-warning detallesAlert" title="Detalles"><i class="fa fa-info" aria-hidden="true"></i></a>';
            } else {
                return '
                <a data-producto-mision="' . $productos->mision . '"  data-usuario-lastname="' . $productos->usuario['apellido'] . '" data-producto-categoria="' . $productos->categoria['tipo'] . '"  data-producto-imagenes=' . $productos['imagenendevarios'] . '  data-usuario-name="' . $productos->usuario['name'] . '" data-producto-destalles="' . $productos->misionDetalladamente . '" class="btn btn-warning detallesAlert" title="Detalles"><i class="fa fa-info" aria-hidden="true"></i></a>'
                 . '<a type="button" class="btn btn-info" href="/aprovar/cancelado' . $productos->id . '/' . $productos->id_usuario . '" title="Aprovar"><i class="fa fa-check-square-o" aria-hidden="true"></i></a>'
                  .'<a type="button" class="btn btn-danger" href="/rechazar/cancelado' . $productos->id . '/' . $productos->id_usuario . '" title="Rechazar"><i class="fa fa-times" aria-hidden="true"></i></a>' . '<a onclick="rechazarAlert(' . $productos->id . ',' . $productos->id_usuario . ')"  class="btn btn-warning" title="Verificar"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>';
            }

        })->make(true);
    }

    public function mostrarTablaCancelados()
    {
        $productos = productos::with('usuario', 'categoria', 'estado', 'imagenendeVarios', 'mensajes', 'propuestas')->select('productos.*','productos.id as id_producto')->whereIn('productos.id_estados_anuncios',[29,31])->orderBy('id','desc');
     
        return Datatables::of($productos)->addColumn('action', function ($productos) {            
                return '
                <a data-producto-mision="' . $productos->mision . '"  data-usuario-lastname="' . $productos->usuario['apellido'] . '" data-producto-categoria="' . $productos->categoria['tipo'] . '"  data-producto-imagenes=' . $productos['imagenendevarios'] . '  data-usuario-name="' . $productos->usuario['name'] . '" data-producto-destalles="' . $productos->misionDetalladamente . '" class="btn btn-warning detallesAlert" title="Detalles"><i class="fa fa-info" aria-hidden="true"></i></a>'.
                '<a id="mensaje-modal" data-productos-id="' . $productos['id_producto'] . '" data-user-id="' . $productos['id_usuario'] . '" class="btn btn-warning modal-mensaje-cancelado" id="cerrar-modal" title="Mensajes"><i class="fa fa-envelope-o" aria-hidden="true"></i></a>'.
                  '<a type="button" class="btn btn-info" href="/cancelado/final/' . $productos->id . '/' . 1 . '" title="Aprovar"><i class="fa fa-check-square-o" aria-hidden="true"></i></a>'
                  .'<a type="button" class="btn btn-danger" href="/cancelado/final/' . $productos->id . '/' . 2 . '" title="Rechazar"><i class="fa fa-times" aria-hidden="true"></i></a>';
        })->make(true);
    }

    public function mensajesCancelar(Request $Mail){
      
        $mensaje = mensajes::where('id_producto', $Mail->id_producto)->first();
   
        $last    = last(json_decode($mensaje['mensajes_value']));
      
    
        $aMeta[] = [
            'titulo'  => 'Nuevo mensaje',
            'msm'     => $Mail->mensaje,
            'date'    => date('Y-m-d h:i'),
            'article' => 'panel-danger',
            'icon'    => 'glyphicon-plus',
            'user'    => 'Administrador: ' . Auth::user()->name . ' ' . Auth::user()->apellido,
        ];

      
        $aMetaValue                 = array_merge(json_decode($mensaje['mensajes_value'], true), $aMeta);
        $mensaje->mensaje           = 'Mensaje de administrador';
        $mensaje->id_usuario        = Auth::user()->id;
        $mensaje->mensajes_value    = json_encode($aMetaValue);
        $mensaje->save();
      
        $productoAlert = productos::where('id',$Mail->id_producto)->first();
        $idUsuarioProducto =  $productoAlert['id_usuario'];
        $idProductoProducto  = $productoAlert['id']; 
        //enviar alerta a propuesta persona
        $this->enviarNotificacion()->notificar($idUsuarioProducto,$idProductoProducto,29,'Nuevo mensaje propuesta de interes');

        return response()->json(true);
    }

    public function cancelarFinal($id_producto,$status){

        $producto = productos::where('id',$id_producto)->first();
        $propuestaProducto = productos::where('id',$id_producto->id_producto_propuesta_final)->first();
        $entrega = EstadoEntrega::where('estado_entrega',$id_producto)->first();
        $propuestas = propuestas::where('id_producto', $id_producto->id_producto_propuesta_final)->first();

        if ($status == 1) {
            $aprovadoCancelado = 22;
        }elseif ($status == 2) {
            $rechazadoCancelado = 22;
        }
       
        //verifica si es ununcio o mision 
        switch ($producto['id_tipo']) {
            case 1:
                $productos->id_estados_anuncios = $canceladofinal;
                $productos->fecha_actualizada = date('Y-m-d h:i');

                $propuestaProducto->id_estados_anuncios = $canceladofinal;
                $propuestaProducto->fecha_actualizada = date('Y-m-d h:i');

                $propuestas->estado = $canceladofinal;
                
                
    
                $entrega->estado = $canceladofinal;
                $entrega->comentarios = 'Mision Cancelado por el'. $producto->usuario->name.' '.$producto->usuario->apellido;
    
                $mensaje = mensajes::where('id_producto', $id_producto->id_producto_propuesta_final)->first();
                $last    = last(json_decode($mensaje['mensajes_value']));
        
        
                $aMeta[] = [
                    'titulo'  => 'Nuevo mensaje',
                    'msm'     => 'El producto fue cancelado por el administrador.',
                    'date'    => date('Y-m-d h:i'),
                    'article' => 'panel-danger',
                    'icon'    => 'glyphicon-plus',
                    'user'    => 'Administrador: ' . Auth::user()->name . ' ' . Auth::user()->apellido,
                ];
        
            
                $aMetaValue                 = array_merge(json_decode($mensaje['mensajes_value'], true), $aMeta);
                $mensaje->mensaje           = 'Mensaje de administrador';
                $mensaje->id_usuario        = Auth::user()->id;
                $mensaje->mensajes_value    = json_encode($aMetaValue);
    
                $estadopago    = Pagos::where('id_producto', 1)->first();
                $estadopago->status = $canceladofinal;
                $estadopago->estado_msg = 'El producto fue cancelado por el administrador.';


                $productos->save();
                $propuestaProducto->save();
                $propuestas->save();
                $entrega->save();
                $mensaje->save();
                $estadopago->save();
                break;
            
            case 2:
                $productos->id_estados_anuncios = $canceladofinal;
                $productos->fecha_actualizada = date('Y-m-d h:i');

                $propuestaProducto->id_estados_anuncios = $canceladofinal;
                $propuestaProducto->fecha_actualizada = date('Y-m-d h:i');

                $propuestas->estado = $canceladofinal;
    
    
                $entrega->estado = $canceladofinal;
                $entrega->comentarios = 'Mision Cancelado por el'. $producto->usuario->name.' '.$producto->usuario->apellido;
    
                $mensaje = mensajes::where('id_producto', $id_producto->id_producto_propuesta_final)->first();
                $last    = last(json_decode($mensaje['mensajes_value']));
        
        
                $aMeta[] = [
                    'titulo'  => 'Nuevo mensaje',
                    'msm'     => 'El producto fue cancelado por el administrador.',
                    'date'    => date('Y-m-d h:i'),
                    'article' => 'panel-danger',
                    'icon'    => 'glyphicon-plus',
                    'user'    => 'Administrador: ' . Auth::user()->name . ' ' . Auth::user()->apellido,
                ];
        
            
                $aMetaValue                 = array_merge(json_decode($mensaje['mensajes_value'], true), $aMeta);
                $mensaje->mensaje           = 'Mensaje de administrador';
                $mensaje->id_usuario        = Auth::user()->id;
                $mensaje->mensajes_value    = json_encode($aMetaValue);
    
                $estadopago    = Pagos::where('id_producto', 1)->first();
                $estadopago->status = $canceladofinal;
                $estadopago->estado_msg = 'El producto fue cancelado por el administrador.';

                $productos->save();
                $propuestaProducto->save();
                $propuestas->save();
                $entrega->save();
                $mensaje->save();
                $estadopago->save();
                break;
        }

        

        //enviar alerta a propuesta persona
        $this->enviarNotificacion()->notificar($idUsuarioProducto,$idProductoProducto,29,'Nuevo mensaje propuesta de interes');

    }
}
