<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\CalificacionUsuario;
use Auth;

class CalificacionUsuarioController extends Controller
{
    public function calificacion(Request $request)
    {  
        if ($request['estado'] == 1) {
            $buena = 1;
            $mala = 0;
        }else{
            $buena = 0;
            $mala = 1;
        }
        $calificacion =  new CalificacionUsuario();
        $calificacion->usuario_auth = $request['idUsuarioAuth'];
        $calificacion->usuario_calificado = $request['idUsuarioPropuesta'];
        $calificacion->cant_mala = $mala;
        $calificacion->cant_buena = $buena;
        $calificacion->date = date('Y-m-d h:i');
        $calificacion->save();

        return response()->json(true);
    }

    public function estadoCalificacion($id_usuario)
    {    
       $calificacion = CalificacionUsuario::where('usuario_calificado', $id_usuario)->where('usuario_auth', Auth::user()->id)->orderBy('id', 'Desc')->first();
        $bien = count(CalificacionUsuario::where('usuario_auth', Auth::user()->id)->where('cant_buena', 1)->orderBy('id', 'Desc')->get());
        $mal = count(CalificacionUsuario::where('usuario_auth', Auth::user()->id)->where('cant_mala', 1)->orderBy('id', 'Desc')->get());

        $list_exit = (empty($calificacion->id))?true:false;

        $result = array(
            'status' =>  $list_exit,
            'bien'   =>  $bien,
            'mal'   =>  $mal,
        );
        return response()->json($result);
    }

}
