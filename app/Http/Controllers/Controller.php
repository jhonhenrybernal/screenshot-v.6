<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function postProductos()
    {
    	return new ProductController();
    }

    public function soap()
    {
    	return new Soap\ConectSoapController();
    }

    public function enviarNotificacion()
    {
    	return new NotificacionController();
    }

    public function entregas()
    {
        return new EstadoEntregaController();
    }

    public function coinbase()
    {
        return new CoinbaseController();
    }

    public function homeAdmin(){
        return new Admin\HomeController();
    }

    public function confSystem(){
        return new Admin\Configuracion\Sistema\ConfSistemaController();
    }
}
