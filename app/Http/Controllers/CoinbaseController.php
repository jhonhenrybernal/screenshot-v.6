<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\PasarelaPagos;
use App\ConfSistema;
use CoinbaseCommerce\ApiClient;
use CoinbaseCommerce\Resources\Checkout;
use CoinbaseCommerce\Resources\Charge;
use CoinbaseCommerce\Webhook;
use CoinbaseCommerce\Resources\Event;


class CoinbaseController extends Controller
{

    function __construct(){
        $conf = ConfSistema::find(1);
        if (empty($conf)) {
            echo 'No tiene el token coingbase, contacte con el administrador';
            return;
        }
        $apiKey = $conf->key_coingbase;// pruebas jhon"3e2c1621-ea0e-4018-bf2e-8ac462d9a32d";
        $this->apiInit = ApiClient::init($apiKey);

    }

    public function changeList(){
        $this->apiInit;
        $listCharge = Charge::getList();
        foreach ($listCharge as $change) {
            PasarelaPagos::where('id_pasarela', $change['checkout']['id'])->update(['code' => $change['code']]);
        }
    }

    public function listStatus(){
        $this->apiInit;

        try {

            foreach (PasarelaPagos::all() as  $pasarela) {
                $chargeObj = Charge::retrieve($pasarela['code']);
                /*
                charge:created = 1
                charge:pending = 2
                charge:confirmed = 3
                charge:failed = 4
                */
                foreach ($chargeObj['timeline'] as $list) {

                    $ps = PasarelaPagos::find($pasarela['id']);

                    /*if ($list["status"] == "NEW") {
                        $ps->update(['status' => 1]);
                    }

                    if ($list["status"] == "PENDING") {
                        $ps->update(['status' => 2]);
                    }*/

                    if ($list["status"] == "COMPLETED") {

                        $ps->update(['status' => 3, 'transaction_id' => $chargeObj['payments'][0]['transaction_id'],'network' => $chargeObj['payments'][0]['network']]);
                    }

                    if ($list["status"] == "CANCELED" || $list["status"] == "EXPIRED") {
                        $ps->update(['status' => 4]);
                    }
                }
            }


            return ['status' => true];
        } catch (\Throwable $th) {

            return ['status' => false, 'error en ' . $th];
        }
    }


    public function sendPayment($request,$data){

          $this->apiInit;


        $newChangeObj = Charge::create($data);
        //$newCheckoutObj = Checkout::create($data);

        if (PasarelaPagos::where('id_propuesta', $request['id_producto'])->doesntExist()) {

            $pasarela = new PasarelaPagos();
            $pasarela->id_proveedor = 2;
            $pasarela->code = $newChangeObj['code'];
            $pasarela->id_pasarela = 11111;
            $pasarela->id_propuesta = $request['id_producto'];
            $pasarela->transaction_id =0;
            $pasarela->network =0;
            $pasarela->status = 1;
            $pasarela->save();

            $result = [
                'procesarPago' => 'https://commerce.coinbase.com/charges/' . $newChangeObj['code'],
                'result'       => true,
                'mensaje'      => 'Los datos fueron actualizados se redirecionara a coinbase',
                'proveedor'    => 'Coinbase'
            ];

            return $result;

        }else {
            $pasarela = PasarelaPagos::where('id_propuesta', $request['id_producto'])->first();
            if ($pasarela->status == 3) {
                $pasarela->id_pasarela = $newCheckoutObj['id'];
                $pasarela->update();

                $result = [
                    'procesarPago' => 'https://commerce.coinbase.com/charges/' . $pasarela['code'],
                    'result'       => true,
                    'mensaje'      => 'Pago realizado, exitoso....',
                    'proveedor'    => 'Coinbase'
                ];
                return $result;
            }

            if ($pasarela->status == 4) {
                $pasarela->id_pasarela = $newCheckoutObj['id'];
                $pasarela->update();

                $result = [
                    'procesarPago' => 'https://commerce.coinbase.com/charges/' . $pasarela['code'],
                    'result'       => true,
                    'mensaje'      => 'Reintentando....',
                    'proveedor'    => 'Coinbase'
                ];
                return $result;
            }

            $result = [
                'procesarPago' => 'https://commerce.coinbase.com/charges/' . $pasarela['code'],
                'result'       => true,
                'mensaje'      => 'Abriendo uno en proceso....',
                'proveedor'    => 'Coinbase'
            ];
            return $result;

        }




    }

    public function Webhook(){
        $secret = 'e5ef37ce-eb4a-470a-bec7-2bd97ee6e9fb';
        $headerName = 'X-Cc-Webhook-Signature';
        $headers = getallheaders();
        $signraturHeader = isset($headers[$headerName]) ? $headers[$headerName] : null;


        $client = new \GuzzleHttp\Client();
        $res = $client->post('https://96a3962f.ngrok.io/screenshot/public/api/coin/webhook/response');
        $payload = (string) $res->getBody();

        try {
            $event = Webhook::buildEvent($payload, $signraturHeader, $secret);
            http_response_code(200);
            echo sprintf('Successully verified event with id %s and type %s.', $event->id, $event->type);
        } catch (\Exception $exception) {
            http_response_code(400);
            echo 'Error occured. ' . $exception->getMessage();
        }
    }


    public function webhookResponse(Request $request){
        $content = $request;
        $fp = fopen(public_path('/img/') . "resultCoinbase.txt","wb");
        fwrite($fp,$content);
        fclose($fp);
    }
}
