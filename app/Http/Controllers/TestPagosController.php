<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Pagos;
use App\Log;
use App\productos;
use Auth;
use App\User;
use App\PreImg;
use App\img;
use App\mensajes;
use App\EstadoEntrega;
use DB;
use App\propuestas;
use Illuminate\Support\Facades\Crypt;
use Mail;
use CoinbaseCommerce\ApiClient;
use CoinbaseCommerce\Resources\Checkout;
use CoinbaseCommerce\Resources\Charge;

class TestPagosController extends Controller
{
	
	public function cambioEstado($id_producto){

        $propuestas = propuestas::where('id_producto_propuesta', $id_producto)->first();
        $productos = productos::find($propuestas['id_producto']);

        $idProductoPropuesta  = $propuestas['id_producto_propuesta'];
        $idProductoProducto  = $productos['id'];

        $log = DB::table('log')->where([
            ['id_producto', '=', $idProductoProducto],
            ['id_propuesta', '=', $idProductoPropuesta],
        ])->exists();


        $button = '
        <a href="http://localhost:8000/api/pago/estado/'.$id_producto.'/1">Aprovar</a>
        </br>
        <br>
        <a href="http://localhost:8000/api/pago/estado/'.$id_producto.'/2">Rechazar</a>
        </br>
        <br>
        <a href="http://localhost:8000/api/pago/estado/'.$id_producto.'/3">Pendiente</a>
        ';
        if ($log) {
            
           $button = '
           </br>
             <a href="http://localhost:8000/api/pago/restaurar/'.$id_producto.'">Restaurar pago</a>
            <br>
            ';
        }
		
	return $button;	
	}

   public function estado($id_producto_propuesta,$estado){
 	
        $aprovado = 1;

        $rechazar = 2;

        $pendiente = 3;

        $propuestaAprovada = 8;

        $iniciarRecibido = 32; $pagoRealizado = 26;
        $pagoRechazado = 21;
        $pagoEspera = 20;
        $productoEntregado = 23;
        $iniciarEntrega = 19;

        $propuestas = propuestas::where('id_producto_propuesta', $id_producto_propuesta)->first();
        $productos = productos::find($propuestas['id_producto']);
        $productosPropuesta = productos::find($id_producto_propuesta);

        $mensajeProducto = mensajes::where('id_producto', $propuestas['id_producto'])->first();
        $mensajePropuesta = mensajes::where('id_producto', $id_producto_propuesta)->first();

        $idUsuarioPropuesta = $propuestas['id_usuario_propuesta'];
        $idProductoPropuesta  = $propuestas['id_producto_propuesta'];

        $idUsuarioProducto =  $productos['id_usuario'];
        $idProductoProducto  = $productos['id'];

        $log = DB::table('log')->where([
            ['id_producto', '=', $idProductoProducto],
            ['id_propuesta', '=', $idProductoPropuesta],
        ])->exists();
        
        if (!$log) {
            
            $aMetaValue[] = [
                'titulo'  => 'Primer pago propuesta',
                'msm'     => 'Anuncio inicia pago prodcuto propuesta',
                'date'    => date('Y-m-d'),
                'id_producto' => $idProductoProducto,
                'id_propuesta'    => $idProductoPropuesta,
            ];
            $this->postProductos()->log($productos['id_usuario'],json_encode($aMetaValue), 'pago',$idProductoProducto,$idProductoPropuesta);
        }


        switch ($estado) {
            case $aprovado:
                $productos->id_estados_anuncios = $iniciarRecibido;
                $productos->id_producto_propuesta_final = $id_producto_propuesta;
                $productos->save();

                $productosPropuesta->id_estados_anuncios = $iniciarEntrega;
                $productosPropuesta->id_producto_propuesta_final = $propuestas['id_producto'];
                $productosPropuesta->save();
                $propuestas->estado = $iniciarEntrega;
                $propuestas->save();

                $mensajeProducto->id_estado_anuncio = $iniciarRecibido;
                $mensajeProducto->save();

                $mensajePropuesta->id_estado_anuncio = $iniciarRecibido;
                $mensajePropuesta->save();

                  //enviar alerta a cliente de propuesta
                $this->enviarNotificacion()->notificar($idUsuarioPropuesta,$idProductoPropuesta,$iniciarEntrega,'Su propuesta fue aprovado');
                 $this->mail($productosPropuesta, 'Su propuesta fue aprovado', 'emails.respuesta_pago');
                
                 //enviar alerta a producto persona
                $this->enviarNotificacion()->notificar($idUsuarioProducto,$idProductoProducto,$iniciarRecibido,'Se inicio la entrega de propuesta');
                //mail
                 $this->mail($productos, 'Se inicio la entrega de propuesta', 'emails.respuesta_pago');
                

                 //Recibiendo por pago realizado para propuesta
                $this->entregas()->iniciarEntrega($idProductoPropuesta,$idUsuarioProducto,$idUsuarioPropuesta ,$iniciarEntrega,'Iniciado');

                //Recibiendo por pago realizado para producto
                $this->entregas()->iniciarEntrega($idProductoProducto,$idUsuarioPropuesta,$idUsuarioProducto ,$iniciarRecibido,'Iniciado');
            break;

            case $rechazar:
                $productos->id_estados_anuncios = $pagoRechazado;
                $productos->save();

                            //enviar alerta a producto persona
                $this->enviarNotificacion()->notificar($idUsuarioPropuesta,$idProductoPropuesta,$pagoRechazado,'Su pago fue rechazado');
                //mail
                 $this->mail($productosPropuesta, 'Su pago fue rechazado', 'emails.respuesta_pago');
                

            break;

             case $pendiente:
                $productos->id_estados_anuncios = $pagoEspera;
                $productos->save();

                           
                 //enviar alerta a producto persona
                $this->enviarNotificacion()->notificar($idUsuarioPropuesta,$idProductoPropuesta,$pagoEspera,'Su pago esta pendiente');
                //mail
                 $this->mail($productosPropuesta, 'Su pago esta pendiente', 'emails.respuesta_pago');  

            break;
        }
        return 'Actializado
                </br>
                <a href="javascript:close_window();">Cerrar ventana</a>';
   }


    public function mail($productos,$msm, $blade)
    {

        $userCliente      = User::find($productos['id_usuario']);

        $datos = [
            'email'          => $userCliente['email'],
            'nombre_Usuario' => $userCliente['name'] . ' ' . $userCliente['apellido'],
            'from'           => 'no-repply@mail.com.co',
            'subject'        => 'SCREENSHOT-Pago fue ' . $msm,
        ];

        $vistaMensage = [
            'nombre_Usuario_cliente' => $userCliente['name'] . ' ' . $userCliente['apellido'],
            'estado'                 => $msm,
            'TransactionId'          => 1,
            'tipo_pago'              => 'test',
            'Aut_pse'                => 'N/A',
            'producto'               => $productos['mision'],
            'destalles_mision:'      => $productos['misionDetalladamente'],
            'url'                    => 'propuestas/ver/' . Crypt::encrypt($productos['id']),
        ];


        Mail::send($blade, ['msm' => $vistaMensage], function ($message) use ($datos) {
            $message->subject($datos);
            $message->to($datos['email']);
            $message->from($datos['from'], $datos['nombre_Usuario']);
            $message->subject($datos['subject']);
        });

    }

    public function aprobarCancelarInit(){
        $productos = productos::where('id_tipo',2)->get();

        foreach ($productos as $producto) {
            
            $button = '
            <a href="http://localhost:8000/api/cancelar/cambio/'.$producto['id'].'/1">Aprovar cancelado id:'.$producto['id'].'</a>
            </br>
            <br>
            <a href="http://localhost:8000/api/cancelar/cambio/'.$producto['id'].'/2">Rechazar cancelado id'.$producto['id'].' </a>
            </br>
            <br>
            <a href="http://localhost:8000/api/cancelar/cambio/'.$producto['id'].'/3">finalizar entrega y envio id'.$producto['id'].' </a> 
            <tr>
            </br>
            <br>
            '
           ;
        }        

        return $button;
    }

    public function adminCancelar($id_producto,$tipo){


        if ($tipo == 1) {//aprovado cancelado
            $estadoProducto = 2; //en curso
            $estatoPropuesta = 22; //cancelado definitivo     
            $msmNotificacionProdu = "Entrega cancelada, Su productos ha sido restaurado para recibir propuestas";
            $msmNotificacionPropu = "Mision cancelado";
        }elseif ($tipo == 2) {//rechazado cancelado
            $estadoProducto =  32; 
            $estatoPropuesta = 19;
            $msmNotificacionProdu = "Producto vuelve en estado de recibido";
            $msmNotificacionPropu = "Mision vuelve a enviando";
        }elseif ($tipo == 3) {//finalizar entrega y envio
            $estadoProducto = 24;
            $estatoPropuesta = 24;
            $msmNotificacionProdu = "Mision realizado";
            $msmNotificacionPropu = "Mision realizado";
        }


        //precancela producto principal
        $productos = productos::find($id_producto);
        $productos->id_estados_anuncios = $estadoProducto;
        $productos->save();

        
        //precancela producto contrario
        $idPropuesta = $productos->id_producto_propuesta_final;
        $productosContrario = productos::find($idPropuesta);
        $productosContrario->id_estados_anuncios = $estatoPropuesta;
        $productosContrario->save();
       
        

        //enviar alerta a producto persona principal
        $idUsuarioProductoPrincipal =  $productos->id_usuario;
        $idProductoProductoPrincipal  = $productos->id;
        $this->enviarNotificacion()->notificar($idUsuarioProductoPrincipal,$idProductoProductoPrincipal,$estatoPropuesta,$msmNotificacionProdu);
        
        $entregaPrincipal = EstadoEntrega::
        where('id_producto',$idProductoProductoPrincipal)->
        where('id_usuario_entregas',$idUsuarioProductoPrincipal)->
        first();

        $entregasPri = EstadoEntrega::find($entregaPrincipal['id']);
        $entregasPri->estado = $estadoProducto; 
        $entregasPri->fecha = date("Y-m-d h:i");
        $entregasPri->save();

        //enviar alerta a producto persona contrario
        $idUsuarioProductoContrario =  $productosContrario->id_usuario;
        $idProductoProductoContrario  = $productosContrario->id;
        $this->enviarNotificacion()->notificar($idUsuarioProductoContrario,$idProductoProductoContrario,$estatoPropuesta,$msmNotificacionPropu);
        
        
        $entregaContrario = EstadoEntrega::
        where('id_producto',$idProductoProductoContrario)->
        where('id_usuario_entregas',$idUsuarioProductoContrario)->
        first();
        
        $entregasCom = EstadoEntrega::find($entregaContrario['id']);
        $entregasCom->estado = $estatoPropuesta; 
        $entregasCom->fecha = date("Y-m-d h:i");
        $entregasCom->save();
        
        if ($tipo == 3) {

            if ($productos->id_tipo == 1) {
                $idproductoPago = $idPropuesta;
                $idpropuestaPago = $productos->id;
                $idUsuproductoPago = $idUsuarioProductoContrario;
                $propuesta        = propuestas::where('id_producto_propuesta',$productos->id)->first();
            }else {
                $idproductoPago = $productos->id;
                $idpropuestaPago = $idPropuesta;
                $idUsuproductoPago = $idUsuarioProductoPrincipal;
                $propuesta        = propuestas::where('id_producto',$productos->id)->first();
            }

            $resultPago = $this->postProductos()->calculoPropuestas($propuesta['presio']);
             $pago = Pagos::create([
                'id_proveedor'  => 1,
                'fecha_pago'    => date('Y-m-d H:i:s'),
                'id_producto'   => $idproductoPago,
                'id_propuesta'  => $idpropuestaPago,
                'id_usuario'    => $idUsuproductoPago,
                'status'        => $estadoProducto,
                'estado_msg'    => $estadoProducto,
                'TransactionId' => 123,
                'metodo_pago'   => 2,
                'iva' => $resultPago['iva'],
                'comision_pasarela'   => $resultPago['pasarela'],
                'comision_screen_total' => $resultPago['conmision_total'],
                'cliente_ganancia' => $resultPago['comision_cliente'],
            ]);
        }
        try {
            
            if ( $productos['id_tipo'] == 1) {
               $idMensaje = $productos->id_producto_propuesta_final;
               $titulo = 'Algo ocurrio con la entrega';
               $msm = "El producto con el titulo de mision ".$productos->mision." y con el detalle " .$productos->misionDetalladamente.", con la propuesta ".$productosContrario->misionDetalladamente.".";
            }elseif ( $productos['id_tipo'] == 2) {              
                $idMensaje = $productos->id;
                $titulo = 'Algo ocurrio con la envio';
                $msm = "El producto con el titulo de mision ".$productosContrario->mision." y con el detalle " .$productosContrario->misionDetalladamente.", con la propuesta ".$productos->misionDetalladamente.".";
            }
            
            $mensaje = mensajes::where('id_producto', $idMensaje)->first();
            $last    = last(json_decode($mensaje['mensajes_value']));
          
            $aMeta[] = [
                'titulo'  => $titulo,
                'msm'     => $msm,
                'date'    => date('Y-m-d h:i'),
                'article' => 'panel-danger',
                'icon'    => 'glyphicon-plus',
                'user'    => 'Administrador',
            ];
                      
            $aMetaValue                 = array_merge(json_decode($mensaje['mensajes_value'], true), $aMeta);
            $mensaje->id_estado_anuncio = $estatoPropuesta;
            $mensaje->mensajes_value    = json_encode($aMetaValue);
            $mensaje->mensaje    = 'Cancelado por administrador';
            $mensaje->save();           
        } catch (Exception $e) {
             return response()->json(false);
        }
        return 'finalizado';
    }

    public function restaurar($id_producto){
        $propuestas = propuestas::where('id_producto_propuesta', $id_producto)->first();
        $productos = productos::find($propuestas['id_producto']);
        $productosPropuesta = productos::find($id_producto);

        $idProductoPropuesta  = $propuestas['id_producto_propuesta'];
        $idProductoProducto  = $productos['id'];


        $mensajeProducto = mensajes::where('id_producto', $idProductoProducto)->first();
        $mensajePropuesta = mensajes::where('id_producto', $idProductoPropuesta)->first();
        $mensajeProducto->id_estado_anuncio = 2;
        $mensajeProducto->save();
        $mensajePropuesta->id_estado_anuncio = 8;
        $mensajePropuesta->save();

        $productos->id_estados_anuncios = 2;
        $productos->id_producto_propuesta_final = null;
        $productos->save();
        $productosPropuesta->id_estados_anuncios = 8;
        $productosPropuesta->id_producto_propuesta_final = null;
        $productosPropuesta->save();
        $propuestas->estado = 8;
        $propuestas->save();

        DB::table('log')->where([
            ['id_producto', '=', $idProductoProducto],
            ['id_propuesta', '=', $idProductoPropuesta],
        ])->delete();

        DB::table('estado_entrega')->where([
            ['id_producto', '=', $idProductoProducto],
            ['id_usuario_entregas', '=', $propuestas['id_usuario_propuesta']],
            ['id_usuario', '=', $productos['id_usuario']],
        ])->delete();

        return redirect()->route('home');
    }


    public function cargarAllAnuncios(){
        


        //crear anuncios
        $request = array(
            "tipo" => "1",
            "mision" => "marketing online",
            "id_categoria" => "5",
            "misionDetalladamente" => "marketing online"
        );
        $usuario = 28;

        for ($i=0; $i < 100; $i++) { 
            # code...
             $oValidacion = $this->postProductos()->validatorMessage($request['misionDetalladamente']);

            
            
            $productos                       = new productos;
            $productos->id_categoria         = $request['id_categoria'];
            $productos->mision               = $request['mision'];
            $productos->id_estados_anuncios  = 1;
            $productos->id_mensaje           = 1;
            $productos->id_usuario           = $usuario;
            $productos->misionDetalladamente = $request['misionDetalladamente'];
            $productos->id_ciudad            = 1;
            $productos->fecha                = date("Y-m-d");
            $productos->fecha_actualizada    = date("Y-m-d");
            $productos->id_tipo              = $request['tipo'];
            $productos->save();

            $id_producto = $productos->id;
            $valor       = "1";

            $aMetaValue[] = [
                'titulo'  => 'Anuncio creado',
                'msm'     => 'Anuncio fue creado mision: ' . $request['mision'] . ', y detalles: ' . $request['misionDetalladamente'] . '.',
                'date'    => date('Y-m-d'),
                'article' => 'panel-info panel-outline',
                'icon'    => 'glyphicon-info-sign',
                'avatar' => 'sys'
            ];

            $mensaje                    = new mensajes();
            $mensaje->id_usuario        = $usuario;
            $mensaje->id_producto       = $id_producto;
            $mensaje->id_estado_anuncio = 1;
            $mensaje->mensaje           = 'Su anuncio fue creado pronto el equipo de screenshot respondera el anuncio';
            $mensaje->save();

            $this->postProductos()->log($usuario, json_encode($aMetaValue), 'anuncio');


            //aprovar
            $productoCurso = 2;
            $id_usuario = $usuario;

            $mensaje->id_usuario        = $id_usuario;
            $mensaje->id_producto       = $id_producto;
            $mensaje->id_estado_anuncio = $productoCurso;
            $mensaje->mensaje           = 'Su Anuncio fue aprovado';
            $mensaje->save();

            $idMsm = $mensaje->id;

            $productos->id_estados_anuncios = $productoCurso;
            $productos->id_mensaje          = $idMsm;
            $productos->save();

            $id_estado = $productos['id_estados_anuncios'];
            

            $id_stados_anuncios = $id_estado;
            
            $productoAlert = productos::where('id',$id_producto)->first();
            $idUsuarioProducto =  $productoAlert['id_usuario'];
            $idProductoProducto  = $productoAlert['id'];   

                  //enviar alerta a propuesta persona
            $this->enviarNotificacion()->notificar($idUsuarioProducto,$idProductoProducto,$id_stados_anuncios,'Su anuncio fue aprovado');

            $titulo = 'Su anuncio fue aprovado';
            $msm    = 'Anuncio fue aprovado con la mision: ' . $productos['mision'] . ' y detalles: ' . $productos['misionDetalladamente'] . '';

        }
          
        if(Auth::check()){
            $aProductsuno = productos::where('id_estados_anuncios', 2)->where('id_usuario', '<>',$usuario)->orderBy('fecha', 'Desc')->get();
            $aProductsdos = productos::where('id_estados_anuncios', 2)->where('id_usuario', '<>',$usuario)->orderBy('fecha', 'Asc')->get();
        }else{
            $aProductsdos = productos::where('id_estados_anuncios', 2)->orderBy('fecha', 'Asc')->get();
            $aProductsuno = productos::where('id_estados_anuncios', 2)->orderBy('fecha', 'Desc')->get();

        }
        return view('inicio.index',['products' => $aProductsuno, 'productosdos' => $aProductsdos]);      

    }

    public function coinbaseCommerce(){
        $apiKey = "3e2c1621-ea0e-4018-bf2e-8ac462d9a32d";


        ApiClient::init($apiKey);
        

        $checkoutData = [
            'name' => 'The Sovereign Individual',
            'description' => 'Mastering the Transition to the Information Age',
            'pricing_type' => 'fixed_price',
            'local_price' => [
                'amount' => '100.00',
                'currency' => 'USD'
            ],
            'requested_info' => []
        ];

        $newCheckoutObj = Checkout::create($checkoutData);
        dd($newCheckoutObj['id']);
        /*$chargeData = [
            'name' => 'The Sovereign Individual',
            'description' => 'Mastering the Transition to the Information Age',
            'local_price' => [
                'amount' => '100.00',
                'currency' => 'USD'
            ],
            'pricing_type' => 'fixed_price'
        ];*/
        //Charge::create($chargeData);
        //dd(Charge::create($chargeData));
    }
}
