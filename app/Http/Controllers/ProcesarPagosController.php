<?php

namespace App\Http\Controllers;

use App\Pagos;
use App\productos;
use App\propuestas;
use App\Proveedores;
use App\tipoDocumento;
use App\User;
use App\img;
use App\MediosPago;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Mail;
use Validator;
use App\EntidadPago;
use App\EstadoEntrega;
use App\Facturas;
use App\PasarelaPagos;
use Carbon\Carbon;
use App\Notificaciones;
use Illuminate\Validation\Rules\Exists;
use Intervention\Image\ImageManagerStatic as Image;

class ProcesarPagosController extends Controller
{

    public function procesarPago($request)
    {

        //debe entrar id producto y id propuesta
        $producto  = productos::find($request['id_producto']);
        $protocol = isset($_SERVER["HTTPS"]) ? 'https' : 'http';
        $user      = User::find(Auth::user()->id);
        $propuesta = propuestas::where('id_producto_propuesta', $producto['id'])->first();
        $img = img::where('id_producto',$request['id_producto'])->first();
        $tipo      = substr($producto->categoria->tipo, -0, -3);


        //test pago//

        /*$result = [
            'procesarPago' => 'http://localhost:8000/api/pago/cambio-estado/'.$request['id_producto'],
            'result'       => true,
            'mensaje'      => 'Datos fueron actualizados',
            'proveedor'    => 'Test pago'
        ];
         return response()->json($result);
         //----//
        */



        /*procesar pago socn pago sinteligentes*/
        /*$params = [
            'Reference1'      => $this->postProductos()->generarCodigo() . $producto['id_producto'],
            'Amount'          => $propuesta['presio'],
            'Description'     => $producto['misionDetalladamente'],
            'FirstName'       => Auth::user()->name,
            'LastName'        => Auth::user()->apellido,
            'Mobile'          => Auth::user()->telefono_local,
            'Email'           => Auth::user()->email,
            'City'            => Auth::user()->ciudades['ciudad'],
            'Region'          => Auth::user()->departamento['nombre'],
            'Address'         => Auth::user()->direccion,
            'ReturnURL'       => $protocol . '://' . $_SERVER['HTTP_HOST'] . '/api/pago/status/' . Crypt::encrypt(Auth::user()->id),
            'IdNumber'        => Auth::user()->numero_documento,
            'IdType'          => tipoDocumento::where('id', Auth::user()->tipo_documento)->value('sigla'),
            'SourceId'        => 'screenshot',
            'AccountPassword' => '1234',
            'Language'        => 'es',
            'Reference2'      => $id_producto,
            'Reference3'      => 1,
        ];

        $return $this->soap()->processPayment($params);*/


        /*procesar con coingbase*/
        $chargeData  = [
            'name' => $producto['mision'],
            'description' => $producto['misionDetalladamente'],
            "logo_url" => $protocol . '://' . $_SERVER['HTTP_HOST'] ."/imagenesproductos/".$img['nombreImagen'],
            'pricing_type' => 'fixed_price',
            'local_price' => [
                'amount' => $propuesta['presio'],
                'currency' => $request['moneda']
            ],
        ];

        $coinResultPay = $this->coinbase()->sendPayment($request, $chargeData);

        return $coinResultPay;

    }

    public function actualizarUsuariorPago(Request $request)
    {
        foreach (request()->all() as $validate) {

            if (!empty($validate)) {
                $status = true;
            }
        }
        if ($status) {
            $usert                   = User::find($request['id_user']);
            $usert->name             = $request['nombre'];
            $usert->lastname         = $request['apellido'];
            //$usert->tipo_documento   = $request['tipo_documento'];
            //$usert->numero_documento = $request['numero_documento'];
            $usert->email            = $request['email'];
            //$usert->id_ciudad        = $request['ciudad'];
            //$usert->id_departamento  = $request['departamento'];
            $usert->celular          = $request['numero_telefonico'];
            $usert->pais             = $request['pais'];
            //$usert->direccion        = $request['direccion'];
            //$usert->barrio           = $request['barrio'];
            $usert->save();
            $result = $this->procesarPago($request);

        } else {
            $result = [
                'result'  => false,
                'mensaje' => 'Debe estar completo el fomulario',
            ];
        }

        return response()->json($result);
    }

    public function estadoPago(Request $request, $token)
    {
        //respuestas pago inteligente

        /*• Iniciada: 0
        o La transacción inicialmente esta en este estado cuando el usuario inicia el proceso de
        pago. Si el usuario cancela la operación sin finalizar el proceso, queda en estado 0.

        • Pendiente: 1
        o La transacción queda en pendiente hasta recibir respuesta de las redes financieras.
        • Aprobada: 2
        o Cuando una transacción es aprobada por las redes financieras queda en este estado.
        • Rechazada: 3
        o La transacción se rechaza por parte de las redes financieras.
        */

        //ref2=propuestas , ref3=provedores
        $Prov       = Proveedores::find($request['Ref3']);
        $prodMision = productos::find($request['Ref2']);

        $propuestaCliente = propuestas::where('id_producto_propuesta', $prodMision['id'])->first();
        $prodCliente      = productos::find($propuestaCliente['id_producto']);
        $prodCliente->fecha_actualizada    = date("Y-m-d");
        $prodClientePropuesta = productos::find($prodMision['id']);
        $prodClientePropuesta->fecha_actualizada    = date("Y-m-d");
        $userCliente      = User::find($prodCliente['id_usuario']);

        $user = User::where('id', $prodMision['id_usuario'])->first();
        //condiciones de pago
        if ($propuestaCliente['presio'] >= 100000) {
            $comision = $propuestaCliente['presio'] * 7 / 100;
        } elseif ($propuestaCliente['presio'] <= 100000) {
            $comision = $propuestaCliente['presio'] * 10 / 100;
        }
        $iva            = $comision * 19 / 100;
        $pasarela       = ($propuestaCliente['presio'] * 3 / 100) + 1000;
        $comision_screen_total = $comision - $iva;
        $cliente_ganancia = $propuestaCliente['presio'] - ($comision + $pasarela);

        $respuesta = [
            'nombre_Usuario_misionero' => $user['name'] . ' ' . $user['apellido'],
            'nombre_Usuario_cliente'   => $userCliente['name'] . ' ' . $userCliente['apellido'],
            'producto'                 => $prodMision['mision'],
            'destalles_mision:'        => $prodMision['misionDetalladamente'],
            'estado'                   => $request['Status'],
            'Msg'                      => $request['Msg'],
            'tipo_pago'                => $request['MethodPayment'],
            'Aut_pse'                  => $request['Aut'],
            'valor_de_pago'            => $request['Total'],
            'num_transaccion'          => $request['TransactionId'],
            'avatar'                   => $user['avatar'],
            'url'                      => 'propuestas/ver/' . $request['Ref2'],
        ];

        $iniciarRecibido = 32; $pagoRealizado = 26;
        $pagoRechazado = 21;
        $pagoIniciado = 20;
        $productoEntregado = 23;
        $iniciarEntrega = 19;

        //parametrizacion notificaciones
        $propuesta        = propuestas::where('id_producto_propuesta', $request['Ref2'])->first();
        $idUsuarioPropuesta = $propuesta['id_usuario_propuesta'];
        $idProductoPropuesta  = $propuesta['id_producto_propuesta'];

        //entregas recibe
        $productoAlert = productos::where('id',$propuesta['id_producto'])->first();
        $idUsuarioProducto =  $productoAlert['id_usuario'];
        $idProductoProducto  = $productoAlert['id'];


        if ($request['Status'] == 0){//2) { • Aprobada: 2


            $pago = Pagos::create([
                'id_proveedor'  => $request['Ref3'],
                'fecha_pago'    => date('Y-m-d H:i:s'),
                'id_producto'   => $propuestaCliente['id_producto'],
                'id_propuesta'  => $request['Ref2'],
                'id_usuario'    => $idUsuarioPropuesta,
                'status'        => $pagoRealizado,
                'estado_msg'    => $request['Msg'],
                'TransactionId' => $request['TransactionId'],
                'metodo_pago'   => $request['MethodPayment'],
                'iva' => $iva,
                'comision_pasarela'   => $pasarela,
                'comision_screen_total' => $comision_screen_total,
                'cliente_ganancia' => $cliente_ganancia,
            ]);


            //se confirma la propuesta con el producto
            $prodCliente->id_estados_anuncios = $iniciarRecibido;
            $prodCliente->id_producto_propuesta_final = $prodMision['id'];
            $prodCliente->save();

            //se confirma la propuesta
            $prodClientePropuesta->id_estados_anuncios = $iniciarEntrega;
            $prodClientePropuesta->id_producto_propuesta_final = $propuestaCliente['id_producto'];
            $prodClientePropuesta->save();
            $id_stados_anuncios = $iniciarRecibido;

            $propuestaCliente->estado = $iniciarEntrega;
            $propuestaCliente->save();

            //enviar alerta a propuesta persona
            $this->enviarNotificacion()->notificar($idUsuarioPropuesta,$idProductoPropuesta,$id_stados_anuncios,'Inicio de envio');

            //enviar alerta a producto persona
            $this->enviarNotificacion()->notificar($idUsuarioProducto,$idProductoProducto,$id_stados_anuncios,'Inicio de entrega');



           //Enviando por pago realizado
            $this->entregas()->iniciarEntrega($idProductoPropuesta,$propuesta['id_usuario_producto'],$idUsuarioPropuesta,$iniciarEntrega,'Enviando');


            //Recibiendo por pago realizado
            $this->entregas()->iniciarEntrega($idProductoProducto,$idUsuarioPropuesta,$idUsuarioProducto ,$iniciarRecibido,'Recibiendo');

        } elseif ($request['Status'] == 1) {//• Pendiente: 1
            $prodCliente->id_estados_anuncios = $pagoRechazado;
            $prodCliente->save();
            $prodClientePropuesta->id_estados_anuncios = $pagoRechazado;
            $prodClientePropuesta->save();
            $id_stados_anuncios = $pagoRechazado;
              //enviar alerta a propuesta persona
            $this->enviarNotificacion()->notificar($idUsuarioPropuesta,$idProductoPropuesta,$id_stados_anuncios,'Su propuesta fue aprovado');

        } elseif ($request['Status'] == 21) {
            $prodCliente->id_estados_anuncios = $pagoRealizado;
            $prodCliente->save();
            $prodClientePropuesta->id_estados_anuncios = $iniciarEntrega;
            $prodClientePropuesta->save();

            $id_stados_anuncios = $pagoRealizado;
              //enviar alerta a propuesta persona
            $this->enviarNotificacion()->notificar($idUsuarioPropuesta,$idProductoPropuesta,$id_stados_anuncios,'Producto rechazado');

            //enviar alerta a producto persona
            $this->enviarNotificacion()->notificar($idUsuarioProducto,$idProductoProducto,$id_stados_anuncios,'Producto rechazado');

        }elseif (/*$request['Status'] == 0 temporal*/false) {//Iniciado: 20
            $prodCliente->id_estados_anuncios = $pagoIniciado;
            $prodCliente->save();
            $prodClientePropuesta->id_estados_anuncios = $pagoIniciado;
            $prodClientePropuesta->save();

            $id_stados_anuncios = $pagoIniciado;

              //enviar alerta a propuesta persona
            $this->enviarNotificacion()->notificar($idUsuarioPropuesta,$idProductoPropuesta,$id_stados_anuncios,'Pago iniciado');

            //enviar alerta a producto persona
            $this->enviarNotificacion()->notificar($idUsuarioProducto,$idProductoProducto,$id_stados_anuncios,'Pago iniciado');

        }




        $datos = [
            'email'          => $userCliente['email'],
            'nombre_Usuario' => $userCliente['name'] . ' ' . $userCliente['apellido'],
            'from'           => 'no-repply@mail.com.co',
            'subject'        => 'SCREENSHOT-Pago fue ' . $request['Msg'],
        ];

        $vistaMensage = [
            'nombre_Usuario_cliente' => $userCliente['name'] . ' ' . $userCliente['apellido'],
            'estado'                 => $request['Msg'],
            'TransactionId'          => $request['TransactionId'],
            'tipo_pago'              => $request['MethodPayment'],
            'Aut_pse'                => $request['Aut'],
            'producto'               => $prodMision['mision'],
            'destalles_mision:'      => $prodMision['misionDetalladamente'],
            'url'                    => 'propuestas/ver/' . Crypt::encrypt($prodMision['id']),
        ];

        $this->mail($datos, $vistaMensage, 'emails.respuesta_pago');
        return view('pago.respuesta', ['datos_respuesta' => $respuesta]);
    }

    //funcion por crontan para actualizar pagos a coingbase
    public function procesarPagosCoing(){
        /*charge:created = 1
            charge:pending = 2
            charge:confirmed = 3
            charge:failed = 4
        */

        //actuaiiza transacciones de pagos
        $coingList = $this->coinbase()->listStatus();


        if ($coingList['status']) {
            //Actualiza estados de los productos
            $produc = productos::all();
            foreach ($produc as $rowPrd) {
                if ($rowPrd->id_estados_anuncios != 24) {
                    if ($rowPrd->id_tipo == 2) {
                        $pasarela = PasarelaPagos::where('id_propuesta', $rowPrd['id'])->first();

                        if (!empty($pasarela)) {
                            $producto      = productos::where('id' ,$rowPrd['id'])->first();//propuesta


                            $iniciarRecibido = 32;
                            $pagoRealizado = 33;
                            $pagoRechazado = 21;
                            $pagoIniciado = 20;
                            $productoEntregado = 23;
                            $iniciarEntrega = 19;




                            if ($pasarela['status'] == 2) {
                                //se confirma al producto
                                $producto->id_estados_anuncios = $pagoIniciado;
                                $producto->save();


                            } else if ($pasarela['status'] == 3) {
                                $propuesta        = propuestas::where('id_producto_propuesta', $pasarela['id_propuesta'])->first();
                                $productoAsPropuesta      = productos::where('id', $propuesta['id_producto'])->first();

                                $idUsuarioPropuesta =  $propuesta['id_usuario_propuesta'];
                                $idProductoPropuesta = $propuesta['id_producto_propuesta'];

                                $idProductoProducto = $productoAsPropuesta['id'];
                                $idUsuarioProducto = $productoAsPropuesta['id_usuario'];

                                //se confirma al producto de la propuesta
                                $producto->id_estados_anuncios = $iniciarEntrega;
                                $producto->id_producto_propuesta_final = $propuesta['id_producto'];
                                $producto->update();

                                //se confirma al producto
                                $productoAsPropuesta->id_estados_anuncios = $iniciarRecibido;
                                $productoAsPropuesta->id_producto_propuesta_final =  $idProductoPropuesta;
                                $productoAsPropuesta->update();

                                //se confirma la propuesta
                                $propuesta->estado = $iniciarEntrega;
                                $propuesta->update();

                                $siIva = false;

                                //comiciones y generar pago
                                if ($propuesta['presio'] >= 28) {
                                    $comisionScr = $propuesta['presio'] * 3 / 100;
                                } else {
                                    $comisionScr = $propuesta['presio'] * 7 / 100;
                                }

                                $iva            = $siIva ? $comisionScr * 19 / 100 : 0;
                                //desicion de david se excluye el iva $comision_screen_total = $comisionScr - $iva;
                                $comision_screen_total = $comisionScr;
                                $cliente_ganancia = $propuesta['presio'] - $comision_screen_total;

                                if (!Pagos::where('transactionId', $pasarela['transaction_id'])->exists()) {
                                    $pago = new Pagos();
                                    $pago->id_proveedor = 2;
                                    $pago->id_producto = $propuesta['id_producto'];
                                    $pago->id_propuesta = $pasarela['id_propuesta'];
                                    $pago->id_usuario = $idUsuarioPropuesta;
                                    $pago->status = $pagoRealizado;
                                    $pago->estado_msg ='Pago realizado';
                                    $pago->metodo_pago = $pasarela['network'];
                                    $pago->transactionId = $pasarela['transaction_id'];
                                    $pago->iva =  $iva;
                                    $pago->comision_pasarela = 0;
                                    $pago->comision_screen_total = $comision_screen_total;
                                    $pago->cliente_ganancia =  $cliente_ganancia;
                                    $pago->fecha_pago = Carbon::now();
                                    $pago->save();
                                }
                                //enviar alerta
                                $id_stados_anuncios_propuesta = $iniciarEntrega;
                                $id_stados_anuncios_producto =  $iniciarRecibido;


                                $msm = array(
                                    ['usuario_id' => $idUsuarioPropuesta, 'prd' => $idProductoPropuesta, 'estado' =>  $id_stados_anuncios_propuesta, 'msm' => 'Inicio de envio'],
                                    ['usuario_id' => $idUsuarioProducto, 'prd' => $idProductoProducto, 'estado' => $id_stados_anuncios_producto, 'msm' => 'Inicio de entrega']
                                );

                                foreach ($msm as $m) {
                                    if (!Notificaciones::where('id_producto', $idProductoPropuesta)->where('id_estado_anuncio',19)->exists() || !Notificaciones::where('id_producto', $idProductoProducto)->where('id_estado_anuncio', 32)->exists()) {
                                        $this->enviarNotificacion()->notificar($m['usuario_id'], $m['prd'], $m['estado'], $m['msm'],'adm');
                                        $datosPropuesta = [
                                            'email'          => $producto->usuario->email,
                                            'nombre_Usuario' => $producto->usuario->name . ' ' . $producto->usuario->apellido,
                                            'from'           => 'no-repply@mail.com.co',
                                            'subject'        => 'SCREENSHOT-Su propuesta a entregar',
                                        ];

                                        $vistaMensagePropuesta = [
                                            'nombre_Usuario_cliente' => $producto->usuario->name . ' ' . $producto->usuario->apellido,
                                            'estado'                 => 'aprovado',
                                            'TransactionId'          => $pasarela['transaction_id'],
                                            'tipo_pago'              => $pasarela['network'],
                                            'producto'               => $producto['mision'],
                                            'destalles_mision:'      => $producto['misionDetalladamente'],
                                            'url'                    => 'propuestas/ver/' . $producto['id'],
                                        ];

                                        $datosProducto = [
                                            'email'          => $productoAsPropuesta->usuario->email,
                                            'nombre_Usuario' => $productoAsPropuesta->usuario->name . ' ' . $productoAsPropuesta->usuario->apellido,
                                            'from'           => 'no-repply@mail.com.co',
                                            'subject'        => 'SCREENSHOT-Pago fue realizado',
                                        ];

                                        $vistaMensageProducto = [
                                            'nombre_Usuario_cliente' => $productoAsPropuesta->usuario->name . ' ' . $productoAsPropuesta->usuario->apellido,
                                            'estado'                 => 'aprovado',
                                            'TransactionId'          => $pasarela['transaction_id'],
                                            'tipo_pago'              => $pasarela['network'],
                                            'producto'               => $productoAsPropuesta['mision'],
                                            'destalles_mision:'      => $productoAsPropuesta['misionDetalladamente'],
                                            'url'                    => 'propuestas/ver/' . $productoAsPropuesta['id'],
                                        ];

                                        $emails = [[$datosPropuesta, $vistaMensagePropuesta,  'emails.respuesta_para_entrega'], [$datosProducto, $vistaMensageProducto, 'emails.respuesta_pago']];

                                        foreach ($emails as $mail) {
                                            $this->mail($mail[0], $mail[1], $mail[2]);
                                        }
                                    }
                                }

                                //iniciar entregas
                                $entregas = array(
                                    ['prd' => $idProductoPropuesta, 'id_usuario_uno' => $propuesta['id_usuario_producto'], 'id_usuario_dos' => $idUsuarioPropuesta, 'estado' =>  $iniciarEntrega, 'msm' => 'Enviando'],
                                    ['prd' => $idProductoProducto, 'id_usuario_uno' => $idUsuarioPropuesta, 'id_usuario_dos' => $idUsuarioProducto, 'estado' =>  $iniciarRecibido, 'msm' => 'Recibiendo']
                                );

                                foreach ($entregas as $e) {
                                    if (!EstadoEntrega::where('id_producto', $idProductoPropuesta)->where('estado', 19)->exists() || !EstadoEntrega::where('id_producto', $idProductoProducto)->where('estado', 32)->exists()) {
                                        $this->entregas()->iniciarEntrega($e['prd'], $e['id_usuario_uno'], $e['id_usuario_dos'], $e['estado'], $e['msm']);
                                    }
                                }

                            } else if ($pasarela['status'] == 4) {

                                //se confirma al producto
                                $producto->id_estados_anuncios = $pagoRechazado;
                                $producto->save();
                                $propuesta        = propuestas::where('id_producto_propuesta', $pasarela['id_propuesta'])->first();
                                $productoAsPropuesta      = productos::where('id', $propuesta['id_producto'])->first();

                                $idUsuarioPropuesta =  $propuesta['id_usuario_propuesta'];
                                $idProductoPropuesta = $propuesta['id_producto_propuesta'];

                                $idProductoProducto = $productoAsPropuesta['id'];
                                $idUsuarioProducto = $productoAsPropuesta['id_usuario'];


                                //enviar alerta
                                $id_stados_anuncios_producto =  $pagoRechazado;
                                $msm = array(
                                    ['usuario_id' => $idUsuarioProducto, 'prd' => $idProductoProducto, 'estado' => $id_stados_anuncios_producto, 'msm' => 'Inicio de entrega']
                                );

                                foreach ($msm as $m) {
                                    $this->enviarNotificacion()->notificar($m['usuario_id'], $m['prd'], $m['estado'], $m['msm']);
                                }

                                //iniciar entregas
                                $entregas = array(
                                   ['prd' => $idProductoProducto, 'id_usuario_uno' => $idUsuarioPropuesta, 'id_usuario_dos' => $idUsuarioProducto, 'estado' =>  $iniciarRecibido, 'msm' => 'Recibiendo']
                                );

                                foreach ($entregas as $e) {
                                    $this->entregas()->iniciarEntrega($e['prd'], $e['id_usuario_uno'], $e['id_usuario_dos'], $e['estado'], $e['msm']);
                                }
                            }
                        }
                    }
                }
            }
        }


    }

    public function estadoPagoInterno($id_producto, $id_proveedor)
    {

        $idProd = Crypt::descrypt($id_producto);
        $idProv = Crypt::descrypt($id_proveedor);
        $Prov   = Proveedores::where('id', $idProv)->first();
        $prod   = productos::where('id', $idProd)->first();
        $user   = User::where('id', $prod['id_usuario'])->first();

        $respuesta = [
            'nombre_Usuario'    => $user['name'] . ' ' . $user['apellido'],
            'producto'          => $prod['mision'],
            'destalles_mision:' => $prod['misionDetalladamente'],
            'estado'            => $request['Msg'],
            'tipo_pago'         => $request['MethodPayment'],
            'Aut_pse'           => $request['Aut'],
            'url'               => '/api/pago/statusInterno/' . Crypt::encrypt($prod['id']),
        ];

        return view('pago.respuesta_interno', ['datos_respuesa' => $respuesta]);
    }

    public function mail($datos, $vistaMensage, $blade)
    {
        Mail::send($blade, ['msm' => $vistaMensage], function ($message) use ($datos) {
            $message->subject($datos);
            $message->to($datos['email']);
            $message->from($datos['from'], $datos['nombre_Usuario']);
            $message->subject($datos['subject']);
        });

    }

    public function verificarEstadoPendiente(Request $request)
    {
        $status        = $this->soap()->clientStatusPayment($request->action);

        $Estadopago    = Pagos::where('status', 1)->where('id_producto', $request['Ref2'])->where('id_usuario', Auth::user()->id)->first();
        foreach ($status as $key => $value) {
            if ($value->Status = 1) {
                return response()->json(false);
            } elseif ($value->Status = 2) {

                $pago         = Pagos::find($Estadopago['id']);
                $pago->estado = $request['Status'];
                $pago->save();

                return response()->json(true);
            }
        }

    }

    public function medios(){
        $bancaria = 1;
        $giros = 2;
        $app = 3;

        $entidadBancaria = EntidadPago::where('tipo', $bancaria)->orderBy('entidad', 'DESC')->get();
        $entidadGiros = EntidadPago::where('tipo', $giros)->orderBy('entidad', 'DESC')->get();
        $entidadApp = EntidadPago::where('tipo', $app)->orderBy('entidad', 'DESC')->get();
        $pagoMedio = MediosPago::where('id_usuario', Auth::user()->id)->first();

        $entidadMedioPago = array();

        if (!empty($pagoMedio->id_entidad_medio_pago)) {
            $entidadMedioPago = json_decode($pagoMedio->id_entidad_medio_pago);
        }

        return view('pago.medios',['bancaria' => $entidadBancaria,'giros'=> $entidadGiros,'app' => $entidadApp,'pagoMedio' => $pagoMedio,'entidadMedioPago' => $entidadMedioPago ]);
    }

    public function addPagoParameter(Request $request){
        $pagoMedio = MediosPago::where('id_usuario', Auth::user()->id)->first();
        if (empty($pagoMedio->id)) {
            $arequest = $request->all();

            //agrupa todo los id key
            $MediosEntPago = array();
            foreach ($arequest as $key => $value) {
                if (is_numeric($key)) {
                    $MediosEntPago[] = $key;
                }
            }

             $pagoParams = MediosPago::create([
                'id_usuario'  => Auth::user()->id,
                'numero_doc'    => $request['numeroDocumento'],
                'numero_cel'   => $request['numeroCel'],
                'numero_cuenta'  =>$request['numeroCuenta'],
                'entidad_bancaria_id'   =>$request['entidadBancariaId'],
                'Nombre_titular_cuenta_bancaria'    => $request['nombreTitular'],
                'tipo_cuenta_bancaria'        => $request['tipoCuenta'],
                'id_entidad_medio_pago'  =>  json_encode($MediosEntPago),
                'fecha_actualizado' => date('Y-m-d H:i:s')
            ]);
        }else {
            $arequest = $request->all();

            //agrupa todo los id key
            $MediosEntPago = array();
            foreach ($arequest as $key => $value) {
                if (is_numeric($key)) {
                    $MediosEntPago[] = $key;
                }
            }

            $pagoMedio->numero_doc = $request['numeroDocumento'];
            $pagoMedio->numero_cel = $request['numeroCel'];
            $pagoMedio->numero_cuenta = $request['numeroCuenta'];
            $pagoMedio->entidad_bancaria_id = $request['entidadBancariaId'];
            $pagoMedio->Nombre_titular_cuenta_bancaria = $request['nombreTitular'];
            $pagoMedio->tipo_cuenta_bancaria = $request['tipoCuenta'];
            $pagoMedio->id_entidad_medio_pago = json_encode($MediosEntPago);

            $pagoMedio->fecha_actualizado = date('Y-m-d H:i:s');
            $pagoMedio->save();
        }

        return redirect()->action('misionesController@indexMision');
    }

    public function depositarPago(Request $request){

        if (empty($request['deposito'])) {

            $result = array('status' => false,'msm' => 'Seleccione un deposito por favor.' );
            return response()->json($result);
        }
        if (empty($request['metodo'])) {

            $result = array('status' => false,'msm' => 'Seleccione el metodo de pago por favor.' );
            return response()->json($result);
        }

        $depositos = array_filter(explode(',',$request['deposito']),'strlen');
        foreach ($depositos as  $deposito) {
            $solicitarPago = Pagos::find($deposito);
            $solicitarPago->metodo_pago = $request['metodo'];
            $solicitarPago->status = 29;
            $solicitarPago->save();
        }
        $result = array('status' => true,'msm' => 'Solicitud de deposito en proceso.' );
        return response()->json($result);

    }

    public function depositarPagoCoing(Request $request){

        if (empty($request['selectedDeposito'])) {

            $result = array('status' => false, 'msm' => 'Seleccione un deposito por favor.');
            return response()->json($result);
        }
        if (empty($request['codigo_virtual']) && empty($request->file('imgInp'))) {

            $result = array('status' => false, 'msm' => 'Uno se los dos medios de pago no estan, por favor ingresar datos');
            return response()->json($result);
        }
        //cargar imagen qr y actualizar medio de pago
        $MedioPago = MediosPago::where('id_usuario', Auth::user()->id)->first();


        $imageName = str_random(10) . '.png';
        $image = Image::make($request->file('imgInp'));
        $image->resize(500, 500);
        $image->save(public_path('/imagenesproductos/codigo_qr/' . $imageName));
        if (empty($MedioPago)) {
            $MedioPago = new MediosPago();
            $MedioPago->id_usuario = Auth::user()->id;
            $MedioPago->id_entidad_medio_pago = 4;
            $MedioPago->fecha_actualizado = date('d-m-Y H:i:s');
            $MedioPago->codigo_qr = $request['codigo_virtual'];
            $MedioPago->imagen_qr = $imageName;
            $MedioPago->save();
        }
        if (empty($MedioPago['codigo_qr'])) {
            $MedioPago->codigo_qr = $request['codigo_virtual'];
            $MedioPago->save();
        }

        if(empty($MedioPago['imagen_qr'])) {
            $MedioPago->imagen_qr = $imageName;
            $MedioPago->save();
        }

        $depositos = array_filter(explode(',', $request['selectedDeposito']), 'strlen');
        foreach ($depositos as  $deposito) {
            $solicitarPago = Pagos::find($deposito);
            if($solicitarPago['status'] == 29){
                $result = array('status' => true, 'msm' => 'La solicitud de deposito '. $solicitarPago['cliente_ganancia'] .' ya esta en proceso.');
                return response()->json($result);
            }
            $solicitarPago->metodo_pago = 'bitcoin';
            $solicitarPago->estado_msg = 'Mision finalizado, solicitan deposito.';
            $solicitarPago->status = 29;
            $solicitarPago->save();
        }

        //notificar al administrador por app
        $this->enviarNotificacion()->notificar(Auth::user()->id, $solicitarPago->id_propuesta, 29, 'Solicitud de deposito', 'adm');

        $result = array('status' => true, 'msm' => 'Solicitud de deposito en proceso.');

        return response()->json($result);
    }

    public function aprovarPago($id){
        $pago = Pagos::find($id);
        $pago->estado_msg = 'Pago aprovado, proceso...';
        $pago->status = 28;

        $pago->save();

        $productos   = productos::find($pago->id_producto);
        $userCliente   = User::find($pago->id_usuario);
        $datos = [
            'email'          => $userCliente['email'],
            'nombre_Usuario' => $userCliente['name'] . ' ' . $userCliente['apellido'],
            'from'           => 'no-repply@mail.com.co',
            'subject'        => 'SCREENSHOT-Pago fue aprovado ' ,
        ];

        $vistaMensage = [
            'nombre_Usuario_cliente' => $userCliente['name'] . ' ' . $userCliente['apellido'],
            'estado'                 => 'Pago fue aprovado y depositado',
            'producto'               => $productos->mision,
        ];

        $id_stados_anuncios = 28;
        $numProducto = $productos->id;
        $sumProductTotal = $productos->id + 1;
        Facturas::create([
            'id_producto'    => $pago->id_propuesta,
            'id_usuario'     => $pago->id_usuario,
            'id_proveedor'   => 0,
            'fecha_generada' => date('Y-m-d H:i:s'),
            'estado'         => $id_stados_anuncios,
            'numero_factura' => date('m') . $productos->id_tipo . $numProducto .'_'.$sumProductTotal
        ]);

        $this->mail($datos, $vistaMensage, 'emails.respuesta_pago');



        $productoAlert = productos::where('id',$pago->id_producto)->first();
        $idUsuarioProducto =  $productoAlert['id_usuario'];
        $idProductoProducto  = $productoAlert['id'];

              //enviar alerta a propuesta persona
        $this->enviarNotificacion()->notificar($idUsuarioProducto,$idProductoProducto,$id_stados_anuncios,'Pago fue aprovado y depositado');


        return redirect()->back()->with('message', ['success', 'Se envio correo al cliente para ser notificado a su aprovación']);
    }

}
