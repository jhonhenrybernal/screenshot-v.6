<?php

namespace App\Http\Controllers;

use App\categoria;
use App\ciudad;
use App\PreImg;
use App\img;
use App\mensajes;
use App\productos;
use Auth;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Pagination\LengthAwarePaginator;

class anuncioController extends Controller
{
    public function index()
    {
        $products = productos::where('id_usuario', Auth::user()->id)
        ->whereIn('id_tipo', [1,2])->orderBy('id','desc')->paginate(10)->onEachSide(5);
        $this->postProductos()->depurarPreImagenes();
        return view('anuncios.anuncio', ['products' => $products]);
    }

    public function listaCategoria()
    {

        $categoria = categoria::all();


        return view('anuncios.crearAnuncio', compact('categoria'));

    }


    public function cargarAnuncio(Request $request)
    {
        $oValidacion = $this->postProductos()->validatorMessage($request->misionDetalladamente);


        if ($oValidacion == 1) {
            return \Redirect::back()->withInput($request->all())->with('message', ['danger', 'EL mensaje no cumple con las buenas practicas de comunicación en la plataforma ']);
        }
        if (Auth::user()->id_ciudad == null) {
            return \Redirect::back()->withInput($request->all())->with('message', ['danger', 'Por favor complete su perfil']);
        }

        $productos                       = new productos;
        $productos->id_categoria         = $request->id_categoria;
        $productos->mision               = $request->mision;
        $productos->id_estados_anuncios  = 1;
        $productos->id_mensaje           = 1;
        $productos->id_usuario           = Auth::user()->id;
        $productos->misionDetalladamente = $request->misionDetalladamente;
        $productos->id_ciudad            = Auth::user()->id_ciudad;
        $productos->fecha                = date("Y-m-d");
        $productos->fecha_actualizada    = date("Y-m-d");
        $productos->id_tipo              = $request->tipo;
        $productos->save();

        $id_producto = $productos->id;
        $valor       = "1";

        $aMetaValue[] = [
            'titulo'  => 'Anuncio creado',
            'msm'     => 'Anuncio fue creado mision: ' . $request->mision . ', y detalles: ' . $request->misionDetalladamente . '.',
            'date'    => date('Y-m-d'),
            'article' => 'panel-info panel-outline',
            'icon'    => 'glyphicon-info-sign',
            'avatar' => 'sys'
        ];

        $mensaje                    = new mensajes();
        $mensaje->id_usuario        = Auth::user()->id;
        $mensaje->id_producto       = $id_producto;
        $mensaje->id_estado_anuncio = 1;
        $mensaje->mensaje           = 'Su anuncio fue creado pronto el equipo de screenshot respondera el anuncio';
        $mensaje->save();

        $this->postProductos()->log(Auth::user()->id, json_encode($aMetaValue), 'anuncio');

        //jbernal 12-09-2019 cambio de modo carga de imagenes
        //if ($request->hasFile('img')) {

            /*
            $this->postProductos()->cargarImagen($request, $id_producto);
            */
            $users = [
                'nombre_Usuario' => str_replace('.com','',$_SERVER["HTTP_HOST"]),
                'email'          =>  $this->confSystem()->getEmail(),
                'from'           => 'no-repply@'.$_SERVER["HTTP_HOST"],
                'subject'        => str_replace('.com','',$_SERVER["HTTP_HOST"]).'- Nuevo anuncio',
            ];

            $mensage = [
                'mision'      => $request->mision,
                'descripcion' => $request['misionDetalladamente'],
            ];

            $this->postProductos()->procesarAllPreImagenes($id_producto);

            $this->postProductos()->mail($users, $mensage, 'emails.anuncios');

             //notificar al administrador por app
            $this->enviarNotificacion()->notificar(Auth::user()->id, $id_producto, 1, 'Nuevo anuncio','adm');

            return redirect()->back()->with('message', ['success', 'Anuncio creado, pronto el equipo de SCREESHOT te notificara por correo la aprovacion del anuncio. ']);

        /*} else {
            return back()->with('msg', 'ups, suele pasar, revisa por favor');
        }*/
    }

    public function resultadoBusquedas(Request $aResultado)
    {

        if ($aResultado['filtro'] == 'true') {
            //si el filtrar, realizara busqeuda y filtro, entran dos parametros uno con lo que esta en busqueda y otro con los filtros rellenaos
            $filtro = $this->filtrarConsultaAnuncio($aResultado);

            foreach ($filtro as $value) {
                $valor       = (array) $filtro;
                $tipos[]     = $value->tipo;
                $unicoTipos  = array_unique($tipos);
                $fechas[]    = $value->fecha;
                $unicoFechas = array_unique($fechas);
                $ciudades[]  = $value->ciudad;
                $unicoCiudad = array_unique($ciudades);

            }
                return view('inicio.buscar', ['productos' => $filtro, 'ciudades' => $unicoCiudad, 'fechas' => $unicoFechas, 'tipos' => $unicoTipos]);
        } else {
            // solo busquedas
            if (empty($aResultado['buscar'])) {
                return redirect()->route('home');
            }
            $aConsulta = $this->consultaAnuncio($aResultado);

           // filtro pasa sacar un unico valor

            foreach ($aConsulta as $key => $value) {
                $unicoTipos[]  = $value->tipo;
                $unicoFechas[] = $value->fecha;
                $unicoCiudad[] = $value->ciudad;
            }
            if (empty($value)) {
                $unicoTipos  = [0];
                $unicoFechas = [0];
                $unicoCiudad = [0];
            }

            if (!empty($aConsulta)) {
                $aConsulta = $aConsulta;
            } else {
                $aConsulta = array_map("unserialize", array_unique(array_map("serialize", $aConsulta)));
            }

            return view('inicio.buscar', ['productos' => $aConsulta, 'ciudades' => array_unique($unicoCiudad), 'fechas' => array_unique($unicoFechas), 'tipos' => array_unique($unicoTipos)]);
        }
    }

    public function consultaAnuncio($aData)
    {
        try {
            $dat = null;

            $val = array_filter(explode(' ', $aData['buscar']));


            if (is_numeric($aData['page'])) {

                $resultado = DB::table('productos')
                    ->join('users', 'productos.id_usuario', '=', 'users.id')
                    ->join('ciudad', 'productos.id_ciudad', '=', 'ciudad.id')
                    ->join('categorias', 'productos.id_categoria', '=', 'categorias.id')
                    ->join('imagenes', 'productos.id', '=', 'imagenes.id_producto')
                    ->select('productos.*', 'categorias.tipo', 'categorias.id as id_tipo', 'ciudad.ciudad','imagenes.nombreImagen')
                    ->paginate(16);

                return $resultado;
            }

            foreach ($val as $value) {

                $resultado = DB::table('productos')
                    ->join('users', 'productos.id_usuario', '=', 'users.id')
                    ->join('ciudad', 'productos.id_ciudad', '=', 'ciudad.id')
                    ->join('categorias', 'productos.id_categoria', '=', 'categorias.id')
                    ->join('imagenes', 'productos.id', '=', 'imagenes.id_producto')
                    ->select('productos.*', 'categorias.tipo', 'categorias.id as id_tipo', 'ciudad.ciudad','imagenes.nombreImagen')
                    ->where('id_estados_anuncios', 2)->Where('misionDetalladamente', 'like', '%'.$value.'%')
                    ->orWhere('mision', 'like', '%'.$value.'%')
                    ->paginate(16);
            }
            return $resultado;
        } catch (Exception $dat) {
            return $dat;
        }
    }

    public function filtrarConsultaAnuncio($aData)
    {

        $categoriaId = categoria::where('tipo',$aData['categoria'])->first();
        $ciudadId = categoria::where('tipo',$aData['ciudad'])->first();


        try {
            $productos = DB::table('productos')
            ->join('ciudad', 'productos.id_ciudad', '=', 'ciudad.id')
            ->join('categorias', 'productos.id_categoria', '=', 'categorias.id')
            ->join('imagenes', 'productos.id', '=', 'imagenes.id_producto')
            ->join('users', 'productos.id_usuario', '=', 'users.id')
            ->where('id_estados_anuncios', 2)
            ->whereIn('productos.id',$aData['abuscarId'] )
            ->Where('id_categoria', 'like', '%' . $categoriaId['id']. '%')
            ->Where('ciudad', 'like', '%' . $ciudadId['id'] . '%')
            ->select('productos.*', 'categorias.tipo', 'categorias.tipo as tipo', 'ciudad.ciudad as ciudad','imagenes.nombreImagen')
            ->paginate(16);
            return $productos;
        } catch (Exception $dat) {
            return $dat;
        }


    }

}
