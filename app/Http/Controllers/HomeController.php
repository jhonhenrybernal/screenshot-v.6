<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;
use Illuminate\Support\Facades\Hash;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function perfil($id)
    {
        $oUSer = \App\User::where('id', $id)->first();
        $ciudad = $oUSer->ciudades;


        return view('products.perfil', ['user' =>$oUSer, 'ciudad' => $ciudad['ciudad']]);
    }

     public function perfilEditar($oData)
    {
        $oUSer = \App\User::where('id', $oData)->first();
        $oCiudad = \App\ciudad::all();

         return view('products.editarPerfil', ['user' =>$oUSer, 'ciudad' => $oCiudad]);
    }

      public function perfilActualizar(Request $oData)
    {
        $oActualizar = \App\User::find($oData->id);
        $oActualizar->username = $oData->usuario;
        $oActualizar->email = $oData->email;
        $oActualizar->direccion = $oData->direccion;
        $oActualizar->id_ciudad = $oData->ciudad;
        $oActualizar->name = $oData->nombre;
        $oActualizar->lastname = $oData->apellido;
        $oActualizar->celular = $oData->telefono;
        $oActualizar->barrio = $oData->barrio;
        $oActualizar->cedula = $oData->cedula;
        $oActualizar->avatar = $oData->avatar;
        $oActualizar->save();

        $oUSer = \App\User::where('id', $oActualizar->id)->first();
        return redirect()->route('perfil.index', $oActualizar->id)->with('success','Actualizado!');
    }


    public function autorizarCambioPw()
    {
        $success = '';
        return view('auth.passwords.cambiar.cambiar',compact('success'));
    }

    public function realizarCambioPw(Request $request)
    {
        if (empty($request['paso1']) || empty($request['paso2'])) {
            return \View::make('auth.passwords.cambiar.cambiar', array('success' => true));
        }
        $oUSer = \App\User::where('id', Auth::user()->id)->first();

        $oPwValid = bcrypt($request->paso1);
        if ($request->paso1 == $request->paso2) {
            $oActualizar = \App\User::find(Auth::user()->id);
            $oActualizar->password = $oPwValid;
            $oActualizar->save();
            return redirect()->route('perfil.index', $oActualizar->id)->with('success','Actualizado!');
        }

        return \View::make('auth.passwords.cambiar.cambiar',array('success' => true));
    }

    public function verMedioPago(Request $request)
    {
        return redirect()->action('misionesController@indexMision');
    }
}


