<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Notificaciones;
use Auth;

class NotificacionController extends Controller
{
   public function verificar(){
    $notifiLista = Notificaciones::where('id_usuario',Auth::user()->id)->where('visto',1)->get();
    $list = array();
    foreach ($notifiLista as $row) {

        $list[] = array(
            'id '=> $row->id,
            'id_usuario' => $row->id_usuario,
            'id_producto' => $row->id_producto,
            'id_estado_anuncio' => $row->id_estado_anuncio,
            'visto'=> $row->visto,
        );

    }
    $count = count($list);
    if (empty($list)) {
        $count =  '';
        $data = '';
        $status = false;
    }else{
        $count =  $count;
        $data = $list;
        $status = true;
    }


    $result = [
        'status'  => $status,
        //'data' => $data,
        'count' => $count,
    ];

    return response()->json($result);
   }

   public function notificar(int $id_usuario,int $id_producto,int $id_estado,$msm,$tipo = 'usu'){

        $noticicacion = new Notificaciones;
        $noticicacion->id_producto = $id_producto;
        $noticicacion->id_usuario =  $id_usuario;
        $noticicacion->id_estado_anuncio  =$id_estado;
        //adicionar aletar al administrador
        if ($tipo == 'adm') {
            $noticicacion->visto_adm = 1;
            $noticicacion->visto = 1;
        }else {
            $noticicacion->visto_adm = 0;
            $noticicacion->visto = 1;
        }
        $noticicacion->mensaje = $msm;
        $noticicacion->save();
    return true;
   }
}
