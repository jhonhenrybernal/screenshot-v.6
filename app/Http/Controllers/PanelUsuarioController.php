<?php

namespace App\Http\Controllers;

use App\categoria;
use App\Facturas;
use App\Http\Controllers\Controller;
use App\mensajes;
use App\productos;
use App\User;
use App\Notificaciones;
use App\propuestas;
use App\EstadoEntrega;
use App\EntidadPago;
use App\Pagos;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Mail;
use Yajra\DataTables\Datatables;

class PanelUsuarioController extends Controller
{
    public function tabla()
    {
        $notifiLista = Notificaciones::where('id_usuario',Auth::user()->id)->get();
        foreach ($notifiLista as $row) {
            $row->visto = 0;
            $row->save();
        }
        return view('tablas.panelusuario');

    }

    public function mostrarTablausu()
    {

        $productos = productos::join('users', 'productos.id_usuario', 'users.id')
            ->join('categorias', 'productos.id_categoria', 'categorias.id')
            ->join('mensajes', 'productos.id_mensaje', 'mensajes.id')
            ->join('estados_anuncios', 'productos.id_estados_anuncios', 'estados_anuncios.id')
            ->select('productos.*', 'productos.id as id_productos', 'categorias.tipo', 'users.name', 'users.apellido', 'estados_anuncios.estado', 'mensajes.mensaje as edit_mensajes')
            ->where('productos.id_usuario', Auth::user()->id)->whereIn('productos.id_estados_anuncios',[1,2,3,4,5,6,8,11,12,13,20,21,22,23,24,28,35])->get();


        return Datatables::of($productos)->addColumn('action', function ($productos) {

            if (
                $productos['id_estados_anuncios'] == 2 or
                $productos['id_estados_anuncios'] == 1 or
                $productos['id_estados_anuncios'] == 3 or
                $productos['id_estados_anuncios'] == 6 or
                $productos['id_estados_anuncios'] == 8 or
                $productos['id_estados_anuncios'] == 5 or
                $productos['id_estados_anuncios'] == 10 or
                $productos['id_estados_anuncios'] == 20 or
                $productos['id_estados_anuncios'] == 21 or
                $productos['id_estados_anuncios'] == 22 or
                $productos['id_estados_anuncios'] == 23 or
                $productos['id_estados_anuncios'] == 24 or
                $productos['id_estados_anuncios'] == 25 or
                $productos['id_estados_anuncios'] == 26 or
                $productos['id_estados_anuncios'] == 27 or
                $productos['id_estados_anuncios'] == 28 or
                $productos['id_estados_anuncios'] == 35 ) {
                return '<a  data-producto-mision="' . $productos['mision'] . '" data-usuario-name="' . $productos['name'] . '" data-usuario-lastname="' . $productos['apellido'] . '" data-producto-categoria="' . $productos['tipo'] . '" data-producto-imagenes=' . $productos->imagenendeVarios . '  data-producto-destalles="' . $productos['misionDetalladamente'] . '" class="btn btn-warning detallesAlert"  title="Detalles"><i class="fa fa-info" aria-hidden="true"></i></a>';
            } elseif ($productos['id_estados_anuncios'] == 4) {
                return '<a type="button" class="btn btn-info" href="/editusu/' . $productos['id_productos'] . '"title="Editar"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>' . '<a  data-producto-mision="' . $productos['mision'] . '" data-usuario-name="' . $productos['name'] . '" data-usuario-lastname="' . $productos['apellido'] . '" data-producto-categoria="' . $productos['tipo'] . '" data-producto-imagenes=' . $productos->imagenendeVarios . '  data-producto-destalles="' . $productos['misionDetalladamente'] . '" class="btn btn-warning detallesAlert"  title="Detalles"><i class="fa fa-info" aria-hidden="true"></i></a><a id="mensaje-modal" data-productos-mensaje="' . $productos['edit_mensajes'] . '" class="btn btn-success detallesAlertCambio" id="cerrar-modal" title="Mensajes"><i class="fa fa-envelope-o" aria-hidden="true"></i></a>';
            } elseif ($productos['id_estados_anuncios'] == 8 or $productos['id_estados_anuncios'] == 11 or $productos['id_estados_anuncios'] == 12) {
                return '<a id="mensaje-modal" data-productos-id="' . $productos['id_productos'] . '" data-user-id="' . $productos['id_usuario'] . '" class="btn btn-warning modal-mensaje-panel" id="cerrar-modal" title="Mensajes"><i class="fa fa-envelope-o" aria-hidden="true"></i></a>' . '<a  data-producto-mision="' . $productos['mision'] . '" data-usuario-name="' . $productos['name'] . '" data-usuario-lastname="' . $productos['apellido'] . '" data-producto-categoria="' . $productos['tipo'] . '" data-producto-imagenes=' . $productos->imagenendeVarios . '  data-producto-destalles="' . $productos['misionDetalladamente'] . '" class="btn btn-warning detallesAlert" title="Detalles"><i class="fa fa-info" aria-hidden="true"></i></a>';
            } elseif ($productos['id_estados_anuncios'] == 13) {
                return '<a type="button" class="btn btn-info" href="editar/propuesta/' . $productos['id_productos'] . '/' . $productos['id_usuario'] . '" title="Editar"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>' . '<a  data-producto-mision="' . $productos['mision'] . '" data-usuario-name="' . $productos['name'] . '" data-usuario-lastname="' . $productos['apellido'] . '" data-producto-categoria="' . $productos['tipo'] . '" data-producto-imagenes=' . $productos->imagenendeVarios . '  data-producto-destalles="' . $productos['misionDetalladamente'] . '" class="btn btn-warning detallesAlert"  title="Detalles"><i class="fa fa-info" aria-hidden="true"></i></a>';
            }

        })->make(true);
    }

    public function edit($id)
    {
        $categorias = categoria::select('tipo', 'id')->get();
        $producto   = productos::findOrFail($id);
        $mensaje    = mensajes::where('id_producto', $producto->id)->first();
        $idMensaje  = $mensaje['id'];

        return view('products.edit', compact('producto', 'categorias', 'idMensaje'));
    }

    public function update(Request $request, $id)
    {

        $producto                       = productos::find($id);
        $producto->id_categoria         = $request['id_categoria'];
        $producto->mision               = $request['mision'];
        $producto->id_estados_anuncios  = 5;
        $producto->id_mensaje           = 5;
        $producto->id_usuario           = Auth::user()->id;
        $producto->misionDetalladamente = $request['misionDetalladamente'];
        $producto->update();

        $mensaje                    = mensajes::where('id_producto' ,$id)->first();
        $mensaje->id_usuario        = Auth::user()->id;
        $mensaje->id_producto       = $id;
        $mensaje->id_estado_anuncio = 5;
        $mensaje->mensaje           = $request['misionDetalladamente'];
        $dato                       = $request['id_usuario'];
        $dates                      = array('id_producto' => $request['id_producto']);
        $mensaje->update();
        $users = [
            'nombre_Usuario' => str_replace('.com','',$_SERVER["HTTP_HOST"]),
            'email'          =>  $this->confSystem()->getEmail(),
            'from'           => 'no-repply@'.$_SERVER["HTTP_HOST"],
            'subject'        => str_replace('.com','',$_SERVER["HTTP_HOST"]).'- Anuncio Actualizado',
        ];
        $mensage = [
            'mision'      => $request['mision'],
            'descripcion' => $request['misionDetalladamente'],
        ];


        $this->postProductos()->mail($users, $mensage, 'emails.estado_cambio_anuncio');

        return redirect()->back()->with('message', ['success', 'Anuncio actualizado, pronto el equipo de SCREESHOT te notificara por correo la aprovacion de nuevo el anuncio.']);
    }

    public function mostrarTablaUsuPago()
    {

        $pagos = Pagos::join('users', 'pago_prd.id_usuario', 'users.id')
            ->join('productos', 'pago_prd.id_producto', 'productos.id')
            ->join('estados_anuncios', 'pago_prd.status', 'estados_anuncios.id')
            ->select('productos.mision', 'pago_prd.fecha_pago as fecha_pago', 'pago_prd.id_propuesta as id_propuesta', 'productos.id as id_producto', 'pago_prd.id as idFactura', 'pago_prd.status as id_estado', 'estados_anuncios.estado as estado','pago_prd.estado_msg')->where('pago_prd.id_usuario', Auth::user()->id)->whereIn('pago_prd.status', [24,27,28,29])->get();

        return Datatables::of($pagos)->addColumn('action', function ($pagos) {
            if ($pagos['id_estado'] == 28) {
                return '<a type="button" class="btn btn-default btn-xs btn-warning" href="descargar/pago/factura/' . $pagos['id_propuesta'] . '" target="_blank">Descargar factura</a>';
            }elseif($pagos['id_estado'] == 24 or $pagos['id_estado'] == 27 or $pagos['id_estado'] == 29){
                return '<a href="/misiones/pago" class="btn btn-default" title="Ir a retirar"><i class="fa fa-money" aria-hidden="true"></i></a>';
            }
        })->make(true);
    }

    public function entregasUsu()
    {
        $entrega = EstadoEntrega::join('users', 'estado_entrega.id_usuario', 'users.id')
        ->join('productos', 'estado_entrega.id_producto', 'productos.id')
        ->join('estados_anuncios', 'estado_entrega.estado', 'estados_anuncios.id')
        ->select(
            'estado_entrega.id as id',
            'productos.mision as mision',
            'users.name as nombre',
            'estado_entrega.comentarios as comentario',
            'estado_entrega.fecha as fecha',
            'users.name as usuario_entrega',
            'estado_entrega.estado as estado',
            'productos.id_tipo as tipo',
            'estado_entrega.id_producto as id_entregas',
            'estados_anuncios.estado as state'
        )
        ->where('estado_entrega.id_usuario_entregas', Auth::user()->id)
        ->whereIn('estado_entrega.estado', [19,23,31,32])
        ->get();

        return Datatables::of($entrega)->addColumn('action', function ($entrega) {
            if ($entrega['estado'] == 19  and $entrega['tipo']  == 2) {
                return '<a href="/misiones/envia/'. $entrega->id_entregas. '" class="btn btn-warning" title="Detalles"><i class="fa fa-info" aria-hidden="true"></i></a>';
            }elseif ($entrega['estado'] == 23  and $entrega['tipo']  == 2) {
                return '<a href="/misiones/envia/'. $entrega->id_entregas. '" class="btn btn-warning" title="Detalles"><i class="fa fa-info" aria-hidden="true"></i></a>';
            }elseif ($entrega['estado'] == 31  and $entrega['tipo']  == 2) {
                return '<a href="/misiones/envia/'. $entrega->id_entregas. '" class="btn btn-warning" title="Detalles"><i class="fa fa-info" aria-hidden="true"></i></a>';
            }elseif ($entrega['estado'] == 34  and $entrega['tipo']  == 2) {
                return '<a href="/misiones/envia/' . $entrega->id_entregas . '" class="btn btn-warning" title="Detalles"><i class="fa fa-info" aria-hidden="true"></i></a>';
            }

            if($entrega['estado'] == 23 and $entrega['tipo']  == 1){
                 return '<a href="/misiones/recibe/'.$entrega->id_entregas.'" class="btn btn-warning" title="Detalles"><i class="fa fa-info" aria-hidden="true"></i></a>';
            }elseif ($entrega['estado'] == 31  and $entrega['tipo']  == 1) {
                return '<a href="/misiones/recibe/' . $entrega->id_entregas . '" class="btn btn-warning" title="Detalles"><i class="fa fa-info" aria-hidden="true"></i></a>';
            }elseif ($entrega['estado'] == 32  and $entrega['tipo']  == 1) {
                return '<a href="/misiones/recibe/'. $entrega->id_entregas. '" class="btn btn-warning" title="Detalles"><i class="fa fa-info" aria-hidden="true"></i></a>';
            }elseif ($entrega['estado'] == 34  and $entrega['tipo']  == 1) {
                return '<a href="/misiones/recibe/'. $entrega->id_entregas. '" class="btn btn-warning" title="Detalles"><i class="fa fa-info" aria-hidden="true"></i></a>';
            }

        })->make(true);
    }

    public function descargarFactura($id)
    {
        $factura = Facturas::join('productos', 'facturas.id_producto', 'productos.id')
            ->join('pago_prd', 'facturas.id_producto', 'pago_prd.id_propuesta')
            ->join('propuestas_aproducto', 'facturas.id_producto', 'propuestas_aproducto.id_producto_propuesta')
            ->select('propuestas_aproducto.presio as presio', 'productos.mision as mision', 'facturas.numero_factura as numero_factura', 'pago_prd.metodo_pago as metodoPago', 'propuestas_aproducto.presio as presioProducto')->where('facturas.id_producto', $id)->first();
        $propuestas = propuestas::where('id_producto_propuesta', $id)->first();
        //condiciones de pago

        //$total_calculo =  $this->postProductos()->calculoPropuestas($factura['presio']);
        $pagos = Pagos::where('id_propuesta', $id)->first();
        //$iva            = $total_calculo['iva'];
        //$pasarela       = $total_calculo['pasarela'];
        $cliente        = $pagos['cliente_ganancia'];
        $comision_screen = $pagos['comision_screen_total'];
        $total          =  $cliente + $comision_screen;
        //$comision       = 1;

        //$entidad = EntidadPago::find($factura['metodoPago']);
        $view       = view('pdf.factura-pago')->with(compact('factura', 'cliente', 'total', 'comision_screen', 'propuestas'));
        $pdf        = \App::make('dompdf.wrapper');
        $paper_size = array(0, 0, 700, 600); //ajuste de hoja
        $pdf->loadHTML($view)->setPaper($paper_size);

        return $pdf->stream();
    }


    public function tablaUsuPropuesta()
    {

        $propuestas = Propuestas::join('users', 'propuestas_aproducto.id_usuario_producto', 'users.id')
            ->join('estados_anuncios', 'propuestas_aproducto.estado', 'estados_anuncios.id')
            ->join('productos', 'propuestas_aproducto.id_producto_propuesta', 'productos.id')
            ->select('productos.mision', 'users.name as nombre', 'propuestas_aproducto.fecha_propuesta', 'estados_anuncios.estado','propuestas_aproducto.id_usuario_producto as id_productos','propuestas_aproducto.id_producto_propuesta','propuestas_aproducto.id_usuario_propuesta')
            ->where('propuestas_aproducto.id_usuario_producto', Auth::user()->id)->whereIn('propuestas_aproducto.estado', [5,8,12])->get();

        return Datatables::of($propuestas)->addColumn('action', function ($propuestas) {
            return '<a type="button" class="btn btn-warning" href="/propuestas/ver/'.$propuestas->id_producto_propuesta. '" title="Detalles" onclick="pageViewDetail()"><i class="fa fa-info" aria-hidden="true"></i></a>'.'<a id="mensaje-modal" data-productos-id="' . $propuestas->id_producto_propuesta . '" data-user-id="' . $propuestas->id_usuario_propuesta  . '" class="btn btn-info modal-mensaje-panel" id="cerrar-modal" title="Mensajes"><i class="fa fa-envelope-o" aria-hidden="true"></i></a>';
        })->make(true);
    }

    public function delete($id)
    {
        $producto   = productos::find($id);
        $producto->id_estados_anuncios  = 36;
        $producto->update();

        return redirect()->route('anuncio.index');
    }

}
