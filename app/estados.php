<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class estados extends Model
{
    protected $table = 'estados_anuncios';
    public $timestamps = false;

    protected $fillable = [
        'id', 'estado', 'observaciones'	, 'valor', 
    ];


    public function usuario(){

        return  $this->belongsTo(User::class,'id_usuario', 'id','name','apellido');
    }

   public function productos(){

        return  $this->belongsTo(User::class,'id_categoria', 'mision', 'id_imagen'	, 'id_oferta'	, 'id_usuario'	, 'misionDetalladamente');
    }       

    public function categoria(){

        return  $this->belongsTo(categoria::class,'id_categoria', 'id','tipo');
    }



}
