<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Proveedores extends Model
{
   protected $table = 'proveedores_pago';
    public $timestamps = false;

    protected $fillable = [ 'id', 'nombre', 'img_logo','status'];

}
