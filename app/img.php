<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\productos;

class img extends Model
{
    protected $table = 'imagenes';
    public $timestamps = false;

    protected $fillable = [
        'id_producto', 'rutaImagen', 'nombreImagen'	, 
    ];

    public function productos(){

        return  $this->belongsTo(productos::class, 'id_producto', 'id', 'mision','misionDetalladamente');
    }
}
