<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tipoDocumento extends Model
{
     protected $table = 'tipo_documento';
    public $timestamps = false;

    protected $fillable = [
      'id', 'sigla','nombre'
    ];
}
