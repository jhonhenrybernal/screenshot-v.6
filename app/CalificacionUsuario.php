<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CalificacionUsuario extends Model
{
    protected $table = 'calificaciones_usuario';
    public $timestamps = false;

    protected $fillable = [
        'id', 'usuario_auth', 'usuario_calificado', 'cant_mala', 'cant_buena','date'
    ];
}
