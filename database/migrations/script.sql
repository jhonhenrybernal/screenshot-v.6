
  ALTER TABLE `users` ADD `tipo_role` varchar(20) DEFAULT NULL;
  ALTER TABLE `users` ADD `avatar` varchar(120)  DEFAULT NULL DEFAULT 'img/default.jpg';
  ALTER TABLE `users` ADD `code` varchar(120)  DEFAULT NULL;
  ALTER TABLE `users` ADD `active` varchar(120)  DEFAULT NULL DEFAULT '0';
  ALTER TABLE `users` ADD `apellido` varchar(120)  DEFAULT NULL;
  ALTER TABLE `users` ADD `username` varchar(120)  DEFAULT NULL;
  ALTER TABLE `users` ADD `celular` varchar(120)  DEFAULT NULL;
  ALTER TABLE `users` ADD `numero_documento` varchar(100)  DEFAULT NULL;
  ALTER TABLE `users` ADD `tipo_documento` varchar(20)  DEFAULT NULL;
  ALTER TABLE `users` ADD `direccion` varchar(30)  DEFAULT NULL;
  ALTER TABLE `users` ADD `id_ciudad` int(5) DEFAULT NULL;
  ALTER TABLE `users` ADD `id_departamento` int(11) DEFAULT NULL;
  ALTER TABLE `users` ADD `lastname` varchar(20)  DEFAULT NULL;
  ALTER TABLE `users` ADD `telefono_local` int(15) DEFAULT NULL;
  ALTER TABLE `users` ADD `barrio` varchar(20)  DEFAULT NULL;
propuesta`;

--

-- Volcado de datos para la tabla `users`
--




-- apartir de aca es para agregar e produccion
--
ALTER TABLE `medios_pago`ADD `imagen_qr` VARCHAR(50) NOT NULL AFTER `fecha_actualizado`;

ALTER TABLE `users` CHANGE `cedula` `cedula` VARCHAR(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL;

ALTER TABLE `users` CHANGE `celular` `celular` VARCHAR(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL;

ALTER TABLE `users` CHANGE `telefono_local` `telefono_local` VARCHAR(50) NULL DEFAULT NULL;

ALTER TABLE `users` ADD `pais` VARCHAR(10) NOT NULL AFTER `direccion`;



CREATE TABLE `screenshot`.`conf_sistemas` ( `id` INT NOT NULL , `key_coingbase` VARCHAR(100) NOT NULL ) ENGINE = InnoDB;
ALTER TABLE `conf_sistemas` ADD PRIMARY KEY(`id`);
ALTER TABLE `conf_sistemas` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT;




ALTER TABLE `notificaciones` ADD `visto_adm` INT NOT NULL AFTER `visto`;
INSERT INTO `tipo_medio_pago` (`id`, `medio`) VALUES ('4', 'coingbase');
INSERT INTO `estados_anuncios` (`id`, `estado`, `observaciones`) VALUES ('35', 'Misión finalizado', 'usuario que recibe el producto y da por finalizado, este es para el que recibe');
UPDATE `estados_anuncios` SET `observaciones` = 'usuario que recibe el producto y da por finalizado, este es para el que entrega' WHERE `estados_anuncios`.`id` = 24;
INSERT INTO `conf_sistemas` (`id`, `key_coingbase`) VALUES ('1', '0f01e08b-701f-4604-b498-3c30fd227eff');
ALTER TABLE `notificaciones` ADD `visto_adm` INT NOT NULL AFTER `visto`;
ALTER TABLE `conf_sistemas` ADD `email_admin` VARCHAR(30) NOT NULL AFTER `key_coingbase`;
ALTER TABLE `users` CHANGE `pais` `pais` VARCHAR(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL;

ALTER TABLE `pago_prd` CHANGE `comision_pasarela` `comision_pasarela` VARCHAR
(20) NULL DEFAULT NULL, CHANGE `comision_screen_total` `comision_screen_total` VARCHAR
(20) NULL DEFAULT NULL, CHANGE `cliente_ganancia` `cliente_ganancia` VARCHAR
(20) NULL DEFAULT NULL;


ALTER TABLE `pasarela_pago` ADD `code` VARCHAR(50) NOT NULL AFTER `id_proveedor`;
ALTER TABLE `pasarela_pago` CHANGE `code` `code` VARCHAR(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL;


INSERT INTO `estados_anuncios` (`id`, `estado`, `observaciones`) VALUES (NULL, 'Eliminado', 'El producto se indentifica en este caso como eliminar pero realmente se oculta.');


ALTER TABLE `conf_sistemas` ADD `redssocial_facebook` VARCHAR(200) NULL DEFAULT NULL AFTER `email_admin`, ADD `redssocial_twitter` VARCHAR(200) NULL DEFAULT NULL AFTER `redssocial_facebook`, ADD `redssocial_instagram` VARCHAR(200) NULL DEFAULT NULL AFTER `redssocial_twitter`;

