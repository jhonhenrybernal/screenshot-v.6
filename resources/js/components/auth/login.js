
window.Vue = require('vue');

import validate from 'jquery-validation';
import swal from 'sweetalert';

new Vue({
	el:'#form',
	data: {
		email:'',
		password:''
	},


	methods: {
	
	    iniciarSession: function () {
	        axios.post('/login-adm', {
	            email: this.email,
	            password: this.password
	        }).then(response => {
	        	if (response.data.status) {
	        		window.location.href = window.location.origin + '/admin/home'
	        	}else if (!response.data.status) {
	        		swal('Ups!',response.data.msm,'error')
	        	}
	        	
	        }).catch(error => {
	        	let err = error.response.data.errors
	        	
	        	if (err.email[0] != '') {
	        		swal('Ups!',err.email[0],'error')
	        	}
	        	if(err.passsword[0] != ''){
	        		swal('Ups!',err.password[0],'error')
	        	}
	        	
	           
	        });
	    } 
	}
});