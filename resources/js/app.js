require('./bootstrap');
import Vue from 'vue'
import { BootstrapVue, ModalPlugin ,BootstrapVueIcons} from 'bootstrap-vue'



/*metronic*/
import '../admDash/tools/webpack/vendors/global';
import '../admDash/tools/webpack/scripts';
import '../admDash/src/assets/js/scripts.bundle';
import '../admDash/src/assets/js/pages/components/extended/bootstrap-notify';
/*---metronic---*/
window.Vue = require('vue');

// Install BootstrapVue
Vue.use(BootstrapVue)
Vue.use(ModalPlugin)
Vue.use(BootstrapVueIcons)

Vue.component('all-product', require('../js/components/administrador/general/productos-misiones.vue').default);
Vue.component('all-pagos', require('../js/components/administrador/general/pagos.vue').default);
Vue.component('all-entregas', require('../js/components/administrador/general/entregas.vue').default);
Vue.component('lista-admin', require('../js/components/configuracion/usuarios/lista-admin.vue').default);
Vue.component('lista-clientes', require('../js/components/configuracion/usuarios/lista-clientes.vue').default);
Vue.component('conf-sistema', require('../js/components/configuracion/sistema/conf.vue').default);
Vue.component('mi-perfil', require('../js/components/miperfil/index.vue').default);

const app = new Vue({
    el: '#app',
	data: {
        currentActivity: 'home'
    }
});
