<HTML>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

<HEAD>

	<TITLE>factura {{$factura['numero_factura']}}</TITLE>

</HEAD>
<style type="text/css">
	.pdf-pago-titulos {
		text-align: center;
		font-family: 'Source Sans Pro', sans-serif;
		font-size: 20px;
	}

	.pdf-pago-subTitulos {
		text-align: center;
		font-family: 'Source Sans Pro', sans-serif;
		font-size: 26px;
	}
</style>

<BODY>

	<!-- Aqui va todo lo chachi -->
	<br>
	<div class="pdf-pago-titulos" style="text-align: center">
		<H1>thitonix</H1>
	</div>
	<HR>
	<div class="pdf-pago-subTitulos">
		<p>{{$factura['mision']}}</p>


		<table WIDTH="95%">
			<thead>
				<tr>
					<th>
						<h4>Servicio</h4>
					</th>
					<th>
						<h4 class="text-right">Descripción</h4>
					</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>Número de factura:</td>
					<td class=" text-right ">{{$factura['numero_factura']}}</td>
				</tr>
				<tr>
					<td>Metodo de pago:</td>
					<td class="text-right ">bitcoin</td>
				</tr>
				<tr>
					<td>Valor del producto o servicio:</td>
					<td class="text-right">{{number_format($propuestas['precio_final'],0,',','.')}}</td>
				</tr>
				<tr>
					<td>Comisión de Cliente:</td>
					<td class="text-right">{{number_format($cliente,0,',','.')}}</td>
				</tr>
				<tr>
					<td>Comisión de SCREENSHOT:</td>
					<td class="text-right">{{number_format($comision_screen,0,',','.')}}</td>
				</tr>
				<tr>
					<td class="text-right">TOTAL :</td>
					<td class="text-right">{{number_format($total,0,',','.')}}</td>
				</tr>
			</tbody>
		</table>
	</div>
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</BODY>

</HTML>