@if ($paginator->hasPages())
            {{-- Previous Page Link --}}
            @if ($paginator->onFirstPage())
                <div><a href="{{$paginator->previousPageUrl()}}" class="anteriorPag" disabled>Anterior</a></div>
            @else
                <div><a href="{{$paginator->previousPageUrl()}}" class="anteriorPag" disabled>Anterior</a></div>
            @endif

            {{-- Pagination Elements --}}
            @foreach ($elements as $element)
                {{-- "Three Dots" Separator --}}
                @if (is_string($element))
                      <li class="disabled" aria-disabled="true"><span>{{ $element }}</span></li>
                @endif

                {{-- Array Of Links --}}
                @if (is_array($element))
                    @foreach ($element as $page => $url)
                        @if ($page == $paginator->currentPage())
                        <div><a href="#" class="activoPag" >{{ $page }}</a></div>
                        @else
                        <div><a href="{{ $url }}" class="enumeracion">{{ $page }}</a></div>
                        @endif
                    @endforeach
                @endif
            @endforeach

            {{-- Next Page Link --}}
            @if ($paginator->hasMorePages())
            <div><a href="{{$paginator->nextPageUrl()}}" class="siguientePag">Siguiente</a></div> 
            @else
            <div><a href="{{$paginator->nextPageUrl()}}" class="siguientePag" disabled>Siguiente</a></div> 
            @endif
@endif
