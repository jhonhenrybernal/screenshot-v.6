<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Contraseña</title>
</head>
<body>
<style>
@import url('https://fonts.googleapis.com/css2?family=Source+Sans+Pro:wght@400;700&display=swap');
*{
    font-family: 'Source Sans Pro', sans-serif;
}
tbody{
display: grid;
justify-items: center;
}
</style>
    <table width="92%" style="max-width:700px; box-shadow: 10px 10px 39px -14px rgba(0,0,0,0.75);margin: 0 auto;padding: 38px;">
<td style="display: grid;justify-items: center;grid-template-columns: 1fr; ">
<td style="display: block;margin: 0 auto;"><img src="https://i.postimg.cc/9fZ4wFGV/imagen-correo.png" style="max-width:130px; display: block;
  margin-left: auto;
  margin-right: auto;
  width: 50%;"></td>
<tr>
<td>
<?php $protocol = isset($_SERVER["HTTPS"]) ? 'https' : 'http'; ?>
<h1 style="color: #494949;margin-bottom: 8px; text-align: center;">Contraseña restablecida</h1>
<p class="texto-mensaje" style="text-align: center;">Señor {{$name}} , Ha sido restablecido su contraseña. <br> Contraseña: {{$code}} .</p>
<a href="{{$protocol.'://'.$_SERVER["HTTP_HOST"].'/admin'}}" class="activar-mensaje" style="text-decoration: none;color: #fff !important;padding: 10px;border-radius: 7px;background-color: #FF5E4A;width:120px;display: block;text-align:center;margin: 0 auto;">Acceder</a>
</td>
</tr>
</table>



    </td>
</body>
</html>
