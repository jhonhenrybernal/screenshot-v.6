<!doctype html>
<html>

<head>
    <meta charset="UTF-8">
    <link rel="icon" type="image/vnd.microsoft.icon" href="{{ asset('/favicon-thitonix.ico') }}">
    <title>Misiones</title>
    <link href="../../css/anuncio.css" rel="stylesheet" type="text/css">
    <link href="../../css/modal-ganancias.css" rel="stylesheet" type="text/css">
    <link href="../../css/paginate.css" rel="stylesheet" type="text/css">
    <link href="../../css/alert-screen.css" rel="stylesheet" type="text/css">
    <!--link href="../../css/perfil.css" rel="stylesheet" type="text/css"-->
    <meta name="viewport"
        content="width=device-width,user-scalable=no,initial-scale=1.0,maximum-scale=1.0,minimum-scale=1.0">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" />
    <link rel="stylesheet" type="text/css"
        href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css" />

</head>

<body>
    @extends('layouts.app')
    @if (!empty($abrirModal))
        <input type="hidden" id="modal-pago" value="1">
    @endif
     <div id="main-body">

            <main class="contenedor" id="main-element-two">

            <section class="mover">
                <div class="nuncio">
                    <h3 class="anuncio"><span class="negro">Mis</span> Misiones</h3>
                </div>
                <p class="comi">Para reclamar tus ganancias da click <a href="#" id="myBtn-ganancias-coing">aquí</a></p>

                @if (!empty($products))
                    @foreach ($products as $list)
                        @if ($list->id_estados_anuncios == 24 or $list->id_estados_anuncios == 23 or $list->id_estados_anuncios == 19 or $list->id_estados_anuncios == 28 or $list->id_estados_anuncios == 30)
                            <div class="perfil">
                                <img src="{{ asset('imagenesproductos/' . $list->imagenendeuno->nombreImagen) }}" alt=""
                                    class="imagen">
                                <p class="texto abajo">{{ $list->ciudades->ciudad }}</p>
                                <p class="texto">{{ $list->misionDetalladamente }}</p>
                                @if ($list->id_tipo == 2)
                                    <a href="{{ route('misiones.envia', $list->id) }}" class="entrar" onclick="pageViewDetail()">
                                        <p class="mirar">Mas detalles</p>
                                    </a>
                                @elseif($list->id_tipo == 1)
                                    <a href="{{ route('misiones.recibe', $list->id) }}" class="entrar" onclick="pageViewDetail()">
                                        <p class="mirar">Mas detalles</p>
                                    </a>
                                @endif
                            </div>
                        @endif
                    @endforeach
                @else
                @endif
                @include('misiones.modal.ganancias-coing')
                <!--script src="../js/modal-ganancias.js"></script-->

                <script src="../js/modal-ganancia-coing.js"></script>
                <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
                <script src="{{ asset('DataTables/js/jquery.dataTables.min.js') }}"></script>

                <script>
                    $('body').on('click', '.modal-ganancias-mision', function(e) {
                        document.getElementById('ganancias-mision').style.display = 'block'
                    })

                    var table = $('#pagoTablausu-modal').DataTable({
                        responsive: true,
                        "order": [
                            [0, "desc"]
                        ],
                        "pageLength": 5,
                        "lengthChange": false,
                        "searching": false,
                        language: {

                            "decimal": "",
                            "emptyTable": "No hay información",
                            "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
                            "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
                            "infoFiltered": "(Filtrado de _MAX_ total entradas)",
                            "infoPostFix": "",
                            "thousands": ",",
                            "lengthMenu": "Mostrar _MENU_ Entradas",
                            "loadingRecords": "Cargando...",
                            "processing": "Procesando...",
                            "search": "Buscar:",
                            "zeroRecords": "Sin resultados encontrados",
                            "paginate": {
                                "first": "Primero",
                                "last": "Ultimo",
                                "next": "Siguiente",
                                "previous": "Anterior"
                            }
                        },
                        processing: true,
                        serverSide: true,
                        ajax: "{{ url('/misiones/ssp/index') }}",
                        "rowCallback": function(row, data, index) {},
                        columnDefs: [{
                            "className": "dt-center",
                            "targets": "_all"
                        }],
                        columns: [{
                                data: 'estado',
                                name: 'estado'
                            },
                            {
                                data: 'mision',
                                name: 'mision'
                            },
                            {
                                data: 'fecha',
                                name: 'fecha'
                            },
                            {
                                data: 'comisionScreenTotal',
                                render: $.fn.dataTable.render.number(',', '.', 2, '$'),
                                name: 'comisionScreenTotal'
                            },
                            {
                                data: 'cliente_ganancia',
                                render: $.fn.dataTable.render.number(',', '.', 2, '$'),
                                name: 'cliente_ganancia'
                            },
                            {
                                data: 'action',
                                name: 'action',
                                orderable: false,
                                searchable: false
                            }
                        ]
                    });


                    function tableReload() {
                        table.ajax.reload();
                    }

                    $(".btn-submit-ganancia-coing").click(function(e) {

                        e.preventDefault();
                        var deposito = document.getElementsByName("deposito");
                        var selectedDeposito = "";
                        for (var i = 0; i < deposito.length; i++) {
                            if (deposito[i].type == 'checkbox' && deposito[i].checked == true)
                                selectedDeposito += deposito[i].value + ",";
                        }

                        //var metodo = $("input[name=metodo]").val();
                        var cod = $("input[name=codigo_virtual]").val();



                        data = new FormData();
                        data.append('imgInp', $('#imgInp')[0].files[0]);
                        data.append('_token', "{{ csrf_token() }}");
                        data.append('codigo_virtual', cod);
                        data.append('selectedDeposito', selectedDeposito);

                        $.ajax({

                            type: 'POST',

                            url: "{{ url('pago/depositar-coing') }}",

                            data: data,
                            processData: false,
                            contentType: false,

                            success: function(data) {

                                if (data.status) {
                                    var alert =
                                        '<div class="alert success"><span class="closebtn">&times;</span><strong>Super!</strong>' +
                                        data.msm + '</div>'
                                    document.getElementById('alert-screenshot').innerHTML = alert;
                                    tableReload()
                                } else {
                                    var alert =
                                        '<div class="alert"><span class="closebtn">&times;</span><strong>Ups..! </strong>' +
                                        data.msm + '</div>'
                                    document.getElementById('alert-screenshot').innerHTML = alert
                                }
                            }
                        });

                    });

                </script>
            </section>
        </main>
        <div class="box-footer">
            <div class="center">
                {{ $products->links() }}
            </div>
        </div>
    </div>
</body>

</html>
