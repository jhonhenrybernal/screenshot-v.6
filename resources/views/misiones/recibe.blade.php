<!doctype html>
<html>

<head>
  <meta charset="UTF-8">
  <link rel="icon" type="image/vnd.microsoft.icon" href="{{ asset('/favicon-thitonix.ico') }}">
  <title>@extends('layouts.app')</title>
  <link href="../../css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <link href="../../css/propon.css" rel="stylesheet" type="text/css">
  <link href="../../css/flexslider.css" rel="stylesheet" type="text/css">
  <link href="../../css/alert-screen.css" rel="stylesheet" type="text/css">
  <link href="../../css/modal.css" rel="stylesheet" type="text/css">
  <meta name="viewport" content="width=device-width,user-scalable=no,initial-scale=1.0,maximum-scale=1.0,minimum-scale=1.0">
</head>

<body>
  @extends('layouts.app')
  <main class="cont-3">
    <div class="titulo-3">
      <h2 class="sub">Busco {{$products->mision}}</h2>
    </div>
    <section class="columna-1">
      <div class="flexslider">
        <ul class="slides">
          @foreach($imagenes as $img)
          <li>
            <img src="{{ asset('imagenesproductos/'.$img['nombreImagen']) }}" />
          </li>
          @endforeach
        </ul>
      </div>
    </section>
    <section class="columna-2">
      <h3 class="informa">Información del misionero</h3>
      <ul class="sub-mirar">
        <li class="usuario">
          <div class="centrar-img"><img src="{{ asset('img/avatar/'.$products->usuario->avatar.'.jpg')}}" class="imagen-mision"></div>
        </li>
        <li class="usuario">
          <p class="correr">Nombre del usuario<br><span class="suav">{{ $products->usuario->name }}</span></p>
        </li>
        <li class="usuario"><span class="espaciado"><span class="fa fa-map-marker"></span></span>
          <p class="correr">Ubicación del misionero<br><span class="suav">{{ $products->ciudades->ciudad }}</span></p>
        </li>
        <li class="usuario"><span class="espaciado"><span class="fa fa-phone"></span></span>
          <p class="correr">Número de contacto<br><span class="suav">{{ $products->usuario->celular }}</span></p>
        </li>
        <li class="usuario"><span class="espaciado"><span class="fa fa-envelope"></span></span>
          <p class="correr">Correo electronico<br><span class="suav">{{ $products->usuario->email }}</span></p>
        </li>
      </ul>
      <div>
        <p class="califica">Calificación del misionero</p>
        <ul class="cuenta">
          <input id="id_usuario_auth" type="hidden" value="{{$productsAuth->usuario->id}}">
          <input id="id_usuario_propuesta" type="hidden" value="{{ (empty($products->usuario))?'':$products->usuario->id}}">
          <li class="manos"><span class="dedos"><span class="fa fa-thumbs-up" onclick="califiBien()" id="calificacion-bien"><span class="num"></span></span></span></li>
          <li class="manos"><span class="mal"><span class="fa fa-thumbs-down" onclick="califiMal()" id="calificacion-mal"><span class="num"></span></span></span></li>
        </ul>
        <p class="comp"><span class="negri">Total de misiones:</span> {{$contMisiones}}</p>
        <p class="comp"><span class="negri">Misiones canceladas:</span>{{$contCanceladas}}</p>
        <div id="myAlert" style="display: none;" class="alert " role="alert">
          <h4 id="msmAlert"></h4>
        </div>
        <!--a href="#" class="hablar">Hablar con el misionero</a-->
        <!--p class="info alinea">Antes de dar click porfavor tener en cuenta la información que esta debajo</p-->
        @if($productsAuth->id_estados_anuncios == 20)
        <h3 class="informa">Proceso de entrega</h3>
        @endif
        @if($productsAuth->id_estados_anuncios == 25)
        <h3 class="informa">Por confirmar finalizado</h3>
        @endif
        @if($productsAuth->id_estados_anuncios == 24 or $productsAuth->id_estados_anuncios == 35)
        <p class="informa">FINALIZADO</p>
        @elseif($productsAuth->id_estados_anuncios == 19 or $productsAuth->id_estados_anuncios == 32)
        <a href="#" class="hablar cancelar cancelar-mision">Cancelar la misión</a>
        <br>
        {{ csrf_field()}}
        <a href="#" data-productos-id="{{$productsAuth->id}}" data-user-id="{{$productsAuth->usuario->id}}" data-final="2" class="hablar finalizar finalizar-mision">Finalizar la misión</a>
        @endif
        @if($productsAuth->id_estados_anuncios == 31 or $productsAuth->id_estados_anuncios == 34)
        <h3 class="informa">Reportaron como cancelado</h3>
        @endif
        <p class="precio">Tener en cuenta</p>
        <p class="info">En caso de que el cliente no halla notificado a SCREENSHOT de la finalización de la misión (solo si el producto o servicio ya fue realizado en caso de no ser asi la notificación sera cancelada), te invitamos a que nos notifiques dando click en FINALIZAR MISIÓN para llevar el preceso y que pueda seguir con el proceso para la entrega de tu dinero.</p>
      </div>
    </section>
    <section class="descripcion-2">
      <h3 class="describir">Descripción de la misión</h3>
      <p class="especific">{{$products->mision}}</p>
    </section>
    @include('misiones.modal.cancelar')
  </main>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
  <script src="../../js/jquery.flexslider-min.js"></script>
  <script src="../../js/jquery.fancybox.pack.js"></script>
  <script>
    $(function() {
      $('.flexslider').flexslider({
        animation: "slider"
      });

    });
    $('body').on('click', '.cancelar-mision', function(e) {
      document.getElementById('cancelar_entrega').style.display = 'block'
    })

    $(function() {
      document.getElementById('cancelar_entrega').style.display = 'none'
      $('#cancelar_entrega form').validator().on('submit', function(e) {
        if (!e.isDefaultPrevented()) {
          url = "{{ url('cancelar/mision') }}";
          $.ajax({
            url: url,
            type: "POST",
            data: $('#cancelar_entrega form').serialize(),
            success: function(data) {
              if (data === false) {
                var alert = '<div class="alert">span class="closebtn">&times;</span><strong>Ups..!</strong>Mensaje no enviado</div>'
                document.getElementById('alert-screenshot').innerHTML = alert
              } else {

                if (data === true) {

                  var alert = '<div class="alert success"><span class="closebtn">&times;</span><strong>Super!</strong> Mensaje enviado.</div>'
                  document.getElementById('alert-screenshot').innerHTML = alert;
                  setTimeout(function() {
                    location.reload();
                  }, 4000);

                }
              }

            },
            error: function() {
              $(document).trigger("add-alerts", [{
                "message": "No se pudo enviar.",
                "priority": 'error'
              }]);
            }

          });

          return false;
        }
      });
    });




    // FINALIZAR MISION
    $(function() {

      $(".finalizar-mision").click(function(e) {

        var id_producto = $(this).data('productos-id')
        $('#id_product').val(id_producto)
        var id_usuario = $(this).data('user-id')
        $('#id_usuario').val(id_usuario)
        var final_status = $(this).data('final')
        $('#id_usuario').val(id_usuario)

        $.ajax({

          type: 'POST',
          url: '{{route("finalizar.mision")}}',
          data: {
            idUsuario: id_usuario,
            idProducto: id_producto,
            final: final_status,
            _token: "{{ csrf_token() }}",
          },

          success: function(data) {

            if (data == true) {
              var elem = document.querySelector('#myAlert');
              elem.style.display = 'block';

              var element = document.getElementById("myAlert");
              element.classList.add("success");

              msmAlert.innerText = "Finalizado";

              if (final_status == 1) {
                setTimeout(function() {
                  location.href = "{{route('misiones.index')}}";
                }, 4000);
              } else if (final_status == 2) {
                setTimeout(function() {
                  location.href = "{{route('anuncio.index')}}";
                }, 4000);
              }
            } else if (data == 'no-finalizado') {
              var elem = document.querySelector('#myAlert');
              elem.style.display = 'block';

              var element = document.getElementById("myAlert");
              element.classList.add("error");

              msmAlert.innerText = "error";

            } else if (data == false) {
              var elem = document.querySelector('#myAlert');
              elem.style.display = 'block';

              var element = document.getElementById("myAlert");
              element.classList.add("error");

              msmAlert.innerText = "Error";

            }
          }
        });
      });
    });


    // calificacion
    function califiBien() {
      calificacion(1)
    }

    function califiMal() {
      calificacion(0)
    }

    function calificacion(estado) {

      var id_usuario = $('#id_usuario_propuesta').val()
      url = '{{url("status/calificacion")}}/' + id_usuario;

      $.get(url, function(data) {

        if (data.status) {
          var id_usuario_auth = $('#id_usuario_auth').val()
          var id_usuario_propuesta = $('#id_usuario_propuesta').val()
          $.ajax({

            type: 'POST',
            url: '{{url("api/enviar/calificacion")}}',
            data: {
              idUsuarioPropuesta: id_usuario_propuesta,
              idUsuarioAuth: id_usuario_auth,
              estado: estado,
            },

            success: function(data) {
              if (data) {
                $("#calificacion-bien").empty();
                $("#calificacion-mal").empty();
                statusCalifiacion()
              } else {
                alert('no se actualizo')
                $(document).trigger("add-alerts", [{
                  "message": 'No se actualizo',

                  "priority": 'error'
                }]);
              }
            }
          });
        } else {
          alert('Mision ya calificado.')
        }
      });
    }

    function statusCalifiacion() {
      var id_usuario = $('#id_usuario_propuesta').val()
      url = '{{url("status/calificacion")}}/' + id_usuario;

      $.get(url, function(data) {
        var bien = '<span class="num">' + data.bien + '</span>';
        var mal = '<span class="num">' + data.mal + '</span>';
        $('#calificacion-bien').append(bien)
        $('#calificacion-mal').append(mal)
      });
    }

    $(function() {
      statusCalifiacion()
    });
  </script>
</body>

</html>
