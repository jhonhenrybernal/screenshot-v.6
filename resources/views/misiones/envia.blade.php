<!doctype html>
<html>

<head>
  <meta charset="UTF-8">
  <link rel="icon" type="image/vnd.microsoft.icon" href="{{ asset('/favicon-thitonix.ico') }}">
  <title>Descripcion</title>
  <link href="../../css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <link href="../../css/process.css" rel="stylesheet" type="text/css">
  <link href="../../css/flexslider.css" rel="stylesheet" type="text/css">
  <link href="../../css/modal.css" rel="stylesheet" type="text/css">
  <link href="../../css/alert-screen.css" rel="stylesheet" type="text/css">
  <meta name="viewport" content="width=device-width,user-scalable=no,initial-scale=1.0,maximum-scale=1.0,minimum-scale=1.0">
</head>

<body>
  @extends('layouts.app')
  <main class="contenedor">
    <section>
      <h2 class="sub">{{$productsEnvio->mision}}</h2>
    </section>
    <span class="observar">
      <section class="columna-1">
        <div class="flexslider">
          <ul class="slides">
            @foreach($imagenes as $img)
            <li>
              <img src="{{ asset('imagenesproductos/'.$img['nombreImagen']) }}" />
            </li>
            @endforeach
          </ul>

        </div>
      </section>
      <section class="columna-2">
        <h3 class="informa">Información del misionero</h3>
        <ul class="sub-mirar">
          <li class="usuario"><span class="forma"><span class="fa fa-user-circle-o"></span></span>
            <p class="correr">Nombre del usuario<br><span class="suav">{{ $productsEnvio->usuario->name }}</span></p>
          </li>
          <li class="usuario"><span class="espaciado"><span class="fa fa-map-marker"></span></span>
            <p class="correr">Ubicación del misionero<br><span class="suav">{{ $productsEnvio->ciudades->ciudad }}</span></p>
          </li>
          <li class="usuario"><span class="espaciado"><span class="fa fa-phone"></span></span>
            <p class="correr">Número de contacto<br><span class="suav">{{ $productsEnvio->usuario->celular }}</span></p>
          </li>
          <li class="usuario"><span class="espaciado"><span class="fa fa-envelope"></span></span>
            <p class="correr">Correo electronico<br><span class="suav">{{ $productsEnvio->usuario->email }}</span></p>
          </li>
        </ul>
        <div>
          <!--p class="califica">Calificación del misionero</-->

        <ul class="cuenta">
            <input id="id_usuario_auth" type="hidden" value="{{$productoEnviado->usuario->id}}">
            <input id="id_usuario_propuesta" type="hidden" value="{{ $productoEnviado->usuario->id}}">
            <!--li class="manos"><span class="dedos"><span class="fa fa-thumbs-up" onclick="califiBien()" id="calificacion-bien"></span></span></li-->
            <!--li class="manos"><span class="mal"><span class="fa fa-thumbs-down"  onclick="califiMal()" id="calificacion-mal"></span></span></li-->
        </ul>
        <!--p class="comp"><span class="negri">Total de misiones:</span> {{$contMisiones}}</p-->
        <!--p class="comp"><span class="negri">Misiones canceladas:</span>{{$contCanceladas}}</p-->
        <!--p class="info alinea">Antes de dar click porfavor tener en cuenta la información que esta debajo</p-->
        <div data-alerts="alerts" data-titles='{"success": "<em>Super!</em>", "error": "<em>Error!</em>"}' data-ids="myid" data-fade="3000"></div>
        {{ csrf_field()}}
        @if($productoEnviado->id_estados_anuncios == 23 or $productoEnviado->id_estados_anuncios == 24 or $productoEnviado->id_estados_anuncios == 35)

            <p class="informa">FINALIZADO</p>
        @else
            @if($productoEnviado->id_estados_anuncios == 22 or $productoEnviado->id_estados_anuncios == 32 or $productoEnviado->id_estados_anuncios == 24)
            <a href="#" class="hablar cancelar cancelar-mision">Cancelar la misión</a>
            <!--a href="#" data-productos-id="{{$productoEnviado->id}}" data-user-id="{{$productoEnviado->usuario->id}}" data-final="1" class="hablar finalizar finalizar-mision">Finalizar la misión</a-->
            @endif
        @endif
            @if($productoEnviado->id_estados_anuncios == 25)
            <h3 class="informa">Por confirmar finalizado</h3>
            @endif
        @if($productoEnviado->id_estados_anuncios == 29 or $productoEnviado->id_estados_anuncios == 31 or $productoEnviado->id_estados_anuncios == 34)
                <h3 class="informa">Reportaron como cancelado</h3>
        @endif


          <p class="precio">Tener en cuenta</p>
          <p class="info">Si el producto o el servicio tuvieron algun problema, porfavor dar click en el botón de CANCELAR MISIÓN para que SCREENSHOT pueda llevar el caso y la posible devolución del dinero. En caso de no ser asi dar en el botón de FINALIZAR MISIÓN para notificar a SCREENSHOT y poder finalizar con el pago del misionero.</p>
        </div>
      </section>
    </span>
    <section class="descripcion">
      <h3 class="describir">Descripción de la misión</h3>
      <p class="especific">{{$productsEnvio->mision}}</p>
    </section>
    @include('misiones.modal.cancelar')
  </main>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
  <script src="../../js/jquery.flexslider-min.js"></script>
  <script src="../../js/jquery.fancybox.pack.js"></script>
  <script>
    $(function() {
      $('.flexslider').flexslider({
        animation: "slider"
      });

    });

    $('body').on('click', '.cancelar-mision', function(e) {
      document.getElementById('cancelar_entrega').style.display = 'block'
    })

    $(function() {

      $('#cancelar_entrega form').validator().on('submit', function(e) {
        if (!e.isDefaultPrevented()) {
          url = "{{ url('cancelar/mision') }}";
          $.ajax({
            url: url,
            type: "POST",
            data: $('#cancelar_entrega form').serialize(),
            success: function(data) {
              if (data === false) {
                var alert = '<div class="alert">span class="closebtn">&times;</span><strong>Ups..!</strong>Mensaje no enviado</div>'
                document.getElementById('alert-screenshot').innerHTML = alert

              } else {

                if (data === true) {


                  var alert = '<div class="alert success"><span class="closebtn">&times;</span><strong>Super!</strong> Mensaje enviado.</div>'
                  document.getElementById('alert-screenshot').innerHTML = alert;
                  setTimeout(function() {
                    location.reload();
                  }, 4000);

                }
              }

            },
            error: function() {
              $(document).trigger("add-alerts", [{
                "message": "No se pudo enviar.",
                "priority": 'error'
              }]);
            }

          });

          return false;
        }
      });
    });
  </script>
</body>

</html>
