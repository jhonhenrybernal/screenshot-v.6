<div id="cancelar_entrega" class="modal">

  <!-- Modal content -->
  <div class="modal-content-cancel">
    <span onClick="document.getElementById('cancelar_entrega').style.display = 'none'"  class="close">&times;</span>
    <h4 class="modal-title">Cuentanos porque desea cancelar?</h4>

    <div id="alert-screenshot"></div>
    <div class="modal-body">
      <form class="form-horizontal" data-toggle="validator">
       {{ csrf_field()}}  {{ method_field('POST')}}
       <input type="hidden" value="{{$products->id_producto_propuesta_final}}" name="id_producto" id="id_producto">

       <textarea rows="5" cols="110" class="textarea" name="mensaje" id="mensaje" required></textarea>

       <div style="padding: 15px 0 0 0;text-align: right;border-top: 1px solid #e5e5e5;">
        <button type="button" onClick="document.getElementById('cancelar_entrega').style.display = 'none'" class="pago-procesar" data-dismiss="modal">Cerrar</button>
        <button type="submit" class="close-button" >Cancelar</button>
      </form>
    </div>
  </div>
</div>