<div id="myModal-ganancias-coing" class="modal">
  <div class="modal-content">
    <span class="close">&times;</span>
    <form class="form-horizontal" data-toggle="validator">
      {{ csrf_field()}} {{ method_field('POST')}}
      <div class="modal-body">
        <div class="modal-body">
          <div class="container-fluid">
            <div class="row">
              <div class="col-sm-12">
                <table id="pagoTablausu-modal" class="display" style="width:100%">
                  <thead>
                    <tr>
                        <th>Estado</th>
                        <th>Nombre del producto</th>
                        <th>Fecha</th>
                        <th>Comición screenshot</th>
                        <th>Ganancia</th>
                        <th>Retirar dinero</th>
                      <!--th>Comición pagos inteligentes</th-->
                      <!--th>iva</th-->
                    </tr>
                  </thead>
                  <tbody>

                  </tbody>
                </table>
                {!! Form::token() !!}
                <div class="row">
                  <div class="col-6 col-sm-6">
                    <span class="negrilla">Billetera Virtual:</span>
                    <input type="text" class="form-control" name="codigo_virtual" id="codigo_virtual" placeholder="codigo_virtual" value="{{(empty($mediosPago->codigo_qr))?'':$mediosPago->codigo_qr}}">
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-6 col-sm-6">
                  <span class="negrilla">Codigo QR:</span>
                  <input type='file' id="imgInp" name="imgInp" />
                  <br>
                  @if (empty($mediosPago->imagen_qr))
                  <h3>Sin imagen</h3>
                  @else
                <img id="blah" src="<?php echo asset('../imagenesproductos/codigo_qr/'.$mediosPago->imagen_qr)  ?>"  width="100" height="100" />
                  @endif
                  <div class="form-group row">
                    <div class="col-sm-10">
                      <div id="alert-screenshot"></div>

                      <button type="submit" class="enviar-msm-button btn-submit-ganancia-coing">Guardar</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
    </form>
  </div>
</div>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script>
  function readImage(input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();
      reader.onload = function(e) {
        $('#blah').attr('src', e.target.result); // Renderizamos la imagen
      }
      reader.readAsDataURL(input.files[0]);
    }
  }

  $("#imgInp").change(function() {
    // Código a ejecutar cuando se detecta un cambio de archivO
    readImage(this);
  });
</script>
