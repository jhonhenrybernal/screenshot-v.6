<div id="myModal-ganancias" class="modal">
<div class="modal-content">
  <span class="close">&times;</span>
  <form class="form-horizontal" data-toggle="validator">
    {{ csrf_field()}}  {{ method_field('POST')}} 
    <div class="modal-body">
      <div class="modal-body">
        <div class="container-fluid">
          <div class="row">
            <div class="col-sm-12">
              <table id="pagoTablausu-modal" class="display" style="width:100%">
                <thead>
                  <tr>
                    <th>Retirar dinero</th>
                    <th>Estado</th>
                    <th>Nombre del producto</th>
                    <th>Fecha</th>
                    <th>Comición screenshot</th>
                    <th>Comición pagos inteligentes</th>
                    <th>iva</th>
                  </tr>
                </thead>
                <tbody>

                </tbody>
              </table>
              <hr width="75%" />
              <div class="row">                   
                <div class="col-4 col-sm-6">
                  <a class="btn btn-warning" href="{{route('medios.pago')}}" role="button">Agregar medios de pago</a>
                  <br>
                  <br>                    
                  @if(!empty($mediosPago->id))
                  <div class="col-4 col-sm-6" >   

                    <table class="table-medios-pago" class="display" style="width:40%">
                      <tr>
                        <th>Medio de pago</th>
                        <th>seleccione</th>
                      </tr>
                      @foreach($entidad as $row)
                      <tr>
                        @if(in_array($row->id,$entidadMedioPago))  
                        <td> <img src="{{ asset('img/entidades financieras/'.$row->imagen) }}" class="imagen" style="width:154px; height:96px;"></td>
                        <td class="text-center">
                          <div class="form-check">
                            <input type="checkbox" class="form-check-input check-ganancia" id="materialUnchecked" name="entidad" value="{{$row->id}}" >
                          </div>
                        </td>
                        @endif  
                      </tr>
                      @endforeach    
                      @if(!empty($mediosPago->entidad_bancaria_id))
                      <tr>
                        <td> <img src="{{ asset('img/entidades financieras').'/' .$mediosPago->entidad_bancaria->imagen }}" class="imagen" style="width:154px; height:96px;">
                          <h5>Numero de cuenta:  ...{{substr($mediosPago->numero_cuenta,-4)}}</h5>
                        </td>
                        <td class="text-center">
                          <div class="form-check">
                            <input type="checkbox" class="form-check-input check-ganancia" id="materialUnchecked" name="entidad" value="{{$mediosPago->entidad_bancaria_id}}">
                          </div>
                        </td>
                      </tr>
                      @endif
                    </table>                       
                  </div>
                  <input type="hidden" name="metodo" id="metodo-asign">
                  <div class="form-group row">
                    <div class="col-sm-10">
                       <div id="alert-screenshot"></div>
                      
                      <button type="submit" class="enviar-msm-button btn-submit-ganancia">Guardar</button>
                    </div>
                  </div>
                  @else
                  <h3>Sin registros para medios de pago</h3>
                  @endif
                </div>  
              </div>
            </div>
          </div>              
        </form>
      </div>
    </div>