<html>
<head>
<meta charset="UTF-8">
<link href="{{ asset('/css/menu.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">
<link rel="icon" type="image/vnd.microsoft.icon" href="{{ asset('/favicon-thitonix.ico') }}">
</head>

<header class="cont" id="main-element-header">
<div class="acomodar">
<h1 class="menu_bar">
<a href="javascript:void(0)" class="bt-menu"><span class="desplegar"><span class="fa fa-bars"></span></span></a><a href="{{ route('home') }}" class="bt-menu titulo"  id="submit-load-page-home">THITONIX</a>
</h1>
<nav class="interno">
<ul class="list">
@role('super-admin')
<li class="lista"><a href="{{ route('homeadmin') }}" class="enlace" id="submit-load-page-inicio"><span class="der"><span class="fa fa-home"></span></span>Inicio</a></li>
@endrole
@role('misionero')
<li class="lista"><a href="{{ route('home') }}" class="enlace" id="submit-load-page-inicio"><span class="der"><span class="fa fa-home"></span></span>Inicio</a></li>
@endrole
@role('misionero')
<li class="lista"><a href="{{ route('anuncio.index') }}" class="enlace" id="submit-load-page-anuncios"><span class="der"><span class="fa fa-pencil" ></span></span>Mis anuncios</a></li>
@endrole
@role('misionero')

<li class="lista"><a href="{{route('perfil.index',Auth::user()->id)}}" class="enlace" id="submit-load-page-perfil"><span class="der"><span class="fa fa-user"></span></span>Mi perfil</a></li>
@endrole
@role('misionero')
<li class="lista"><a href="{{ route('misiones.index') }}" class="enlace" id="submit-load-page-misiones"><span class="der"><span class="fa fa-book"></span></span>Mis misiones</a></li>
@endrole
@role('super-admin')
<li class="lista"><a href="{{ route('products.tabla') }}" class="enlace" id="submit-load-pagetbl"><span class="der"><span class="fa fa-book"></span></span>Panel admin</a></li>
@endrole
@role('misionero')
<li class="lista"><a href="{{ route('products.tablausuario') }}" class="enlace" id="submit-load-page-tbladm"><span class="der"><span class="fa fa-bell" id="campana"></span></span>Mensajes</a></li>
@endrole

<form name="theForm" action="{{ route('logout') }}" method="POST" style="display: none;">
{{ csrf_field() }}
</form>
@role('misionero|super-admin')
<li class="lista"><a  href="#" onClick="submitform()" class="enlace final" id="submit-load-page-salir"><span class="der">  <span class="fa fa-power-off"></span></span>Salir</a></li>
@endrole
@if (!Auth::check())
<li class="lista"><a  href="login" class="enlace-login final-login" onclick="pageViewDetail()"><span class="der">  <span class="fa fa-power-off"></span></span>Login</a></li>
@endif
</ul>
</nav>
</div>
</header>
@yield('content')

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
<script src="{{ asset('jquery/jquery.min.js') }}"></script>
<script src="{{ asset('js/menu.js') }}"></script>

<script src="{{ asset('DataTables/js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
<script type="text/javascript" language="javascript" src="//cdn.datatables.net/plug-ins/a5734b29083/integration/bootstrap/3/dataTables.bootstrap.js"></script>
<script src="{{ asset('validator/validator.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script src="{{ asset('js/jquery.flexslider-min.js') }}"></script>
<script src="{{ asset('js/jquery.fancybox.pack.js') }}"></script>
<script src="{{ asset('js/croppie.js') }}"></script>
 <script src="{{ asset('js/loadPage.js') }}"></script>
<script>
$(function() {
  $('.flexslider').flexslider({
    animation: "slider"
  });

    });

    $.ajaxSetup({


headers: {

    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

}

});

function submitform(){
  document.theForm.submit()
}



$(document).ready(function() {
    $('#buscar').on('keyup', function() {
        var key = $(this).val();
        var dataString = 'q='+key;
  $.ajax({
            type: "get",
            url: "/products/all/buscar",
            data: dataString,
            success: function(data) {
               //Escribimos las sugerencias que nos manda la consulta
                $('#suggestions').fadeIn(1000).html(data[0].misionDetalladamente);
                //Al hacer click en alguna de las sugerencias
                $('.suggest-element').on('click', function(){
                        //Obtenemos la id unica de la sugerencia pulsada
                        var id = $(this).attr('id');
                        //Editamos el valor del input con data de la sugerencia pulsada
                        $('#buscar').val($('#'+id).attr('data'));
                        //Hacemos desaparecer el resto de sugerencias
                        $('#suggestions').fadeOut(1000);
                        alert('Has seleccionado el '+id+' '+$('#'+id).attr('data'));
                        return false;
                });
            }
        });
    });
});


//envio de editar panel administrador
$(function(){
  $('#modal-form form').validator().on('submit', function(e){
    if (!e.isDefaultPrevented()){
      if (save_method == 'add') url = "{{ url('verificar') }}";
      else url = "{{ url('verificar') }}";

      var data = {
        'id_producto' : $('#id_producto').val(),
        'id_usuario' : $('#id_usuario').val(),
        'mensaje' :  $('#mensaje').val()
      }

      $.ajax({
        url : url,
        type : "POST",
        data : $('#modal-form form').serialize(),
        success : function(data){
          $('#modal-form').modal('show');
          $(document).trigger("add-alerts", [
          {
            "message": "mensaje Enviado.",
            "priority": 'success'
          }
        ]);
          $().DataTable().ajax.reload();
        },
        error : function(){
           $(document).trigger("add-alerts", [
          {
            "message": "No se pudo enviar.",
            "priority": 'error'
          }
        ]);
        }

      });

      return false;
    }
  });
});



//panel de usuario
  $.fn.dataTable.ext.errMode = 'none';
$('#anuncio-tablausu').DataTable({
  responsive: true,
  "order": [[0, 'desc']],
  bProcessing: true,
  bAutoWidth: false,
  language: {

        "decimal": "",
        "emptyTable": "No hay información",
        "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
        "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
        "infoFiltered": "(Filtrado de _MAX_ total entradas)",
        "infoPostFix": "",
        "thousands": ",",
        "lengthMenu": "Mostrar _MENU_ Entradas",
        "loadingRecords": "Cargando...",
        "processing": "Procesando...",
        "search": "Buscar:",
        "zeroRecords": "Sin resultados encontrados",
        "paginate": {
            "first": "Primero",
            "last": "Ultimo",
            "next": "Siguiente",
            "previous": "Anterior"
        }
    },
    processing: true,
    serverSide: true,
    ajax: "{{ route('api.panelusu') }}",
    "rowCallback": function(row, data,index){
      if (data["estado"] == "Nuevo mensaje") {
        $(row).find('td:eq(3)').css("color", "Red");
      }
    },
    columns:[
    {data: 'id_productos', name: 'id_productos'},
    {data: 'mision', name: 'mision'},
    {data: 'misionDetalladamente', name: 'productos.misionDetalladamente'},
    {data: 'estado', name: 'estado'},
    {data: 'fecha', name: 'fecha'},
    {data: 'action', name: 'action', orderable: false, searchable: false},

    ]
  });

  $.fn.dataTable.ext.errMode = 'none';

   $(function() {
      $(".fancybox").fancybox();
   });


//capturar consulta para modal de rverificar
  function rechazarAlert(id,id_usuario,id_msm){
   save_method = 'posVerificar';
  $('input[name=_method]').val('POST');
  $('#modal-form form')[0].reset();
  $.ajax({
    url: "{{ url('paneladm/verificar') }}" + '/' + id + '/' + id_usuario + "/ver",
    type: "GET",
    dataType:"JSON",
      success: function(data){
        $('#modal-form').modal('show');
        $('#id_producto').val(data.id);
        $('#id_usuario').val(data.id_usuario);
        $('#mision').val(data.mision);
        $('#name').val(data.usuario.name);


      },
      error : function(){
        alert(" no datos");
      }

    });
}


//panel de administrador

$('#anuncio-tabla').DataTable({
  responsive: true,
  "order": [[4, 'desc']],
  language: {

        "decimal": "",
        "emptyTable": "No hay información",
        "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
        "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
        "infoFiltered": "(Filtrado de _MAX_ total entradas)",
        "infoPostFix": "",
        "thousands": ",",
        "lengthMenu": "Mostrar _MENU_ Entradas",
        "loadingRecords": "Cargando...",
        "processing": "Procesando...",
        "search": "Buscar:",
        "zeroRecords": "Sin resultados encontrados",
        "paginate": {
            "first": "Primero",
            "last": "Ultimo",
            "next": "Siguiente",
            "previous": "Anterior"
        }
    },
    processing: true,
    serverSide: true,
    ajax: "{{ route('api.paneladm') }}",
    columns:[
    {data: 'usuario.name', name: 'usuario.name'},
    {data: 'usuario.apellido', name: 'usuario.apellido'},
    {data: 'mision', name: 'mision'},
    {data: 'estado.estado', name: 'estado.estado'},
    {data: 'fecha', name: 'fecha'},
    {data: 'action', name: 'action', orderable: false, searchable: false},

    ]
  });


//enviar mensajes misionero de las propuestas



$('body').on('click','.modal-pago-infousu', function(e){
  document.getElementById('pago-infousu').style.display = 'block'
    var id_usuario = $('#id_usuario').val()
    $('#id_usuario_mod').val(id_usuario)
     var id_producto = $('#id_producto').val()
    $('#id_producto_mod').val(id_producto)

  })




//envio de mensajes panel  propuestas ya aceptada

  $('#enviar-cliente form').validator().on('submit', function(e){
     if (!e.isDefaultPrevented()){
       url = "{{ url('enviar/mensaje') }}";
      $.ajax({
        url : url,
        type : "POST",
        data : $('#enviar-cliente form').serialize(),
        success : function(data){
         // $('#modal-form').modal('hide');
          if (data === false) {
            var alert = '<div class="alert">span class="closebtn">&times;</span><strong>Ups..!</strong>Mensaje no enviado</div>'
            document.getElementById('alert-screenshot').innerHTML = alert
          }else{
           if (data === true) {
           var alert = '<div class="alert success"><span class="closebtn">&times;</span><strong>Super!</strong> Mensaje enviado.</div>'
              document.getElementById('alert-screenshot').innerHTML = alert;
            setTimeout(function(){
                document.getElementById('historial').innerHTML = '';
                document.getElementById('mensaje').value = '';
                loadMensage( $('#enviar-cliente form').serialize())
                document.getElementById('alert-screenshot').innerHTML = ''
              //location.reload();
            },2000)

            }
          }

         },
        error : function(){
           $(document).trigger("add-alerts", [
              {
                "message": "No se pudo enviar el mensaje",
                "priority": 'error'
              }
            ]);
        }

      });

      return false;
    }
  });

  function loadMensage(data){
    //var editserialize = decodeURIComponent(data.replace(/&/g, ",").replace(/=/g, ":"))
    var editserialize = JSON.parse('{"' + decodeURI(data).replace(/"/g, '\\"').replace(/&/g, '","').replace(/=/g,'":"') + '"}')
    var id_producto = editserialize.id_producto
    $('#id_product').val(id_producto)
    var id_usuario = editserialize.id_usuario
    $('#id_usuario').val(id_usuario)
    var status = editserialize.status

     url = "{{ url('consulta/mensajes') }}" + '/' + id_producto + '/' + id_usuario + '/' + status;

      $.ajax({
        url : url,
        type : "get",
        dataType : 'json',
        data : $('#productos-id form').serialize(),
        success : function(data){
          var html = '';
           $.each(data,function(key, data) {
              html += '<div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 250px;"><div class="box-body chat" id="chat-box" style="overflow: hidden; width: auto; height: 250px;"><div class="box-body chat" id="chat-box" style="overflow: hidden; width: auto; height: 250px;"><div class="container" style="width: 830px"><!-- Timeline --><div class="timeline"><!-- Line component --><div class="line text-muted"></div><!-- Separator --><div class="separator text-muted"><time>'+data.date+'</time></div><!-- /Separator --><!-- Panel --><article class="panel panel-danger panel-outline">   <!-- Body -->   <!-- /Body --></article>  <!-- /Panel -->    <!-- Panel -->  <article class="panel '+data.article+'">      <!-- Icon -->       <div class="panel-heading icon" style=" width: 50px;  height: 50px;   background-image: url(../img/avatar/icon-chat/'+data.avatar+'.jpg);"> <class="glyphicon glyphicon-cog"></i>      </div>     <!-- /Icon -->            <!-- Heading -->   <div class="panel-heading">         <h2 class="panel-title">'+data.titulo+'</h2>      </div>    <!-- /Heading -->       <!-- Body -->          <div class="panel-body" ><h5 style=" text-align: justify;">'+data.msm+'</h5></div>         <!-- /Body -->  <!-- Footer --> <div class="panel-footer">  <small>'+data.user+'</small>   </div>   <!-- /Footer -->    </article>   <!-- /Panel -->   </div> <!-- /Timeline --></div></div></div></div>';
              $('#historial').html(html);
          });
           var scroll = document.getElementById('scroll');
           scroll.scrollTop = scroll.scrollHeight;
        },
        error : function(){
          alert('verifique desde admin');
        }

      });
  }

//envio de mensajes cancelar mision
$('body').on('click','.modal-mensaje-cancelado', function(e){
    $('#enviar-cliente-cancelado').modal('show');
    var id_producto = $(this).data('productos-id')
    $('#id_product').val(id_producto)
    var id_usuario = $(this).data('user-id')
    $('#id_usuario').val(id_usuario)
    var status = $('#status').val()

     url = "{{ url('consulta/mensajes') }}" + '/' + id_producto + '/' + id_usuario + '/' + status;

      $.ajax({
        url : url,
        type : "get",
        dataType : 'json',
        data : $('#productos-id form').serialize(),
        success : function(data){
          $('#modal-form').modal('hide');
          var html = '';
           $.each(data,function(key, data) {
              html += '<div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 250px;"><div class="box-body chat" id="chat-box" style="overflow: hidden; width: auto; height: 250px;"><div class="box-body chat" id="chat-box" style="overflow: hidden; width: auto; height: 250px;"><div class="container"><!-- Timeline --><div class="timeline"><!-- Line component --><div class="line text-muted"></div><!-- Separator --><div class="separator text-muted"><time>'+data.date+'</time></div><!-- /Separator --><!-- Panel --><article class="panel panel-danger panel-outline">   <!-- Body -->   <!-- /Body --></article>  <!-- /Panel -->    <!-- Panel -->  <article class="panel '+data.article+'">      <!-- Icon -->       <div class="panel-heading icon" > <class="glyphicon glyphicon-cog"></i>      </div>     <!-- /Icon -->            <!-- Heading -->   <div class="panel-heading">         <h2 class="panel-title">'+data.titulo+'</h2>      </div>    <!-- /Heading -->       <!-- Body -->          <div class="panel-body">'+data.msm+'</div>         <!-- /Body -->  <!-- Footer --> <div class="panel-footer">  <small>'+data.user+'</small>   </div>   <!-- /Footer -->    </article>   <!-- /Panel -->   </div> <!-- /Timeline --></div></div></div></div>';
              $('#historial').html(html);
          });
           var scroll = document.getElementById('scroll');
           scroll.scrollTop = scroll.scrollHeight;
        },
        error : function(){
          alert('verifique desde admin');
        }

      });
  })
  function msmCancelar(){
       url = "{{ url('enviar/mensaje/cancelar') }}";
       var msm = $('#mensaje-cancela').val()
       var idPrducto = $('#id_product').val()
      $.ajax({
        url : url,
        type : "POST",
        data : {mensaje:msm,id_producto:idPrducto},
        success : function(data){
          $('#modal-form').modal('hide');
            if (data === false) {
            $(document).trigger("add-alerts", [
            {
              "message": "'EL mensaje con cumple con las buenas practicas de comuncacion en la plataforma.",
              "priority": 'error'
            }
          ]);
          }else{
           if (data === true) {
            $(document).trigger("add-alerts", [
              {
                "message": "mensaje Enviado.",
                "priority": 'success'
              }
            ]);
            setTimeout(function(){
              $('#enviar-cliente').modal('hide');
              location.reload();
            },4000)

            }
          }

         },
        error : function(){
           $(document).trigger("add-alerts", [
              {
                "message": "No se pudo enviar el mensaje",
                "priority": 'error'
              }
            ]);
        }

      });

      return false;
  }
   $(function() {
      $('#load').hide();
  });

  $('.list-ciudad').select2({

});
$('.list-departamento').select2({

});


 //panel de usuario usuario pago
 $("#pagos-tab-usu").click(function(){
    $('#pagoTablausu').DataTable().ajax.reload()
});

$('#pagoTablausu').DataTable({
  responsive: true,
  "order": [[0, "asc" ]],
  language: {

        "decimal": "",
        "emptyTable": "No hay información",
        "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
        "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
        "infoFiltered": "(Filtrado de _MAX_ total entradas)",
        "infoPostFix": "",
        "thousands": ",",
        "lengthMenu": "Mostrar _MENU_ Entradas",
        "loadingRecords": "Cargando...",
        "processing": "Procesando...",
        "search": "Buscar:",
        "zeroRecords": "Sin resultados encontrados",
        "paginate": {
            "first": "Primero",
            "last": "Ultimo",
            "next": "Siguiente",
            "previous": "Anterior"
        }
    },
    processing: true,
    serverSide: true,
    ajax: "{{ route('api.panelusuPago') }}",
    "rowCallback": function(row, data,index){
    },
    columns:[
    {data: 'id_producto', name: 'id_producto'},
    {data: 'mision', name: 'mision'},
    {data: 'fecha_pago', name: 'fecha_pago'},
    {data: 'estado_msg', name: 'estado_msg'},
    {data: 'action', name: 'action', orderable: false, searchable: false},]
  });

 //panel de admin pago
 $("#pagos-tab-adm").click(function(){
    $('#pagoTablaadm').DataTable().ajax.reload()
});
$('#pagoTablaadm').DataTable({
  responsive: true,
  "order": [[0, "asc" ]],
  language: {

        "decimal": "",
        "emptyTable": "No hay información",
        "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
        "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
        "infoFiltered": "(Filtrado de _MAX_ total entradas)",
        "infoPostFix": "",
        "thousands": ",",
        "lengthMenu": "Mostrar _MENU_ Entradas",
        "loadingRecords": "Cargando...",
        "processing": "Procesando...",
        "search": "Buscar:",
        "zeroRecords": "Sin resultados encontrados",
        "paginate": {
            "first": "Primero",
            "last": "Ultimo",
            "next": "Siguiente",
            "previous": "Anterior"
        }
    },
    processing: true,
    serverSide: true,
    ajax: "{{ route('api.paneladmPago') }}",
    "rowCallback": function(row, data,index){
    },
    columns:[
      {data: 'estado', name: 'estado'},
      {data: 'mision', name: 'mision'},
      {data: 'name', name: 'name'},
      {data: 'comisionScreenTotal', name: 'comisionScreenTotal'},
      {data: 'comisionPasarela', name: 'comisionPasarela'},
      {data: 'iva', name: 'iva'},
      {data: 'action', name: 'action',orderable: false, searchable: false},]
  });

/*
$(function(){
$('button[type="button"].btn-outline-secondary').remove();
});
$("#input-b5").fileinput({
      language : 'en',
      'showUpload': false,
      showZoom: true,
      showDrag: false,
      uploadUrl: 'someurl',
      allowedFileType : ['image'],
      allowedFileExtensions : ['jpg','jpeg','png'],
      previewFileType :'image',
      showCaption: false,
      dropZoneEnabled: false,
      showRemove: true,
      maxFileCount: 4,
      maxFileSize: 100000,
      browseClass: "btn btn-success",
      browseLabel: "Elegir Imagen",
      browseIcon: "<i class=\"fa fa-file-image-o\"></i> ",
      removeClass: 'btn btn-danger',
      removeTitle: 'Borrar Imagen',
      removeLabel: "Borrar",
      removeIcon: "<i class=\"fa fa-trash-o\"></i> ",
      uploadClass: "btn btn-info",
      uploadLabel: "Subir",
      uploadIcon: "<i class=\"fa fa-upload\"></i> "
    });
*/


  //panel de usuario admin entrega
$("#entregas-tab-adm").click(function(){
    $('#entregaTablaadm').DataTable().ajax.reload()
});
$('#entregaTablaadm').DataTable({
  responsive: true,
  "order": [[0, "asc" ]],
  language: {

        "decimal": "",
        "emptyTable": "No hay información",
        "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
        "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
        "infoFiltered": "(Filtrado de _MAX_ total entradas)",
        "infoPostFix": "",
        "thousands": ",",
        "lengthMenu": "Mostrar _MENU_ Entradas",
        "loadingRecords": "Cargando...",
        "processing": "Procesando...",
        "search": "Buscar:",
        "zeroRecords": "Sin resultados encontrados",
        "paginate": {
            "first": "Primero",
            "last": "Ultimo",
            "next": "Siguiente",
            "previous": "Anterior"
        }
    },
    processing: true,
    serverSide: true,
    ajax: "{{ url('entregas/admin') }}",
    "rowCallback": function(row, data,index){
    },
    columns:[
    {data: 'mision', name: 'mision'},
    {data: 'name', name: 'name'},
    {data: 'fechaPago', name: 'fechaPago'},
    {data: 'estado', name: 'estado'},
    {data: 'action', name: 'action', orderable: false, searchable: false},]
  });

  //panel de usuario usuario entrega
$("#entregas-tab-usu").click(function(){
    $('#entregaTablausu').DataTable().ajax.reload()
});

$('#entregaTablausu').DataTable({
  responsive: true,

  "order": [[0, "desc" ]],
  language: {

        "decimal": "",
        "emptyTable": "No hay información",
        "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
        "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
        "infoFiltered": "(Filtrado de _MAX_ total entradas)",
        "infoPostFix": "",
        "thousands": ",",
        "lengthMenu": "Mostrar _MENU_ Entradas",
        "loadingRecords": "Cargando...",
        "processing": "Procesando...",
        "search": "Buscar:",
        "zeroRecords": "Sin resultados encontrados",
        "paginate": {
            "first": "Primero",
            "last": "Ultimo",
            "next": "Siguiente",
            "previous": "Anterior"
        }
    },
    processing: true,
    serverSide: true,
    ajax: "{{ url('entregas/usu') }}",
    "rowCallback": function(row, data,index){

    },
    columns:[
    {data: 'id', name: 'id'},
    {data: 'mision', name: 'mision'},
    {data: 'usuario_entrega', name: 'usuario_entrega'},
    {data: 'fecha', name: 'fecha'},
    {data: 'state', name: 'state'},
    {data: 'action', name: 'action', orderable: false, searchable: false},]
  });

  //ganancias

  $(function() {
    var a = $("#modal-pago").val();
    if (a == 1) {
      // Get the modal
      var modal = document.getElementById("myModal-ganancias");
       modal.style.display = "block";

    }

  });

    $('#table-medios-pago').DataTable( {
      responsive: true,
    });


//seleccion de un checkbo
let Checked = null;
//The class name can vary
for (let CheckBox of document.getElementsByClassName('check-ganancia')){
	CheckBox.onclick = function(){
  	if(Checked!=null){
      var deposito = document.getElementsByName("deposito");

      Checked.checked = false;
      Checked = CheckBox;
    }
    Checked = CheckBox;
    $("#metodo-asign").val(Checked.value);
  }
}


  $(function() {
    verifiNotificacion()
  });

  //notificaciones cada 10 min
  setInterval('verifiNotificacion()',100000);
  function verifiNotificacion(){

    $.get("{{ url('notificaciones') }}", function(data, status){

      if(data.status){
        $(".fa-bell").css("animation-name", "notificacion");
        var text = document.createTextNode("This just got added");
      }

    });
    var element = document.getElementById('campana');
    setTimeout( element.style.removeProperty("animation-name"), 2000);

  }

  //panel de usuario usuario pago
$("#propuestas-tab-usu").click(function(){
    $('#propuestaTablausu').DataTable().ajax.reload()
});
$('#propuestaTablausu').DataTable({
  responsive: true,
  "order": [[0, "asc" ]],
  language: {

        "decimal": "",
        "emptyTable": "No hay información",
        "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
        "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
        "infoFiltered": "(Filtrado de _MAX_ total entradas)",
        "infoPostFix": "",
        "thousands": ",",
        "lengthMenu": "Mostrar _MENU_ Entradas",
        "loadingRecords": "Cargando...",
        "processing": "Procesando...",
        "search": "Buscar:",
        "zeroRecords": "Sin resultados encontrados",
        "paginate": {
            "first": "Primero",
            "last": "Ultimo",
            "next": "Siguiente",
            "previous": "Anterior"
        }
    },
    processing: true,
    serverSide: true,
    ajax: "{{ route('api.propuestas') }}",
    "rowCallback": function(row, data,index){
    },
    columns:[
    {data: 'mision', name: 'mision'},
    {data: 'nombre', name: 'nombre'},
    {data: 'fecha_propuesta', name: 'fecha_propuesta'},
    {data: 'estado', name: 'estado'},
    {data: 'action', name: 'action', orderable: false, searchable: false},]
  });

//mostrar el medio de consignacion
$('body').on('click','.modal-medio-consignar', function(e){
    $('#modal-consignar').modal('show');
    $('.table-medios-app').hide();
    $('.table-medios-giros').hide();
    $('.table-medios-bancaria').hide();

    var metodoPago = $(this).data('pago-metodo')
    var idUsuario = $(this).data('usuario')
    var urlImg = '/img/entidades financieras/';
    $.ajax({

        type:'POST',
        url:'{{url("consultar/medio/pago")}}',
        data: { metodoPago:metodoPago,idUsuario:idUsuario},

        success:function(data){

          if(data.tipo == 3){
            $('.table-medios-app').show();
            $(".imagen").attr("src", urlImg+data.imagen);
            document.getElementById("numeroCel").innerHTML = data.numeroTel;
          }else if(data.tipo == 2){
            $('.table-medios-giros').show();
            $(".imagen").attr("src", urlImg+data.imagen);
            document.getElementById("numeroDoc").innerHTML = data.numeroDoc;
          }else if(data.tipo == 1){
            $('.table-medios-bancaria').show();
            $(".imagen").attr("src", urlImg+data.imagen);
            document.getElementById("numeroDocu").innerHTML = data.numeroDoc;
            document.getElementById("nombreTitular").innerHTML = data.nombreTitular;
            document.getElementById("numeroCuenta").innerHTML = data.numeroCuenta;
            document.getElementById("tipoCuenta").innerHTML = data.tipoCuenta;
          }
        }
      });

})

//panel de administrador cancelados

$('#anuncio-tabla-cancelados').DataTable({
  responsive: true,
  "order": [[4, 'desc']],
  language: {

        "decimal": "",
        "emptyTable": "No hay información",
        "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
        "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
        "infoFiltered": "(Filtrado de _MAX_ total entradas)",
        "infoPostFix": "",
        "thousands": ",",
        "lengthMenu": "Mostrar _MENU_ Entradas",
        "loadingRecords": "Cargando...",
        "processing": "Procesando...",
        "search": "Buscar:",
        "zeroRecords": "Sin resultados encontrados",
        "paginate": {
            "first": "Primero",
            "last": "Ultimo",
            "next": "Siguiente",
            "previous": "Anterior"
        }
    },
    processing: true,
    serverSide: true,
    ajax: "{{ route('api.cancelados') }}",
    columns:[
    {data: 'usuario.name', name: 'usuario.name'},
    {data: 'usuario.apellido', name: 'usuario.apellido'},
    {data: 'mision', name: 'mision'},
    {data: 'estado.estado', name: 'estado.estado'},
    {data: 'fecha', name: 'fecha'},
    {data: 'action', name: 'action', orderable: false, searchable: false},

    ]
  });

function closeModal(){
  document.getElementById('modal-enviar').style.display = 'none'
  document.getElementById('pago-infousu').style.display = 'none'
  document.getElementById('cancelar_entrega').style.display = 'none'
  document.getElementById('modal-detalle').style.display = 'none'
}



//limitar cantidad de caracteres tipulo de propuesta y uevo producto

function contarcaracteres(){

   //Numero de caracteres permitidos
   var total=20;

    setTimeout(function(){
    var valor=document.getElementById('contador');
    var respuesta=document.getElementById('res');
    var cantidad=valor.value.length;
    document.getElementById('res').innerHTML = (total - cantidad) ;
    if(cantidad>total){
        respuesta.style.color = "red";
    }
    else {
        respuesta.style.color = "black";
    }
    },10);

}


</script>
</html>
