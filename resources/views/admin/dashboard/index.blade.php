@extends('admin.layout.app')
@section('content')
<!DOCTYPE html>
<html lang="en">

<head>

    <title>Dashboard </title>
     <link href="{{ asset('../css/style.bundle.css')}}" rel="stylesheet" type="text/css" />


<body>

    <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" style="padding-top: 32px;" id="kt_wrapper">

        <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
            <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="kt-portlet">
                            <div class="kt-portlet__head">
                                <div class="kt-portlet__head-label">
                                    <h3 class="kt-portlet__head-title">
                                        Dashboard
                                    </h3>
                                </div>
                            </div>
                            <div class="row">
							<div class="col-xl-4 col-lg-4">

								<!--begin:: Widgets/Daily Sales-->
								<div class="kt-portlet kt-portlet--height-fluid">
									<div class="kt-widget14">
										<div class="kt-widget14__header kt-margin-b-30">
											<div class="col bg-light-primary px-6 py-8 rounded-xl mb-7">
                                                <span class="svg-icon svg-icon-3x svg-icon-primary d-block my-2">
                                                    <!--begin::Svg Icon | path:/metronic/theme/html/demo1/dist/assets/media/svg/icons/Communication/Add-user.svg-->
                                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                            <polygon points="0 0 24 0 24 24 0 24"></polygon>
                                                            <path d="M18,8 L16,8 C15.4477153,8 15,7.55228475 15,7 C15,6.44771525 15.4477153,6 16,6 L18,6 L18,4 C18,3.44771525 18.4477153,3 19,3 C19.5522847,3 20,3.44771525 20,4 L20,6 L22,6 C22.5522847,6 23,6.44771525 23,7 C23,7.55228475 22.5522847,8 22,8 L20,8 L20,10 C20,10.5522847 19.5522847,11 19,11 C18.4477153,11 18,10.5522847 18,10 L18,8 Z M9,11 C6.790861,11 5,9.209139 5,7 C5,4.790861 6.790861,3 9,3 C11.209139,3 13,4.790861 13,7 C13,9.209139 11.209139,11 9,11 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"></path>
                                                            <path d="M0.00065168429,20.1992055 C0.388258525,15.4265159 4.26191235,13 8.98334134,13 C13.7712164,13 17.7048837,15.2931929 17.9979143,20.2 C18.0095879,20.3954741 17.9979143,21 17.2466999,21 C13.541124,21 8.03472472,21 0.727502227,21 C0.476712155,21 -0.0204617505,20.45918 0.00065168429,20.1992055 Z" fill="#000000" fill-rule="nonzero"></path>
                                                        </g>
                                                    </svg>
                                                    <!--end::Svg Icon-->
                                                </span>
                                                <a href="#" class="text-primary font-weight-bold font-size-h6 mt-2">Total de clientes: {{$countUsers}}</a>
                                            </div>
										</div>
										<div class="kt-widget14__chart" style="height:120px;">
											<canvas id="kt_chart_daily_sales"></canvas>
										</div>
									</div>
								</div>

								<!--end:: Widgets/Daily Sales-->
							</div>
                            <div class="col-xl-4 col-lg-4">
                                <div id="chart_div" style="width: 600px; height: 500px;"></div>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawVisualization);


      function drawVisualization() {
        // Some raw data (not necessarily accurate)

        var data = google.visualization.arrayToDataTable(<?= $prdPropu?>);

        var options = {
          title : 'Cantidad de productos por mes',
          vAxis: {title: 'Cantidad'},
          hAxis: {title: 'Mes'},
          seriesType: 'bars',
          series: {50: {type: 'line'}}
        };

        var chart = new google.visualization.ComboChart(document.getElementById('chart_div'));
        chart.draw(data, options);
      }
    </script>


