<!DOCTYPE html>

<!--
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 4 & Angular 8
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
Renew Support: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<?php $nameProyect = (empty($nameProyect))?'Screenshot':$nameProyect ?>
<html lang="en">

	<!-- begin::Head -->
	<head>
		<base href="../../../">
		<meta charset="utf-8" />
		<title>{{$nameProyect}} | Login Admin</title>
		<meta name="description" content="Login page example">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

		<!--begin::Fonts -->
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700|Roboto:300,400,500,600,700">

		<!--end::Fonts -->

		<!--modulo metronic -->
		<link href="{{ asset('../css/app.css') }}" rel="stylesheet" type="text/css" >
		<!--//modulo metronic -->

		<!--end::Layout Skins -->
		<link rel="shortcut icon" href="assets/media/logos/favicon.ico" />
		<meta name="csrf-token" content="{{ csrf_token() }}">
	</head>

	<!-- end::Head -->

	<!-- begin::Body -->
	<body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled kt-subheader--fixed kt-subheader--solid kt-aside--enabled kt-aside--fixed kt-page--loading">
		<!-- begin:: Page -->

		<div class="kt-grid kt-grid--ver kt-grid--root">
			<div class="kt-grid kt-grid--hor kt-grid--root  kt-login kt-login--v4 kt-login--signin" id="kt_login">
				<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" style="background-image: url(assets/media/bg/bg-2.jpg);">
					<div class="kt-grid__item kt-grid__item--fluid kt-login__wrapper">
						<div class="kt-login__container" id="app_login">
							<div class="kt-login__logo">
								<a href="#">
									<img src="assets/media/logos/logo-5.png">
								</a>
							</div>
							<form class="kt-form" action=""  id="form">
								<div class="kt-login__signin">
									<div class="kt-login__head">
										<h3 class="kt-login__title">@lang('login_titulo') {{$nameProyect}}</h3>
									</div>
									@auth
										<h1>hola</h1>
									@endauth
										<div class="input-group">
											<input class="form-control" type="text" placeholder="@lang('playhol_us')" id="email" v-model="email" name="email" autocomplete="off">
										</div>
										<div class="input-group">
											<input class="form-control" type="password" placeholder="@lang('playhol_pw')" id="password" v-model="password" name="password">
										</div>
										<div class="row kt-login__extra">
											<div class="col">
												<label class="kt-checkbox">
													<input type="checkbox" name="remember"> @lang('login_remember')
													<span></span>
												</label>
											</div>
											
										</div>
										<div class="kt-login__actions">
											<button  type="button" class="btn btn-brand btn-pill kt-login__btn-primary" @click="iniciarSession()">@lang('login')</button>
										</div>
								</div>
							</form>
							<div class="kt-login__forgot">
								<div class="kt-login__head">
									<h3 class="kt-login__title"> @lang('password_titulo')</h3>
									<div class="kt-login__desc">@lang('password_sudtitulo')</div>
								</div>
								<form class="kt-form" action="">
									<div class="input-group">
										<input class="form-control" type="text" placeholder="@lang('playhol_pw_mail')" name="email" id="kt_email" autocomplete="off">
									</div>
									<div class="kt-login__actions">
										<button id="kt_login_forgot_submit" class="btn btn-brand btn-pill kt-login__btn-primary">@lang('button_enviar')</button>&nbsp;&nbsp;
										<button id="kt_login_forgot_cancel" class="btn btn-secondary btn-pill kt-login__btn-secondary">@lang('button_cancelar')</button>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- end:: Page -->
		<script src="{{ asset('../js/app.js') }}"></script>
		<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.2/dist/jquery.validate.js"></script>
		<!-- begin::Global Config(global config for global JS sciprts) -->
		<script>
			var KTAppOptions = {
				"colors": {
					"state": {
						"brand": "#5d78ff",
						"dark": "#282a3c",
						"light": "#ffffff",
						"primary": "#5867dd",
						"success": "#34bfa3",
						"info": "#36a3f7",
						"warning": "#ffb822",
						"danger": "#fd3995"
					},
					"base": {
						"label": [
							"#c5cbe3",
							"#a1a8c3",
							"#3d4465",
							"#3e4466"
						],
						"shape": [
							"#f0f3ff",
							"#d9dffa",
							"#afb4d4",
							"#646c9a"
						]
					}
				}
			};

			$('#form').validate({
			  	rules: {
			        email: "required",
			        password: "required"
			    },
			  	messages:{
			  		email:{
			  			required:"Usuario requerido."
			  		},
			  		password:{
			  			required:"Contraseña requerida."
			  		}

			  	},
			  	errorPlacement: function (error, element) {
			        error.css({'position':'absolute'});
			        error.addClass("arrow")
			        error.insertAfter(element);
			    },
			  	submitHandler: function(form) {
			      	form.button();
			    }
			});
		</script>

		
	</body>

	<!-- end::Body -->
</html>