{{-- Extends layout --}}

@extends('admin.layout.app')
@section('content')
<!DOCTYPE html>
<html lang="en">

<head>

	<title>Lista de Clientes </title>
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700|Roboto:300,400,500,600,700">
	<link href="{{ asset('../DataTables/css/dataTables.bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
	<link href="{{ asset('../DataTables/css/jquery.dataTables.min.css')}}" rel="stylesheet" type="text/css" />
	<!--begin::Global Theme Styles(used by all pages) -->
	<link href="assets/plugins/global/plugins.bundle.css" rel="stylesheet" type="text/css" />
	<link href="assets/css/style.bundle.css" rel="stylesheet" type="text/css" />

	<!--infornacion detallada modal-->
	<link href="{{ asset('../css/admin/admin-modal-detalle/producto.css')}}" rel="stylesheet" type="text/css" />
	<meta name="csrf-token" content="{{ csrf_token() }}">

<body>

	<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" style="padding-top: 32px;" id="kt_wrapper">

		<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
			<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
				<div class="row">
					<div class="col-xl-12">
						<div class="kt-portlet">
							<div class="kt-portlet__head">
								<div class="kt-portlet__head-label">
									<h3 class="kt-portlet__head-title">
										Lista de Clientes
									</h3>
								</div>
							</div>
							<div id="app">
								<lista-clientes></lista-clientes>
							</div>

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
