<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<link rel="icon" type="image/vnd.microsoft.icon" href="{{ asset('/favicon-thitonix.ico') }}">
<title>Publicar mision</title>
<link href="{{ asset('css/css-inicio/exam-publi.css')}}" rel="stylesheet" type="text/css">
<meta name="viewport" content="width=device-width,user-scalable=no,initial-scale=1.0,maximum-scale=1.0,minimum-scale=1.0">
<link href="{{ asset('css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">
</head>

<body>
@include('inicio-thitonix.cabecera-informativa')
<main class="contenedor-publicar">
<section class="portada-publicar">
<div><img src="{{ asset('img/inicio/publicar-mision.jpg')}}" class="imagen-uno-publicar" alt="publicar-mision"></div>
<div class="caja-uno-publicar">
<h2 class="uno-publicar-anuncio">01</h2>
<h2 class="titulo-uno-publicar">Obtener una misión</h2>
<p class="texto-uno-publicar">Una misión dentro de la plataforma significa que fue aprobado una propuesta que realizaste en una publicación para otro usuario sobre un producto o servicio que puedes realizar. Para tener una aprobación primero debes buscar un anuncio en el cual estes interesado realizar y llenar el formulario para hacer la propuesta. La cantidad de propuestas que puedes hacer no tienen limite y tampoco tienes que pagar por membresías.</p>
</div>
</section>

<section class="caja-dos-publicar">
<div>
	<span class="icono-publicar"><span class="fa fa-eye"></span></span>
	<h2 class="titulo-dos-publicar">02 Aprobación de la propuesta</h2>
	<p class="texto-dos-publicar"> Cada propuesta que realices a otros usuarios entraran en revisión para verificar si cumple con las políticas de nosotros. Cuando este aprobado tienes las posibilidad de que seas contactado para resolver dudas o que tu propuesta sea pagada para comenzar una nueva misión.</p>

</div>

<div>
	<span class="icono-publicar"><span class="fa fa-check-square"></span></span>
	<h2 class="titulo-dos-publicar">03 Realizar misión</h2>
	<p class="texto-dos-publicar">Cuando tu propuesta es escogida y es pagado lo acordado se convertirá automáticamente en un misión donde lo podras verificar en la sección del menú de "Mis misiones". En esta sección podras realizar todo el seguimiento para la entrega del producto o realizar el servicio.</p>

</div>

<div>
	<span class="icono-publicar"><span class="fa fa-handshake-o"></span></span>
	<h2 class="titulo-dos-publicar">04 Misión terminada</h2>
	<p class="texto-dos-publicar">Una vez hallas terminado con el trabajo es necesario que le recuerdes a la otra persona que nos notifique de la finalización de la misión. Cuando este finalizado podras comenzar hacer el proceso para la entrega de tu dinero que se hará por medio de Bitcoin.</p>

</div>
</section>

</main>
</body>
</html>
