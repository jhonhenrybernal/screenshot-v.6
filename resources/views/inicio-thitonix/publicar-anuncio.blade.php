<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<link rel="icon" type="image/vnd.microsoft.icon" href="{{ asset('/favicon-thitonix.ico') }}">
<title>Publicar anuncio</title>
<link href="{{ asset('css/css-inicio/exam-publi.css')}}" rel="stylesheet" type="text/css">
<meta name="viewport" content="width=device-width,user-scalable=no,initial-scale=1.0,maximum-scale=1.0,minimum-scale=1.0">
<link href="{{ asset('css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">
</head>

<body>
@include('inicio-thitonix.cabecera-informativa')
<main class="contenedor-publicar">
<section class="portada-publicar">
<div><img src="{{ asset('img/inicio/publicar-anuncio.jpg')}}" class="imagen-uno-publicar" alt="publicar-anuncio"></div>
<div class="caja-uno-publicar">
<h2 class="uno-publicar-anuncio">01</h2>
<h2 class="titulo-uno-publicar">Crear un anuncio</h2>
<p class="texto-uno-publicar">Para publicar un anuncio de un servicio o producto que estes buscando tienes que ir a la sección del menu donde dice "Mis anuncios", hay encontraras un boton de crear anuncio. Al momento de entrar econtraras un formulario el cual llenaras con la información que te sera pedida.</p>
</div>
</section>

<section class="caja-dos-publicar">
<div>
	<span class="icono-publicar"><span class="fa fa-eye"></span></span>
	<h2 class="titulo-dos-publicar">02 Aprovación de anuncio</h2>
	<p class="texto-dos-publicar"> Cuando hallas llenado el formulario con la infomación que te pedimos, sera puesta en revision y donde sera aceptada para ser publicada en la plataforma. Tu anuncio puede ser rechazado o puesto en verificacion si no llega a cumplir con nuestras politicas.</p>

</div>

<div>
	<span class="icono-publicar"><span class="fa fa-newspaper-o"></span></span>
	<h2 class="titulo-dos-publicar">03 Realizar misión</h2>
	<p class="texto-dos-publicar">Si tu anuncio llega ser aprovado, sera publicado en la plataforma para que empieces a recibir propuestas de precios. En las seccion de "Mis anuncios" apareceran tus avisos el cual podras darle todo el seguimiento de todo el proceso que acabas de comenzar.</p>

</div>

<div>
	<span class="icono-publicar"><span class="fa fa-handshake-o"></span></span>
	<h2 class="titulo-dos-publicar">04 Finaliza la misión</h2>
	<p class="texto-dos-publicar">Si has recibido tu producto o el servicio este terminado, dale finalizar misión para que la persona que te ayudo pueda recibir su pago lo mas pronto posible. En caso de tener problemas puedes dar cancelar misión y comunicarte con nosotros para llegar a un acuerdo.</p>

</div>
</section>

</main>
</body>
</html>
