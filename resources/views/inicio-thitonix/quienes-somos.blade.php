<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>Quienes somos</title>
<link rel="icon" type="image/vnd.microsoft.icon" href="{{ asset('/favicon-thitonix.ico') }}">
<link href="{{ asset('css/css-inicio/quienes-somos.css')}}" rel="stylesheet" type="text/css">
<meta name="viewport" content="width=device-width,user-scalable=no,initial-scale=1.0,maximum-scale=1.0,minimum-scale=1.0">
<link href="{{ asset('css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">
</head>

<body>
@include('inicio-thitonix.cabecera-informativa')
<main class="contenedor-somos">
<section class="nosotros-caja">

<div class="titular-somos">
<h2 class="titulo-somos">AYUDA A OTRAS PERSONAS Y GANA DINERO</h2>
<p class="texto-uno-somos">Prueba la plataforma donde podras publicar lo que tanto buscas y hacer propuestas de productos y servicios, donde tendras la posibilidad de interactuar con varios usuarios de una forma  fácil.</p>
<p class="texto-uno-somos">Crea un anuncio<br>
Recibe propuestas de usuarios<br>
Paga lo acordado<br>
Y listo ten tu producto o servicio</p>
<a href="{{ url('/register') }}" class="boton-somos">Comienza ahora</a>
</div>
<div class="contiene-imagen-somos">
<img src="{{ asset('img/inicio/quienes-somos.jpg')}}" class="imagen-somos" alt="quienes-somos">
</div>
</section>
<section class="somos-exp">
<div class="titular-dos-somos">
<h2 class="titulo-dos-somos">¿Quienes somos?</h2>
<p class="texto-dos-somos">Somos una plataforma donde podras publicar servicios o productos que estes buscando, donde tambien podras crear propuestas para las personas que estén necesitando tu ayuda en encontrar lo que tanto les hace falta.<br>

Nuestros servicios dentro de la plataforma son totalmente gratis en todo el proceso y donde tendrás la posibilidad de hacer anuncios y propuestas de forma ilimitada, solamente generamos cobro de una comisión al momento que vallas a pagar.</p>
</div>
<div class="contiene-dos-somos">

<img src="{{ asset('img/inicio/computadora-thitonix.png')}}" class="segunda-somos" alt="computadora-thitonix">

</div>
</section>

<section class="cuadros-info">
<div>
<span class="somos-iconos"><span class="fa fa-handshake-o"></span></span>
<h3 class="titulo-cuadro-somos">Negocios seguros</h3>
	<p class="cuadro-text-somos">Titonic tiene presente tu seguridad en cada negocio que generes con otros usuarios dentro de la página, es por eso que cada anuncio y propuesta que se genere dentro de la página tendrá unos filtros de seguridad para ser aprobados. Si llegas a necesitar ayuda da click <a href="{{ route('servicio-cliente') }}">aquí</a></p>


</div>

<div>
	<span class="somos-iconos"><span class="fa fa-bitcoin"></span></span>
<h3 class="titulo-cuadro-somos">Pagos con criptomonedas</h3>
	<p class="cuadro-text-somos">Para hacer pagos en cualquier parte del mundo y sin complicaciones, hemos aceptado solo el uso de criptomenodas en la pasarela de pagos y donde tendrás nuestro respaldo en el cuidado de tu dinero. Si quieres saber más da click <a href="{{ route('medios-pago') }}">aquí.</a> </p>


</div>

<div>
	<span class="somos-iconos"><span class="fa fa-line-chart"></span></span>
<h3 class="titulo-cuadro-somos">Crece con nosotros</h3>
<p class="cuadro-text-somos">Nosotros estamos comprometidos en el crecimiento de tu negocio, es por eso que no vamos a generar cobros por membresías o ponerte limites que te generen dolores de cabeza. Solo tienes que preocuparte de nuestras pequeñas comisiones.</p>


</div>
</section>
</main>
</body>
</html>
