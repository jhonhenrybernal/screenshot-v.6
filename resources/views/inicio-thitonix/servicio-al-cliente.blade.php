<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<link rel="icon" type="image/vnd.microsoft.icon" href="{{ asset('/favicon-thitonix.ico') }}">
<title>Servicio al cliente</title>
<link href="{{ asset('css/css-inicio/servicio-al-cliente.css')}}" rel="stylesheet" type="text/css">
<link href="{{ asset('css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">
</head>

<body>
@include('inicio-thitonix.cabecera-informativa')
<main class="medios-contenido">
<section class="seccion-servicio">
<div class="contiene-peque-servicio">
<img src="{{ asset('img/inicio/contacto.jpg')}}" class="imagen-servicio" alt="servicio-al-cliente">
</div>

<div class="contiene-uno-servicio">
<div class="contiene-dos-servicio">
<h3 class="titulo-servicio">Servicio al cliente</h3>
<p class="text-info-servicio">
Si tienes cualquier pregunta o simplemente quieres decir hola, la mejor manera de contactarnos es enviar un email. Leemos cada email y los distribuimos internamente, terminarás hablando con la persona adecuada. Hacemos todo lo posible para dar una respuesta en menos de 24 horas, pero ten en cuenta que los fines de semana también nos tomamos un descanso. <br>
<span class="correo-servicio">screenshot.com</span>
<br>Si tardamos un poco más, no desesperes, seguro que estamos a punto de responder o tratando de buscar la mejor solución a tu caso.
<br>
<span class="contacto-servicio">Contactanos</span>
<span class="logo-contacto-pago"><span class="fa fa-telegram"></span></span> +57 3102390123</p>
</div>
</div>
</section>

	</main>

</body>
</html>
