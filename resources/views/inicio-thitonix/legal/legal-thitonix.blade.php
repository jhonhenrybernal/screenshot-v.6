<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<link rel="icon" type="image/vnd.microsoft.icon" href="{{ asset('/favicon-thitonix.ico') }}">
<title>TERMINOS Y POLITICAS DE PRIVACIDAD</title>
<link href="{{ asset('css/css-inicio/legal.css')}}" rel="stylesheet" type="text/css">
<meta name="viewport" content="width=device-width,user-scalable=no,initial-scale=1.0,maximum-scale=1.0,minimum-scale=1.0">
</head>

<body>
<main class="contenedor-legal">
<header>
	<h1 class="logo-thito"><a href="{{ url('login') }}" class="ini-legal">THITONIX</a></h1>
</header>
<section class="alinear-legal" >
<div class="seccion-legal">
	<h2 class="sub-legal">TERMINOS Y POLITICAS DE PRIVACIDAD</h2>
	<p class="legal-bienvenida">Bienvenido a tithonix conoce nuestros términos y condiciones y políticas de privarcidad y cookies. En las siguientes dos opciones.</p>
	<a href="{{ route('terminos') }}" class="seleccion-legal">TERMINIOS Y CONDICIONES</a>
	<a href="{{ route('politica-privacidad') }}" class="seleccion-legal">POLITICAS DE PRIVACIDAD Y COOKIES</a>
</div>



</section>
</main>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
<script type="text/javascript" src="js/jquery.backstretch.min.js"></script>
<script>
$(document).ready(function(e) {
$.backstretch(["img/inicio/fotociudad.jpg"]);
});

</script>
</body>
</html>
