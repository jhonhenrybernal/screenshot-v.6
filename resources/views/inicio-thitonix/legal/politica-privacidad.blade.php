<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>Privacidad y cookies</title>
<link rel="icon" type="image/vnd.microsoft.icon" href="{{ asset('/favicon-thitonix.ico') }}">
<meta name="viewport" content="width=device-width,user-scalable=no,initial-scale=1.0,maximum-scale=1.0,minimum-scale=1.0">
<link href="{{ asset('css/css-inicio/terminoscon.css')}}" rel="stylesheet" type="text/css">
</head>
<main class="conteiner-rul">
<header class="cabecera-rul">
<h1 class="titulo-legal"><a href="{{ route('legal-thitonix') }}" class="link-rul">THITONIX</a></h1>
<div class="traductor-rul">@include('inicio-thitonix.traductor')</div>
</header>
<section class="seccion-rul">
<div class="sub-contenedores-rul">
<div>
<h2 class="sub-cabecera-uno">Política de privacidad y Cookies</h2>
</div>
<div>
<h2 class="sub-titulo-rul">Qué información recolectamos?</h2>
<p class="texto-rul">Recolectamos información tuya cuando te registras en nuestro sitio, llenas un formulario o navegas nuestro sitio.
	Cuando te registres en nuestro sitio te pediremos que ingreses: nombre, nombre de usuario, email, contraseña. También se solicitará un teléfono previo a realizar un pago. Sin embargo, podrás visitar nuestro sitio en forma anónima.</p>

</div>

</div>



<div class="sub-contenedores-rul">
	<div><h2 class="sub-titulo-rul">Para qué utilizamos tu información?</h2></div>
<p class="texto-rul">Cualquiera de las informaciones que recolectamos sobre ti podrá ser usada de alguna de estas formas:</br>
	●	Para personalizar tu experiencia
(tu información nos ayuda a responder mejor a tus necesidades particulares)</br>
●	Para mejorar nuestro sitio web
(nos esforzamos continuamente por mejorar las ofertas de nuestro sitio web, basándonos en la información y feedback que nos proporcionas)</br>
●	Para mejorar el servicio al cliente
(tu información nos ayuda a responder de una manera más eficaz a tus consultas referentes a Servicio al Cliente, y a satisfacer tus necesidades)</br>
●	Para procesar transacciones
Tu información, ya sea pública o privada, no será vendida, intercambiada, transferida o cedida a ninguna otra compañía por ninguna razón, sin tu consentimiento, excepto para el expreso propósito de prestar el servicio requerido.</br>
●	Para gestionar un concurso, promoción, encuesta u otra funcionalidad del sitio</br>
●	Para enviar correos electrónicos periódicos</br>
La dirección de correo electrónico que nos brindas puede ser utilizada para enviarte información y actualizaciones, además de que ocasionalmente puedas recibir noticias de la compañía, información sobre los servicios, etc.</br>
	Nota: Si en algún momento quieres dejar de recibir tales correos electrónicos, incluimos las instrucciones al final de cada e-mail.</p>

</div>

<div class="sub-contenedores-rul">
	<div><h2 class="sub-titulo-rul">Cómo protegemos tu información?</h2></div>
<div><p class="texto-rul">Implementamos una serie de medidas de seguridad para mantener protegida tu información personal cada vez que ingresas, envías o accedes a tu información personal.
Utilizamos un servidor seguro. Toda la información sensible es transmitida a través de tecnología Secure Socket Layer (SSL). Enseguida es encriptada en nuestra Base de Datos para que sólo puedan acceder quienes posean permisos específicos a nuestros sistemas, y se les exige que mantengan la confidencialidad de esa información.
	Luego de una transacción, tu información privada (monedero de bitcoin, códigos QR, etc) no será almacenada en nuestro archivo. De ninguna manera esta información será publica ni podrá ser cedida, vendida o utilizada de modo alguno por Thitonix.</p></div>

</div>


<div class="sub-contenedores-rul">
	<div><h2 class="sub-titulo-rul">Utilizamos cookies?</h2></div>
<div><p class="texto-rul">Sí (las Cookies son pequeños archivos que un sitio, o su prestador de servicio, transfiere al disco duro de tu computadora a través de tu navegador Web (si así lo habilitas). Esto permite que los sitios y sistemas de prestación de servicios reconozcan tu navegador, y capturen y recuerden cierta información).
	Las cookies nos ayudan a recordar, a entender y a almacenar tus preferencias para visitas futuras, y a compilar información adicional sobre el tráfico e interacción en el sitio, de modo de poder ofrecerte una mejor experiencia de usuario y perfeccionar las herramientas a futuro. Podremos también celebrar contratos con terceros que nos ayuden a entender mejor a nuestros visitantes del sitio. Estos prestadores de servicio no pueden utilizar en nuestro nombre la información recolectada, excepto para ayudarnos a llevar adelante nuestro negocio y mejorarlo.</p></div>


</div>

<div class="sub-contenedores-rul">
	<div><h2 class="sub-titulo-rul">Revelamos información a terceros?</h2></div>
	<div><p class="texto-rul">No vendemos, comercializamos ni transferimos a terceros tu información personal identificable. Esto no incluye a terceros en los que confiamos para asistirnos en la operación del sitio, en la gestión de nuestro negocio, o en el servicio que te ofrecemos, siempre y cuando ellos se comprometan a mantener la confidencialidad de esa información. También podremos ceder tu información cuando consideremos que es necesario para acatar la ley, hacer cumplir nuestras políticas del sitio, o proteger los derechos, propiedades y seguridad propios o ajenos. Sin embargo, la información identificable no personal puede ser suministrada a terceros para su utilización en acciones de marketing, publicidad y otras.</p></div>

</div>

<div class="sub-contenedores-rul">
<div><h2 class="sub-titulo-rul">Protección de la Privacidad En Línea </h2></div>
	<div><p class="texto-rul">Todos los usuarios de nuestro sitio podrán modificar su información en cualquier momento, ingresando a su panel de control y dirigiéndose a la página “Editar Perfil” asimismo podrán cancelar su cuenta cuando lo deseen y pedir la eliminación de los datos cargados.</p></div>

</div>


<div class="sub-contenedores-rul">
<div><h2 class="sub-titulo-rul">Cumplimiento del Acta de Protección de la Privacidad En Línea de los Niños</h2></div>
	<div><p class="texto-rul">Cumplimos con los requisitos del Acta de Protección de la Privacidad En Línea de los Niños (COPPA por sus siglas en inglés). No recolectamos información de ningún individuo menor de 13 años. Nuestro sitio web, productos y servicios están dirigidos a personas que tengan por lo menos 13 años o que sean mayores de esa edad.</p></div>


</div>

<div class="sub-contenedores-rul">
	<div><h2 class="sub-titulo-rul">Únicamente Política de Privacidad Online</h2></div>
	<div><p class="texto-rul">Esta política de privacidad online aplica únicamente para información recolectada a través de nuestro sitio web, y no a información recabada en forma offline.</p></div>

</div>

<div class="sub-contenedores-rul">
	<div><h2 class="sub-titulo-rul">Términos y Condiciones</h2></div>
	<div><p class="texto-rul">Por favor visita nuestra sección Términos y Condiciones respecto al uso, renuncia y limitación de responsabilidad, que rige la utilización de nuestro sitio web, en http://www.workana.com/terms.</p></div>


</div>

<div class="sub-contenedores-rul">
<div><h2 class="sub-titulo-rul">Tu Consentimiento</h2></div>
	<div><p class="texto-rul">Al utilizar nuestro sitio, aceptas nuestra política de privacidad.</p></div>

</div>

<div class="sub-contenedores-rul">
	<div><h2 class="sub-titulo-rul">Cambios a nuestra Política de Privacidad</h2></div>
<div><p class="texto-rul">Si decidimos alterar nuestra política de privacidad, publicaremos los cambios en esta página, y/o actualizaremos la fecha de modificación de la Política de Privacidad que figura debajo.</p></div>



</div>

<div class="sub-contenedores-rul">
	<div><h2 class="sub-titulo-rul">Contáctanos</h2></div>
	<div><p class="texto-rul">Si tienes alguna consulta respecto a la política de privacidad, puedes contactarnos a la siguiente dirección soporte@thitonix.com</p></div>

</div>


</section>
</main>
<body>
</body>
</html>
