<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>Untitled Document</title>
<link href="{{ asset('css/css-inicio/canecera.css')}}" rel="stylesheet" type="text/css">
</head>

<body>
 <div id="main-body">
            <main id="main-element-two">

<div class="contenedor-informativo">
<header class="fondo-informativo">
<div><h1 class="titulo-informativo"><a href="{{ url('/login') }}" class="link-info"  onclick="pageViewDetail()">THITONIX</a></h1></div>
<div> @include('inicio-thitonix.traductor')</div>
</header>
</div>
  <script src="{{ asset('js/loadPage.js') }}"></script>

</body>
</html>
