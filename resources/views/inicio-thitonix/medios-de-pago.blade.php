<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<link rel="icon" type="image/vnd.microsoft.icon" href="{{ asset('/favicon-thitonix.ico') }}">
<title>Medios de pago</title>
<link href="{{ asset('css/css-inicio/medios-pago.css')}}" rel="stylesheet" type="text/css">
<meta name="viewport" content="width=device-width,user-scalable=no,initial-scale=1.0,maximum-scale=1.0,minimum-scale=1.0">
<link href="{{ asset('css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">
</head>

<body>
@include('inicio-thitonix.cabecera-informativa')
<main class="medios-contenido">
<section class="seccion-servicio">


<div class="contiene-uno-servicio">
<div class="contiene-dos-servicio">
<h3 class="titulo-servicio">Medio de pago</h3>
<p class="text-info-servicio">
La moneda de referencia que se maneja dentro de Titonic es el USD, pero los pagos van a hacer realizados con las monedas virtuales disponibles en la pasarela de pago, no te tienes que preocupar por la conversión del dinero ya que se hace automáticamente al momento de pagar. La entrega del dinero o devoluciones se harán solamente por medio de bitcoin. En caso de que tengas problemas o necesites ayuda para tus transacciones te puedes comunicar al siguiente correo: <br>
<span class="correo-servicio">screenshot.com</span>
<br>Si tardamos un poco más, no desesperes, seguro que estamos a punto de responder o tratando de buscar la mejor solución a tu caso.

<span class="contacto-servicio">Contacto</span>
<span class="logo-contacto-pago"><span class="fa fa-telegram"></span></span> +57 3102390123
</p>
</div>
</div>
<div class="contiene-peque-servicio">
<img src="{{ asset('img/inicio/medio-de-pago.jpg')}}" class="imagen-servicio" alt="bitcoin">
</div>
</section>

	</main>



</body>
</html>
