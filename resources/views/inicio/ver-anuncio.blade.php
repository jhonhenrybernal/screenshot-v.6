<!doctype html>
<html>

<head>
    <meta charset="UTF-8">
    <link rel="icon" type="image/vnd.microsoft.icon" href="{{ asset('/favicon-thitonix.ico') }}">
    <title>Descripcion</title>
    <link href="../../css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="../../css/descrip.css" rel="stylesheet" type="text/css">
    <link href="../../css/flexslider.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{ asset('/css/croppie.css') }}" />
    <link href="../../css/alert-screen.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{ asset('/css/modal-cargar-imagenes.css') }}" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport"
        content="width=device-width,user-scalable=no,initial-scale=1.0,maximum-scale=1.0,minimum-scale=1.0">
</head>

<body>
    @extends('layouts.app')
    <div id="main-body">
        <main class="contenedor" id="main-element-two">
            <section class="cont-2">
                <div class="titulo-2">
                    <h2 class="sub">Busco {{ $product->mision }}</h2>
                </div>
                <div class="columna-1">
                    <div class="flexslider">
                        <ul class="slides">
                            @foreach ($imagenes as $img)
                                <li>
                                    <img src="{{ asset('imagenesproductos/' . $img->nombreImagen) }}" required/>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                <div class="columna-2">
                    <h3 class="informa">Información del anunciante</h3>
                    @if (session('message'))
                        <!-- Al poner este condicional, se habilita la opción de que al redirigir a una pagina se muestre un mensaje -->
                        <div class="alert {{ session('message')[0] }}">
                            <b>
                                <center>{{ session('message')[1] }}</center>
                            </b>
                        </div>
                    @endif
                    <ul class="sub-mirar">

                        <li class="usuario">
                            <span class="forma"><img src="{{ asset('img/avatar/' . $usuario->avatar) }}.jpg"
                                    class="avatar">
                                <p class="correr tamano">Nombre del usuario<br><span
                                        class="unico">{{ $usuario->name }}</span></p>
                        </li>
                        <li class="usuario"><span class="espaciado"><span class="fa fa-map-marker"></span></span>
                            <p class="correr">Ubicación del anunciante<br><span
                                    class="suav">{{ $ciudad->ciudad }}</span>
                            </p>
                        </li>
                    </ul>

                    <form action="{{ route('realizar.propuesta') }}" method="post" class="propon"
                        enctype="multipart/form-data" id="form-page">
                        {{ csrf_field() }}
                        <input required type="hidden" name="id" value="{{ $product->id }}" data-tipo-image="propuesta">
                        <input required type="hidden" name="id_categoria" value="{{ $product->id_categoria }}">
                        <input required type="hidden" name="mision" value="{{ $product->mision }}">
                        <input required type="hidden" name="usuario" value="{{ $product->id_usuario }}">
                        <p class="pre">Adjuntar 4 imagenes para proponer</p>
                        <input type="file" name="insert_image[]" id="insert_image" accept="image/*" />
                        <br />
                        <div id="store_image_propuesta"></div>
                        <textarea required name="descripcion" rows="15" cols="35"
                            class="palabras">Describir el producto o servicio a ofrecer</textarea>
                        <p class="adver">Antes de proponer un precio mira las siguientes comisiones que debes tener
                            encuenta
                            <a href="#">aquí</a>
                        </p>
                        <p class="pre">Proponer precio</p>
                        <input required type="number" name="presio" class="precio" value="{{ old('presio') }}">
                        <button onclick="pageViewDetail()" class="envio">Enviar propuesta</button>
                    </form>
                </div>
                <div class="descripcion">
                    <h3 class="describir">Descripción de la misión</h3>
                    <p class="especific">
                        {{ $product->misionDetalladamente }}
                    </p>
                </div>
            </section>
        </main>
    </div>
    @include('propuestas.modal.upload_image')
    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

    <!-- Latest compiled JavaScript -->
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="{{ asset('js/bootstrap-fileinput/fileinput.min.js') }}"></script>
    <script src="{{ asset('js/croppie.js') }}"></script>
    <script src="{{ asset('js/upload_image-propuesta.js') }}"></script>
    <script>
        $(function() {
            $('.flexslider').flexslider({
                animation: "slider"
            });

        });

    </script>
</body>

</html>
