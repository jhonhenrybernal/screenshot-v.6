@extends('layouts.app')

    <head>
        <meta charset="UTF-8">
        <link rel="icon" type="image/vnd.microsoft.icon" href="{{ asset('/favicon-thitonix.ico') }}">
        <title>Inicio</title>
        <link href="../../css/buscar.css" rel="stylesheet" type="text/css">
        <link href="../../css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="../../css/paginate.css" rel="stylesheet" type="text/css">
        <meta name="viewport"
            content="width=device-width,user-scalable=no,initial-scale=1.0,maximum-scale=1.0,minimum-scale=1.0">
    </head>

    <body>
        <div id="main-body">

            <main class="contenedor" id="main-element-two">
                <section class="buscar">
                    <form class="encontrar" action="{{ route('anuncio.buscar') }}" method="get" id="encontrar">
                        <input type="search" class="barra" placeholder="Buscar" name="buscar"><span class="icono"><span
                                class="fa fa-search"></span></span>
                        <input  id="submit-load-page-search" type="submit" value="Buscar" class="boton">
                    </form>
                </section>
                <section class="filtrar">
                    <form action="{{ route('anuncio.buscar') }}" method="get" class="buscando">
                        @if (empty($productos[0]))
                            <div class="alert alert-danger" role="alert">
                                No se encontraron los resultados
                            </div>
                        @else
                            <input type="hidden" name="filtro" value="true">
                            @foreach ($productos as $valor)
                                <input type="hidden" name="abuscar[]"
                                    value="{{ isset($valor->misionDetalladamente) ? $valor->misionDetalladamente : '' }}">
                                <input type="hidden" name="abuscarId[]" value="{{ isset($valor->id) ? $valor->id : '' }}">
                            @endforeach
                            <select type="select" placeholder="Ciudad" class="ingresar ancho" name="ciudad">
                                <option value="">Ciudad</option>
                                @foreach ($ciudades as $valor)
                                    <option value="{{ $valor }}">{{ $valor }}</option>
                                @endforeach
                            </select>
                            <select type="select" placeholder="Categoria" class="ingresar" name="categoria">
                                <option value="">Categoria</option>
                                @foreach ($tipos as $valor)
                                    <option value="{{ $valor }}">{{ $valor }}</option>
                                @endforeach
                            </select>
                            <input type="submit" value="FILTRAR" class="ingreso">
                    </form>

                    <div class="columna-1">
                        <h2 class="sub-titulo">Busq<span class="sub-titulo-dos">ueda</span></h2>

                        @foreach ($productos as $valor)
                            <div class="perfil">
                                <img src="{{ isset($valor->nombreImagen) ? asset('imagenesproductos/' . $valor->nombreImagen) : '' }}"
                                    alt="" class="imagen">
                                <p class="texto abajo">{{ isset($valor->ciudad) ? $valor->ciudad : '' }}</p>
                                <p class="texto"> {{ isset($valor->mision) ? $valor->mision : '' }}</p>
                                <a onclick="pageViewDetail()" href="{{ route('home.show', $valor->id) }}" class="entrar">
                                    <p class="mirar">Leer más</p>
                                </a>
                            </div>
                        @endforeach
                    </div>


                    @endif


                </section>
            </main>
        </div>
        <div class="box-footer">
            <div class="center">
                {{ $productos->links() }}
            </div>
        </div>
    </body>
