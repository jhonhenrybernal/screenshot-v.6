<!doctype html>
<html>

<head>
    <link rel="icon" type="image/vnd.microsoft.icon" href="../../favicon-thitonix.ico">
    <meta charset="UTF-8">
    <title>Inicio</title>
    <link href="../css/inicio.css" rel="stylesheet" type="text/css">
    <link href="../../css/paginate.css" rel="stylesheet" type="text/css">
    <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="../../css/paginacion.css" media="screen" />
    <meta name="viewport"
        content="width=device-width,user-scalable=no,initial-scale=1.0,maximum-scale=1.0,minimum-scale=1.0">
</head>

<body>
    @extends('layouts.app')
    <div id="main-body">
        <main class="contenedor" id="main-element-two">
            <section class="buscar">
                <form action="{{ route('anuncio.buscar') }}" method="get" class="encontrar" id="encontrar">
                    <input type="search" class="barra" placeholder="Buscar" name="buscar"><span class="icono"><span
                            class="fa fa-search"></span></span>
                    <input type="submit" value="Buscar" class="boton" id="submit-load-page-search">
                </form>
            </section>
            <section class="filtrar">
                <div class="traductor"></div>
                <div class="col-1">

                    <div class="caja">
                        <h2 class="sub-titulo">Nuestras <span class="sub-titulo-dos">Misiones</span></h2>
                    </div>


                    @foreach ($products as $valor)
                        @if ($valor->id_estados_anuncios == 2 && $valor->id_tipo == 1)
                            <div class="perfil">
                                <img src="{{ asset('imagenesproductos/' . $valor->imagenendeuno->nombreImagen) }}"
                                    alt="" class="imagen">
                                <p class="texto abajo">{{ $valor->ciudades->ciudad }}</p>
                                <p class="texto">{{ $valor->mision }}</p>
                                <a onclick="pageViewDetail()" href="{{ route('home.show', $valor->id) }}" class="entrar">
                                    <p class="mirar">Leer más</p>
                                </a>
                            </div>
                        @else
                        @endif
                    @endforeach
                </div>
                <div class="vacio"></div>
                <div class="col-2">
                    @foreach ($aProductsdos as $valor)
                        @if ($valor->id_estados_anuncios == 2 && $valor->id_tipo == 1)
                            <div class="perfil">
                                <img src="{{ asset('imagenesproductos/' . $valor->imagenendeuno->nombreImagen) }}"
                                    alt="" class="imagen">
                                <p class="texto abajo">{{ $valor->ciudades->ciudad }}</p>
                                <p class="texto">{{ $valor->mision }}</p>
                                <a onclick="pageViewDetail()" href="{{ route('home.show', $valor->id) }}" class="entrar">
                                    <p class="mirar">Leer más</p>
                                </a>
                            </div>
                        @else
                        @endif
                    @endforeach
                </div>
            </section>
        </main>
    </div>
    <section class="contenedorPag"> 
        {{ $products->links('vendor.pagination.thitonix') }}
    </section>
</body>

</html>
