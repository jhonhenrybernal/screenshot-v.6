<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<link rel="icon" type="image/vnd.microsoft.icon" href="{{ asset('/favicon-thitonix.ico') }}">
<title>Restablecer contraseña</title>
<link href="../css/registro.css" rel="stylesheet" type="text/css">
</head>

<body>
<main class="contenedor">
<header class="head">
<h1 class="titulo"><a href="/" class="link">THITONIX</a></h1>
<p class="sub">Busca lo que tanto deseas</p>
</header>
<section class="form">
<form action="{{ route('password.email') }}" method="POST" class="formulario"  >
    {{ csrf_field() }}
    <div class="alert alert-success">
       Esta cuenta ya esta activa.
    </div>
</form>
</section>
</main>
</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
<script type="text/javascript" src="../js/jquery.backstretch.min.js"></script>
<script>
$(document).ready(function(e) {
    $.backstretch(["../img/screenshot_inicio.jpg"]);
    });

</script>
</html>
