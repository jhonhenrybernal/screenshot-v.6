
<head>
	<meta charset="UTF-8">
	<title>Medio de pago</title>
	<meta name="viewport" content="width=device-width,user-scalable=no,initial-scale=1.0,maximum-scale=1.0,minimum-scale=1.0">
	<link href="../../css/perfil.css" rel="stylesheet" type="text/css">
</head>

<body>
	@extends('layouts.app')
	<main class="contenedor">
	 <form action="{{ route('pagos.add')}}" method="post" class="formulario" name"frm" enctype="multipart/form-data">       
		<h2 class="perfil"><span class="negro">Medio de</span> pago</h2>
		<section class="ajuste">
			<div id="cabecera" >
				<h3 class="datos">Consignación bancaria</h3>
			</div>
			{{ csrf_field() }}
			@if(session('message')) <!-- Al poner este condicional, se habilita la opción de que al redirigir a una pagina se muestre un mensaje -->
			<div class="alert alert-{{ session('message')[0] }}">
				<b><center>{{ session('message')[1] }}</center></b>
			</div>
			@endif
			<span class="adjunto">
				<ul class="info">
					<li class="detalle"><span class="negrilla">Nombre titular:</span> 
						<input type="text" class="form-control" name="nombreTitular" placeholder="Nombre" value="<?php echo (empty($pagoMedio->Nombre_titular_cuenta_bancaria))?'':$pagoMedio->Nombre_titular_cuenta_bancaria;?>">
					</li>

					<li class="detalle"><span class="negrilla">Numero de documento: </span> 
						<input type="text" class="form-control" name="numeroDocumento" placeholder="Numero" value="{{$pagoMedio['numero_doc']}}">
					</li>
					<li class="detalle"><span class="negrilla">Numero de cuenta: </span> 
						<input type="number" class="form-control" name="numeroCuenta" placeholder="Numero" value="<?php  echo (empty($pagoMedio->numero_cuenta))?'':$pagoMedio->numero_cuenta;?>">
					</li>
					
				</ul>
				<ul class="info">
					<li class="detalle"><span class="negrilla">Entidad bancaria: </span> 
						<select name="entidadBancariaId" class="form-control">
							<option value="">Seleccione</option>
							@foreach($bancaria as $row)
							<option value="{{$row->id}}" <?php if($pagoMedio['entidad_bancaria_id'] == $row->id){echo 'selected';}?>>{{$row->entidad}}</option>
							@endforeach
						</select>
					</li>
					<li class="detalle"><span class="negrilla">Tipo de cuenta: </span> 
						<select name="tipoCuenta" class="form-control">
							<option value="">Seleccione</option>
							<option value="cuentaAhorros" <?php if($pagoMedio['tipo_cuenta_bancaria'] == 'cuentaAhorros'){echo 'selected';}?> >Cuenta Ahorros</option>
							<option value="cuentaCorriente" <?php if($pagoMedio['tipo_cuenta_bancaria'] == 'cuentaCorriente'){echo 'selected';}?> >Cuenta Corriente</option>
						</select>
					</li>					
				</ul>      
			</span>

		</section>
		<section class="ajuste">
			<div id="cabecera" >
				<h3 class="datos">Giros</h3>
			</div>
			<span class="adjunto">
				<ul class="info">		
					@foreach($giros as $row)
					<li class="detalle"><span class="negrilla"><img src="{{ asset('img/entidades financieras/'.$row->imagen) }}" class="imagen" style="width:154px; height:96px;">&nbsp;&nbsp;Seleccione:</span>  <input type="checkbox" class="check" id="materialUnchecked" name="{{$row->id}}" <?php if( in_array($row->id,$entidadMedioPago)){echo 'checked';}?>>
						
					</li>
					@endforeach
					
				</ul>				 
			</span>

		</section>
		<section class="ajuste">
			<div id="cabecera" >
				<h3 class="datos">Giros virtuales</h3>
			</div>
			<span class="adjunto">
				<ul class="info">
					<li class="detalle"><span class="negrilla">Numero de telefono:</span> 
						 <input type="text" class="form-control" name="numeroCel" placeholder="Numero" value="{{$pagoMedio['numero_cel']}}" >
					</li>

				</ul>				 
			</span>
			<span class="adjunto">
				<ul class="info">
					@foreach($app as $row)  
					<li class="detalle"><span class="negrilla"><img src="{{ asset('img/entidades financieras/'.$row->imagen) }}" class="imagen" style="width:154px; height:96px;"></span> 
						 <input type="checkbox" class="check" id="materialUnchecked" name="{{$row->id}}"<?php if( in_array($row->id,$entidadMedioPago)){echo 'checked';}?>>
					</li>
					@endforeach	
				</ul>				 
			</span>
			
		</section>
		<section class="ajuste">
			<div id="cabecera" >
				<h3 class="datos"> <button type="submit" class="btn btn-primary">Guardar</button></h3>
			</div>
		</section>
		</form>						
	</main>
</body>
