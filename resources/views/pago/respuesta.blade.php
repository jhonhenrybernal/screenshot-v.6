<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Estado de transacción</title>
<link href="{{ asset('/css/respuesta.css') }}" rel="stylesheet" type="text/css">
</head>
<body onload="estadoPendienteUno();">
<input type="hidden" name="" id="estado" value="{{ $datos_respuesta['estado']}}">
<input type="hidden" name="" id="transacción_id" value="{{ $datos_respuesta['num_transaccion']}}">
<main class="contenedor">
@if($datos_respuesta['estado'] == 1 )
<h2 class="funciona"><span class="negro">Tu transacción</span> se encuentra en proceso</h2>
@else
@endif
@if($datos_respuesta['estado'] == 2 )
<h2 class="funciona"><span class="negro">Tu transacción ha </span> sido todo un éxito</h2>
@else
@endif
@if($datos_respuesta['estado'] == 3 )
<h2 class="funciona"><span class="negro">Tu transacción ha </span> Sido rechazada</h2>
@else
@if($datos_respuesta['estado'] == 0 )
<h2 class="funciona"><span class="negro">Tu transacción ha </span> se encuentra en proceso</h2>
@endif
@endif
<section class="white">
<img src="{{ asset('img/avatar/'.$datos_respuesta['avatar']) }}.jpg" class="avatar">
<p class="persona">Misionero:</p>
<p class="especi">{{$datos_respuesta['nombre_Usuario_misionero']}}</p>
<ul class="dis">
<li class="tener">Nombre del producto: <span class="lig">{{$datos_respuesta['producto']}}</span></li>
<li class="tener">Nombre del cliente: <span class="lig">{{$datos_respuesta['nombre_Usuario_cliente']}}</span></li>
<li class="tener">Valore de la misión: <span class="lig">${{number_format($datos_respuesta['valor_de_pago'],0,',','.')}} CO</span></li>
<li class="tener">Número de pago: <span class="lig">{{$datos_respuesta['num_transaccion']}}</span></li>
</ul>
<p class="poder">Ya podras tener toda la información personal del misionero</p>
<a href="{{url('/')}}/{{$datos_respuesta['url']}}" class="regre">Regresar a la misión</a>
</section>
</main>
<script src="{{ asset('jquery/jquery.min.js') }}"></script>
<script src="{{ asset('js/consulta-pago.js') }}"></script>
</body>
</html>
