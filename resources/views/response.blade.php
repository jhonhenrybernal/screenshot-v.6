<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width,user-scalable=no,initial-scale=1.0,maximum-scale=1.0,minimum-scale=1.0">
<title>Screenshot</title>
<link href="../css/estilo.css" rel="stylesheet" type="text/css">
<link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css">
</head>
<body>
<main class="contenedor">
<header class="head">
<h1 class="titulo">SCREENSHOT</h1>
<p class="sub">Un objetivo, una misión, busca el mejor postor</p>
</header>
<section class="part">
<span class="uno">
<ul class="palabras">
<div class="panel-body"><h1>                    
                    Super! se envio un correo a tu cuenta.
                </h1></div>

</ul>


</span>
<span class="cont">
<form action="{{ route('login') }}" method="POST" class="formulario">

     {{ csrf_field() }}
     <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
<span class="bloque"><span class="icono"><span class="fa fa-user-o" aria-hidden="true"></span></span><input id="email" name="email" type="email" required placeholder="Correo" value="{{ old('email') }}"class="form">
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </span>
                        </div>



<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
<span class="bloque sin"><span class="icono"><span class="fa fa-key" aria-hidden="true"></span></span><input type="password" required placeholder="Contraseña" id="password" name="password" class="form">
 @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
</span>
</div>



<a href="olvidar.php" class="olvido"><p class="text">¿Ha olvidado su contraseña?</p></a>
<input type="submit" value="Iniciar cuenta" class="inicio">
<a href="{{ route('register') }}" class="olvido"><p class="text margen">¡Deseo registrarme!</p></a>
</form>
</span>
</section>
</main>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
<script type="text/javascript" src="../js/jquery.backstretch.min.js"></script>
<script>
$(document).ready(function(e) {
    $.backstretch(["../img/screenshot_inicio.jpg"]);
    });

</script>
</body>
</html>
