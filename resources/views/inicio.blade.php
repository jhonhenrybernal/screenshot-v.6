<!doctype html>
<html>

<head>
    <meta charset="UTF-8">
    <title>Inicio</title>
    <link href="css/inicio.css" rel="stylesheet" type="text/css">
    <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <meta name="viewport"
        content="width=device-width,user-scalable=no,initial-scale=1.0,maximum-scale=1.0,minimum-scale=1.0">
</head>

<body>
    <? include ('menu.php') ?>
<main class="contenedor">
<section class="buscar">
<form action="" method="post" class="encontrar">
<input type="search" class="barra" placeholder="Buscar"><span class="icono"><span class="fa fa-search"></span></span>
<input type="submit" value="Buscar" class="boton">
</form>
</section>
<section class="filtrar">
<form action="" method="post" class="buscando">
<input type="text" placeholder="Ciudad" class="ingresar">
<input type="text" placeholder="Ingresar dos palabras claves" class="ingresar ancho">
<input type="number" placeholder="hora mas reciente" class="ingresar">
<input type="submit" value="FILTRAR" class="ingreso">
</form>
<div class="perfiles">
<div class="col-1">
<h2 class="sub-titulo">Nuestras</h2>
<h2 class="sub-titulo-dos">Misiones</h2>

<span class="contiene">
<div class="perfil">
<img src="img/samsung.jpg" alt="" class="imagen">
<p class="texto abajo">Bogotá</p>
<p class="texto">Necesito un galaxy s6 version avengers</p>
<a href="descripcion.php" class="entrar"><p class="mirar">Leer más</p></a>
</div>

<div class="perfil">
<img src="img/samsung.jpg" alt="" class="imagen">
<p class="texto abajo">Bogotá</p>
<p class="texto">Necesito un galaxy s6 version avengers</p>
<a href="descripcion.php" class="entrar"><p class="mirar">Leer más</p></a>
</div>


<div class="perfil">
<img src="img/samsung.jpg" alt="" class="imagen">
<p class="texto abajo">Bogotá</p>
<p class="texto">Necesito un galaxy s6 version avengers</p>
<a href="descripcion.php" class="entrar"><p class="mirar">Leer más</p></a>
</div>
</span>
</div>


<div class="col-2">
<div class="perfil">
<img src="img/samsung.jpg" alt="" class="imagen">
<p class="texto abajo">Bogotá</p>
<p class="texto">Necesito un galaxy s6 version avengers</p>
<a href="descripcion.php" class="entrar"><p class="mirar">Leer más</p></a>
</div>

<div class="perfil">
<img src="img/samsung.jpg" alt="" class="imagen">
<p class="texto abajo">Bogotá</p>
<p class="texto">Necesito un galaxy s6 version avengers</p>
<a href="descripcion.php" class="entrar"><p class="mirar">Leer más</p></a>
</div>

</div>

</div>
</div>
</section>
</main>
</body>
</html>
