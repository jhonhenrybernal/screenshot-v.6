<!doctype html>
<html>

<head>
    <meta charset="UTF-8">
    <link rel="icon" type="image/vnd.microsoft.icon" href="{{ asset('/favicon-thitonix.ico') }}">
    <title>Propuestas</title>
    <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="../css/descrip.css" rel="stylesheet" type="text/css">
    <link href="../css/flexslider.css" rel="stylesheet" type="text/css">
    <meta name="viewport"
        content="width=device-width,user-scalable=no,initial-scale=1.0,maximum-scale=1.0,minimum-scale=1.0">
</head>

<body>
     <div id="main-body">

        <main class="cont-2" id="main-element-two">
            @extends('layouts.app')
            <div class="titulo-2">
                <h2 class="sub">Busco Samsung Galaxy S6</h2>
            </div>
            <section class="columna-1">
                <div class="flexslider">
                    <ul class="slides">
                        @foreach ($imagenes as $valor)
                            <li>
                                <img src="{{ asset('imagenesproductos/' . $valor['nombreImagen']) }}"
                                    style="width: 500px; height: 500px;" />
                            </li>
                        @endforeach
                    </ul>

                </div>
            </section>
            <section class="columna-2">
                <h3 class="informa">Propuestas de precio</h3>
                <ul class="lista-2">
                    @foreach ($propuesta as $valor)
                    @endforeach
                    @if (!$valor->name == null and $valor->estado == 8 or $valor->estado == 11 or $valor->estado == 12)
                        @foreach ($propuesta as $valor)
                            <li class="user"><img src="../../img/avatar/{{ $valor->avatar }}.jpg" class="usu">
                                <p class="correr"><span class="suav">{{ $valor->name }}</span></p><a
                                    href="ver/{{ $valor->id_producto_propuesta }}" class="pagar" onclick="pageViewDetail()">{{ $valor->presio }}
                                    USD</a>
                            </li>

                        @endforeach
                    @else
                        <li class="user">
                            <p class="correr"><span class="suav">No tienes propuestas</span></p>
                        </li>
                    @endif

                </ul>
            </section>
            <section class="descripcion">
                <h3 class="describir">Descripción de la misión</h3>
                <p class="especific">
                    {{ $product->misionDetalladamente }}
                </p>
            </section>
    </main>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="../js/jquery.flexslider-min.js"></script>
    <script src="../js/jquery.fancybox.pack.js"></script>
    <script>
        $(function() {
            $('.flexslider').flexslider({
                animation: "slider"
            });

        });

    </script>
</body>

</html>
