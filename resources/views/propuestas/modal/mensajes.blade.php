<div  id="modal-enviar" class="modal">
  <div class="modal-content-mensaje">
  
    <div class="modal-body">
       <span onClick="closeModal()" class="close">&times;</span>
    <h4>Enviar mensaje al misionero.</h4>
      <form class="form-horizontal" data-toggle="validator">
       {{ csrf_field()}}  {{ method_field('POST')}}
       <div id="alert-screenshot"></div>
       
       <input type="hidden" id="id_producto_mod" name="id_producto">
       <input type="hidden" id="id_usuario_mod" name="id_usuario">  

       <textarea rows="5" cols="115" class="textinput" name="mensaje" id="mensaje" required></textarea>

       <div style="padding: 15px 0 0 0;text-align: right;border-top: 1px solid #e5e5e5;">
        <button type="button" class="close-button" data-dismiss="modal" onClick="closeModal()">Cerrar</button>
        <button type="submit" class="enviar-msm-button" >Enviar</button>
      </div>
    </div>
  </div>
</div>