<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <link rel="icon" type="image/vnd.microsoft.icon" href="{{ asset('/favicon-thitonix.ico') }}">
	<title>Propuesta</title>
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<link href="../../css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link href="../../css/propon.css" rel="stylesheet" type="text/css">
	<link href="../../css/modal.css" rel="stylesheet" type="text/css">
	<link href="../../css/alert-screen.css" rel="stylesheet" type="text/css">
	<link href="../../css/propuesta.css" rel="stylesheet" type="text/css">
    <link href="../../css/flexslider.css" rel="stylesheet" type="text/css">
    <link href="../../css/formulario-pago.css" rel="stylesheet" type="text/css">
	<meta name="viewport" content="width=device-width,user-scalable=no,initial-scale=1.0,maximum-scale=1.0,minimum-scale=1.0">
</head>

<body>
	@extends('layouts.app')
	<main class="cont-3">
		<div class="titulo-3"><h2 class="sub">Busco {{$productos['mision']}}</h2></div>
		<section class="columna-1">
			<div class="flexslider">
                <ul class="slides">
                    @foreach($imagenes as $key=>$lista)
                    <li>
						<img src="{{ asset('imagenesproductos/'.$lista['nombreImagen']) }}"  />
					</li>
                    @endforeach
				</ul>

			</div>
		</section>
		<section class="columna-2">
			{{ csrf_field() }}
			@if(session('message')) <!-- Al poner este condicional, se habilita la opción de que al redirigir a una pagina se muestre un mensaje -->
			    <div class="alert {{ session('message')[0] }}">
			        <b><center>{{ session('message')[1] }}</center></b>
			    </div>
			@endif
			<h3 class="informa">Información del misionero</h3>
			<ul class="sub-mirar">
				<li class="usuario"><div class="centrar-img"><img src="{{asset('img/avatar/'.$productos->usuario->avatar.'.jpg')}}" class="imagen-mision"></div></li>
				<li class="usuario"><p class="correr">Nombre del usuario<br><span class="suav">{{ $productos->usuario->name }}</span></p></li>
				<!--input id="id_usuario_auth" type="hidden" value="{{(empty($productsAuth->usuario))?'':$productsAuth->usuario->id}}" -->
				<!--input id="id_usuario_propuesta" type="hidden" value="{{ (empty($productos->usuario))?'':$productos->usuario->id}}" -->
				<input type="hidden" id="id_producto" value="{{$productos->id}}">
				<input type="hidden" id="id_usuario" value="{{ $productos->id_usuario }}">
				<li class="usuario"><span class="espaciado"><span class="fa fa-map-marker"></span></span><p class="correr">Ubicación del misionero<br><span class="suav">{{ $productos->ciudades->ciudad }}</span></p></li>
			</ul>
<!--p class="califica">Calificación del misionero</p>
<ul class="cuenta">
<li class="manos"><span class="dedos"><span class="fa fa-thumbs-up" onclick="califiBien()" id="calificacion-bien"><span class="num"></span></span></span></li>
<li class="manos"><span class="mal"><span class="fa fa-thumbs-down" onclick="califiMal()" id="calificacion-mal"><span class="num"></span></span></span></li>
</ul-->
<p class="comp"><span class="negri">Total de misiones:</span> {{$contMisiones}}</p>
<p class="comp"><span class="negri">Misiones canceladas:</span>{{$contCanceladas}}</p>
<a href="#" class="hablar" onclick="clickModalMensaje()">Hablar con el misionero</a>
<p class="precio">Precio tolal</p>
<p class="numero">{{ number_format($oPropuestas['presio'],0,',','.') }} USD</p>
@if($pagos['status'] == 1 or $pagos['status'] == 2 )
<div class="alert alert-success">
	<b><center>Su pago actual es: {{ $pagos['estado_msg'] }}</center></b>
</div>
@else
<p class="info">En el precio total se incluye la comisión que genera la plataforma dependiendo del monto del misionero que es de 5% + 500 pesos </p>
<a href="#" class="pago  modal-pago-infousu">PAGAR</a>
@endif
</div>
</section>
<section class="descripcion-2">
	<h3 class="describir">Descripción de la misión</h3>
	<p class="especific">{{ $productos->misionDetalladamente }}</p>
</section>
</main>
@include('propuestas.modal.mensajes')
@include('propuestas.modal.info_usu_pago')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
<script src="{{ asset('jquery/jquery.min.js') }}"></script>
<script src="../../js/jquery.flexslider-min.js"></script>
<script src="../../js/jquery.fancybox.pack.js"></script>
<script>
	$(function() {
		$('.flexslider').flexslider({
			animation: "slider"
		});
		document.getElementById('modal-enviar').style.display = "none";
	});

// calificacion
function califiBien(){
	calificacion(1)
}

function califiMal(){
	calificacion(0)
}

function calificacion(estado){

	var id_usuario = $('#id_usuario_propuesta').val()
	url = '{{url("status/calificacion")}}/'+id_usuario;

	$.get( url, function( data ) {

		if(data.status){
			var id_usuario_auth = $('#id_usuario_auth').val()
			var id_usuario_propuesta = $('#id_usuario_propuesta').val()
			$.ajax({

				type:'POST',
				url:'{{url("enviar/calificacion")}}',
				data: { idUsuarioPropuesta:id_usuario_propuesta,
					idUsuarioAuth:id_usuario_auth,
					estado:estado,
				},

				success:function(data){
					if(data){
						$("#calificacion-bien").empty();
						$("#calificacion-mal").empty();
						statusCalifiacion()
					}else{
						$(document).trigger("add-alerts", [
						{
							"message": 'No se actualizo',

							"priority": 'error'
						}
						]);
					}
				}
			});
		}
	});
}

function statusCalifiacion(){
	var id_usuario = $('#id_usuario_propuesta').val()
	url = '{{url("status/calificacion")}}/'+id_usuario;

	$.get( url, function( data ) {
		var bien =  '<span class="num">'+data.bien+'</span>';
		var mal =  '<span class="num">'+data.mal+'</span>';
		$('#calificacion-bien').append(bien)
		$('#calificacion-mal').append(mal)
	});
}

$(function() {
	statusCalifiacion()
});

function clickModalMensaje(){
	document.getElementById('modal-enviar').style.display = "block";
	var id_usuario = $('#id_usuario').val()
	$('#id_usuario_mod').val(id_usuario)
	var id_producto = $('#id_producto').val()
	$('#id_producto_mod').val(id_producto)

}


//envio  mensaje para duda de propuesta
$(function(){
	$('#modal-enviar form').validator().on('submit', function(e){
		if (!e.isDefaultPrevented()){
			url = "{{ url('enviar/mensaje') }}";
			$.ajax({
				url : url,
				type : "POST",
				data : $('#modal-enviar form').serialize(),
				success : function(data){
					if (data === false) {

						var alert = '<div class="alert">span class="closebtn">&times;</span><strong>Ups..!</strong>EL mensaje con cumple con las buenas practicas de comunicación en la plataforma.</div>'
						document.getElementById('alert-screenshot').innerHTML = alert


					}else{

						if (data === true) {

							var alert = '<div class="alert success"><span class="closebtn">&times;</span><strong>Super!</strong> Mensaje enviado.</div>'
							document.getElementById('alert-screenshot').innerHTML = alert
							setTimeout(function(){
								window.location.href = "{{url('panelusu')}}";
							},4000)

						}
					}

				},
				error : function(){
					var alert = '<div class="alert">span class="closebtn">&times;</span><strong>Ups..!</strong>No se pudo enviar el 	mensaje.</div>'
					document.getElementById('alert-screenshot').innerHTML = alert

				}

			});

			return false;
		}
	});
});


 //Envio de pago

    $.ajaxSetup({

        headers: {

            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

        }

    });


    $(".btn-submit-pago").click(function(e){
        $('#load').show();
        e.preventDefault();

        var nombre = $("input[name=nombre]").val();
        var apellido = $("input[name=apellido]").val();
        var tipo_documento = $("select[name=tipo_documento]").val();
        var numero_documento = $("input[name=numero_documento]").val();
        var email = $("input[name=email]").val();
        var ciudad = $("select[name=ciudad]").val();
        var departamento = $("select[name=departamento]").val();
        var numero_telefonico = $("input[name=numero_telefonico]").val();
        var direccion = $("input[name=direccion]").val();
        var barrio = $("input[name=barrio]").val();
        var id_user = $("input[name=id_user]").val();
        var id_producto = $("input[name=id_producto]").val();
        var moneda = $("select[name=tipomoneda]").val();
        var pais = $("select[name=id_pais]").val();


        $.ajax({

           type:'POST',

           url:'/users/create/pago',

           data:{nombre:nombre, apellido:apellido, tipo_documento:tipo_documento,numero_documento:numero_documento,email:email,ciudad:ciudad,numero_telefonico:numero_telefonico,direccion:direccion,barrio:barrio,id_user:id_user,id_producto:id_producto,departamento:departamento,moneda:moneda,pais:pais},

           success:function(data){
            if(data.result){
              var alert = '<div class="alert success"><span class="closebtn">&times;</span><strong>Super..!</strong> '+data.mensaje+'.</div>'
            document.getElementById('alert-screenshot-pago').innerHTML = alert

              setTimeout(function(){window.open(data.procesarPago, "Procesar Pago", "width=400, height=700, top=50, left=50", true)},5000);
              $('#load').hide();
              }else if(!data.result){
                 $('#load').hide();
                 var alert = '<div class="alert"><span class="closebtn">&times;</span><strong>Ups..!</strong> '+data.mensaje+'.</div>'
            document.getElementById('alert-screenshot-pago').innerHTML = alert

              }
           }

        });
        event.preventDefault();
  });
</script>
</body>
</html>


