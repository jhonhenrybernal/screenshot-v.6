@extends('layouts.app')

@section('content')
<main class="contenedor">
<section>
<h2 class="sub">Busco {{$product->mision}}</h2>
</section>
<span class="observar">
<section class="columna-1">
<div class="flexslider">
<ul class="slides">
@foreach($imagenes as $img)
    <li>
      <img src="{{ asset('imagenesproductos/'.$img->nombreImagen) }}"  />
    </li>
@endforeach
  </ul>

</div>
</section>
<section class="columna-2">
<h3 class="informa">Información del anunciante</h3>
  @if(session('message')) <!-- Al poner este condicional, se habilita la opción de que al redirigir a una pagina se muestre un mensaje -->
                <div class="alert alert-{{ session('message')[0] }}">
                    <b><center>{{ session('message')[1] }}</center></b>
                </div>
            @endif
<ul class="sub-mirar">
<li class="usuario"><span class="forma"><span class="fa fa-user-circle-o"></span></span><p class="correr">Nombre del usuario<br><span class="suav">{{$usuario->name}}</span></p></li>
<li class="usuario"><span class="espaciado"><span class="fa fa-map-marker"></span></span><p class="correr">Ubicación del anunciante<br><span class="suav">{{$ciudad->ciudad}}</span></p></li>
</ul>
<form action="{{ route('actualizar.propuesta')}}" method="post" class="propon" enctype="multipart/form-data">
  {{ csrf_field() }}
<input required type="hidden" name="id_producto" value="{{$product->id}}">
<input required type="hidden" name="id_categoria" value="{{$product->id_categoria}}">
<input required type="hidden" name="mision" value="{{$product->mision}}">
<input required type="hidden" name="id_producto_propuesta" value="{{$propuesta->id_producto_propuesta}}">
<!--p class="pre">Adjuntar 4 imagenes para proponer, estas mismas imagenes seran adjuntados a la misma propuesta si desea mas, adjunte </p-->
<!--input  type="file" name="img[]" class="imagen" multiple-->
<textarea required name="descripcion"  placeholder="Hola! estoy interesado en tu anuncio..." rows="15" cols="35">{{ old('descripcion') }}{{ $propuesta->descripcion }}</textarea>
<p class="pre">Proponer precio</p>
<input required type="number" name="presio" class="precio" value="{{ old('presio') }}{{ $propuesta->presio }}">
<input type="submit" required  value="Enviar propuesta" class="envio">
</form>
</section>
</span>
<section class="descripcion">
<h3 class="describir">Descripción de la misión</h3>
<p class="especific">{{$product->misionDetalladamente}}</p>
</section>
</main>
@endsection
