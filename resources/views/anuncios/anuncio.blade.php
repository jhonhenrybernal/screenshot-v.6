@extends('layouts.app')

<head>
    <link rel="icon" type="image/vnd.microsoft.icon" href="{{ asset('/favicon-thitonix.ico') }}">
    <meta charset="UTF-8">
    <title>Anuncios</title>
    <link href="../../css/anuncio.css" rel="stylesheet" type="text/css">
    <link href="../../css/paginate.css" rel="stylesheet" type="text/css">
    <link href="../../css/label-screenshhot.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="../../css/paginacion.css" media="screen" />
    <meta name="viewport"
        content="width=device-width,user-scalable=no,initial-scale=1.0,maximum-scale=1.0,minimum-scale=1.0">
</head>

<body>
    <div id="main-body">

        <main class="contenedor" id="main-element-two">
            <section class="mover">
                <div class="nuncio">
                    <h3 class="anuncio"><span class="negro">Mis</span> Anuncios</h3>
                </div>
                <div class="ir"><a href="{{ route('anuncio.crearAnuncio') }}" class="crear" onclick="pageViewDetail()">Crear anuncio</a></div>
                @foreach ($products as $datos)
                    @if ($datos->id_estados_anuncios == 3)

                    @elseif(
                        $datos->id_estados_anuncios == 1 or
                        $datos->id_estados_anuncios == 2 or
                        $datos->id_estados_anuncios == 8 or
                        $datos->id_estados_anuncios == 11 or
                        $datos->id_estados_anuncios == 12 or
                        $datos->id_estados_anuncios == 20 or
                        $datos->id_estados_anuncios == 21 or
                        $datos->id_estados_anuncios == 35 or
                        $datos->id_estados_anuncios == 29 or
                        $datos->id_estados_anuncios == 31 or
                        $datos->id_estados_anuncios == 32)
                        <div class="perfil">
                            <img src="{{ asset('imagenesproductos/' . $datos->imagenendeuno['nombreImagen']) }}" alt=""
                                class="imagen">
                            @if ($datos->id_estados_anuncios == 2)
                                @if (count($datos->alertas) > 0)
                                    <span class="label success"><i class="fa fa-clock-o"></i>
                                        {{ count($datos->alertas) }} Propuetas nuevas</span>
                                @else
                                    <span class="label warning"><i class="fa fa-clock-o"></i>
                                        {{ count($datos->alertas) }} Propuetas nuevas</span>
                                @endif
                            @endif
                            <p class="texto abajo">{{ $datos->ciudades->ciudad }}</p>
                            <p class="texto">{{ $datos->mision }}</p>
                            @if ($datos->id_estados_anuncios == 1 or $datos->id_estados_anuncios == 5 or $datos->id_estados_anuncios == 4 or $datos->id_estados_anuncios == 6)
                                <a href="#" class="entrar">
                                    <p class="mirar" >Por aprovar</p>
                                </a>
                            @elseif($datos->id_estados_anuncios == 2 or $datos->id_estados_anuncios == 8 or
                                $datos->id_estados_anuncios == 11 or $datos->id_estados_anuncios == 12)
                                <a href="{{ route('propuestas.show', $datos->id) }}" class="entrar">
                                    <p class="mirar" onclick="pageViewDetail()">En curso</p>
                                </a>
                                <a href="editusu/{{ $datos->id }}" class="editar">Editar</a>
                                @if ($datos->id_estados_anuncios == 2 or $datos->id_estados_anuncios == 8 or
                                $datos->id_estados_anuncios == 11 or $datos->id_estados_anuncios == 12 or $datos->id_estados_anuncios == 6 or $datos->id_estados_anuncios == 4)
                                 <a href="deleteusu/{{ $datos->id }}" class="editar" id="delete_product">Eliminar</a>
                                @endif
                            @endif
                            @if ($datos->id_estados_anuncios == 20)
                                <a href="#" class="entrar">
                                    <p class="mirar" onclick="pageViewDetail()">Pago espera</p>
                                </a>
                            @else
                                @if ($datos->id_estados_anuncios == 21)
                                    <a href="{{ route('propuestas.show', $datos->id) }}" class="entrar">
                                        <p class="mirar" onclick="pageViewDetail()">Pago rechazado</p>
                                    </a>
                                @else
                                @endif
                                @if ($datos->id_tipo == 2 and $datos->id_estados_anuncios == 19)
                                    <a href="{{ route('misiones.envia', $datos->id) }}" class="entrar">
                                        <p class="mirar" onclick="pageViewDetail()">Enviado</p>
                                    </a>
                                @elseif($datos->id_tipo == 1 and $datos->id_estados_anuncios == 32)
                                    <a href="{{ route('misiones.recibe', $datos->id) }}" class="entrar">
                                        <p class="mirar" onclick="pageViewDetail()">Recibiendo</p>
                                    </a>
                                @elseif( $datos->id_estados_anuncios == 24 || $datos->id_estados_anuncios == 35)
                                    <a href="{{ route('misiones.recibe', $datos->id) }}" class="entrar">
                                        <p class="mirar" onclick="pageViewDetail()">Finalizado</p>
                                    </a>
                                @endif
                                @if ($datos->id_estados_anuncios == 29 or $datos->id_estados_anuncios == 31)
                                    <a href="#" class="entrar">
                                        <p class="mirar" onclick="pageViewDetail()">Cancelado</p>
                                    </a>
                                @endif
                            @endif

                        </div>
                    @endif
                @endforeach
            </section>
        </main>
        <section class="contenedorPag"> 
        {{ $products->links('vendor.pagination.thitonix') }}
        </section>
    </div>
</body>