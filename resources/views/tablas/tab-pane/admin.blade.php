<div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Panel de Anuncios</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
   @if(session('message')) <!-- Al poner este condicional, se habilita la opción de que al redirigir a una pagina se muestre un mensaje -->
                <div class="alert alert-{{ session('message')[0] }}">
                    <b><center>{{ session('message')[1] }}</center></b>
                </div>
            @endif
<table id="anuncio-tabla" class="display responsive nowrap" cellspacing="0" style="width:100%">
        <thead>
            <tr>
                  <th>Nombre</th>
                  <th>Apellido</th>
                  <th>Anuncio</th>
                  <th>Estado</th>
                  <th>creado</th>
                  <th>Cambiar</th>

            </tr>
        </thead>
        <tbody>
        </tbody>
       </table>
     </div>
   </div>
   <div>
    </div>
    <div>
   @include('products.modal.rechazar')
  </div>
 </div>
