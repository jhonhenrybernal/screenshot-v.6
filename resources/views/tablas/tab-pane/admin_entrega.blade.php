
  <div class="col-md-12">
   <div class="box">
    <div class="box-header with-border">
              <h3 class="box-title">Panel de entrega</h3>
            </div>
    <div class="box-body">
     @if(session('message')) <!-- Al poner este condicional, se habilita la opción de que al redirigir a una pagina se muestre un mensaje -->
     <div class="alert alert-{{ session('message')[0] }}">
      <b><center>{{ session('message')[1] }}</center></b>
     </div>
     @endif
     <table id="entregaTablaadm" class="display responsive nowrap" cellspacing="0" style="width:100%">
      <thead>
       <tr>
       <th>Mision</th>
        <th>Usuario</th>
        <th>Fecha</th>
        <th>estado</th>
        <th>action</th>
        </tr>
       </thead>
       <tbody></tbody>
      </table>
     </div>
     <div>
     </div>
    </div>
   </div>
