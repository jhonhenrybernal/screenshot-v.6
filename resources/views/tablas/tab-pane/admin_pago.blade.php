
  <div class="col-md-12">
   <div class="box">
    <div class="box-header with-border">
              <h3 class="box-title">Panel de pagos</h3>
            </div>
    <div class="box-body">
     @if(session('message')) <!-- Al poner este condicional, se habilita la opción de que al redirigir a una pagina se muestre un mensaje -->
     <div class="alert alert-{{ session('message')[0] }}">
      <b><center>{{ session('message')[1] }}</center></b>
     </div>
     @endif
     <table id="pagoTablaadm" class="display responsive nowrap" cellspacing="0" style="width:100%">
      <thead>
       <tr>
        <th>Estado</th>
        <th>Nombre del producto</th>
        <th>Usuario</th>
        <th>Comición screenshot</th>
        <th>Comición pagos inteligentes</th>
        <th>iva</th>
        <th>Accion</th>  
        </tr>
       </thead>
       <tbody>         
       </tbody>
      </table>
     </div>
     <div>
     </div>
    </div>
   </div>
