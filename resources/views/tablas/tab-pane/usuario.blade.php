        <div class="col-md-12">
          <div class="box">
           <div class="box-body">
         @if(session('message')) <!-- Al poner este condicional, se habilita la opción de que al redirigir a una pagina se muestre un mensaje -->
                <div class="alert alert-{{ session('message')[0] }}">
                    <b><center>{{ session('message')[1] }}</center></b>
                </div>
            @endif
<table id="anuncio-tablausu" class="display responsive nowrap" cellspacing="0" style="width:100%">
        <thead>
            <tr>
                <th>Numero registro</th>
                <th>Titulo</th>
                <th>Mision</th>
                <th>Estado</th>
                <th>Actualizado</th>
                <th>Accion</th>
            </tr>
        </thead>
        <tbody></tbody>
       </table>
     </div>
      <div>
   @include('products.modal.detalleProducto')
   @include('products.modal.detalleEditar')
    </div>
    
    </div>
  </div>
