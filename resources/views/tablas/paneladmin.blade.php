<!DOCTYPE html>
<html>
<head>
   <link rel="icon" type="image/vnd.microsoft.icon" href="{{ asset('/favicon-thitonix.ico') }}">
  <title>Panel admin</title>
  <meta charset="UTF-8">
<meta name="csrf-token" content="{{ csrf_token() }}">
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
<link href="{{ asset('/css/menu.css') }}" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css"/>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css"/>
<link href="{{ asset('/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/dropzone.css">
<link href="{{ asset('/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('/css/descrip.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('/css/buscar.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('/css/style.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('css/visual.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('css/jquery.fancybox.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('css/prettify.css') }}" rel="stylesheet">
<link href="{{ asset('/css/propon.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('/css/flexslider.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('/css/anuncio.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('/css/process.css') }}" rel="stylesheet" type="text/css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<meta name="csrf-token" content="{{ csrf_token() }}" />
<link href="{{ asset('/css/bootstrap-fileinput/fileinput.min.css') }}" media="all" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<!--<link href="{{ asset('/vertical-timeline/css/style.timeline.css') }}" rel="stylesheet" type="text/css">-->

<meta name="viewport" content="width=device-width,user-scalable=no,initial-scale=1.0,maximum-scale=1.0,minimum-scale=1.0">

</head>
<body>
@extends('layouts.app')
  {{ csrf_field()}}
<div class="row">
<div class="container">
<h2 class="page-header">Mensajes</h2>
  <div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
      <li class="active"><a href="#tab_1" data-toggle="tab">Estado de anuncios</a></li>
       <li><a href="#tab_2" data-toggle="tab" id="pagos-tab-adm">Pagos</a></li>
       <li><a href="#tab_3" data-toggle="tab" id="entregas-tab-adm">Entregas</a></li>
       <li><a href="#tab_4" data-toggle="tab" id="cancelado-tab-adm">Cancelados</a></li>
    </ul>
    <div class="tab-content">
      <div class="tab-pane active" id="tab_1">
        @include('tablas.tab-pane.admin')
      </div>
      <!-- /.tab-pane -->
      <div class="tab-pane" id="tab_2">
        @include('tablas.tab-pane.admin_pago')
      </div>
      <!-- /.tab-pane -->
      <div class="tab-pane" id="tab_3">
        @include('tablas.tab-pane.admin_entrega')
      </div>
      <!-- /.tab-pane -->
       <!-- /.tab-pane -->
       <div class="tab-pane" id="tab_4">
        @include('tablas.tab-pane.admin_cancelado')
      </div>
      <!-- /.tab-pane -->
    </div>
    <!-- /.tab-content -->
  </div>
  @include('tablas.modal.enviar-mensajes-cancelar')
</div>
</div>


@include('products.modal.detalleProducto')
@include('tablas.modal.detalle_consignar')

<script src="{{ asset('jquery/jquery.min.js') }}"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
<script src="{{ asset('js/jquery.flexslider-min.js') }}"></script>
<script src="{{ asset('js/jquery.fancybox.pack.js') }}"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<script src="{{ asset('js/croppie.js') }}"></script>
<!-- optionally if you need a theme like font awesome theme you can include it as mentioned below -->
<script src="{{ asset('js/bootstrap-fileinput/theme.js') }}"></script>
<!-- optionally if you need translation for your language then include  locale file as mentioned below -->
<script src="{{ asset('js/bootstrap-fileinput/locales/es.js') }}"></script>
<script src="{{ asset('bootstrap/dist/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('DataTables/js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
<script type="text/javascript" language="javascript" src="//cdn.datatables.net/plug-ins/a5734b29083/integration/bootstrap/3/dataTables.bootstrap.js"></script>
<script src="{{ asset('validator/validator.min.js') }}"></script>
@yield('scripts')
<script src="{{ asset('js/menu.js') }}"></script>
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/dropzone.js"></script>
<script src="{{ asset('js/jquery.bsAlerts.js') }}"></script>
<script src="{{ asset('vertical-timeline/js/index.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script>
  //modal de detalles

  $('body').on('click','.detallesAlert', function(e){
    document.getElementById('modal-detalle').style.display = 'block';
     var usuario = $(this).data('usuario-name')
     var apellido = $(this).data('usuario-lastname')
     $('#clientes').text(usuario+' '+apellido)
     var mision = $(this).data('producto-mision')
     $('#mision').text(mision)
     var categoria = $(this).data('producto-categoria')
     $('#categoria').text(categoria)
     var destalles = $(this).data('producto-destalles')
      $('#destalles').text(destalles)
     var imagenes = $(this).data('producto-imagenes')
     var img = function(imagenes){
     var html = '';
     $.each(imagenes,function(key, dat) {
          html += '<a  href="imagenesproductos/'+imagenes[key].nombreImagen+'" class="fancybox" rel="group"><img  src="imagenesproductos/'+imagenes[key].nombreImagen+'" width="280px" height="190px" class="editar"></a>';
          $('#imagen-mostrar').html(html);
     });
     }
     img(imagenes)


  });
</script>

</body>
</html>
