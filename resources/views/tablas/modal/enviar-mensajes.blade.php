<div id="enviar-cliente" class="modal">

  <!-- Modal content -->
  <div class="modal-content-chat">
    <div class="modal-body">
    <span class="close" onClick="document.getElementById('enviar-cliente').style.display = 'none'">&times;</span>
      <form class="form-horizontal" data-toggle="validator">
       {{ csrf_field()}}  {{ method_field('POST')}}
       <input type="hidden" id="id_product" name="id_producto" value="#">
       <input type="hidden" id="id_usuario" name="id_usuario" value="#" >
       <input type="hidden" id="status" name="status" value="1" >
       <div class="content-wrapper">
        <div id="alert-screenshot"></div>
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            <small>Historial del mensaje</small>
          </h1>
          <div class="preview-text form-control" id="scroll" disabled="" style="height: 300px; width: 100%; overflow-y: hidden; overflow: scroll;">
            <ol class="breadcrumb">
              <a type="text" id="historial"></a>
            </ol>
          </div>
        </section>
      </div>
      <h1>
        <small>Redactar mensaje</small>
      </h1>
      <textarea rows="5" cols="110" class="textinput" name="mensaje" id="mensaje" required></textarea>

      <div style="padding: 15px 0 0 0;text-align: right;border-top: 1px solid #e5e5e5;">
        <button type="button" class="close-button" data-dismiss="modal" onClick="document.getElementById('enviar-cliente').style.display = 'none'" >Cerrar</button>
        <button type="submit" class="enviar-msm-button" >Enviar</button>
      </div>
    </div>
  </form>
</div>
</div>
