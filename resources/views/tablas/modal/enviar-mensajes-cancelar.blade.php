<!-- Modal -->
<div  id="enviar-cliente-cancelado" class="modal fade" role="dialog">
  <div class="modal-dialog modal-md" id="detalle-mensaje">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header bg-dark font-white">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Enviar mensajes</h4>
      </div>
     <div data-alerts="alerts" data-titles='{"success": "<em>Super!</em>", "error": "<em>Error!</em>"}' data-ids="myid" data-fade="3000"></div>
      <div class="modal-body">
        <form class="form-horizontal" data-toggle="validator">
         {{ csrf_field()}}  {{ method_field('POST')}} 
        <input type="hidden" id="id_product" name="id_producto" value="#">
          <input type="hidden" id="id_usuario" name="id_usuario" value="#" > 
          <input type="hidden" id="status" name="status" value="1" >  
          <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
              <h1>
                <small>Historial del mensaje</small>
              </h1>
              <div class="preview-text form-control" id="scroll" disabled="" style="height: 300px; width: 100%; overflow-y: hidden; overflow: scroll;">
              <ol class="breadcrumb">
                <a type="text" id="historial"></a>
              </ol>
            </div>
            </section>
         </div>
         <h1>
            <small>Redactar mensaje</small>
          </h1>
         <textarea rows="5" cols="110" class="textinput" name="mensaje" id="mensaje-cancela" required></textarea>

          <div style="padding: 15px 0 0 0;text-align: right;border-top: 1px solid #e5e5e5;">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn primary" onclick="msmCancelar()">Enviar</button>
      </div>
      </div>
      </form>
    </div>
  </div>
</div>
