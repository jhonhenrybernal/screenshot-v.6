<div  id="modal-consignar" class="modal fade" role="dialog">
  <div class="modal-dialog modal-md" id="detalle-mensaje">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header bg-dark font-white">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>     
     <form class="form-horizontal" data-toggle="validator">                 
        <div class="modal-body">
            <div class="modal-body">
            <div class="container-fluid">
              <div class="row">
                <div class="col-sm-12">                    
                        <table class="table-medios-app">
                          <tr>
                            <th>Medio de pago</th>
                            <th>numero celular</th>
                          </tr>
                          <tr>
                          <td> <img src="" class="imagen" style="width:154px; height:96px;"></td>
                            <td class="text-center">
                                <div class="form-check">
                                 <p id="numeroCel"></p>
                                </div>
                            </td>
                          </tr>                     
                        </table>
                        <table class="table-medios-giros">
                          <tr>
                            <th>Medio de pago</th>
                            <th>Numero de documento</th>
                          </tr>
                          <tr>
                          <td> <img src="" class="imagen" style="width:154px; height:96px;"></td>
                            <td class="text-center">
                                <div class="form-check">
                                <p id="numeroDoc"></p>
                                </div>
                            </td>
                          </tr>
                      
                        </table>                       
                        <table class="table-medios-bancaria">
                          <tr >
                            <th width="20%" align="left">Medio de pago</th>                            
                            <th width="20%" align="left">Numero de cuenta</th>
                            <th width="20%" align="left">Tipo de cuenta</th>
                            <th width="20%" align="left">Nombre de titular</th>
                            <th width="20%" align="left">Numero de documento</th>                                 
                          </tr>                    
                          <tr>
                          <td width="22%" align="left"> <img src="" class="imagen" style="width:154px; height:96px;"></td>
                          <td><p id="numeroCuenta"></p></td>
                          <td><p id="tipoCuenta"></p></td>
                          <td><p id="nombreTitular"></p></td>
                          <td><p id="numeroDocu"></p></td>
                          </tr>
                        </table>                                              
                
                </div>
              </div>              
          </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>