<!DOCTYPE html>
<html>

<head>
    <link rel="icon" type="image/vnd.microsoft.icon" href="{{ asset('/favicon-thitonix.ico') }}">
    <title>Mensajes</title>
    <meta charset="UTF-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
        integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css"
        integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
    <link href="{{ asset('/css/menu.css') }}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" />
    <link rel="stylesheet" type="text/css"
        href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css" />
    <link href="{{ asset('/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/dropzone.css">
    <link href="{{ asset('/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">
    <!--link href="{{ asset('/css/descrip.css') }}" rel="stylesheet" type="text/css"-->
    <!--link href="{{ asset('/css/style.css') }}" rel="stylesheet" type="text/css"-->
    <!--link href="{{ asset('css/visual.css') }}" rel="stylesheet" type="text/css"-->
    <link href="{{ asset('css/jquery.fancybox.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/prettify.css') }}" rel="stylesheet">
    <link href="{{ asset('css/modal.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('/css/propon.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('/css/flexslider.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('/css/anuncio.css') }}" rel="stylesheet" type="text/css">
    <link href="../../css/alert-screen.css" rel="stylesheet" type="text/css">
    <link href="{{ asset('/css/process.css') }}" rel="stylesheet" type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <link href="{{ asset('/css/bootstrap-fileinput/fileinput.min.css') }}" media="all" rel="stylesheet"
        type="text/css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <!--<link href="{{ asset('/vertical-timeline/css/style.timeline.css') }}" rel="stylesheet" type="text/css">-->

    <meta name="viewport"
        content="width=device-width,user-scalable=no,initial-scale=1.0,maximum-scale=1.0,minimum-scale=1.0">

</head>

<body>
    @extends('layouts.app')
    <div id="main-body">

        <div id="main-element-two">
            {{ csrf_field() }}
            <div class="row">
                <div class="container">
                    <h2 class="page-header">Mensajes</h2>
                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#tab_1" data-toggle="tab">Estado de anuncios</a></li>
                            <li><a href="#tab_2" data-toggle="tab" id="propuestas-tab-usu">Propuestas</a></li>
                            <li><a href="#tab_3" data-toggle="tab" id="entregas-tab-usu">Entregas y Envios</a></li>
                            <li><a href="#tab_4" data-toggle="tab" id="pagos-tab-usu">Pagos</a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab_1">
                                @include('tablas.tab-pane.usuario')
                            </div>
                            <!-- /.tab-pane -->
                            <div class="tab-pane" id="tab_2">

                                @include('tablas.tab-pane.usuario_propuestas')
                            </div>
                            <!-- /.tab-pane -->
                            <div class="tab-pane" id="tab_3">
                                @include('tablas.tab-pane.usuario_entrega')
                            </div>
                            <div class="tab-pane" id="tab_4">
                                @include('tablas.tab-pane.usuario_pago')
                            </div>
                        </div>
                        <!-- /.tab-content -->
                    </div>
                    @include('tablas.modal.enviar-mensajes')
                </div>
                <!-- Content here -->
            </div>
        </div>
    </div>

    <script src="{{ asset('jquery/jquery.min.js') }}"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="{{ asset('js/jquery.flexslider-min.js') }}"></script>
    <script src="{{ asset('js/jquery.fancybox.pack.js') }}"></script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
        integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous">
    </script>
    <script src="{{ asset('js/bootstrap-fileinput/fileinput.min.js') }}"></script>
    <script src="{{ asset('js/croppie.js') }}"></script>
    <!-- optionally if you need a theme like font awesome theme you can include it as mentioned below -->
    <script src="{{ asset('js/bootstrap-fileinput/theme.js') }}"></script>
    <!-- optionally if you need translation for your language then include  locale file as mentioned below -->
    <script src="{{ asset('js/bootstrap-fileinput/locales/es.js') }}"></script>
    <script src="{{ asset('DataTables/js/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js">
    </script>
    <script type="text/javascript" language="javascript"
        src="//cdn.datatables.net/plug-ins/a5734b29083/integration/bootstrap/3/dataTables.bootstrap.js"></script>
    <script src="{{ asset('validator/validator.min.js') }}"></script>
    <script src="{{ asset('js/menu.js') }}"></script>
    <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/dropzone.js"></script>
    <script src="{{ asset('js/jquery.bsAlerts.js') }}"></script>
    <script src="{{ asset('vertical-timeline/js/index.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>


    <script>
        //mostrar mensajes entre panel vista historial
        $('body').on('click', '.modal-mensaje-panel', function(e) {
            document.getElementById('enviar-cliente').style.display = "block";
            var id_producto = $(this).data('productos-id')
            $('#id_product').val(id_producto)
            var id_usuario = $(this).data('user-id')
            $('#id_usuario').val(id_usuario)
            var status = $('#status').val()

            url = "{{ url('consulta/mensajes') }}" + '/' + id_producto + '/' + id_usuario + '/' + status;

            $.ajax({
                url: url,
                type: "get",
                dataType: 'json',
                data: $('#productos-id form').serialize(),
                success: function(data) {
                    var html = '';
                    $.each(data, function(key, data) {
                        html +=
                            '<div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 250px;"><div class="box-body chat" id="chat-box" style="overflow: hidden; width: auto; height: 250px;"><div class="box-body chat" id="chat-box" style="overflow: hidden; width: auto; height: 250px;"><div class="container" style="width: 830px"><!-- Timeline --><div class="timeline"><!-- Line component --><div class="line text-muted"></div><!-- Separator --><div class="separator text-muted"><time>' +
                            data.date +
                            '</time></div><!-- /Separator --><!-- Panel --><article class="panel panel-danger panel-outline">   <!-- Body -->   <!-- /Body --></article>  <!-- /Panel -->    <!-- Panel -->  <article class="panel ' +
                            data.article +
                            '">      <!-- Icon -->       <div class="panel-heading icon" style=" width: 50px;  height: 50px;   background-image: url(../img/avatar/icon-chat/' +
                            data.avatar +
                            '.jpg);"> <class="glyphicon glyphicon-cog"></i>      </div>     <!-- /Icon -->            <!-- Heading -->   <div class="panel-heading">         <h2 class="panel-title">' +
                            data.titulo +
                            '</h2>      </div>    <!-- /Heading -->       <!-- Body -->          <div class="panel-body" ><h5 style=" text-align: justify;">' +
                            data.msm +
                            '</h5></div>         <!-- /Body -->  <!-- Footer --> <div class="panel-footer">  <small>' +
                            data.user +
                            '</small>   </div>   <!-- /Footer -->    </article>   <!-- /Panel -->   </div> <!-- /Timeline --></div></div></div></div>';
                        $('#historial').html(html);
                    });
                    var scroll = document.getElementById('scroll');
                    scroll.scrollTop = scroll.scrollHeight;
                },
                error: function() {
                    alert('verifique desde admin');
                }

            });
        })

        //modal de detalles

        $('body').on('click', '.detallesAlert', function(e) {
            document.getElementById('modal-detalle').style.display = 'block';
            var usuario = $(this).data('usuario-name')
            var apellido = $(this).data('usuario-lastname')
            $('#clientes').text(usuario + ' ' + apellido)
            var mision = $(this).data('producto-mision')
            $('#mision').text(mision)
            var categoria = $(this).data('producto-categoria')
            $('#categoria').text(categoria)
            var destalles = $(this).data('producto-destalles')
            $('#destalles').text(destalles)
            var imagenes = $(this).data('producto-imagenes')
            var img = function(imagenes) {
                var html = '';
                $.each(imagenes, function(key, dat) {
                    html += '<a  href="imagenesproductos/' + imagenes[key].nombreImagen +
                        '" class="fancybox" rel="group"><img  src="imagenesproductos/' + imagenes[
                            key].nombreImagen +
                        '" width="280px" height="190px" class="editar"></a>';
                    $('#imagen-mostrar').html(html);
                });
            }
            img(imagenes)


        });

        //modal de detalles para editar

        $('body').on('click', '.detallesAlertCambio', function(e) {
            document.getElementById('modal-cambio').style.display = "block";
            var mensaje = $(this).data('productos-mensaje')
            console.log(mensaje)
            $('#detalles_editar').text(mensaje)
        });

        $('body').on('click', '#cerrar-modal', function(e) {
            document.getElementById('modal-cambio').style.display = "none";
        });

    </script>
</body>

</html>
