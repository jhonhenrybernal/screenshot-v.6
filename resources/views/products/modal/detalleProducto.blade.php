  <!-- Modal -->
<div id="modal-detalle" class="modal" role="dialog">
  <div class="modal-dialog modal-md" id="detalle-modal">
    <!-- Modal content-->
    <div class="modal-content-tabla-usu">
      <div class="modal-header bg-dark font-white">
        <button type="button" class="close" data-dismiss="modal" onclick="document.getElementById('modal-detalle').style.display = 'none'">&times;</button>
      </div>
      <div class="modal-body" >
        <main class="modal-contenedor">
        <header class="conti">
          <h1 class="titulo">SCREENSHOT</h1>
        </header>
        <section class="info">
          <h2 class="cliente" ><b>Cliente: </b><i id="clientes" ></i></h2>
          <p class="necesidad" ><b>Titulo de la misión: </b> <i id="mision"></i></p>
          <p class="necesidad" ><b>Categoria: </b> <i id="categoria"></i></p>
          <div class="contiene">
            <ul class="port">
            <li class="imagen" id="imagen-mostrar"></li>

          </ul>
          </div>
          <h2 class="des"><b>Descripción de la misión:</b></h2>
          <div class="desing">
            <p class="text" id="destalles" ></p>
          </div>
        </section>
          <div>
                  <button type="button" class="close-button" data-dismiss="modal" onclick="document.getElementById('modal-detalle').style.display = 'none'">Cerrar</button>
          </div>
        </main>
       </div>
    </div>
  </div>
</div>

