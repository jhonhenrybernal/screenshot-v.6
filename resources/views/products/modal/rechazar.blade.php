<!-- Modal -->
<div  id="modal-form" class="modal fade" role="dialog">
  <div class="modal-dialog modal-md">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header bg-dark font-white">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Responder anuncio a editar</h4>
      </div>
      <div data-alerts="alerts" data-titles='{"success": "<em>Super!</em>", "error": "<em>Error!</em>"}' data-ids="myid" data-fade="3000"></div>
      <div class="modal-body">
        <form class="form-horizontal" data-toggle="validator">
       {{ csrf_field()}}  {{ method_field('POST')}}
        <input type="hidden" id="id_producto" name="id_producto">
        <input type="hidden" id="id_usuario" name="id_usuario">

       <p>Escribe su mensaje &hellip;</p>
         <textarea rows="15" cols="78" class="area" name="mensaje" id="mensaje" required></textarea>

          <div style="padding: 15px 0 0 0;text-align: right;border-top: 1px solid #e5e5e5;">
              <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button type="submit" class="btn primary" >Enviar</button>
      </div>
      </div>
    </div>
  </div>
</div>
