  <!-- Modal -->
<div id="modal-cambio" class="modal fade in" role="dialog">
  <div class="modal-dialog modal-lg" id="detalle-modal-cambio">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header bg-dark font-white">
        <button type="button" id="cerrar-modal" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body" >
        <main class="model-editar">
        <header class="conti">
          <h1 class="titulo">SCREENSHOT</h1>
        </header>
        <section class="info">
          <h2 class="des"><b>Descripción del cambio:</b></h2>
          <div class="desing">
            <p class="text" id="detalles_editar" ></p>
          </div>
        </section>
        </main>
          <div style="padding: 15px 0 0 0;text-align: right;border-top: 1px solid #e5e5e5;">
                  <button type="button" class="btn btn-default" data-dismiss="modal" id="cerrar-modal">Cerrar</button>
          </div>
       </div>
    </div>
  </div>
</div>
