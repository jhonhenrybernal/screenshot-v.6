<HTMl>

<head>
    <meta charset="gb18030">

    <link href="../../css/perfil.css" rel="stylesheet" type="text/css">
</head>

<body>
    @extends('layouts.app')

    <div id="main-body">
        <main class="contenedor" id="main-element-two">
            <h2 class="perfil"><span class="negro">Perfil</span> de usuario</h2>
            <section class="ajuste">
                <img src="../../img/avatar/{{ $user->avatar }}.jpg" class="avat">
                <h3 class="datos">Datos de cuenta</h3>
                <form action="{{ route('perfil.actualizar') }}" method="post" id="form-page">
                    <span class="adjunto">
                        <ul class="info">
                            {{ csrf_field() }}
                            <input type="hidden" name="id" value="{{ Auth::user()->id }}">
                            <li class="detalle"><span class="negrilla">usuario:</span> <input type="text" name="usuario"
                                    value="{{ $user->username }}"></input> </li>
                            <li class="detalle"><span class="negrilla">Email:</span> <input type="text" name="email"
                                    value="{{ $user->email }}" style="width:260px"></input> </li>
                            <li class="detalle"><span class="negrilla">Dirección:</span> <input type="text"
                                    name="direccion" value="{{ $user->direccion }}"></input></li>
                            <li class="detalle"><span class="negrilla">Ciudad:</span>
                                <select name="ciudad" id="{{ $user->id_ciudad }}">
                                    @foreach ($ciudad as $lista)
                                        <option value="{{ $lista['id'] }}">{{ $lista['ciudad'] }}</option>
                                    @endforeach
                                </select>
                            </li>
                        </ul>
                        <ul class="info">
                            <li class="detalle"><span class="negrilla">Nombre: </span> <input type="text" name="nombre"
                                    value="{{ $user->name }}"></input></li>
                            <li class="detalle"><span class="negrilla">Apellido: </span> <input type="text"
                                    name="apellido" value="{{ $user->apellido }}"></input> </li>
                            <li class="detalle"><span class="negrilla">Cedula: </span> <input type="text" name="cedula"
                                    value="{{ $user->cedula }}"></input> </li>
                            <li class="detalle"><span class="negrilla">Barrio: </span> <input type="text" name="barrio"
                                    value="{{ $user->barrio }}"></input></li>
                            <li class="detalle"><span class="negrilla">Telefono: </span> <input type="text"
                                    name="telefono" value="{{ $user->celular }}"></input></li>
                        </ul>
                    </span>
                    <p class="elegir">Selecciona tu misionero</p>
                    <select class="avatares" name="avatar">
                        <option value="1" <?= $user->avatar === '1' ? 'selected' : '' ?> >Misionero 1</option>
                    <option value="2" <?= $user->avatar === '2' ? 'selected' : '' ?> >Misionero 2</option>
                    <option value="3" <?= $user->avatar === '3' ? 'selected' : '' ?> >Misionero 3</option>
                    <option value="4" <?= $user->avatar === '4' ? 'selected' : '' ?> >Misionero 4</option>
                </select>
                <ul class="intro">
                    <li class="person"><img src="../../img/avatar/1.jpg" class="people"><p class="nombre">Misionero 1</p></li>
                    <li class="person"><img src="../../img/avatar/2.jpg" class="people"><p class="nombre">Misionero 2</p></li>
                    <li class="person"><img src="../../img/avatar/3.jpg" class="people"><p class="nombre">Misionero 3</p></li>
                    <li class="person"><img src="../../img/avatar/4.jpg" class="people"><p class="nombre">Misionero 4</p></li>
                </ul>
                <button class="boton" onclick="pageViewDetail()">Actualizar</button>
                </form>


        </section>
    </main>
</div>
</body>

</HTMl>
