<!doctype html>
<html>

<head>
    <meta charset="UTF-8">
    <link rel="icon" type="image/vnd.microsoft.icon" href="{{ asset('/favicon-thitonix.ico') }}">
    <title>Editar anuncio</title>
    <link href="../../css/crear.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{ asset('/css/croppie.css') }}" />
    <link rel="stylesheet" href="{{ asset('/css/modal-cargar-imagenes.css') }}" />

    <link href="../../css/alert-screen.css" rel="stylesheet" type="text/css">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>

<body>
    @extends('layouts.app')
    <div id="main-body">

        <main class="contenedor" id="main-element-two">
            <h2 class="creando"><span class="negro">Editar</span> anuncio</h2>
            <section class="editar">
                <form action="{{ route('panelusu.update',['id' => $producto->id]) }}" method="post" class="formulario" name="frm"
                    enctype="multipart/form-data" id="form-page">
                    {{ csrf_field() }}
                    @if (session('message'))
                        <!-- Al poner este condicional, se habilita la opción de que al redirigir a una pagina se muestre un mensaje -->
                        <div class="alert {{ session('message')[0] }}">
                            <b>
                                <center>{{ session('message')[1] }}</center>
                            </b>
                        </div>
                    @endif
               
                    <!--AQUI SE AGREGAN LAS IMAGENES QUE SE VAN A CARGAR CUANDO EL USUARIO LAS SELECCIONA Y TIENEN LA OPCION DE BORRAR-->
                    <div class="espacio">
                        <div class="create">
                            <p class="seleccion">Ponle un titulo a la misión</p>
                            <input typer="text" placeholder="maximo 180 caracteres" class="mision" name="mision"
                                value="{{ $producto->mision }}" required>
                        </div>
                        <div class="create">
                            <p class="seleccion">Selecciona una categoria</p>
                            <select class="ver" name="id_categoria" required>
                                @foreach ($categorias as $categoria)
                                    <option value="{{ $categoria['id'] }}" <?= ($categoria['id'] == $producto->id_categoria)?'selected':'' ?>>{{ $categoria['tipo'] }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <p class="detalle">Decribe la misión detalladamente</p>
                    <textarea rows="15" cols="35" name="misionDetalladamente"
                        class="area">{{ $producto->misionDetalladamente }}</textarea>
                    <span class="bloque">
                        <input onclick="pageViewDetail()" type="submit" value="Crear" class="send">
                        <span>


                </form>
            </section>
        </main>
    </div>
    @include('anuncios.modal.upload_image')
    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

    <!-- Latest compiled JavaScript -->
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="{{ asset('js/bootstrap-fileinput/fileinput.min.js') }}"></script>
    <script src="{{ asset('js/croppie.js') }}"></script>
    <script src="{{ asset('js/upload_image.js') }}"></script>
</body>

</html>

