@extends('layouts.app')

<head>
    <meta charset="UTF-8">
    <link rel="icon" type="image/vnd.microsoft.icon" href="../../favicon-thitonix.ico">
    <title>Perfil</title>
    <meta name="viewport"
        content="width=device-width,user-scalable=no,initial-scale=1.0,maximum-scale=1.0,minimum-scale=1.0">
    <link href="../../css/perfil.css" rel="stylesheet" type="text/css">
</head>


<body>
    <div id="main-body">
        <main class="contenedor" id="main-element-two">
            <h2 class="perfil"><span class="negro">Perfil</span> de usuario</h2>
            <section class="ajuste">
                <img src="../../img/avatar/{{ $user->avatar }}.jpg" class="avat">
                <h3 class="datos">Datos de cuenta</h3>
                @if ($message = Session::get('success'))
                    <div class="w3-panel w3-pale-green w3-border">
                        <p>
                            <center>{{ $message }}</center>
                        </p>
                    </div>
                @endif
                <p class="dinero ">GANANCIA APROXIMADA: 2000000 COP</p>
                <a href="#" class="notificar">Notifica a Screenshot para el desembolso del dinero</a>
                <span class="adjunto">
                    <ul class="info">
                        <li class="detalle"><span class="negrilla">usuario:</span> {{ $user->username }}</li>
                        <li class="detalle"><span class="negrilla">Email:</span> {{ $user->email }}</li>
                        <li class="detalle"><span class="negrilla">Dirección:</span> {{ $user->direccion }}</li>
                        <li class="detalle "><span class="negrilla">Ciudad:</span> {{ $ciudad }}</li>
                    </ul>
                    <ul class="info">
                        <li class="detalle"><span class="negrilla">Nombre: </span> {{ $user->name }}</li>
                        <li class="detalle"><span class="negrilla">Apellido: </span> {{ $user->apellido }}</li>
                        <li class="detalle"><span class="negrilla">Cedula: </span> {{ $user->cedula }}</li>
                        <li class="detalle"><span class="negrilla">Barrio: </span> {{ $user->barrio }}</li>
                        <li class="detalle"><span class="negrilla">Celular: </span> {{ $user->celular }}</li>
                    </ul>
                </span>
                <a href="{{ route('perfil.editar', Auth::user()->id) }}" class="boton" onclick="pageViewDetail()">Editar</a>
                <a href="{{ route('perfil.cambiopw') }}" class="mas">cambiar de contraseña</a>
                <p class="siguente">Conozca toda la información sobre la página en el siguiente link</p>
                <a href="#" class="link">más información</a>


            </section>
        </main>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
</body>
