<!doctype html>
<html>

<head>
    <meta charset="UTF-8">
    <link rel="icon" type="image/vnd.microsoft.icon" href="{{ asset('/favicon-thitonix.ico') }}">
    <meta name="viewport"
        content="width=device-width,user-scalable=no,initial-scale=1.0,maximum-scale=1.0,minimum-scale=1.0">
    <title>Thitonix</title>
    <link href="../../css/estilo.css" rel="stylesheet" type="text/css">
    <link href="../../css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="../../css/alert-screen.css" rel="stylesheet" type="text/css">
</head>

<body>

    <form action="{{ route('login') }}" method="POST" id="form-page">
        {{ csrf_field() }}
        <div id="main-body">

            <main class="contenedor" id="main-element-two">
                <header class="head">
                    <h1 class="titulo">THITONIX</h1>
                    <p class="sub">Un objetivo, una misión, busca el mejor postor</p>
                </header>
                <div class="cont">
                    <section class="part">
                        @if (session('message'))
                            <div class="alert alert-login success">
                                <b>
                                    <center>{{ session('message')[1] }}</center>
                                </b>
                            </div>
                        @endif
                        <div class="formulario">

                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <span class="bloque"><span class="icono"><span class="fa fa-user-o"
                                            aria-hidden="true"></span></span><input id="email" name="email" type="email"
                                        required placeholder="Correo" value="{{ old('email') }}" class="form">
                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </span>
                            </div>

                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <span class="bloque sin"><span class="icono"><span class="fa fa-key"
                                            aria-hidden="true"></span></span><input type="password" required
                                        placeholder="Contraseña" id="password" name="password" class="form">
                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                </span>
                            </div>
                            <a href="{{ route('password.request') }}" class="olvido" onclick="pageViewDetail()">
                                <p class="text" >¿Ha olvidado su contraseña?</p>
                            </a>
                            <input type="submit" value="Iniciar cuenta" class="inicio" id="submit-load-page">
                            <a href="{{ route('register') }}" class="olvido" onclick="pageViewDetail()">
                                <p class="text margen">¡Deseo registrarme!</p>
                            </a>
                        </div>
                </div>
                <div class="uno">
                    <ul class="palabras">
                        <li class="palabra">Conviertete en misionero y atiende las necesidades de otros</li>
                        <li class="palabra">oferta por un producto o servicio</li>
                        <li class="palabra">Elige el precio que buscas</li>
                        <li class="palabra">Se el mejor misionero y posicionate</li>

                    </ul>
                </div>

                </section>
                <div class="mirar">
                    <a href="{{ route('home') }}" class="solo"  onclick="pageViewDetail()"><span class="ojo"><span class="fa fa-eye"
                                aria-hidden="true"></span></span>Mira nuestras misiones</a>
                </div>


            </main>
            <div class="up">
                <p class="derechos"><a href="#" class="univone">Univone design</a> y Screenshot todos los derechos
                    reservados</p>
            </div>
            <footer class="pie">

                <a onclick="pageViewDetail()" href="{{ route('quienes-somos') }}" class="pie-info, primero" onclick="pageViewDetail()">QUIENES SOMOS</a>
                <a onclick="pageViewDetail()" href="{{ route('publicar-anuncio') }}" class="pie-info, primero" onclick="pageViewDetail()">PUBLICAR UN ANUNCIO</a>
                <a onclick="pageViewDetail()" href="{{ route('mis-misiones') }}" class="pie-info, segundo" onclick="pageViewDetail()">MIS MISIONES</a>
                <a onclick="pageViewDetail()" href="{{ route('medios-pago') }}" class="pie-info, segundo" onclick="pageViewDetail()">MEDIOS DE PAGO</a>
                <a onclick="pageViewDetail()" href="https://discord.gg/zD9kVc9Hcg" class="pie-info, tercero">COMUNIDAD</a>
                <a onclick="pageViewDetail()" href="{{ route('servicio-cliente') }}" class="pie-info, tercero" onclick="pageViewDetail()">SERVIO AL CLIENTE</a>
                <div class="redes">
                    <span class="pie-iconos"><a href="#" class="fa fa-facebook" id="facebook" aria-hidden="true"></a></span>
                    <span class="pie-iconos"><a href="#" class="fa fa-instagram" id="instagram" aria-hidden="true"></a></span>
                    <span class="pie-iconos"><a href="#" class="fa fa-twitter" id="twitter" aria-hidden="true"></a></span>
                </div>
            </footer>
        </div>
    </form>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script type="text/javascript" src="js/jquery.backstretch.min.js"></script>
    <script>
        $(document).ready(function(e) {
            $.backstretch(["img/screenshot_inicio.jpg"]);
            //peticion conf sis
              fetch('/sistema/redes-sociales')
            .then(function(response) {
                return response.text();
            })
            .then(function(text) {
                var red = JSON.parse(text)
                document.getElementById('facebook').href =red.facebook ;
                document.getElementById('twitter').href = red.twitter;
                document.getElementById('instagram').href = red.instagram;
            })
            .catch(function(error) {
                log('Request failed', error)
            });
        });


    </script>
    <script src="{{ asset('js/loadPage.js') }}"></script>
</body>

</html>
