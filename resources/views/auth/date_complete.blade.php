<!doctype html>
<html>
<head>
<link rel="icon" type="image/vnd.microsoft.icon" href="../favicon-thitonix.ico">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width,user-scalable=no,initial-scale=1.0,maximum-scale=1.0,minimum-scale=1.0">
<title>Registro</title>
<link href="../css/registro.css" rel="stylesheet" type="text/css">
</head>

<body>
<main class="contenedor">
<header class="head">
<h1 class="titulo"><a href="" class="link">THITONIX</a></h1>
<p class="sub">Un objetivo, una misión, busca el mejor postor</p>
</header>
<section class="form">
<form action="{{ url('/complete/'.$id) }}" method="POST" class="formulario">
     {{ csrf_field() }}
  @if(session('message')) <!-- Al poner este condicional, se habilita la opción de que al redirigir a una pagina se muestre un mensaje -->
    <div class="alert {{ session('message')[0] }}">
        <b><center>{{ session('message')[1] }}</center></b>
    </div>
@endif
     <div class="col-md-6">
<input type="password" required placeholder="Contraseña" class="entrada" id="password" name="password">

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                            </div>
<input type="password" required placeholder="Repetir contraseña" class="entrada" id="password-confirm" name="password-confirm">

<input type="submit" value="Registrarme" class="enviar">
</form>
<a href="#" class="politicas">Al hacer click en el botón de registrarme, acepta que ha leido nuestras politicas y acepta nuestras condiciones</a>
</section>
</main>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
<script type="text/javascript" src="../js/jquery.backstretch.min.js"></script>
<script>
$(document).ready(function(e) {
    $.backstretch(["../img/screenshot_inicio.jpg"]);
    });

</script>
</body>
</html>
