<!doctype html>
<html>

<head>
    <meta charset="gb18030">
    <link rel="icon" type="image/vnd.microsoft.icon" href="{{ asset('/favicon-thitonix.ico') }}">
    <meta name="viewport"
        content="width=device-width,user-scalable=no,initial-scale=1.0,maximum-scale=1.0,minimum-scale=1.0">
    <title>Registro</title>
    <link href="css/registro.css" rel="stylesheet" type="text/css">
</head>

<body>
    <form action="{{ route('register') }}" method="POST" id="form-page">
        {{ csrf_field() }}
        <div id="main-body">

            <main class="contenedor" id="main-element-two">
                <header class="head">
                    <h1 class="titulo"><a href="{{ url('login') }}" class="link" onclick="pageViewDetail()">THITONIX</a>
                    </h1>
                    <p class="sub">Un objetivo, una misión, busca el mejor postor</p>
                </header>
                <section class="form">
                    <div class="formulario">
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <input type="email" id="email" required placeholder="Dirección de email" class="entrada"
                                name="email" value="{{ old('email') }}">
                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                        <span class="entrada out">
                            <input type="text" required placeholder="Nombre" id='name' name="name"
                                value="{{ old('name') }}" class="adaptar">
                            <input type="text" required placeholder="Apellido" class="adaptar" id="apellido"
                                name="apellido" value="{{ old('apellido') }}">
                        </span>
                        @if ($errors->has('apellido'))
                            <p>{{ $errors->first('apellido') }}</p>
                        @endif
                        <!--input type="text" required placeholder="cedula" class="entrada" id='cedula' name='cedula' value="{{ old('cedula') }}"-->

                        <input type="text" required placeholder="Nombre de usuario" class="entrada" id='username'
                            name='username' value="{{ old('username') }}">
                        @if ($errors->has('username'))
                            <p>{{ $errors->first('username') }}</p>
                        @endif

                        <input type="number" placeholder="Celular" class="entrada" id="celular" name="celular"
                            value="{{ old('celular') }}">
                        <p class="elegir">Selecciona tu misionero</p>
                        <select class="avatares" name="avatar">
                            <option value="1">Misionero 1</option>
                            <option value="2">Misionero 2</option>
                            <option value="3">Misionero 3</option>
                            <option value="4">Misionero 4</option>
                        </select>
                        <br>
                        <ul class="intro">
                            <li class="person"><img src="../../img/avatar/1.jpg" class="people">
                                <p class="nombre">Misionero 1</p>
                            </li>
                            <li class="person"><img src="../../img/avatar/2.jpg" class="people">
                                <p class="nombre">Misionero 2</p>
                            </li>
                            <li class="person"><img src="../../img/avatar/3.jpg" class="people">
                                <p class="nombre">Misionero 3</p>
                            </li>
                            <li class="person"><img src="../../img/avatar/4.jpg" class="people">
                                <p class="nombre">Misionero 4</p>
                            </li>
                        </ul>

                        <input type="submit" value="Enviar para activar" class="enviar" onclick="pageViewDetail()">
                    </div>
                    <a href="{{ route('legal-thitonix') }}" class="politicas">Despues del registro procedera a generar
                        la
                        contraseña. Al hacer click en el botón de registrarme, acepta que ha leido nuestras politicas y
                        acepta
                        nuestras condiciones</a>
                </section>
            </main>
        </div>
    </form>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="{{ asset('js/loadPage.js') }}"></script>
    <script type="text/javascript" src="../js/jquery.backstretch.min.js"></script>
    <script>
        $(document).ready(function(e) {
            $.backstretch(["../img/screenshot_inicio.jpg"]);
        });

    </script>
</body>

</html>
