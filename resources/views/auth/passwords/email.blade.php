<!doctype html>
<html>

<head>
    <meta charset="UTF-8">
    <link rel="icon" type="image/vnd.microsoft.icon" href="{{ asset('/favicon-thitonix.ico') }}">
    <title>Restablecer contraseña</title>
    <link href="../css/registro.css" rel="stylesheet" type="text/css">
</head>

<body>
    <form action="{{ route('password.email') }}" method="POST" id="form-page">
        {{ csrf_field() }}
        <div id="main-body">

        <main class="contenedor" id="main-element-two">

            <header class="head">
                <h1 class="titulo"><a href="/" class="link" onclick="pageViewDetail()">THITONIX</a></h1>
                <p class="sub">Busca lo que tanto deseas</p>
            </header>
            <section class="form">
                <div class="formulario">

                        @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                        @endif
                        <span class="entrada out">
                            <input type="text" class="entrada" required placeholder="Ingrese su correo" id="email"
                            name="email" value="{{ old('email') }}">
                            @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                            @endif

                        </span>
                        <input type="submit" value="Enviar" class="enviar" onclick="pageViewDetail()">
                    </div>
                </section>
            </main>
        </div>
    </form>
</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
<script type="text/javascript" src="../js/jquery.backstretch.min.js"></script>
<script>
    $(document).ready(function(e) {
        $.backstretch(["../img/screenshot_inicio.jpg"]);
    });

</script>
 <script src="{{ asset('js/loadPage.js') }}"></script>

</html>
