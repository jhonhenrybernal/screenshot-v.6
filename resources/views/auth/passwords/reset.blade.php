<!doctype html>
<html>

<head>
    <meta charset="UTF-8">
    <link rel="icon" type="image/vnd.microsoft.icon" href="../../favicon-thitonix.ico">
    <meta name="viewport"
        content="width=device-width,user-scalable=no,initial-scale=1.0,maximum-scale=1.0,minimum-scale=1.0">
    <title>Actualizar contraseña</title>
    <link href="../../css/estilo.css" rel="stylesheet" type="text/css">
    <link href="../../css/font-awesome.min.css" rel="stylesheet" type="text/css">
</head>

<body>
    <div id="main-body">

        <main class="contenedor" id="main-element-two">
            <header class="head">
                <h1 class="titulo">THITONIX</h1>
                <p class="sub">Un objetivo, una misión, busca el mejor postor</p>
                <br>
                <p class="sub">RESTABLECER CONTRASEÑA</p>
            </header>
            <section class="part">
                <div class="cont">
                    <form class="formulario" method="POST" action="{{ route('password.request') }}">
                        {{ csrf_field() }}

                        <input type="hidden" name="token" value="{{ $token }}">
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <input id="email" type="hidden" class="form-control" name="email" value="{{ $email }}"
                                required autofocus>
                        </div>


                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <span class="bloque"><span class="icono"><span class="fa fa-key"
                                        aria-hidden="true"></span></span>
                                <input placeholder="contraseña" id="password" type="password" class="form"
                                    name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </span>
                        </div>

                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            <span class="bloque sin"><span class="icono"><span class="fa fa-key"
                                        aria-hidden="true"></span></span>
                                <input placeholder="Confirmar contraseña" id="password-confirm" type="password"
                                    class="form" name="password_confirmation" required>

                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
                            </span>
                        </div>
                        <input type="submit" value="Iniciar cuenta" class="inicio">
                    </form>
                </div>
                <div class="uno">
                    <ul class="palabras">
                        <li class="palabra">Conviertete en misionero y atiende las necesidades de otros</li>
                        <li class="palabra">oferta por un producto o servicio</li>
                        <li class="palabra">Elige el precio que buscas</li>
                        <li class="palabra">Se el mejor misionero y posicionate</li>

                    </ul>
                </div>

            </section>


        </main>
    </div>
    <div class="up">
        <p class="derechos"><a href="#" class="univone">Univone design</a> y THITONIX todos los derechos reservados
        </p>
    </div>
    <footer class="pie">

        <a href="quienes-somos.php" class="pie-info, primero">QUIENES SOMOS</a>
        <a href="{{ route('anuncio.anuncio') }}" class="pie-info, primero">PUBLICAR UN ANUNCIO</a>
        <a href="{{ route('misiones.index') }}" class="pie-info, segundo">MIS MISIONES</a>
        <a href="medios-de-pago.php" class="pie-info, segundo">MEDIOS DE PAGO</a>
        <a href="#" class="pie-info, tercero">BLOG</a>
        <a href="servicio-al-cliente.php" class="pie-info, tercero">SERVIO AL CLIENTE</a>
        <div class="redes">
            <span class="pie-iconos"><a href="#" class="fa fa-facebook" aria-hidden="true"></a></span>
            <span class="pie-iconos"><a href="#" class="fa fa-instagram" aria-hidden="true"></a></span>
            <span class="pie-iconos"><a href="#" class="fa fa-twitter" aria-hidden="true"></a></span>
        </div>
    </footer>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script type="text/javascript" src="../js/jquery.backstretch.min.js"></script>
    <script>
        $(document).ready(function(e) {
            $.backstretch(["../img/screenshot_inicio.jpg"]);
        });

    </script>
</body>
