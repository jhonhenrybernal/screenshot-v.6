@extends('layouts.app')

<div id="main-body">

    <div class="contenedor" id="main-element-two">

        <head>
            <link rel="icon" type="image/vnd.microsoft.icon" href="{{ asset('/favicon-thitonix.ico') }}">
            <link href="../../css/perfil.css" rel="stylesheet" type="text/css">
            <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
        </head>

        <main class="contenedor">
            <h2 class="perfil"><span class="negro">Cambio </span> de Password</h2>
            <section class="ajuste">
                <span class="circulo"><span class="fa fa-user-circle-o"></span></span>
                <h3 class="datos">Proceso</h3>
                @if ($success)
                    <div class="w3-panel w3-red">
                        <p>
                            <center>Clave no coincide</center>
                        </p>
                    </div>
                @endif
                <ul class="info">
                    <form action="{{ route('perfil.cambiopwcambio') }}" method="post" id="form-page">
                        {{ csrf_field() }}
                        <div class="create">
                            <p class="seleccion">Nueva clave</p>
                            <input type="Password" placeholder="" class="mision" name="paso1" required>
                        </div>
                        <div class="create">
                            <p class="seleccion">Ingrese de nuevo la clave</p>
                            <input type="Password" placeholder="" class="mision" name="paso2" required>
                        </div>
                </ul>

                </span>
                <button class="boton" onclick="pageViewDetail()">Actualizar</button>
                </form>


            </section>
        </main>
    </div>
</div>
