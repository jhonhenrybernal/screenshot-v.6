<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width,user-scalable=no,initial-scale=1.0,maximum-scale=1.0,minimum-scale=1.0">
<title>Screenshot</title>
<link href="css/estilo.css" rel="stylesheet" type="text/css">
<link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
</head>
<body>
<main class="contenedor">
<header class="head">
<h1 class="titulo">SCREENSHOT</h1>
<p class="sub">Un objetivo, una misión, busca el mejor postor</p>
</header>
<section class="part">
<span class="uno">
<ul class="palabras">
<li class="palabra">Conviertete en misionero y atiende las necesidades de otros</li>
<li class="palabra">oferta por un producto o servicio</li>
<li class="palabra">Elige el precio que buscas</li>
<li class="palabra">Se el mejor misionero y posicionate</li>

</ul>
</span>
<span class="cont">
<form action="" method="post" class="formulario">
<span class="bloque"><span class="icono"><span class="fa fa-user-o" aria-hidden="true"></span></span><input type="email" required placeholder="Correo" class="form"></span>
<span class="bloque sin"><span class="icono"><span class="fa fa-key" aria-hidden="true"></span></span><input type="password" required placeholder="Contraseña" class="form">
</span>
<a href="olvidar.php" class="olvido"><p class="text">¿Ha olvidado su contraseña?</p></a>
<input type="submit" value="Iniciar cuenta" class="inicio">
<a href="registro.php" class="olvido"><p class="text margen">¡Deseo registrarme!</p></a>
</form>
</span>
</section>
</main>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
<script type="text/javascript" src="js/jquery.backstretch.min.js"></script>
<script>
$(document).ready(function(e) {
	$.backstretch(["img/screenshot_inicio.jpg"]);
	});

</script>
</body>
</html>