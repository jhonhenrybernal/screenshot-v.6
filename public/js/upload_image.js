//iniciar boton
var btnId = document.getElementById("insert_image");
var btnQuery = document.getElementById("insert_image");
window.onload = clearPreImages();

function clearPreImages(){
    $.ajax({
        url: "/anuncios/depurar/img",
        success: function (data) {
            $('#store_image_propuesta').html(data);
        }
    })
}

// When the user clicks on <span> (x), close the modal
 function closeModalImage() {
      var modalInser = document.getElementById("insertimageModal");
      document.getElementById('insert_image').value = null;
      modalInser.style.display = "none";
}

btnId.onclick = function() {
}

$(document).ready(function(){
    var modalInser = document.getElementById("insertimageModal");



   $image_crop = $('#image_demo').croppie({
      enableExif: true,
      viewport: {
        width:279,
        height:279,
        type:'square' //circle
      },
      boundary:{
        width:300,
        height:300
      }
    });

    $('#insert_image').on('change', function(){
      var reader = new FileReader();
      reader.onload = function (event) {
        $image_crop.croppie('bind', {
          url: event.target.result
        }).then(function(){
          console.log('jQuery bind complete');
        });
      }
      reader.readAsDataURL(this.files[0]);
      modalInser.style.display = "block";
    });

    $('#insert_image_propuesta').on('change', function(){
      var reader = new FileReader();
      reader.onload = function (event) {
        $image_crop.croppie('bind', {
          url: event.target.result
        }).then(function(){
          console.log('jQuery bind complete');
        });
      }
      reader.readAsDataURL(this.files[0]);
      $('#insertimageModal_propuesta').modal('show');
    });

    $('.crop_image').click(function(event){
	    if (document.getElementsByClassName("load-img").length > 3) {
			 alert('Solo se permite 5 imagenes')
		}

      var tipo = $(this).data('tipo-image')
      $image_crop.croppie('result', {
        type: 'canvas',
        size: 'viewport'
      }).then(function(response){
        $.ajax({
          url:"/anuncios/cargar/img",
          type:'POST',
          data:{"image":response},
          success:function(data){
            if(tipo == 'propuesta'){
              $('#insertimageModal_propuesta').modal('hide');
              load_images_propuesta();
            }else{
               modalInser.style.display = "none";
               load_images();
            }
          }
        })
      });
    });

      load_images_propuesta();
      load_images();

  });

  function load_images()
  {
    $.ajax({
      url:"/anuncios/mostrar/img",
      success:function(data)
      {
        $('#store_image').html(data);
      }
    })
  }

  function load_images_propuesta()
  {
    $.ajax({
      url:"/propuesta/mostrar/img",
      success:function(data)
      {
        $('#store_image_propuesta').html(data);
      }
    })
  }

  function eliminarPreImagenes(id) {
    var tipo = $(this).data('tipo-image')
    $.ajax({
        url:"/anuncios/eliminar/img",
        type:'POST',
         data:{id:id},
        success:function(data)
        {
          if (data) {
              load_images();
              load_images_propuesta();
          } else {
            alert('error al eliminar')
          }
        }
      })
  }
