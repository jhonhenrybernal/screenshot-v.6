// Get the modal
var modal_pago = document.getElementById("pago_modal_propuesta");
var modal_propuesta = document.getElementById("mensageModal_propuesta");

// Get the button that opens the modal
var btn = document.getElementById("modal-mensaje");

// Get the button that opens the modal
var btn_pago = document.getElementById("modal-pago-infousu");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks the button, open the modal 
btn.onclick = function() {
  modal_propuesta.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
  modal_propuesta.style.display = "none";
  modal_pago.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
  if (event.target == modal_pago) {
    modal_pago.style.display = "none";
  }

  if (event.target == modal_propuesta) {   
    modal_propuesta.style.display = "none";
  }
}

btn_pago.onclick = function() {
  modal_pago.style.display = "block";
}

