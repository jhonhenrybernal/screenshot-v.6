//validar estados de notificaciones

window.addEventListener('load', statusNotify, false);
window.addEventListener('load', cronNotify, false);
function cronNotify(){
    setInterval(function () {
        statusNotify();
    }, 10000);
}

function statusNotify() {
    
    var req = new XMLHttpRequest();
    req.responseType = 'json';
    req.open('GET', '/admin/notificacion-status', true);
    req.onload = function () {
        var jsonResponse = req.response;

        //activar alerta
        var v = document.getElementById("activar-alerta");
        v.className = ' kt-header__topbar-icon ' + jsonResponse.alerta + ' kt-pulse--brand'; 
       
    };
    req.send(null);
}


function clickUpdNotify() {
    let diViewProd = document.getElementById('viewAlert');
    diViewProd.innerHTML = "";
    var req = new XMLHttpRequest();
    req.responseType = 'json';
    req.open('GET', '/admin/notificacion-status', true);
    req.onload = function () {
        var jsonResponse = req.response;

        /* activar , desactivar y ver tipo de alerta */

        //ajustar css
        document.getElementById('editHeight').style.height = jsonResponse.viewCantCss;
        //cantidad total
        var div = document.getElementById('countAlert');
        div.innerHTML = 'total: '+ jsonResponse.countAlert;
        //productos
        if (jsonResponse.viewPrd) {
            diViewProd.innerHTML += ' <a href="/admin/general/productos-misiones" class="kt-notification__item"><div class="kt-notification__item-icon"><i class="flaticon-squares-3 text-success"></i></div><div class="kt-notification__item-details"><div class="kt-notification__item-title">Productos</div><div class="kt-notification__item-time" id="countPrd">Total: ' + jsonResponse.countPrd + '</div></div></a>';
        }
        //propuesta
        if (jsonResponse.viewPropu) {
            diViewProd.innerHTML += ' <a href="/admin/general/productos-misiones" class="kt-notification__item"><div class="kt-notification__item-icon"><i class="flaticon-shapes kt-font-brand"></i></div><div class="kt-notification__item-details"><div class="kt-notification__item-title">Propuestas</div><div class="kt-notification__item-time" id="countPropu">Total: ' + jsonResponse.countPropu + '</div></div></a>';
        }

        //propuesta
        if (jsonResponse.viewEntrega) {
            diViewProd.innerHTML += '  <a  href="/admin/general/entregas" class="kt-notification__item" ><div class="kt-notification__item-icon"><i class="flaticon-truck kt-font-danger"></i></div><div class="kt-notification__item-details"><div class="kt-notification__item-title">Entregas</div><div class="kt-notification__item-time" id="countEntrega">Total: ' + jsonResponse.countEntrega + '</div></div></a>';
        }

        //propuesta
        if (jsonResponse.viewPagos) {
            diViewProd.innerHTML += ' <a href="/admin/general/pagos" class="kt-notification__item"><div class="kt-notification__item-icon"><i class="flaticon-price-tag kt-font-danger"></i></div><div class="kt-notification__item-details"><div class="kt-notification__item-title">Pagos</div><div class="kt-notification__item-time">Total: ' + jsonResponse.countpago + '</div></div></a>';
        }

        //no hay notificaciones
        if (jsonResponse.viewNull) {
            diViewProd.innerHTML += ' <div class="kt-grid kt-grid--hor kt-grid__item kt-grid__item--fluid kt-grid__item--middle"><div class="kt-grid__item kt-grid__item--middle kt-align-center">Actualmente!<br>No tiene notificaciones.</div></div>';
        }




    };



    req.send(null);
}

