
$(function(){
  $('#modal-form form').validator().on('submit', function(e){
    if (!e.isDefaultPrevented()){
      var id = $('#id').val();
      var id_usuario = $('#id_usuario').val();
      var mensaje = $('#mensaje').val();
      if (save_method == 'add') url = "{{ url('verificar') }}";
      else url = "{{ url('verificar') }}" + '/' + id + '/' + id_usuario;

      $.ajax({
        url : url,
        type : "POST",
        data : $('#modal-form form').serialize(),
        success : function($data){
          $('#modal-form').modal('hide');
          $().DataTable().ajax.reload();
          alert('Mensaje enviado');
        },
        error : function(){
          alert('verifique desde admin');
        }

      });

      return false;
    }
  });
});

$('#anuncio-tabla').DataTable({
  language: {
    "order": [[ 5, "desc" ]],
        "decimal": "",
        "emptyTable": "No hay información",
        "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
        "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
        "infoFiltered": "(Filtrado de _MAX_ total entradas)",
        "infoPostFix": "",
        "thousands": ",",
        "lengthMenu": "Mostrar _MENU_ Entradas",
        "loadingRecords": "Cargando...",
        "processing": "Procesando...",
        "search": "Buscar:",
        "zeroRecords": "Sin resultados encontrados",
        "paginate": {
            "first": "Primero",
            "last": "Ultimo",
            "next": "Siguiente",
            "previous": "Anterior"
        }
    },
    processing: true,
    serverSide: true,
    ajax: "{{ route('api.paneladm') }}",
    columns:[
    {data: 'usuario.name', name: 'usuario.name'},
    {data: 'usuario.apellido', name: 'usuario.apellido'},
    {data: 'mision', name: 'mision'},
    {data: 'categoria.tipo', name: 'categoria.tipo'},
    {data: 'estado.estado', name: 'estado.estado'},
    {data: 'action', name: 'action', orderable: false, searchable: false},

    ]
  });
