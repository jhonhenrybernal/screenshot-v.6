@extends('layouts.app')
<head>
<meta charset="UTF-8">
<title>Misiones</title>
<link href="../../css/anuncio.css" rel="stylesheet" type="text/css">
<meta name="viewport" content="width=device-width,user-scalable=no,initial-scale=1.0,maximum-scale=1.0,minimum-scale=1.0">
</head>

<body>
<main class="contenedor">
<section class="mover">
<div class="nuncio">
<h3 class="anuncio"><span class="negro">Mis</span> Misiones</h3>
</div>
<p class="comi">Para reclamar tus ganancias da click <a class="modal-ganancias-mision">aquí</a></p>
@if(!empty($products))
    @foreach($products as $list)
    @if($list->id_estados_anuncios == 24 or $list->id_estados_anuncios == 23 or $list->id_estados_anuncios == 19 or $list->id_estados_anuncios == 30)
    <div class="perfil">
        <img src="{{ asset('imagenesproductos/'.$list->imagenendeuno->nombreImagen) }}" alt="" class="imagen">
        <p class="texto abajo">{{$list->ciudades->ciudad}}</p>
        <p class="texto">{{$list->misionDetalladamente}}</p>
        @if($list->id_tipo == 2)
        <a href="{{ route('misiones.envia',$list->id) }}" class="entrar"><p class="mirar">Mas detalles</p></a>
        @elseif($list->id_tipo == 1)
        <a href="{{ route('misiones.recibe',$list->id) }}" class="entrar"><p class="mirar">Mas detalles</p></a>
        @endif
    </div>
    @endif
    @endforeach
@else
@endif
@include('misiones.modal.ganancias')
</section>
</main>
<script src="{{ asset('js/mosiones.js') }}"></script>
</body>
